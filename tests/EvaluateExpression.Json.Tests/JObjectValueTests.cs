﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Xunit;

using Newtonsoft.Json.Linq;

using EvaluateExpression;
using FluentAssertions;
using AutoFixture;
using AutoFixture.Xunit2;

namespace EvaluateExpression.Json.Tests
{
    /// <summary>
    /// Summary description for JObjectValueTests
    /// </summary>
    
    public class JObjectValueTests
    {
        [Fact]
        public void JObjectValue_PropsTest() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json1_deep.json"));
            JObject json = JObject.Parse(s);
            JObjectValue value = new JObjectValue(json);

            ValueAssert.CheckAsAccessors(value, false, false, false, false, false);
            ValueAssert.CheckIsProps(value, false, false, false, false, EvaluateExpression.ValueType.Unknown);
        }

        [Fact]
        public void JObjectValue_GetSubValueTest() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json1_deep.json"));
            JObject json = JObject.Parse(s);
            JObjectValue value = new JObjectValue(json);

            Assert.Equal(30, value.GetSubValue("Age").AsInt);
            Assert.Equal("qwerty", value.GetSubValue("strval").AsString);
            Assert.Null(value.GetSubValue("___not_exist"));

            IValue inner = value.GetSubValue("address");
            Assert.NotNull(inner);
            Assert.IsType<JObjectValue>(inner);
            Assert.Equal("Moscow", (inner as IComplexValue).GetSubValue("city").AsString);
        }

        [Fact]
        public void JObjectValue_EnumerateTest() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json_short_deep.json"));
            JObject json = JObject.Parse(s);
            JObjectValue value = new JObjectValue(json);

            Assert.Equal(new string[] { "A", "B", "C", "D" }, value.Select(v => v.Key).ToArray());
            Assert.Equal(
                new int[] { 1, 2, 4 },
                value
                    .Select(v => v.Value)
                    .Where(v => (v is IComplexValue) == false)
                    .Select(v => v.AsInt)
                    .ToArray()
                    );
            var list = value.Where(v => v.Value is IComplexValue);
            Assert.Single(list);
            Assert.Equal("C", list.First().Key);
        }

        [Fact]
        public void JObjectValue_EvalTest() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json_short_deep.json"));
            JObject json = JObject.Parse(s);
            JObjectValue value = new JObjectValue(json);

            Assert.Same(value, value.Eval(null));
        }

        [Fact]
        public void JArrayValue_JTokenContainer() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json_short_deep.json"));
            JObject json = JObject.Parse(s);
            IValue value = new JObjectValue(json);
            Assert.Same(json, (value as IJTokenContainer).JToken);
        }

        [Theory, JTokenAutoData]
        public void Property_Test(JObject jo)
        {
            // setup
            var value = new JObjectValue(jo);
            var property = jo.Properties().First();

            // action
            var result = value.Property(property.Name);

            // asserts
            result.Should().NotBeNull();
        }

        [Theory, JTokenAutoData]
        public void Property_NotFound_Test(JObject jo)
        {
            // setup
            var value = new JObjectValue(jo);

            // action
            var result = value.Property(Guid.NewGuid().ToString());

            // asserts
            result.Should().BeNull();
        }
    }
}
