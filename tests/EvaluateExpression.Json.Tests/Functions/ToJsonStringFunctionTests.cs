﻿using EvaluateExpression.Asserts;
using EvaluateExpression.Functions;
using EvaluateExpression.Json.Functions;
using EvaluateExpression.Values;
using FluentAssertions;
using FluentAssertions.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace EvaluateExpression.Json.Tests.Functions
{
    public class ToJsonStringFunctionTests
    {
        readonly ToJsonStringFunction target = new();

        [Fact]
        public void StructToJsonStringTest()
        {
            // setup
            var value = new StructValue
            {
                { "field1", 123.ToValue() },
                { "field2", "abc".ToValue() }
            };

            // action
            var result = target.Perform(value, null).AsString;

            // asserts
            var jo = JToken.Parse(result);
            jo.Should().BeEquivalentTo(JsonConvert.SerializeObject(new { field1 = 123, field2 = "abc" }));
        }

        [Fact]
        public void StringToJsonStringTest()
        {
            // setup
            var value = "abc".ToValue();

            // action
            var result = target.Perform(value, null).AsString;

            // asserts
            result.Should().Be("\"abc\"");
        }

        [Fact]
        public void NullToJsonStringTest()
        {
            // setup
            var value = NullValue.Null;

            // action
            var result = target.Perform(value, null).AsString;

            // asserts
            result.Should().Be("null");
        }
    }
}
