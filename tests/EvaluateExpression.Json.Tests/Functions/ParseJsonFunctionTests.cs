﻿using System;
using Xunit;

using EvaluateExpression.Functions;
using EvaluateExpression.Values;
using EvaluateExpression;
using AutoFixture.Xunit2;
using Newtonsoft.Json.Linq;
using FluentAssertions;
using EvaluateExpression.Asserts;
using FluentAssertions.Json;

namespace EvaluateExpression.Json.Functions.Tests
{
    public class ParseJsonFunctionTests
    {
        private readonly ParseJsonFunction func = new ParseJsonFunction();

        [Fact]
        public void ArgsCount_Test()
        {
            func.ArgCount.Should().Be(1);
        }

        [Theory, JTokenAutoData]
        public void Parse_JObject_Test(JObject jo)
        {
            // setup
            string str = jo.ToString();

            // action
            IValue value = func.Perform(str.ToValue(), null);

            // asserts
            value.Should()
                .BeComplexValue().And
                .BeOfType<JObjectValue>()
                .Which.Value.Should().BeOfType<JObject>().Which
                .Should().BeEquivalentTo(jo);
        }

        [Theory, JTokenAutoData]
        public void Parse_JArray_Test(JArray ja)
        {
            // setup
            string str = ja.ToString();

            // action
            IValue value = func.Perform(str.ToValue(), null);

            // asserts
            value.Should()
                .BeArrayValue().And
                .BeOfType<JArrayValue>()
                .Which.Value.Should().BeOfType<JArray>().Which
                .Should().BeEquivalentTo(ja);
        }

        [Fact]
        public void ParseSuccess() {
            IValue value = func.Perform("{ \"name\": \"somename\" }".ToValue(), null);
            Assert.Equal(ValueType.Unknown, value.Type);
            IComplexValue complex = value as IComplexValue;
            Assert.NotNull(complex);
            Assert.Equal("somename", complex.GetSubValue("name").AsString);
        }

        [Fact]
        public void ParseFailed() {
            // setup 
            string invalidJson = "{ \"name\" \"somename\" }";

            // action
            Action action = () => func.Perform(invalidJson.ToValue(), null);

            // asserts
            action.Should()
                .ThrowExactly<ParseJsonFunctionException>()
                .WithInnerException<Newtonsoft.Json.JsonException>();
        }
    }
}
