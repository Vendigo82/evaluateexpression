﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentAssertions;

namespace AutoFixture.Xunit2
{
    public class JTokenAutoDataAttributeTests
    {
        [Theory, JTokenAutoData]
        public void GetValue_Test(JToken token, JObject jo, JArray ja)
        {
            //asserts
            token.Should().NotBeNull();
            jo.Should().NotBeNull();
            ja.Should().NotBeNull();
        }
    }
}
