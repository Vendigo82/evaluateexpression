﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Xunit;

using Newtonsoft.Json.Linq;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Json.Tests
{
   
    public class JArrayValueTests
    {
        private const string JSON = "[10, 20, 30]";

        [Fact]
        public void JArrayValue_PropsTest() {
            JArray json = JArray.Parse(JSON);
            JArrayValue value = new JArrayValue(json);

            ValueAssert.CheckAsAccessors(value, false, false, false, false, false);
            ValueAssert.CheckIsProps(value, false, false, false, false, ValueType.Unknown);
        }

        [Fact]
        public void JArrayValue_IndexTest()
        {
            JArray json = JArray.Parse(JSON);
            JArrayValue value = new JArrayValue(json);

            Assert.Equal(10, value[0.ToValue()].AsInt);
            Assert.Equal(20, value[1.ToValue()].AsInt);
            Assert.Equal(30, value[2.ToValue()].AsInt);
            Assert.Throws<EvalIndexOutOfRangeException>(() => value[3.ToValue()].AsInt);
        }

        [Fact]
        public void JArrayValue_EvalTest() {
            JArray json = JArray.Parse(JSON);
            JArrayValue value = new JArrayValue(json);

            Assert.Same(value, value.Eval(null));
        }

        [Fact]
        public void JArrayValue_EnumTest() {
            JArray json = JArray.Parse(JSON);
            JArrayValue value = new JArrayValue(json);
            Assert.Equal(new int[] { 10, 20, 30 }, (value as IIndexableValue).Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void JArrayValue_CountTest() {
            JArray json = JArray.Parse(JSON);
            JArrayValue value = new JArrayValue(json);
            Assert.Equal(3, value.GetSubValue(value.CountPropertyName()).AsInt);
        }

        [Fact]
        public void JArrayValue_JTokenContainer() {
            JArray json = JArray.Parse(JSON);
            IValue value = new JArrayValue(json);
            Assert.Same(json, (value as IJTokenContainer).JToken);
        }
    }
}
