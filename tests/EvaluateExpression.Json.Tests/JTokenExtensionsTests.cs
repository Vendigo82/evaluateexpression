﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xunit;
using AutoFixture.Xunit2;

using Newtonsoft.Json.Linq;
using EvaluateExpression.Values;
using FluentAssertions;
using FluentAssertions.Json;

namespace EvaluateExpression.Json.Tests
{ 
    public class JTokenExtensionsTests
    {
        [Fact]
        public void JToken_ToValue_SimpleTest() {
            ValueAssert.CheckInt(10, new JValue(10).ToValue());
            ValueAssert.CheckDouble(5.67, new JValue(5.67).ToValue());
            ValueAssert.CheckString("qwerty", new JValue("qwerty").ToValue());
            Guid g = Guid.NewGuid();
            ValueAssert.CheckString(g.ToString(), new JValue(g).ToValue());
            ValueAssert.CheckBool(true, new JValue(true).ToValue());
            Assert.True(new JValue((object)null).ToValue().IsNull);
            DateTime dt = DateTime.Now;
            ValueAssert.CheckDateTime(dt, new JValue(dt).ToValue());
        }

        [Fact]
        public void JToken_JObject_ToValue_Test() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json_short_deep.json"));
            JObject json = JObject.Parse(s);

            IValue val = json.ToValue();
            Assert.IsAssignableFrom<IComplexValue>(val);

            Assert.Equal(new string[] { "A", "B", "C", "D" }, (val as IComplexValue).Select(v => v.Key).ToArray());
        }

        [Fact]
        public void JToken_JArray_ToValue_Test() {
            string s = System.IO.File.ReadAllText(System.IO.Path.Combine("Example", "json_various.json"));
            JToken json = JObject.Parse(s);
            IValue val = json["Array"].ToValue();
            Assert.IsAssignableFrom<IIndexableValue>(val);

            Assert.Equal(new int[] { 10, 20, 30 }, (val as IIndexableValue).Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ToJObjectIntTest()
        {
            JToken token = 10.ToValue().ToJson();
            Assert.Equal(JTokenType.Integer, token.Type);
            Assert.Equal(10, (int)token);
        }

        [Fact]
        public void ToJObjectDoubleTest()
        {
            JToken token = 1.5.ToValue().ToJson();
            Assert.Equal(JTokenType.Float, token.Type);
            Assert.Equal(1.5, (double)token);
        }

        [Fact]
        public void ToJObjectStringTest()
        {
            JToken token = "abc".ToValue().ToJson();
            Assert.Equal(JTokenType.String, token.Type);
            Assert.Equal("abc", (string)token);
        }

        [Fact]
        public void ToJObjectBoolTest()
        {
            JToken token = true.ToValue().ToJson();
            Assert.Equal(JTokenType.Boolean, token.Type);
            Assert.True((bool)token);
        }

        [Fact]
        public void ToJObjectDateTimeTest()
        {
            DateTime dt = DateTime.Now;
            JToken token = dt.ToValue().ToJson();
            Assert.Equal(JTokenType.Date, token.Type);
            Assert.Equal(dt, (DateTime)token);
        }

        [Fact]
        public void ToJObjectNullTest()
        {
            JToken token = NullValue.Null.ToJson();
            Assert.Equal(JTokenType.Null, token.Type);
        }

        [Fact]
        public void ToJObjectArrayTest()
        {
            int[] iArray = new int[] { 10, 20, 30 };
            ArrayValue value = new ArrayValue(iArray.Select(i => i.ToValue()).ToArray());
            JToken jo = value.ToJson();
            Assert.IsType<JArray>(jo);
            JArray ja = jo as JArray;
            Assert.Equal(iArray, ja.Select(j => (int)j).ToArray());
        }

        [Fact]
        public void ToJObjectComplexTest()
        {
            StructValue complex = new StructValue();
            complex.Add("f1", 10.ToValue());
            complex.Add("f2", "abc".ToValue());
            JToken token = complex.ToJson();

            Assert.IsType<JObject>(token);
            JObject obj = token as JObject;

            Assert.Equal(
                (obj as IEnumerable<KeyValuePair<string, JToken>>).Select(j => j.Key).OrderBy(s => s).ToArray(),
                new string[] { "f1", "f2" });

            Assert.Equal(10, (int)obj["f1"]);
            Assert.Equal("abc", (string)obj["f2"]);
        }

        [Fact]
        public void ToJObjectJTokenContainer()
        {
            JArray ja = new JArray();
            ja.Add(new JValue(10));
            ja.Add(new JValue(20));
            ja.Add(new JValue(30));
            IValue value = new JArrayValue(ja);
            Assert.Same(ja, value.ToJson());
        }

        [Theory]
        [JTokenInlineAutoData("x")]
        [JTokenInlineAutoData("x.y.z")]
        [JTokenInlineAutoData("a.field")]
        [JTokenInlineAutoData("a.aa.field")]
        [JTokenInlineAutoData("a.nulltoken")]   //should exchange null value
        [JTokenInlineAutoData("a.nulltoken.field")] // null value should be exchange to JObject with new fields
        [JTokenInlineAutoData("a")] //can rewrite value
        [JTokenInlineAutoData("a.bb")] //can rewrite value
        [JTokenInlineAutoData("b")] //can rewrite value
        public void AppendPath_Test(string path, JToken valueToAdd)
        {
            // setup
            var jo = new JObject() {
                { "a", new JObject {
                    { "aa", new JObject() },
                    { "bb", new JValue("abc") },
                    { "nulltoken", JValue.CreateNull() }
                } },
                { "b", new JValue(1) }
            };

            // action
            jo.AppendPath(path, valueToAdd);

            // asserts
            jo.SelectToken(path).Should().NotBeNull().And.BeEquivalentTo(valueToAdd);
        }

        [Theory]
        [JTokenInlineAutoData("a", "aa,bb,nulltoken")]
        [JTokenInlineAutoData("a.aa", "")]
        [JTokenInlineAutoData("", "a,b")]
        public void AppendPath_Merge_Test(string path, string expectedProperties, JObject valueToAdd)
        {
            // setup
            var jo = new JObject() {
                { "a", new JObject {
                    { "aa", new JObject() },
                    { "bb", new JValue("abc") },
                    { "nulltoken", JValue.CreateNull() }
                } },
                { "b", new JValue(1) }
            };

            // action
            jo.AppendPath(path, valueToAdd);

            // asserts
            var token = jo.SelectToken(path);
            var jobject = token.Should().BeOfType<JObject>().Which;

            var properties = expectedProperties.Split(',', StringSplitOptions.RemoveEmptyEntries).Concat(valueToAdd.Properties().Select(i => i.Name)).ToArray();
            jobject.Should().HaveCount(properties.Length);
            foreach (var p in valueToAdd)
                jobject[p.Key].Should().BeEquivalentTo(p.Value);
        }

        [Theory]
        [JTokenInlineAutoData("b.field")]   //can't add property to primitive type of value
        [JTokenInlineAutoData("a.bb.field")]  //can't add property to primitive type of value
        [JTokenInlineAutoData("a.array.field")] //can't add property to array
        public void AppendPath_Fail_Test(string path, JToken valueToAdd)
        {
            // setup
            var jo = new JObject() {
                { "a", new JObject {
                    { "aa", new JObject() },
                    { "bb", new JValue("abc") },
                    { "array", new JArray() },
                    { "nulltoken", JValue.CreateNull() }
                } },
                { "b", new JValue(1) }
            };

            // action
            Action action = () => jo.AppendPath(path, valueToAdd);

            // asserts
            action.Should().ThrowExactly<InvalidOperationException>();
        }
    }
}