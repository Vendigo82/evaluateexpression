﻿using AutoFixture;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace AutoFixture
{
    public class JTokenCustomizationTests
    {
        readonly Fixture fixture;

        public JTokenCustomizationTests()
        {
            fixture = new Fixture();
            fixture.Customize(new JTokenCustomization());
        }

        [Fact]
        public void Customize_JToken_Test()
        {
            //setup
            
            //action
            var value = fixture.Create<JToken>();

            //asserts
            value.Type.Should().Match<JTokenType>(t => t == JTokenType.String);
            ((string)value).Should().NotBeNullOrEmpty();
        }

        //[Fact]
        //public void Customize_JValue_Test()
        //{
        //    // setup

        //    // action
        //    var value = fixture.Create<JValue>();

        //    // aserts
        //    value.Type.Should().Be(JTokenType.String);
        //    ((string)value).Should().NotBeNullOrEmpty();
        //}

        [Fact]
        public void Customize_JObject_Test()
        {
            // setup

            // action
            var value = fixture.Create<JObject>();

            // asserts
            value.Properties().Should()
                .NotBeEmpty().And
                .HaveCountGreaterThan(1).And
                .OnlyHaveUniqueItems(i => i.Name);
        }

        [Fact]
        public void Customize_JArray_Test()
        {
            //setup

            // action
            var value = fixture.Create<JArray>();

            // asserts
            value.Should()
                .HaveCountGreaterThan(1).And
                .AllBeOfType<JValue>();
        }
    }
}
