﻿using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentAssertions;
using Xunit.Sdk;
using Newtonsoft.Json.Linq;
using AutoFixture.Xunit2;
using EvaluateExpression.Json;

namespace EvaluateExpression.Asserts.Tests
{
    public class ValueAssertionsTests
    {
        [Fact]
        public void BeComplexValue_Success_Test()
        {
            // setup 
            var value = new StructValue() {
                {  "abc", 10.ToValue() }
            };

            // action
            Action action = () => value.Should().BeComplexValue();

            // asserts
            action.Should().NotThrow();
        }

        [Fact]
        public void BeComplexValue_Primitive_Failed_Test()
        {
            // setup
            var value = 10.ToValue();

            // action
            Action action = () => value.Should().BeComplexValue();

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Fact]
        public void BeArrayValue_Success_Test()
        {
            // setup
            var value = new ArrayValue(new[] { 1.ToValue() });

            // action
            Action action = () => value.Should().BeArrayValue();

            // asserts
            action.Should().NotThrow();
        }

        [Fact]
        public void BeArrayValue_Primitive_Failed_Test()
        {
            // setup
            var value = 10.ToValue();

            // action
            Action action = () => value.Should().BeArrayValue();

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData(10, true)]
        [InlineData(11, false)]
        public void Be_AsInt_Test(int beValue, bool expectedSuccess)
        {
            // setup
            var value = new IntValue(10);

            // action
            Action action = () => value.Should().Be(beValue);

            // asserts
            if (expectedSuccess)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData(10.5, true)]
        [InlineData(11.5, false)]
        public void Be_AsDouble_Test(double beValue, bool expectedSuccess)
        {
            // setup
            var value = new DoubleValue(10.5);

            // action
            Action action = () => value.Should().Be(beValue);

            // asserts
            if (expectedSuccess)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData(10L, true)]
        [InlineData(11L, false)]
        public void Be_AsInt_Long_Test(long beValue, bool expectedSuccess)
        {
            // setup
            var value = new IntValue(10);

            // action
            Action action = () => value.Should().Be(beValue);

            // asserts
            if (expectedSuccess)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData("abc", true)]
        [InlineData("xyz", false)]
        public void Be_AsString_Test(string beValue, bool expectedSuccess)
        {
            // setup
            var value = new StringValue("abc");

            // action
            Action action = () => value.Should().Be(beValue);

            // asserts
            if (expectedSuccess)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }


        [Theory]
        [InlineData(true, true, true)]
        [InlineData(true, false, false)]
        [InlineData(false, true, false)]
        [InlineData(false, false, true)]
        public void Be_AsBool_Test(bool originValue, bool beValue, bool expectedSuccess)
        {
            // setup
            var value = BoolValue.Get(originValue);

            // action
            Action action = () => value.Should().Be(beValue);

            // asserts
            if (expectedSuccess)
                action.Should().NotThrow();
            else
                action.Should().Throw<XunitException>();
        }

        [Fact]
        public void Be_AsDateTime_Success_Test()
        {
            // setup
            var dt = DateTime.Now;
            var value = new DateTimeValue(dt);

            // action
            Action action = () => value.Should().Be(dt);

            // asserts
            action.Should().NotThrow();
        }

        [Fact]
        public void Be_AsDateTime_Fail_Test()
        {
            // setup
            var dt = DateTime.Now;
            var value = new DateTimeValue(dt);

            // action
            Action action = () => value.Should().Be(dt.AddSeconds(3));

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Fact]
        public void BeNullValue_Success_Test()
        {
            // setup
            var value = NullValue.Null;

            // action
            Action action = () => value.Should().BeNullValue();

            // asserts
            action.Should().NotThrow();
        }

        [Theory]
        [InlineData(1)]
        [InlineData("abc")]
        [InlineData(true)]
        [InlineData(false)]
        [InlineData(1.25)]
        public void BeNullValue_Fails_Test(object src)
        {
            // setup
            var value = src.ToValue();

            // action
            Action action = () => value.Should().BeNullValue();

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Theory, JTokenAutoData]
        public void Be_JObject_Test(JObject src)
        {
            // setup
            var value = new JObjectValue(src);

            // action
            Action action = () => value.Should().Be(src);

            // asserts
            action.Should().NotThrow();
        }

        [Theory, JTokenAutoData]
        public void Be_JObject_FailOnDifferentJObjects_Test(JObject src, JObject expected)
        {
            // setup
            var value = new JObjectValue(src);

            // action
            Action action = () => value.Should().Be(expected);

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Theory]
        [JTokenInlineAutoData(1)]
        [JTokenInlineAutoData("abc")]
        [JTokenInlineAutoData(true)]
        [JTokenInlineAutoData(false)]
        [JTokenInlineAutoData(1.5)]
        public void Be_JObject_DifferentTypes_Test(object src, JObject expected)
        {
            // setup
            var value = src.ToValue();            

            // action
            Action action = () => value.Should().Be(expected);

            // asserts
            action.Should().Throw<XunitException>();
        }

        [Theory]
        [InlineData("a,b,c", false)]
        [InlineData("b,c,a", false)]
        [InlineData("a,b", true)]
        public void HaveProperties_Test(string propertiesList, bool expectedFail)
        {
            // setup
            var value = new StructValue {
                { "a", 1.ToValue() },
                { "b", 2.ToValue() },
                { "c", 3.ToValue() }
            };

            // action
            Action action = () => value.Should().HaveProperties(propertiesList.Split(','));

            // asserts
            if (expectedFail)
                action.Should().Throw<XunitException>();
            else
                action.Should().NotThrow();
        }
    }
}
