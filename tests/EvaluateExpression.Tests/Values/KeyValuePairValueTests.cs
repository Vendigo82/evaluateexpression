﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using EvaluateExpression.Asserts;

namespace EvaluateExpression.Values.Tests
{
    
    public class KeyValuePairValueTests
    {
        [Fact]
        public void KeyValuePairValue_GetSubValueTest() {
            KeyValuePairValue v = new KeyValuePairValue("abc".ToValue(), "qwerty".ToValue());

            Assert.Equal("Key", KeyValuePairValue.KEY);
            Assert.Equal("Value", KeyValuePairValue.VALUE);            

            Assert.Equal("abc", v.GetSubValue(KeyValuePairValue.KEY).AsString);
            Assert.Equal("qwerty", v.GetSubValue(KeyValuePairValue.VALUE).AsString);
            Assert.Null(v.GetSubValue("something"));
        }

        [Fact]
        public void KeyValuePairValue_EnumTest() {
            KeyValuePairValue v = new KeyValuePairValue("abc".ToValue(), "qwerty".ToValue());
            Assert.Equal(
                new KeyValuePair<string, IValue>[] {
                    new KeyValuePair<string, IValue>(KeyValuePairValue.KEY, "abc".ToValue()),
                    new KeyValuePair<string, IValue>(KeyValuePairValue.VALUE, "qwerty".ToValue())
                },
                v.ToArray()
            );
        }

        [Theory]
        [InlineData("a", "Key")]
        [InlineData("xxx", "Value")]
        [InlineData(null, "unknown")]
        public void Property_Test(string expected, string name)
        {
            // setup
            var pairValue = new KeyValuePairValue("a".ToValue(), "xxx".ToValue());

            // action
            var value = pairValue.Property(name);

            // asserts
            if (expected != null)
                value.Should().Be(expected);
            else
                value.Should().BeNull();

        }
    }
}
