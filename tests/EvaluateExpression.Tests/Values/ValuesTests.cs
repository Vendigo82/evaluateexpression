﻿using System;
using Xunit;

namespace EvaluateExpression.Values.Tests
{
    
    public class ValuesTests
    {
        [Fact]
        public void AsBool() {
            Assert.True(BoolValue.True.AsBool);
            Assert.False(BoolValue.False.AsBool);
            Assert.True(new DoubleValue(0.1).AsBool);
            Assert.False(new DoubleValue(0).AsBool);
            Assert.True(new IntValue(2).AsBool);
            Assert.False(new IntValue(0).AsBool);
        }
    }
}
