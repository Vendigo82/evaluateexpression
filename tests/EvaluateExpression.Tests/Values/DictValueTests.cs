﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xunit;

using FluentAssertions;
using EvaluateExpression.Asserts;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Values.Tests
{
    
    public class DictValueTests
    {
        [Fact]
        public void DictValueAccessTest()
        {
            DictValue dict = new DictValue();
            dict.Add(0.ToValue(), "0".ToValue());
            dict.Add(10.ToValue(), "10".ToValue());

            Assert.Equal("0", dict[0.ToValue()].AsString);
            Assert.Equal("10", dict[10.ToValue()].AsString);
            Assert.Equal(2, dict.GetSubValue(dict.CountPropertyName()).AsInt);

            Assert.True(dict[5.ToValue()].IsNull);

            Assert.Null(dict.GetSubValue("nothing"));
            Assert.Throws<EvalDictDublicateKetException>(() => dict.Add(0.ToValue(), "20".ToValue()));
        }

        [Fact]
        public void DictValueEnumTest() {
            DictValue dict = new DictValue();
            dict.Add(0.ToValue(), "0".ToValue());
            dict.Add(10.ToValue(), "10".ToValue());

            Assert.Equal(
                new KeyValuePair<int, string>[] {
                    new KeyValuePair<int, string>(0, "0"),
                    new KeyValuePair<int, string>(10, "10")
                },
                (dict as IEnumerable<IValue>)
                    .Cast<KeyValuePairValue>()
                    .Select(p => new KeyValuePair<int, string>(
                        p.GetSubValue(KeyValuePairValue.KEY).AsInt,
                        p.GetSubValue(KeyValuePairValue.VALUE).AsString))
                    .ToArray()
                );
        }

        [Theory]
        [InlineData(2, "Count")]
        [InlineData(null, "a")]
        [InlineData(null, "b")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var dictValue = new DictValue() {
                { "a".ToValue(), 10.ToValue() },
                { "b".ToValue(), 20.ToValue() }
            };

            // action
            var value = dictValue.Property(name);

            // asserts
            if (expected.HasValue)
                value.Should().Be(expected.Value);
            else
                value.Should().BeNull();
        }

    }
}
