﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Values.Tests
{
    
    public class ArrayValueStrategyTests
    {
        [Fact]
        public void ArrayValueStrategyTest() {
            int[] iarray = new int[] { 10, 5, 8, 12 };
            IValue[] array = iarray.Select(i => i.ToValue()).ToArray();
            ArrayValueStrategy s = new ArrayValueStrategy(array);

            Assert.Equal(array.Length, s.Count);
            for (int i = 0; i < iarray.Length; ++i)
                Assert.Equal(iarray[i], s[i].AsInt);
            Assert.Same(array, s.Value);
            Assert.Equal(iarray, s.Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ListValueStrategyTest() {
            int[] iarray = new int[] { 10, 5, 8, 12 };
            List<IValue> array = iarray.Select(i => i.ToValue()).ToList();
            ListValueStrategy s = new ListValueStrategy(array);

            Assert.Equal(iarray.Length, s.Count);
            for (int i = 0; i < iarray.Length; ++i)
                Assert.Equal(iarray[i], s[i].AsInt);
            Assert.NotSame(array, s.Value);
            Assert.Equal(iarray, s.Select(v => v.AsInt).ToArray());

            //add element to origin list, but strategy elements still same
            array.Add(44.ToValue());
            Assert.Equal(iarray.Length, s.Count);

            s.Add(44.ToValue());
            Assert.Equal(iarray.Length + 1, s.Count);
            Assert.Equal(44, s[s.Count - 1].AsInt);
            Assert.Equal(array.Select(i => i.AsInt).ToArray(), s.Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void EnumerableValueStrategyTest() {
            int[] iarray = new int[] { 10, 5, 8, 12 };
            List<IValue> array = iarray.Select(i => i.ToValue()).ToList();
            EnumerableValueStrategy s = new EnumerableValueStrategy(array);

            Assert.Equal(iarray.Length, s.Count);
            for (int i = 0; i < iarray.Length; ++i)
                Assert.Equal(iarray[i], s[i].AsInt);
            Assert.Same(array, s.Value);
            Assert.Equal(iarray, s.Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void EnumerableValueStrategyEmptyTest() {
            IEnumerable<IValue> array = Enumerable.Empty<IValue>();
            EnumerableValueStrategy s = new EnumerableValueStrategy(array);

            Assert.Equal(0, s.Count);
            Assert.Equal(Enumerable.Empty<IValue>().ToArray(), s.ToArray());
        }
    }
}
