﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using System.Linq;

namespace EvaluateExpression.Values.Tests
{
    /// <summary>
    /// Summary description for ValueExtentionsTests
    /// </summary>
    
    public class ValueExtentionsTests
    {
        [Fact]
        public void ObjectToValueTest() {
            int i32 = 10;
            uint ui32 = 11;
            short i16 = 12;
            UInt16 ui16 = 13;
            Int64 i64 = 14;
            UInt64 ui64 = 15;
            byte b = 16;
            float f = 17;
            double d = 18;
            string s = "123";
            bool bb = true;
            decimal dec = 1.5M;
            DateTime dateTime = DateTime.Now;
            Nullable<int> ni32 = 10;
            Nullable<uint> nui32 = 11;
            Nullable<short> ni16 = 12;
            Nullable<UInt16> nui16 = 13;
            Nullable<Int64> ni64 = 14;
            Nullable<UInt64> nui64 = 15;
            Nullable<byte> nb = 16;
            Nullable<float> nf = 17;
            Nullable<double> nd = 18;
            Nullable<bool> nbb = true;
            Nullable<int> inull = null;
            Nullable<DateTime> ndateTime = DateTime.Now;
            decimal? ndec = 2.5M;
            string stringGuid = "f597f9e3-2095-42a8-bf99-57d88a8cc2db";
            Guid guid = Guid.Parse(stringGuid);
            Guid? nguid = Guid.Parse(stringGuid);
            string nullString = null;

            ValueAssert.CheckInt(i32, ((object)i32).ToValue());
            ValueAssert.CheckInt((int)ui32, ((object)ui32).ToValue());
            ValueAssert.CheckInt(i16, ((object)i16).ToValue());
            ValueAssert.CheckInt(ui16, ((object)ui16).ToValue());
            ValueAssert.CheckInt((int)i64, ((object)i64).ToValue());
            ValueAssert.CheckInt((int)ui64, ((object)ui64).ToValue());
            ValueAssert.CheckInt(b, ((object)b).ToValue());

            ValueAssert.CheckDouble(f, ((object)f).ToValue());
            ValueAssert.CheckDouble(d, ((object)d).ToValue());

            ValueAssert.CheckString(s, ((object)s).ToValue());

            ValueAssert.CheckBool(bb, ((object)bb).ToValue());

            ValueAssert.CheckDateTime(dateTime, ((object)dateTime).ToValue());

            ValueAssert.CheckInt(ni32.Value, ((object)ni32).ToValue());
            ValueAssert.CheckInt((int)nui32.Value, ((object)nui32).ToValue());
            ValueAssert.CheckInt(ni16.Value, ((object)ni16).ToValue());
            ValueAssert.CheckInt(nui16.Value, ((object)nui16).ToValue());
            ValueAssert.CheckInt((int)ni64.Value, ((object)ni64).ToValue());
            ValueAssert.CheckInt((int)nui64.Value, ((object)nui64).ToValue());
            ValueAssert.CheckInt(nb.Value, ((object)nb.Value).ToValue());

            ValueAssert.CheckDouble(nf.Value, ((object)nf).ToValue());
            ValueAssert.CheckDouble(nd.Value, ((object)nd).ToValue());

            ValueAssert.CheckBool(nbb.Value, ((object)nbb).ToValue());

            Assert.True(((object)null).ToValue().IsNull);
            Assert.True(((object)inull).ToValue().IsNull);

            ValueAssert.CheckDateTime(ndateTime.Value, ((object)ndateTime).ToValue());

            Assert.Equal(stringGuid, ((object)guid).ToValue().AsString);
            Assert.Equal(stringGuid, ((object)nguid).ToValue().AsString);

            Assert.True(nullString.ToValue().IsNull);
            Assert.Equal(ValueType.Null, nullString.ToValue().Type);

            Assert.Equal(decimal.ToDouble(dec), ((object)dec).ToValue().AsDouble);
            Assert.Equal(decimal.ToDouble(ndec.Value), ((object)ndec).ToValue().AsDouble);
        }

        [Fact]
        public void ToDictionary_Test() {
            var dict = new Dictionary<string, object> {
                { "a", 1 },
                { "b", "abc" },
                { "c", null }
            };
            IValue tmp = dict.ToValue();
            Assert.IsAssignableFrom<IComplexValue>(tmp);
            IComplexValue val = (IComplexValue)tmp;
            Assert.Equal(new string[] { "a", "b", "c" }, val.Select(i => i.Key).OrderBy(i => i).ToArray());
            ValueAssert.CheckInt(1, val.GetSubValue("a"));
            ValueAssert.CheckString("abc", val.GetSubValue("b"));
            Assert.True(val.GetSubValue("c").IsNull);

            object obj = dict;
            tmp = obj.ToValue();
            Assert.IsAssignableFrom<IComplexValue>(tmp);
            val = (IComplexValue)tmp;
            Assert.Equal(new string[] { "a", "b", "c" }, val.Select(i => i.Key).OrderBy(i => i).ToArray());
        }
    }
}
