﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentAssertions;

namespace EvaluateExpression.Values.Base.Tests
{
    public class PrimitiveValueBaseTests
    {
        readonly PrimitiveValueBase value = new PrimitiveValueBase();

        [Fact]
        public void GetValue_Test()
        {
            // action
            var result = value.Property("xxx");

            // asserts
            result.Should().BeNull();
        }
    }
}
