﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using FluentAssertions;
using AutoFixture.Xunit2;

namespace EvaluateExpression.Values.Tests
{
    public class ArrayPatternValueTests
    {
        readonly Mock<IArrayValueStrategy> mockStrategy = new Mock<IArrayValueStrategy>();
        readonly ArrayPatternValue array;

        public ArrayPatternValueTests()
        {
            array = new ArrayPatternValue(mockStrategy.Object);
        }

        [Theory]
        [InlineData(10, "Count")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            mockStrategy.Setup(f => f.Count).Returns(10);

            // action
            var value = array.Property(name);

            // asserts
            if (expected == null)
                value.Should().BeNull();
            else
                value.AsInt.Should().Be(expected);
        }
    }
}
