﻿using System;
using Xunit;

namespace EvaluateExpression.Values.Tests
{
    
    public class LazyValueTests
    {
        [Fact]
        [Obsolete]
        public void LazyValue_LazyTest() {
            int cnt = 0;
            IValue val = new LazyValue(() => { cnt += 1; return 10.ToValue(); });
            Assert.Equal(0, cnt);
            Assert.Equal(10, val.AsInt);
            Assert.Equal(1, cnt);

            Assert.Equal(10, val.AsInt);
            Assert.Equal(1, cnt);

            ValueAssert.CheckInt(10, val);
            Assert.Equal(1, cnt);

            Assert.Equal(10, (int)val.Value);
            Assert.Equal(1, cnt);
        }

        [Fact]
        [Obsolete]
        public void LazyValue_WrapperTest() {
            ValueAssert.CheckBool(true, new LazyValue(() => BoolValue.True));
            DateTime dt = DateTime.Now;
            ValueAssert.CheckDateTime(dt, new LazyValue(() => dt.ToValue()));
            ValueAssert.CheckDouble(0.5, new LazyValue(() => 0.5.ToValue()));
            ValueAssert.CheckInt(1, new LazyValue(() => 1.ToValue()));
            ValueAssert.CheckString("abc", new LazyValue(() => "abc".ToValue()));
        }
    }
}
