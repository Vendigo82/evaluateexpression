﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Values.Tests
{
    /// <summary>
    /// Summary description for ArrayValueTests
    /// </summary>
    
    public class ArrayValueTests
    {

        [Fact]
        public void ArrayValueTest()
        {
            ArrayValue val = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                true.ToValue()
            });

            Assert.Equal(10, val[0.ToValue()].AsInt);
            Assert.Equal(20, val[1.ToValue()].AsInt);
            Assert.True(val[2.ToValue()].AsBool);
            Assert.Throws<IndexOutOfRangeException>(() => val[3.ToValue()]);

            ValueAssert.CheckIsProps(val, false, false, false, false, ValueType.Unknown);
            ValueAssert.CheckAsAccessors(val, false, false, false, false, false);
        }

        [Fact]
        public void ArrayValueEnumerateTest() {
            ArrayValue val = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                30.ToValue()
            });

            Assert.Equal(new int[] { 10, 20, 30 }, (val as IIndexableValue).Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ArrayValueGetCount() {
            ArrayValue val = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                30.ToValue()
            });

            Assert.Equal(3, val.GetSubValue(val.CountPropertyName()).AsInt);
        }
    }
}
