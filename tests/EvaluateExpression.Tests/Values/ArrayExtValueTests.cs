﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Values.Tests
{
    
    public class ArrayExtValueTests
    {
        private class FakeArrayStrategy : IArrayValueStrategy
        {
            public static readonly int[] Array = new int[] { 0, 1, 2, 3, 4 };

            public IValue this[int index] => index.ToValue();

            public int Count => Array.Length;

            public object Value => Array;

            public bool IsSolid => true;

            public IEnumerator<IValue> GetEnumerator() {
                return Array.Select(i => i.ToValue()).GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator() {
                return GetEnumerator();
            }
        }

        [Fact]
        public void ArrayExtValueTest() {
            ArrayPatternValue val = new ArrayPatternValue(new FakeArrayStrategy());

            Assert.Equal(FakeArrayStrategy.Array.Length, val.GetSubValue(IValueExtentions.COUNT_PROPERTY).AsInt);
            for (int i = 0; i < FakeArrayStrategy.Array.Length; ++i)
                Assert.Equal(FakeArrayStrategy.Array[i], val[i.ToValue()].AsInt);
            Assert.Same(FakeArrayStrategy.Array, val.Value);
            Assert.Equal(FakeArrayStrategy.Array, (val as IEnumerable<IValue>).Select(v => v.AsInt).ToArray());
        }
    }
}
