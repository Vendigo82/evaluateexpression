﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using EvaluateExpression.Asserts;

namespace EvaluateExpression.Values.Tests
{
    /// <summary>
    /// Summary description for DateTimeValueTests
    /// </summary>
    
    public class DateTimeValueTests
    {
        [Fact]
        public void DateTimeValueTest() {
            DateTime dt = DateTime.Now;
            DateTimeValue val = new DateTimeValue(dt);

            ValueAssert.CheckIsProps(val, false, false, false, true, ValueType.DateTime);
            ValueAssert.CheckAsAccessors(val, false, false, false, false, true);
            Assert.Equal(dt, val.AsDateTime);
        }

        [Fact]
        public void DateTimeValue_ComponenetsTest() {
            DateTime dt = DateTime.Now;
            DateTimeValue val = new DateTimeValue(dt);

            ValueAssert.CheckInt(dt.Year, val.GetSubValue("Year"));
            ValueAssert.CheckInt(dt.Month, val.GetSubValue("Month"));
            ValueAssert.CheckInt(dt.Day, val.GetSubValue("Day"));
            ValueAssert.CheckInt(dt.Hour, val.GetSubValue("Hour"));
            ValueAssert.CheckInt(dt.Minute, val.GetSubValue("Min"));
            ValueAssert.CheckInt(dt.Second, val.GetSubValue("Sec"));

            ValueAssert.CheckDateTime(dt.Date, val.GetSubValue("Date"));
        }

        [Fact]
        public void Property_Test()
        {
            // setup
            var dt = DateTime.Now;
            var value = new DateTimeValue(dt);

            // asserts
            value.Property("Year").Should().Be(dt.Year);
            value.Property("Month").Should().Be(dt.Month);
            value.Property("Day").Should().Be(dt.Day);
            value.Property("Hour").Should().Be(dt.Hour);
            value.Property("Min").Should().Be(dt.Minute);
            value.Property("Sec").Should().Be(dt.Second);

            value.Property("Date").Should().Be(dt.Date);

            value.Property("unknown").Should().BeNull();
        }
    }
}
