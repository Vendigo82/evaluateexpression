﻿using System;
using Xunit;
using FluentAssertions;
using EvaluateExpression.Asserts;

namespace EvaluateExpression.Values.Tests
{
    
    public class StringValueTests
    {
        [Fact]
        public void StringValue_Length() {
            Assert.Equal(3, new StringValue("abc").GetSubValue("Length").AsInt);
            Assert.Equal(0, new StringValue("").GetSubValue("Length").AsInt);
        }

        [Theory]
        [InlineData(3, "abc", "Length")]
        [InlineData(null, "abc", "unknown")]
        public void Property_Test(int? expected, string str, string name)
        {
            // setup
            var value = new StringValue(str);

            // action
            var result = value.Property(name);

            // asserts
            if (expected != null)
                result.Should().Be(expected.Value);
            else
                result.Should().BeNull();
        }
    }
}
