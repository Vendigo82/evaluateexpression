﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using EvaluateExpression.Asserts;

namespace EvaluateExpression.Values.Tests
{
    /// <summary>
    /// Summary description for DictionaryValuesTests
    /// </summary>
    
    public class StructValuesTests
    {

        [Fact]
        public void StructValues_Test() {
            StructValue val = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 10.ToValue()),
                new KeyValuePair<string, IValue>("B", true.ToValue())
            });

            Assert.Equal(10, val.GetSubValue("A").AsInt);
            Assert.True(val.GetSubValue("B").AsBool);
            Assert.Null(val.GetSubValue("C"));

            ValueAssert.CheckIsProps(val, false, false, false, false, ValueType.Unknown);
            ValueAssert.CheckAsAccessors(val, false, false, false, false, false);
        }

        [Fact]
        public void StructValues_EnumTest() {
            StructValue val = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 10.ToValue()),
                new KeyValuePair<string, IValue>("B", 20.ToValue())
            });

            Assert.Equal(new string[] { "A", "B" }, val.Select(v => v.Key).ToArray());
            Assert.Equal(new int[] { 10, 20 }, val.Select(v => v.Value.AsInt).ToArray());
        }

        [Fact]
        public void StructValues_ReplaceTest() {
            StructValue val = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 10.ToValue()),
                new KeyValuePair<string, IValue>("B", 20.ToValue())
            });

            Assert.Equal(10, val.GetSubValue("A").AsInt);

            val.Add("A", 30.ToValue());
            Assert.Equal(30, val.GetSubValue("A").AsInt);
        }

        [Theory]
        [InlineData(10, "a")]
        [InlineData(20, "b")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var structValue = new StructValue() {
                { "a", 10.ToValue() },
                { "b", 20.ToValue() }
            };

            // action
            var result = structValue.Property(name);

            // asserts
            if (expected != null)
                result.Should().NotBeNull().And.Be(expected.Value);
            else
                result.Should().BeNull();
        }
    }
}
