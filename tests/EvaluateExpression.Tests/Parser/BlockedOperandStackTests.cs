﻿using Xunit;
using EvaluateExpression.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operands;

namespace EvaluateExpression.Parser.Tests {
    
    public class BlockedOperandStackTests {
        [Fact]
        public void BlockedOperandStackTest() {
            BlockedOperandStack stack = new BlockedOperandStack();
            stack.put(new StaticOperand(1));
            stack.put(new StaticOperand(2));

            Assert.Equal(2, stack.count);
            Assert.False(stack.empty);

            Assert.Equal(2, stack.extract().Eval(null).AsDouble);
            Assert.Equal(1, stack.count);
            Assert.False(stack.empty);

            Assert.Equal(1, stack.extract().Eval(null).AsDouble);
            Assert.Equal(0, stack.count);
            Assert.True(stack.empty);
        }

        [Fact]
        public void BlockedOperandStackTestBlock()
        {
            BlockedOperandStack stack = new BlockedOperandStack();
            stack.put(new StaticOperand(1));
            stack.addBlock();
            stack.put(new StaticOperand(2));

            Assert.Equal(2, stack.count);
            Assert.False(stack.empty);

            Assert.Equal(2, stack.extract().Eval(null).AsDouble);
            Assert.Equal(1, stack.count);
            Assert.True(stack.empty);

            Assert.Throws<InvalidOperationException>(() => stack.extract());

            stack.removeLastBlock();
            Assert.Equal(1, stack.count);
            Assert.False(stack.empty);

            Assert.Equal(1, stack.extract().Eval(null).AsDouble);
            Assert.Equal(0, stack.count);
            Assert.True(stack.empty);
        }

        [Fact]
        public void BlockedOperandStackTestBlock2() {
            BlockedOperandStack stack = new BlockedOperandStack();
            stack.put(new StaticOperand(1));
            stack.addBlock();
            stack.put(new StaticOperand(2));

            Assert.Equal(2, stack.count);
            Assert.False(stack.empty);

            stack.removeLastBlock();

            Assert.Equal(2, stack.extract().Eval(null).AsDouble);
            Assert.Equal(1, stack.count);
            Assert.False(stack.empty);

            Assert.Equal(1, stack.extract().Eval(null).AsDouble);
            Assert.Equal(0, stack.count);
            Assert.True(stack.empty);
        }
    }
}