﻿using Xunit;
using EvaluateExpression.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operations;
using EvaluateExpression.Operands;
using EvaluateExpression.Functions;
using EvaluateExpression.Parser.Parsers;
using EvaluateExpression.Script;

namespace EvaluateExpression.Parser.Tests {
    using System.Collections;
    using Values;

    
    public class ParserTests {
        const int eps = 6;//0.000001;

        private class TestOper : IOperand {
            public IOperand a = null;
            public IOperand b = null;

            public TestOper(IOperand b) {
                this.b = b;
            }

            public TestOper(IOperand a, IOperand b) {
                this.a = a;
                this.b = b;
            }

            public IValue Eval(INameResolver context) {
                throw new NotImplementedException();
            }

            public class Creator : BinaryOrUnaryOperationInfo.ICreator {
                public IOperand create(IOperand b) {
                    return new TestOper(b);
                }

                public IOperand create(IOperand a, IOperand b) {
                    return new TestOper(a, b);
                }
            }
        }

        [Fact]
        public void ParserTest() {
            Dictionary<char, IOperationInfo> opers = new Dictionary<char, IOperationInfo>();
            opers.Add('+', new BinaryOrUnaryOperationInfo(1, new TestOper.Creator()));

            Parser parser = new Parser(opers);
            {
                IOperand result = parser.Parse("2+1");
                Assert.IsAssignableFrom<TestOper>(result);
                TestOper oper = (TestOper)result;
                Assert.IsAssignableFrom<IValue>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(2, oper.a.Eval(null).AsDouble);
                Assert.Equal(1, oper.b.Eval(null).AsDouble);
            }

            {
                IOperand result = parser.Parse("2+1+3");
                Assert.IsAssignableFrom<TestOper>(result);
                TestOper oper = (TestOper)result;
                Assert.IsAssignableFrom<TestOper>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(3, oper.b.Eval(null).AsDouble);

                oper = (TestOper)oper.a;
                Assert.IsAssignableFrom<IValue>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(2, oper.a.Eval(null).AsDouble);
                Assert.Equal(1, oper.b.Eval(null).AsDouble);
            }

            {
                IOperand result = parser.Parse("a+1");
                Assert.IsAssignableFrom<TestOper>(result);
                TestOper oper = (TestOper)result;
                Assert.IsAssignableFrom<VariableOperand>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(1, oper.b.Eval(null).AsDouble);
            }

            {
                IOperand result = parser.Parse("(2+1)+3");
                Assert.IsAssignableFrom<TestOper>(result);
                TestOper oper = (TestOper)result;
                Assert.IsAssignableFrom<TestOper>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(3, oper.b.Eval(null).AsDouble);

                oper = (TestOper)oper.a;
                Assert.IsAssignableFrom<IValue>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(2, oper.a.Eval(null).AsDouble);
                Assert.Equal(1, oper.b.Eval(null).AsDouble);
            }

            {
                IOperand result = parser.Parse("2+(1+3)");
                Assert.IsAssignableFrom<TestOper>(result);
                TestOper oper = (TestOper)result;
                Assert.IsAssignableFrom<IValue>(oper.a);
                Assert.IsAssignableFrom<TestOper>(oper.b);
                Assert.Equal(2, oper.a.Eval(null).AsDouble);

                oper = (TestOper)oper.b;
                Assert.IsAssignableFrom<IValue>(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(1, oper.a.Eval(null).AsDouble);
                Assert.Equal(3, oper.b.Eval(null).AsDouble);
            }

            {
                IOperand result = parser.Parse("+1");
                Assert.IsAssignableFrom<TestOper>(result);
                TestOper oper = (TestOper)result;
                Assert.Null(oper.a);
                Assert.IsAssignableFrom<IValue>(oper.b);
                Assert.Equal(1, oper.b.Eval(null).AsDouble);
            }
        }

        [Fact]
        public void ParserTest1() {
            Parser p = new Parser(OperationsPack.Create());

            Assert.Equal(-1, p.Parse("-1").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("+1").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("-1+2").Eval(null).AsDouble, eps);
            Assert.Equal(6, p.Parse("2+2*2").Eval(null).AsDouble, eps);
            Assert.Equal(8, p.Parse("(2+2)*2").Eval(null).AsDouble, eps);
            Assert.Equal(2.5, p.Parse("(2+2+1)/2").Eval(null).AsDouble, eps);
            Assert.Equal(-11, p.Parse("(2-3)-10").Eval(null).AsDouble, eps);
            Assert.Equal(18, p.Parse("(2+2)*(2+2)+2").Eval(null).AsDouble, eps);
        }

        [Fact]
        public void ParserTestBoolean() {
            Parser p = new Parser(OperationsPack.Create());

            Assert.Equal(1, p.Parse("1&1").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("1&2").Eval(null).AsDouble, eps);
            Assert.Equal(0, p.Parse("1&(1&0)").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("1|(1&0)").Eval(null).AsDouble, eps);
            Assert.Equal(0, p.Parse("!11").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("!(1&0)").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("(2+2)>3").Eval(null).AsDouble, eps);
            Assert.Equal(0, p.Parse("(2+2)<3").Eval(null).AsDouble, eps);
        }

        [Fact]
        public void ParserTestConst() {
            ConstNameResolver context = new ConstNameResolver();
            context.Add("a", new DoubleValue(1));
            context.Add("b", new DoubleValue(2));
            context.Add("PI", new DoubleValue(3.14));
            Parser p = new Parser(OperationsPack.Create());

            Assert.Equal(-1, p.Parse("-a").Eval(context).AsDouble, eps);
            Assert.Equal(3, p.Parse("a+b").Eval(context).AsDouble, eps);
            Assert.Equal(6, p.Parse("(a+b)*2").Eval(context).AsDouble, eps);
            Assert.Equal(6.28, p.Parse("PI*2").Eval(context).AsDouble, eps);
        }

        [Fact]
        public void ParserDoubleCharOperationsTests() {            
            Parser p = new Parser(OperationsPack.CreateDefFactory(), null);
            Assert.Equal(1, p.Parse("10<11").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("10<=11").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("11<=11").Eval(null).AsDouble, eps);
            Assert.Equal(0, p.Parse("12<=11").Eval(null).AsDouble, eps);

            Assert.Equal(1, p.Parse("12>=11").Eval(null).AsDouble, eps);
            Assert.Equal(1, p.Parse("11>=11").Eval(null).AsDouble, eps);
            Assert.Equal(0, p.Parse("11>11").Eval(null).AsDouble, eps);
        }

        [Fact]
        public void ParserTest2() {
            string s = "X=null||X=0";

            IValueParser valueParser = new ValueParserList(new IValueParser[] { new ValueParser(), new NullTrueFalseParser() }, true);
            Parser p = new Parser(valueParser, null, OperationsPack.CreateDefFactory(), null, null);
            IOperand oper = p.Parse(s);

            INameResolver c = new ConstNameResolver("X", new DoubleValue(3));
            Assert.False(oper.Eval(c).AsBool);
        }

        private static IValue IfFunc(IValue condition, IValue t, IValue f) {
            return condition.AsBool ? t : f;
        }

        [Fact]
        public void ParseFunctionTests() {
            SimpleFunctionFactory functionFactory = new SimpleFunctionFactory();
            functionFactory.Add("sum", new FunctionSignatureVarArg(Functions.Tests.FunctionSignatureTests.VarArgSum));
            functionFactory.Add("if", new FunctionSignature3Arg(IfFunc));
            Parser p = new Parser(OperationsPack.CreateDefFactory(), functionFactory);
            Assert.Equal(50, p.Parse("10+sum(5,20)+15").Eval(null).AsDouble, eps);

            Assert.Equal(50, p.Parse("10+sum(5,20)+15").Eval(null).AsDouble, eps);
            Assert.Equal(16, p.Parse("1+sum(1,2,3,4,5)").Eval(null).AsDouble, eps);
            Assert.Equal(5, p.Parse("sum(2,3)").Eval(null).AsDouble, eps);
            Assert.Equal(2, p.Parse("if(2+3>4,sum(1,1),0)").Eval(null).AsDouble, eps);
        }

        class OrderNameResolver : INameResolver {
            public List<string> resolveOrder = new List<string>();

            public string Description => "";

            public IContext BaseContext => throw new NotImplementedException();

            public bool Assign(string name, IValue value) =>throw new NotImplementedException();
            public bool DeclareVariable(string name, IValue value = null) => throw new NotImplementedException();
            public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() => throw new NotImplementedException();
            IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();

            public IValue Resolve(string name) {
                resolveOrder.Add(name);
                return new DoubleValue(1);
            }

            public IValue Property(string name) => Resolve(name);
        }

        [Fact]
        public void OrderEqualOperationsTest() {
            OrderNameResolver context = new OrderNameResolver();
            Parser parser = new Parser(OperationsPack.CreateDefFactory(), null);
            string expr = "a<b||c<d||e<f";
            IOperand operand = parser.Parse(expr);
            double result = operand.Eval(context).AsDouble;

            List<string> expectedOrder = new List<string> { "a", "b", "c", "d", "e", "f" };
            Assert.Equal(expectedOrder, context.resolveOrder);
            Assert.Equal(0, result);
        }

        [Fact]
        public void ParserKeyWordsTest() {
            Parser parser = new Parser(
                new ValueParserList(new IValueParser[] {new ValueParser(), new NullTrueFalseParser()}, true),
                null, OperationsPack.CreateDefFactory(), null, null);
            Assert.True(parser.Parse("1<2=true").Eval(null).AsBool);
            Assert.True(parser.Parse("3<2=false").Eval(null).AsBool);
            Assert.True(parser.Parse("true!=false").Eval(null).AsBool);
            Assert.True(parser.Parse("null").Eval(null).IsNull);
        }

        [Fact]
        public void ParserConstsTest() {
            INameResolver nr = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", new IntValue(10)),
                new KeyValuePair<string, IValue>("B", new IntValue(20))
            });
            Parser parser = new Parser(null, null, null, null, nr);
            IOperand operand = parser.Parse("(A+B)/2");
            Assert.Equal(15, operand.Eval(null).AsInt);
        }

        [Fact]
        public void ParserSkipSeparatorsTest() {
            Parser p = new Parser();

            Assert.Equal(6, p.Parse("2 + 2 * 2").Eval(null).AsInt);
            Assert.Equal(6, p.Parse("   2    +    2   *   2").Eval(null).AsInt);
            Assert.Equal(6, p.Parse(@"2 + 2 *
                 2").Eval(null).AsInt);
            Assert.Equal(6, p.Parse("2\t+ 2*\t2").Eval(null).AsInt);

            INameResolver context = new ConstNameResolver("a", new IntValue(1));
            Assert.Equal(1, p.Parse("  a").Eval(context).AsInt);
            Assert.Equal(1, p.Parse("  a  ").Eval(context).AsInt);
            Assert.Equal(1, p.Parse("a;").Eval(context).AsInt);
            Assert.Equal(1, p.Parse("  a  ;").Eval(context).AsInt);
        }

        [Fact]
        public void ParseSequenceOfExpressionsTest() {
            Parser p = new Parser(OperationsPack.Create());

            string expr = "2+2;3*2";
            int index = 0;        
            Assert.Equal(4, p.Parse(expr, ref index).SingleOperand.Eval(null).AsInt);
            Assert.Equal(4, index);
            Assert.Equal(6, p.Parse(expr, ref index).SingleOperand.Eval(null).AsInt);
            Assert.Equal(expr.Length, index);

            expr = "2+2;;3*2;";
            index = 0;
            IParseResult result = p.Parse(expr, ref index);
            Assert.Equal(ParseFinishReason.EndLine, result.FinishReason);
            Assert.Equal(4, result.SingleOperand.Eval(null).AsInt);
            Assert.Equal(4, index);

            result = p.Parse(expr, ref index);
            Assert.Equal(ParseFinishReason.EndLine, result.FinishReason);
            Assert.Equal(0, result.Count);
            Assert.Equal(5, index);

            result = p.Parse(expr, ref index);
            Assert.Equal(ParseFinishReason.EndLine, result.FinishReason);
            Assert.Equal(6, result.SingleOperand.Eval(null).AsInt);
            Assert.Equal(expr.Length, index);

            result = p.Parse(expr, ref index);
            Assert.Equal(ParseFinishReason.Eof, result.FinishReason);
            Assert.Equal(0, result.Count);
            Assert.Equal(expr.Length, index);
        }

        [Fact]
        public void ParserKeywordsTest() {
            KeyWordCollection kwc = new KeyWordCollection(new KeyValuePair<string, int>[] {
                new KeyValuePair<string, int>("IF", 1),
                new KeyValuePair<string, int>("THEN", 2)
            });
            Parser parser = new Parser(keyWords: kwc);

            string expr = "IF 2+5 THEN";
            int index = 0;
            IParseResult r = parser.Parse(expr, ref index);
            Assert.Equal(ParseFinishReason.KeyWord, r.FinishReason);
            Assert.Equal(1, r.KeyWord);
            Assert.Equal(0, r.Count);
            Assert.Equal(2, index);

            r = parser.Parse(expr, ref index);
            Assert.Equal(ParseFinishReason.KeyWord, r.FinishReason);
            Assert.Equal(2, r.KeyWord);
            Assert.Equal(1, r.Count);
            Assert.Equal(expr.Length, index);
            Assert.Equal(7, r.SingleOperand.Eval(null).AsInt);
        }

        [Fact]
        public void Parser_Comments() {
            string expr = "1 + //some comment" + Environment.NewLine + "2//3";
            Parser p = new Parser();
            Assert.Equal(3, p.Parse(expr).Eval(null).AsInt);
        }
    }
}