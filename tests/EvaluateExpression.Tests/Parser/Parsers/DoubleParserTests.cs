﻿using Xunit;
using EvaluateExpression.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers.Tests {
    
    public class DoubleParserTests {
        private DoubleParser parser = new DoubleParser();

        [Fact]
        public void parseTest() {
            string s = "123+56+18.36";
            int index = 0;
            Assert.Equal(123, parser.Parse(s, ref index).AsDouble);
            Assert.Equal(3, index);

            index += 1;

            Assert.Equal(56, parser.Parse(s, ref index).AsDouble);
            Assert.Equal(6, index);

            index += 1;
            Assert.Equal(18.36, parser.Parse(s, ref index).AsDouble, 5);
            Assert.Equal(12, index);
        }

        [Fact]
        public void DoubleParserValueTypeTest() {
            Assert.Equal(ValueType.Integer, Parse("123").Type);
            Assert.Equal(ValueType.Decimal, Parse("12.3").Type);
            Assert.Null(Parse("\"12.3\""));
        }

        [Fact]
        public void isDigitTest() {
            Assert.True(parser.isDigit('0'));
            Assert.True(parser.isDigit('9'));
            Assert.True(parser.isDigit('5'));
            Assert.False(parser.isDigit('a'));
        }

        private IValue Parse(string s) {
            int index = 0;
            return parser.Parse(s, ref index);
        }
    }
}