﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Parser.Parsers.Tests
{
    /// <summary>
    /// Summary description for LexemeExParserTests
    /// </summary>
    
    public class LexemeExParserTests
    {

        [Fact]
        public void LexemeExParserTest()
        {
            LexemeExParser lex = new LexemeExParser();
            int index = 0;
            Assert.Equal("qwerty", lex.Parse("qwerty zz", ref index));
            Assert.Equal(6, index);

            index = 6;
            Assert.Equal("dog amongs cats", lex.Parse("black @\"dog amongs cats\" alone", ref index));
            Assert.Equal(18 + 6, index);

            index = 0;
            Assert.Equal("dog amongs cats", lex.Parse("@\"dog amongs cats\"", ref index));
            Assert.Equal(18, index);

            index = 0;
            Assert.Null(lex.Parse("@qqqq", ref index));
            Assert.Equal(0, index);

            index = 0;
            Assert.Throws<ParserException>(() => lex.Parse("@\"\"", ref index));

            index = 0;
            Assert.Throws<ParserException>(() => lex.Parse("@\"along", ref index));
        }
    }
}
