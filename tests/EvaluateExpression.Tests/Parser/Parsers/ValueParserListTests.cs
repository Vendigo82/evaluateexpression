﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Parser.Parsers.Tests {
    /// <summary>
    /// Summary description for ValueParserListTests
    /// </summary>
    
    public class ValueParserListTests {        

        [Fact]
        public void ValueParserListTest() {
            ValueParserList p = new ValueParserList(new IValueParser[] { new ValueParser(), new NullTrueFalseParser() }, true);
            string s = "123";
            int index = 0;
            Assert.Equal(123, p.Parse(s, ref index).AsInt);
            Assert.Equal(s.Length, index);

            index = 0;
            Assert.False(p.Parse("false", ref index).AsBool);
            Assert.Equal(5, index);
        }

        [Fact]
        public void ValueParserSkipLeadingSpaces() {
            ValueParserList p = new ValueParserList( new IValueParser[] { new ValueParser() }, true);
            string s = " 123";
            int index = 0;
            Assert.Equal(123, p.Parse(s, ref index).AsInt);
            Assert.Equal(s.Length, index);

            s = "   123";
            index = 0;
            Assert.Equal(123, p.Parse(s, ref index).AsInt);
            Assert.Equal(s.Length, index);

            s = "  ";
            index = 0;
            Assert.Null(p.Parse(s, ref index));
            Assert.Equal(s.Length, index);
        }
    }
}
