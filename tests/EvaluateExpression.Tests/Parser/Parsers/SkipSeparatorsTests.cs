﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Parser.Parsers.Tests
{
    
    public class SkipSeparatorsTests
    {
        [Fact]
        public void SkipSeparatorsTest() {
            SkipSeparators s = new SkipSeparators();
            string str = "012 456\t89" + Environment.NewLine + "123 ";
            int index = 0;

            List<int> skipIndexes = new () { 3, 7, 10, 14 + Environment.NewLine.Length - 1 };
            if (Environment.NewLine.Length != 1)
                skipIndexes.Add(10 + 1);

            while (index < str.Length) {
                int charIndex = index;
                bool result = s.Skip(str, ref index, out bool newLine);

                if (!result)
                    index += 1;

                if (result)
                    skipIndexes.Should().Contain(charIndex);
                else
                    skipIndexes.Should().NotContain(charIndex);

                //Assert.Equal(result, skipIndexes.Contains(charIndex));
                if (newLine)
                    str[index].Should().Be('1', $"Expected char '1' at position {charIndex} in string {str}");
                    //Assert.Equal(11, charIndex);
            }
        }

        [Theory]
        [InlineData("123 456", 3, 4)]
        [InlineData("123   456", 3, 6)]
        [InlineData("123:456", 3, 3)]
        [InlineData("123 ", 3, 4)]
        [InlineData("123", 3, 3)]
        [InlineData("123", 4, 4)]   // out of range
        public void SkipAllTests(string expression, int index, int expectedIndex)
        {
            // setup
            var target = new SkipSeparators();

            // action
            target.SkipAll(expression, ref index);

            // asserts
            index.Should().Be(expectedIndex);
        }
    }
}
