﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Parser.Parsers.Tests {
    /// <summary>
    /// Summary description for LexemeParser
    /// </summary>
    
    public class LexemeParserTests {        

        [Fact]
        public void LexemeParserTest() {
            string str = "abc q1 _qwerty";
            LexemeParser p = new LexemeParser();
            int index = 0;
            Assert.Equal("abc", p.Parse(str, ref index));
            Assert.Equal(3, index);

            index += 1;
            Assert.Equal("q1", p.Parse(str, ref index));
            Assert.Equal(6, index);

            index += 1;
            Assert.Equal("_qwerty", p.Parse(str, ref index));
            Assert.Equal(14, index);
        }

        [Fact]
        public void LexemeParserNotFoundTest() {
            LexemeParser p = new LexemeParser();
            int index = 0;
            Assert.Null(p.Parse("1abc", ref index));
            Assert.Equal(0, index);

            Assert.Null(p.Parse("-ab", ref index));
            Assert.Equal(0, index);

            Assert.Null(p.Parse(" ab", ref index));
            Assert.Equal(0, index);
        }

        [Fact]
        public void LexemeParserRusTest() {
            LexemeParser p = new LexemeParser();
            int index = 0;
            Assert.Equal("АВГД", p.Parse("АВГД", ref index));
            Assert.Equal(4, index);
        }

        [Fact]
        public void LexemeParserWithDotTest() {
            LexemeParserWithDot p = new LexemeParserWithDot();
            int index = 0;
            Assert.Equal("prefix.value", p.Parse("prefix.value", ref index));
            Assert.Equal(12, index);

            index = 0;
            Assert.Null(p.Parse(".value", ref index));
            Assert.Equal(0, index);
        }
    }
}
