﻿using EvaluateExpression.Exceptions;
using FluentAssertions;
using System;
using Xunit;

namespace EvaluateExpression.Parser.Parsers.Tests
{
    public class ValueParserTests
    {
        readonly ValueParser target = new();

        [Theory]
        [InlineData("\"ab123\"", 0, "ab123", 0)]
        [InlineData("\"\\\"abc\\\"\"", 0, "\"abc\"", 0)]
        [InlineData("\"\\abc\"", 0, "\\abc", 0)]
        [InlineData("ab+\"qwerty\"-45", 3, "qwerty", '-')]
        public void ParseSingleString(string expression, int index, string expectedValue, char expectedNextChar)
        {
            // setup

            // action
            var result = target.Parse(expression, ref index).AsString;

            // asserts
            result.Should().Be(expectedValue);

            if (expectedNextChar == 0)
                index.Should().Be(expression.Length);
            else
                expression[index].Should().Be(expectedNextChar);
        }

        [Theory]
        [InlineData("\"\"", 0, 0)]
        [InlineData("ab+\"\"-45", 3, '-')]
        public void ParseEmptyString(string expression, int index, char expectedNextChar)
        {
            // setup

            // action
            var result = target.Parse(expression, ref index).AsString;

            // asserts
            result.Should().Be(string.Empty);

            if (expectedNextChar == 0)
                index.Should().Be(expression.Length);
            else
                expression[index].Should().Be(expectedNextChar);
        }

        [Theory]
        [InlineData("\"ab123", 0)]
        [InlineData("\"\\", 0)]
        public void ParseUnfinishedString(string expression, int index)
        {
            Action action = () => target.Parse(expression, ref index);

            action.Should().Throw<ParserException>();
        }

        [Theory]
        [InlineData("a12", 0)]
        [InlineData("ab", 1)]
        public void Parse_WhenNotStartsFromQuotas_ShouldReturnNull(string expression, int index)
        {
            // setup

            // action
            var result = target.Parse(expression, ref index);

            // asserts
            result.Should().BeNull();
        }
    }
}