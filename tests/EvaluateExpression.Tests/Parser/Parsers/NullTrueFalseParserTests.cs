﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Parser.Parsers.Tests {
    /// <summary>
    /// Summary description for NullTrueFalseParserTests
    /// </summary>
    
    public class NullTrueFalseParserTests {        

        [Fact]
        public void NullTrueFalseParserTest() {
            NullTrueFalseParser p = new NullTrueFalseParser();
            int index = 0;
            Assert.True(p.Parse("true", ref index).AsBool);
            Assert.Equal(4, index);

            index = 0;
            Assert.False(p.Parse("false", ref index).AsBool);
            Assert.Equal(5, index);

            index = 0;
            Assert.True(p.Parse("null", ref index).IsNull);
            Assert.Equal(4, index);

            index = 0;
            Assert.Null(p.Parse("truesome", ref index));
            Assert.Equal(0, index);

            index = 0;
            Assert.True(p.Parse("true+1", ref index).AsBool);
            Assert.Equal(4, index);
        }
    }
}
