﻿using AutoFixture.Xunit2;
using EvaluateExpression.Parser;
using FluentAssertions;
using Moq;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.Tests.Parser.ParserExtensionsTests
{
    public class ParseCommaSeparatedListTests
    {
        readonly Mock<IParser> parserMock = new();
        int index = 0;

        [Theory, AutoMockData]
        public void SingleElementListTest(string expression, IOperand operand)
        {
            // setup
            var parseResult = new ParseResult(operand, ParseFinishReason.EndLine);
            parserMock.Setup(f => f.Parse(expression, ref It.Ref<int>.IsAny)).Returns(parseResult);

            // action
            var tuple = parserMock.Object.ParseCommaSeparatedList(expression, ref index);

            // asserts
            tuple.Item1.Should().BeSameAs(parseResult);
            tuple.Item2.Should().ContainSingle().Which.Should().BeSameAs(operand);

            parserMock.Verify(f => f.Parse(expression, ref It.Ref<int>.IsAny), Times.Once);
        }

        [Theory, AutoMockData]
        public void MultiElementsListTest(string expression, IOperand[] operands)
        {
            // setup
            var parseResults =  operands
                .Select(i => new ParseResult(i, i == operands[^1] ? ParseFinishReason.EndLine : ParseFinishReason.CommaSeparator))
                .ToArray();
            var queue = new Queue<ParseResult>(parseResults);

            parserMock.Setup(f => f.Parse(expression, ref It.Ref<int>.IsAny)).Returns(() => queue.Dequeue());

            // action            
            var tuple = parserMock.Object.ParseCommaSeparatedList(expression, ref index);

            // asserts
            tuple.Item1.Should().BeSameAs(parseResults.Last());
            tuple.Item2.Should().Equal(operands);

            parserMock.Verify(f => f.Parse(expression, ref It.Ref<int>.IsAny), Times.Exactly(operands.Length));
        }

        [Theory, AutoData]
        public void EmptyResult_ShouldBeParserException_Test(string expression)
        {
            // setup
            var parseResult = new ParseResult(ParseFinishReason.EndLine);
            parserMock.Setup(f => f.Parse(expression, ref It.Ref<int>.IsAny)).Returns(parseResult);

            // action
            Action action = () => parserMock.Object.ParseCommaSeparatedList(expression, ref index);

            // asserts
            action.Should().ThrowExactly<Exceptions.ParserException>();
        }
    }
}
