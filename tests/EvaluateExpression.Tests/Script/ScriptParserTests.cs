﻿using System;
using System.Text;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Xunit;

using EvaluateExpression.Values;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Script.Tests {
    using Operands;

    /// <summary>
    /// Summary description for ScriptParserTests
    /// </summary>
    
    public class ScriptParserTests {

        private ScriptParser scriptParser = new ScriptParser();

        [Fact]
        public void ScriptParserIfTest() {
            TestNameResolver context = new TestNameResolver() { Value = 1.ToValue() };
            //IParser parser = new Parser.Parser(keyWords: ScriptParser.GetKeyWords());
            //ScriptParser scriptParser = ;

            string script;

            script = "if 1<5 then a; else b; end";
            scriptParser.Parse(script).Perform(context);
            Assert.Equal("a", context.Name);

            script = "if 6<5 then a; else b; end";
            scriptParser.Parse(script).Perform(context);
            Assert.Equal("b", context.Name);

            script = "if 1<5 then if 2<5 then c; else d; end else e; end";
            scriptParser.Parse(script).Perform(context);
            Assert.Equal("c", context.Name);

            script = "if 1<5 then if 6<5 then c; else d; end else e; end";
            scriptParser.Parse(script).Perform(context);
            Assert.Equal("d", context.Name);

            script = "if 6<5 then if 6<5 then c; else d; end else e; end";
            scriptParser.Parse(script).Perform(context);
            Assert.Equal("e", context.Name);
        }

        [Fact]
        public void ScriptParserReturnTest() {
            //IParser parser = new Parser.Parser(keyWords: ScriptParser.GetKeyWords());
            //ScriptParser scriptParser = new ScriptParser(parser);

            string script;

            script = "return 5 + 3;";
            Assert.Equal(8, scriptParser.Parse(script).Perform(null).AsInt);            

            script = "if 1<5 then return 3; else return 4; end";
            Assert.Equal(3, scriptParser.Parse(script).Perform(null).AsInt);

            script = "1+2; return 5 + 3;";
            Assert.Equal(8, scriptParser.Parse(script).Perform(null).AsInt);

            script = "1+2; return 5 + 3; return 5;";
            Assert.Equal(8, scriptParser.Parse(script).Perform(null).AsInt);
        }

        [Fact]
        public void ScriptParserDeclareTest() {
            string text = "declare a; a := 10; return a;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver();
            IValue val = script.Perform(nr);
            Assert.Equal(10, val.AsInt);
            Assert.Equal(10, nr.Resolve("a").AsInt);
        }

        [Fact]
        public void ScriptForeachTest() {
            string text = "declare sum; sum := 0; foreach iter in array do sum := sum + iter; end return sum;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver() {
                { "array", new ArrayValue(new int[] {3, 5, 2}.Select(i => i.ToValue()).ToArray()) }
            };

            IValue result = script.Perform(nr);
            Assert.Equal(10, result.AsInt);
        }

        [Fact]
        public void ScriptForeachAndReturnTest() {
            string text = "foreach iter in array do if iter = 5 then return 1; end end return 0;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver() {
                { "array", new ArrayValue(new int[] {3, 5, 2}.Select(i => i.ToValue()).ToArray()) }
            };
            Assert.True(script.Perform(nr).AsBool);

            nr = new VarNameResolver() {
                { "array", new ArrayValue(new int[] {3, 4, 2}.Select(i => i.ToValue()).ToArray()) }
            };
            Assert.False(script.Perform(nr).AsBool);
        }

        [Fact]
        public void ScriptForeachBreakTest() {
            string text = "declare sum; sum := 0; foreach iter in array do sum := sum + iter; break; end return sum;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver() {
                { "array", new ArrayValue(new int[] {3, 5, 2}.Select(i => i.ToValue()).ToArray()) }
            };

            IValue result = script.Perform(nr);
            Assert.Equal(3, result.AsInt);
        }

        [Fact]
        public void ScriptForeachContinueTest() {
            string text = "declare sum; sum := 0; foreach iter in array do continue; sum := sum + iter; end return sum;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver() {
                { "array", new ArrayValue(new int[] {3, 5, 2}.Select(i => i.ToValue()).ToArray()) }
            };

            IValue result = script.Perform(nr);
            Assert.Equal(0, result.AsInt);
        }

        [Fact]
        public void ScriptWhileTest() {
            string text = "declare sum, i; i := 1; sum := 0; while i <= 3 do sum := sum + i; i := i + 1; end return sum;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver();

            IValue result = script.Perform(nr);
            Assert.Equal(6, result.AsInt);
        }

        [Fact]
        public void ScriptWhileBreakTest() {
            string text = "declare sum, i; i := 1; sum := 0; while i <= 3 do sum := sum + i; i := i + 1; break; end return sum;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver();

            IValue result = script.Perform(nr);
            Assert.Equal(1, result.AsInt);
        }

        [Fact]
        public void ScriptWhileContinueTest() {
            string text = "declare sum, i; i := 1; sum := 0; while i <= 3 do i := i + 1; continue; sum := sum + i;  end return sum;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver();

            IValue result = script.Perform(nr);
            Assert.Equal(0, result.AsInt);
        }

        [Fact]
        public void ScriptWhileEndlessLoppTest()
        {
            string text = "while 1 do end";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver();

            Assert.Throws<EvalEndlessLoopException>(() => script.Perform(nr));
        }

        [Fact]
        public void ScriptLastRowExecutionTest() {
            string text = "declare a; a := 10;";
            IScript script = scriptParser.Parse(text);

            INameResolver nr = new VarNameResolver();
            script.Perform(nr);
            Assert.Equal(10, nr.Resolve("a").AsInt);
        }

        [Fact]
        public void ScriptLastRowWithoutCloseTest()
        {
            string text = "declare a; a := 10";
            Assert.Throws<ParserException>(() => scriptParser.Parse(text));
        }

    }
}
