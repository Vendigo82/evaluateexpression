﻿using System;
using System.Linq;
using Xunit;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Script.Blocks.Tests
{
    
    public class DeclareBlockParserTests
    {
        ScriptParser scriptParser = new ScriptParser();
        DeclareBlockParser blockParser = new DeclareBlockParser();

        [Fact]
        public void DeclareParse_Test() {
            int index = 0;
            IParseResult result = null;

            DeclareBlock block = blockParser.Parse("a, b, c;", ref index, scriptParser, ref result) as DeclareBlock;
            Assert.NotNull(block);
            Assert.Equal(new string[] { "a", "b", "c" }, block.Names.ToArray());
        }

        [Fact]
        public void DeclareParse_ErrorTest()
        {
            int index = 0;
            IParseResult result = null;
            Assert.Throws<ParserException>(() => blockParser.Parse("a, b, a + c;", ref index, scriptParser, ref result));
        }

        [Fact]
        public void DeclareParse_LineNotCloseTest()
        {
            int index = 0;
            IParseResult result = null;
            Assert.Throws<ParserException>(() => blockParser.Parse("a, b, a + c", ref index, scriptParser, ref result));
        }
    }
}
