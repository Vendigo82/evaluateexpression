﻿using EvaluateExpression.Operands;
using System;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Script.Blocks.Tests
{
    
    public class DeclareBlockTests
    {
        [Fact]
        public void Declare_PerformTest()
        {
            DeclareBlock b1 = new DeclareBlock(new string[] { "A", "B" });
            VarNameResolver nr = new VarNameResolver();
            b1.Perform(nr);

            Assert.NotNull(nr.Resolve("A"));
            Assert.NotNull(nr.Resolve("B"));

            DeclareBlock b2 = new DeclareBlock(new string[] { "A" });
            Assert.Throws<DublicateDeclareVariableException>(() => b2.Perform(nr));
        }
    }
}
