﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using EvaluateExpression.Values;
using EvaluateExpression.Exceptions;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Script.Blocks.Tests
{
    
    public class ForeachBlockTests
    {
        private class SaverScriptBlock : IScriptBlock
        {
            public string Name { get; }
            public List<IValue> Values { get; } = new List<IValue>();

            public SaverScriptBlock(string name) {
                Name = name;
            }

            public void Perform(INameResolver context) {
                Values.Add(context.Resolve(Name));
            }
        }

        [Fact]
        public void ForeachTest() {
            int[] array = new int[] { 4, 7, 0 };
            SaverScriptBlock block = new SaverScriptBlock("iter");
            ForeachBlock fe = new ForeachBlock(
                block.Name,
                new ArrayValue(array.Select(i => i.ToValue()).ToArray()),
                block);
            VarNameResolver nr = new VarNameResolver();
            fe.Perform(nr);

            Assert.Equal(array, block.Values.Select(v => v.AsInt).ToArray());
            Assert.NotNull(nr.Resolve(fe.LocalName));
        }

        [Fact]
        public void Foreach_NotIterableError()
        {
            SaverScriptBlock block = new SaverScriptBlock("iter");
            ForeachBlock fe = new ForeachBlock(
                block.Name,
                10.ToValue(),
                block);
            VarNameResolver nr = new VarNameResolver();
            Assert.Throws<EvalCastToArrayException>(() => fe.Perform(nr));
        }
    }
}
