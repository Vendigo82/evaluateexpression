﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using FluentAssertions;

namespace EvaluateExpression.Tests
{
    public class PropertyNavigationExtensionsTests
    {
        [Fact]
        public void Container_PropertyRequired_NotFound_Test()
        {
            // setup
            Mock<IPropertyNavigation> fakeValue = new Mock<IPropertyNavigation>();
            fakeValue.Setup(f => f.Property(It.IsAny<string>())).Returns((IValue)null);

            // action
            Action action = () => fakeValue.Object.PropertyRequired("xxx");

            // asserts
            var e = action.Should().ThrowExactly<Exceptions.EvalValueNotFoundException>().Which;
            e.Name.Should().Be("xxx");
            e.Value.Should().BeSameAs(fakeValue.Object);
        }

        [Fact]
        public void Container_PropertyRequired_ExistsValue_Test()
        {
            // setup
            Mock<IPropertyNavigation> fakeValue = new Mock<IPropertyNavigation>();
            var value = new Mock<IValue>().Object;
            fakeValue.Setup(f => f.Property("xxx")).Returns(value);

            // action
            var result = fakeValue.Object.PropertyRequired("xxx");

            // asserts
            result.Should().BeSameAs(value);
        }

        [Fact]
        public void Value_PropertyRequired_NotFound_Test()
        {
            // setup
            Mock<IValue> fakeValue = new Mock<IValue>();
            fakeValue.Setup(f => f.Property(It.IsAny<string>())).Returns((IValue)null);

            // action
            Action action = () => fakeValue.Object.PropertyRequired("xxx");

            // asserts
            var e = action.Should().ThrowExactly<Exceptions.EvalComplexSubValueNotFound>().Which;
            e.Name.Should().Be("xxx");
            e.Value.Should().BeSameAs(fakeValue.Object);
        }

        [Fact]
        public void Value_PropertyRequired_ExistsValue_Test()
        {
            // setup
            Mock<IValue> fakeValue = new Mock<IValue>();
            var value = new Mock<IValue>().Object;
            fakeValue.Setup(f => f.Property("xxx")).Returns(value);

            // action
            var result = fakeValue.Object.PropertyRequired("xxx");

            // asserts
            result.Should().BeSameAs(value);
        }
    }
}
