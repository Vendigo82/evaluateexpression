﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.Array.Tests
{
    public static class ArrayHelpers
    {
        public static void ArrayLazyTest(IFunctionSignature f, IOperand[] args)
        {
            //check all operations will fail
            Assert.Throws<NotImplementedException>(() => (f.Perform(args, null) as IComplexValue).GetSubValue("Count"));

            IValue result = f.Perform(args, null);
            Assert.NotNull(result);
        }
    }
}
