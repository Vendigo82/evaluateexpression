﻿using System;
using System.Linq;
using Xunit;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Functions.Array.Tests
{
    using System.Collections.Generic;
    using Values;

    
    public class ArrayFunctionsTests
    {
        [Fact]
        public void ArrayFirstOrNullTest() {
            IValue array = new ArrayValue(new int[] { 10, 30 }.Select(i => i.ToValue()).ToArray());
            Assert.Equal(10, ArrayFunctions.FirstOrNull(array).AsInt);
            Assert.True(ArrayFunctions.FirstOrNull(new ArrayValue(new IValue[] { })).IsNull);
            Assert.Equal(10, ArrayFunctions.FirstOrNull(10.ToValue()).AsInt);
        }

        [Fact]
        public void ArrayFirstOrValueTest() {
            IValue array = new ArrayValue(new int[] { 10, 30 }.Select(i => i.ToValue()).ToArray());
            Assert.Equal(10, ArrayFunctions.FirstOrValue(array, 5.ToValue()).AsInt);
            Assert.Equal(5, ArrayFunctions.FirstOrValue(new ArrayValue(new IValue[] { }), 5.ToValue()).AsInt);
            Assert.Equal(10, ArrayFunctions.FirstOrValue(10.ToValue(), 5.ToValue()).AsInt);
            Assert.Equal(5, ArrayFunctions.FirstOrValue(NullValue.Null, 5.ToValue()).AsInt);
        }

        [Fact]
        public void ArrayContainsTest() {
            IValue array = new ArrayValue(new int[] { 10, 30 }.Select(i => i.ToValue()).ToArray());
            Assert.True(ArrayFunctions.Contains(array, 10.ToValue()).AsBool);
            Assert.True(ArrayFunctions.Contains(array, 30.ToValue()).AsBool);
            Assert.False(ArrayFunctions.Contains(array, 15.ToValue()).AsBool);

            array = new ArrayValue(new string[] { "abc", "xyz" }.Select(i => i.ToValue()).ToArray());
            Assert.True(ArrayFunctions.Contains(array, "abc".ToValue()).AsBool);
            Assert.False(ArrayFunctions.Contains(array, "ddf".ToValue()).AsBool);

            Assert.True(ArrayFunctions.Contains("abc".ToValue(), "abc".ToValue()).AsBool);
            Assert.False(ArrayFunctions.Contains("abc1".ToValue(), "abc".ToValue()).AsBool);
        }

        [Fact]
        public void ArrayIntersectTest() {
            IValue array1 = new ArrayValue(new int[] { 10, 30, 50 }.Select(i => i.ToValue()).ToArray());
            IValue array2 = new ArrayValue(new int[] { 30, 50, 80 }.Select(i => i.ToValue()).ToArray());
            Assert.Equal(
                new int[] { 30, 50 },
                (ArrayFunctions.Intersect(array1, array2) as IIndexableValue).Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ArrayIsEmptyTest() {
            IValue array1 = new ArrayValue(new int[] { 10, 30, 50 }.Select(i => i.ToValue()).ToArray());
            IValue array2 = new ArrayValue(new int[] {}.Select(i => i.ToValue()).ToArray());
            ValueAssert.CheckBool(false, ArrayFunctions.IsEmpty(array1));
            ValueAssert.CheckBool(true, ArrayFunctions.IsNotEmpty(array1));

            ValueAssert.CheckBool(true, ArrayFunctions.IsEmpty(array2));
            ValueAssert.CheckBool(false, ArrayFunctions.IsNotEmpty(array2));
        }

        [Fact]
        public void ToArrayTest() {
            IValue array1 = new ArrayValue(new int[] { 5, 10, 15 }.Select(i => i.ToValue()).ToArray());
            Assert.Same(array1, ArrayFunctions.ToArray(array1));

            //function X >= 10
            IValue enumValue = new ArrayFilterFunction().Perform(ArrayFilterFunctionTests.GetArguments(array1), null);
            Assert.IsAssignableFrom<ArrayPatternValue>(enumValue);
            Assert.False((enumValue as ArrayPatternValue).Array.IsSolid);

            IValue arrValue = ArrayFunctions.ToArray(enumValue);
            Assert.IsAssignableFrom<ArrayPatternValue>(arrValue);
            Assert.True((arrValue as ArrayPatternValue).Array.IsSolid);
            Assert.Equal(
                new int[] { 10, 15 },
                (arrValue as IIndexableValue).Select(v => v.AsInt).ToArray()
                );
        }

        [Theory]
        [InlineData(new[] { 1, 2, 3, 4 }, 0)]
        [InlineData(new[] { 1, 2, 3, 4 }, 1)]
        [InlineData(new[] { 1, 2, 3, 4 }, 2)]
        [InlineData(new[] { 1, 2, 3, 4 }, 3)]
        [InlineData(new[] { 1, 2, 3, 4 }, 4)]
        [InlineData(new[] { 1, 2, 3, 4 }, 5)]
        public void Skip_Test(int[] source, int cnt)
        {
            var result = ArrayFunctions.Skip(new ArrayValue(source.Select(i => i.ToValue()).ToArray()), cnt.ToValue());
            Assert.Equal(source.Skip(cnt), (result as IEnumerable<IValue>).Select(i => i.AsInt));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void Skip_NotArray_Test(int cnt)
        {
            var argument = "abc".ToValue();
            Assert.Throws<EvalCastToArrayException>(() => ArrayFunctions.Skip(argument, cnt.ToValue()));
        }
    }
}
