﻿using AutoFixture.Xunit2;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.Array.ArraySortFunctions
{
    public class ArraySortPrimitiveTests
    {
        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public void SortInt_Test(bool ascending, int[] values)
        {
            // setup
            var func = new ArraySortPrimitiveFunction() { Ascending = ascending };
            var value = new Values.ArrayValue(values.Select(i => i.ToValue()).ToArray());

            // action
            var result = func.Perform((IOperand)value, null);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsInt).Should().Equal(ascending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }

        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]

        public void SortDouble_Test(bool ascending, double[] values)
        {
            // setup
            var func = new ArraySortPrimitiveFunction() { Ascending = ascending };
            var value = new Values.ArrayValue(values.Select(i => i.ToValue()).ToArray());

            // action
            var result = func.Perform((IOperand)value, null);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsDouble).Should().Equal(ascending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }

        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]

        public void SortString_Test(bool ascending, string[] values)
        {
            // setup
            var func = new ArraySortPrimitiveFunction() { Ascending = ascending };
            var value = new Values.ArrayValue(values.Select(i => i.ToValue()).ToArray());

            // action
            var result = func.Perform((IOperand)value, null);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsString).Should().Equal(ascending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }

        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]

        public void SortDateTime_Test(bool ascending, DateTime[] values)
        {
            // setup
            var func = new ArraySortPrimitiveFunction() { Ascending = ascending };
            var value = new Values.ArrayValue(values.Select(i => i.ToValue()).ToArray());

            // action
            var result = func.Perform((IOperand)value, null);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsDateTime).Should().Equal(ascending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }
    }
}
