﻿using AutoFixture.Xunit2;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;
using EvaluateExpression.Operations.Strategy;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.Array.ArraySortFunctions
{
    public class ArraySortWithExpressionFunctionTests
    {
        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public void SortTest(bool ascending, int[] values)
        {
            // setup
            var array = new Values.ArrayValue(values
                .Select(i => new Values.StructValue(new[] { KeyValuePair.Create("fld", i.ToValue()) }))
                .ToArray());
            var func = new ArraySortWithExpressionFunction() { Ascending = ascending };

            // action
            var result = func.Perform((IOperand)array, 
                new VariableOperand("X"),
                new BinaryOperation(new Multiply(), new GetSubValueOperator(new VariableOperand("X"), new VariableOperand("fld")), (-1).ToValue()),
                null);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Cast<IComplexValue>().Select(i => i.PropertyRequired("fld").AsInt)
                .Should().Equal(ascending ? values.OrderByDescending(i => i) : values.OrderBy(i => i));
        }
    }
}
