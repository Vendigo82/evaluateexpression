﻿using AutoFixture.Xunit2;
using EvaluateExpression.Asserts;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.Array.ArraySortFunctions
{
    public class ArraySortByFieldNameTests
    {
        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public void SortTest(bool ascending, int[] values)
        {
            // setup
            var array = new Values.ArrayValue(values
                .Select(i => new Values.StructValue(new[] { KeyValuePair.Create("fld", i.ToValue()) }))
                .ToArray());
            var func = new ArraySortByFieldNameFunction() { Ascending = ascending };

            // action
            var result = func.Perform((IOperand)array, "fld".ToValue(), null);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Cast<IComplexValue>().Select(i => i.PropertyRequired("fld").AsInt)
                .Should().Equal(ascending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }
    }
}
