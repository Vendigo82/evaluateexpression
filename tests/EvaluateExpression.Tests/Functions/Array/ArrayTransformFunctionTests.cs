﻿using System;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Functions.Array.Tests
{
    using Values;
    using Operands;
    using Operations;
    using Operations.Strategy;

    
    public class ArrayTransformFunctionTests
    {
        private IOperand[] GetArguments(IValue[] values) {
            return new IOperand[] {
                new ArrayValue(values),
                new VariableOperand("X"),
                new BinaryOperation(new Multiply(), new VariableOperand("X"), 2.ToValue())
            };
        }

        [Fact]
        public void ArrayTransformFunctionTest() {
            int[] iarray = new int[] { 5, 10, 23, 8 };

            ArrayTransformFunction f = new ArrayTransformFunction();
            IValue result = f.Perform(GetArguments(iarray.Select(i => i.ToValue()).ToArray()), null);

            Assert.NotNull(result);
            Assert.IsAssignableFrom<IIndexableValue>(result);

            IIndexableValue resArray = result as IIndexableValue;
            Assert.Equal(4, resArray.Count());
            Assert.Equal(10, resArray[0.ToValue()].AsInt);
            Assert.Equal(20, resArray[1.ToValue()].AsInt);
            Assert.Equal(new int[] { 10, 20, 46, 16 }, resArray.Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ArrayTransformLazyTest() {
            ArrayHelpers.ArrayLazyTest(new ArrayTransformFunction(), GetArguments(new IValue[] { new FailValue(), new FailValue() }));
        }
    }
}
