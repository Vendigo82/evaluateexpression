﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using EvaluateExpression;
using EvaluateExpression.Values;
using EvaluateExpression.Functions;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;

namespace EvaluateExpression.Functions.Array.Tests
{
    
    public class ArraySumFunctionTests
    {
        private class TestStrategy : SpecialValueBase, IValue, IArrayFunctionStrategy
        {
            public LinkedList<IValue> Items { get; } = new LinkedList<IValue>();

            public IValue Result => this;

            public override object Value => throw new NotImplementedException();

            public string Description => "";

            public void Eval(IValue value) {
                Items.AddLast(value);
            }

            public IValue Eval(INameResolver context) => this;

            public IValue Property(string name) => null;
        }

        [Fact]
        public void ArrayTemplateFunction_ArgCountTest() {
            IFunctionSignature f = new ArrayTemplateFunction<TestStrategy>();
            Assert.Equal(1, f.ArgCount);

            IFunctionSignature fex = new ArrayTemplateFunctionEx<TestStrategy>();
            Assert.Equal(3, fex.ArgCount);
        }

        [Fact]
        public void ArrayTemplateFunctionTest() {
            int []array = new int[] { 1, 3, 5 };

            IFunctionSignature f = new ArrayTemplateFunction<TestStrategy>();
            IValue result = f.Perform(new IOperand[] { new ArrayValue(array.Select(i => i.ToValue()).ToArray()) }, null);

            Assert.Equal(array, (result as TestStrategy).Items.Select(v => v.AsInt).ToArray());
        }

        private IValue GetDictValue(int i, double d) {
            StructValue value = new StructValue();
            value.Add("field", i.ToValue());
            value.Add("val", d.ToValue());
            return value;
        }

        [Fact]
        public void ArrayTemplateFunctionExTest() {
            ArrayValue value = new ArrayValue(new IValue[] { GetDictValue(1, 1.5), GetDictValue(3, 3.5), GetDictValue(5, 5) });
            IFunctionSignature f = new ArrayTemplateFunctionEx<TestStrategy>();

            IOperand[] operands = new IOperand[] {
                value,
                new VariableOperand("X"),
                new GetSubValueOperator(new VariableOperand("X"), new VariableOperand("field"))
            };

            IValue result = f.Perform(operands, null);

            Assert.Equal(
                (value as IIndexableValue).Select(v => (v as IComplexValue).GetSubValue("field").AsInt).ToArray(),
                (result as TestStrategy).Items.Select(v => v.AsInt).ToArray());            
        }
    }
}
