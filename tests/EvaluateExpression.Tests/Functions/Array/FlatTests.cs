﻿using AutoFixture.Xunit2;
using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.Array.Tests
{
    public class FlatTests
    {
        [Theory, AutoData]
        public void FlatTest(IEnumerable<int> array1, IEnumerable<int> array2)
        {
            // setup
            var arrayValue = new ArrayValue(new IValue[] {
                new ArrayValue(array1.Select(i => i.ToValue()).ToArray()),
                new ArrayValue(array2.Select(i => i.ToValue()).ToArray())
            });

            // action
            var result = ArrayFunctions.Flat(arrayValue);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsInt).Should().Equal(array1.Concat(array2));
        }

        [Theory, AutoData]
        public void FlatWithNotArrayItemsTest(IEnumerable<int> array1, int item2)
        {
            // setup
            var arrayValue = new ArrayValue(new IValue[] {
                new ArrayValue(array1.Select(i => i.ToValue()).ToArray()),
                item2.ToValue()
            });

            // action
            var result = ArrayFunctions.Flat(arrayValue);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsInt).Should().Equal(array1.Concat(new[] { item2 }));
        }

        [Theory, AutoData]
        public void FlatAlreadyPlainArrayTest(IEnumerable<int> array1)
        {
            // setup
            var arrayValue = new ArrayValue(array1.Select(i => i.ToValue()).ToArray());

            // action
            var result = ArrayFunctions.Flat(arrayValue);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsInt).Should().Equal(array1);
        }
    }
}
