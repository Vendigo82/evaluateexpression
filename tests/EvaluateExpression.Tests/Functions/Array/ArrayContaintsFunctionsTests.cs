﻿using System;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;
using EvaluateExpression.Operations.Strategy;
using EvaluateExpression.Values;
using Xunit;

using EvaluateExpression.Values.Tests;

namespace EvaluateExpression.Functions.Array.Tests
{
    
    public class ArrayContaintsFunctionsTests
    {
        private IOperand[] GetOperands(IValue[] values) {
            return new IOperand[] {
                new ArrayValue(values),
                new VariableOperand("X"),
                new BinaryOperation(new Equal(), new VariableOperand("X"), 5.ToValue())
            };
        }


        [Fact]
        public void ArrayContainsTest()
        {
            ArrayContaintsFunction f = new ArrayContaintsFunction();

            ValueAssert.CheckBool(true, f.Perform(GetOperands(new IValue[] { 5.ToValue(), 6.ToValue() }), null));
            ValueAssert.CheckBool(false, f.Perform(GetOperands(new IValue[] { 7.ToValue(), 6.ToValue() }), null));
            ValueAssert.CheckBool(true, f.Perform(GetOperands(new IValue[] { 5.ToValue(), new FailValue() }), null));
            Assert.Throws<NotImplementedException>(()
                => ValueAssert.CheckBool(true, f.Perform(GetOperands(new IValue[] { 6.ToValue(), new FailValue() }), null)));
        }
    }
}
