﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using EvaluateExpression;
using EvaluateExpression.Values;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;

namespace EvaluateExpression.Functions.Array.Tests
{
    public class ArraySumStrategyTests
    {
        [Fact]
        public void ArraySumFunctionIntTest() {
            ArrayValue value = new ArrayValue(new int[] { 1, 3, 5 }.Select(i => i.ToValue()).ToArray());

            ArraySumStrategy s = new ArraySumStrategy();
            foreach (IValue val in value)
                s.Eval(val);

            Assert.Equal(EvaluateExpression.ValueType.Integer, s.Result.Type);
            Assert.Equal(9, s.Result.AsInt);
        }

        [Fact]
        public void ArraySumFunction_DoubleTest() {
            ArrayValue value = new ArrayValue(new double[] { 1.5, 3.5, 5 }.Select(i => i.ToValue()).ToArray());

            ArraySumStrategy s = new ArraySumStrategy();
            foreach (IValue val in value)
                s.Eval(val);

            Assert.Equal(EvaluateExpression.ValueType.Decimal, s.Result.Type);
            Assert.Equal(10, s.Result.AsDouble);
        }

        [Fact]
        public void ArraySumFunction_MixTest() {
            ArrayValue value = new ArrayValue(new IValue[] { 1.5.ToValue(), 3.5.ToValue(), 5.ToValue() });

            ArraySumStrategy s = new ArraySumStrategy();
            foreach (IValue val in value)
                s.Eval(val);

            Assert.Equal(EvaluateExpression.ValueType.Decimal, s.Result.Type);
            Assert.Equal(10, s.Result.AsDouble);
        }

        [Fact]
        public void ArrayMinStrategyTest() {
            ArrayValue value = new ArrayValue(new int[] { 8, 1, 3, 10, 5 }.Select(i => i.ToValue()).ToArray());
            ArrayMinStrategy smin = new ArrayMinStrategy();
            ArrayMaxStrategy smax = new ArrayMaxStrategy();

            Assert.True(smin.Result.IsNull);
            Assert.True(smax.Result.IsNull);

            foreach (IValue val in value) {
                smin.Eval(val);
                smax.Eval(val);
            }

            Assert.Equal(1, smin.Result.AsInt);
            Assert.Equal(10, smax.Result.AsInt);
        }
    }
}
