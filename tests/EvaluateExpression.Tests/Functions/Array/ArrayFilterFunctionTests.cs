﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Functions.Array.Tests
{
    using Values;
    using Operands;
    using Operations;
    using Operations.Strategy;

    
    public class ArrayFilterFunctionTests
    {
        static public IOperand[] GetArguments(IValue[] values) {
            return GetArguments(new ArrayValue(values));
        }

        static public IOperand[] GetArguments(IValue values) {
            return new IOperand[] {
                values,
                new VariableOperand("X"),
                new BinaryOperation(new GreaterOrEqual(), new VariableOperand("X"), 10.ToValue())
            };
        }

        private IOperand[] GetArgumentsEx(IValue[] values) {
            return new IOperand[] {
                new ArrayValue(values),
                new VariableOperand("X"),
                new BinaryOperation(new GreaterOrEqual(), new VariableOperand("X"), 10.ToValue()),
                new BinaryOperation(new Multiply(), new VariableOperand("X"), 2.ToValue())
            };
        }

        [Fact]
        public void ArrayFilterTest() {
            int[] iarray = new int[] { 5, 10, 23, 8 };
            ArrayFilterFunction f = new ArrayFilterFunction();
            IValue result = f.Perform(GetArguments(iarray.Select(i => i.ToValue()).ToArray()), null);

            Assert.NotNull(result);
            Assert.IsAssignableFrom<IIndexableValue>(result);

            IIndexableValue resArray = result as IIndexableValue;
            Assert.Equal(2, resArray.Count());
            Assert.Equal(10, resArray[0.ToValue()].AsInt);
            Assert.Equal(23, resArray[1.ToValue()].AsInt);
            Assert.Equal(new int[] { 10, 23 }, resArray.Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ArrayFilterExTest() {
            int[] iarray = new int[] { 5, 10, 23, 8 };

            ArrayFilterExFunction f = new ArrayFilterExFunction();
            IValue result = f.Perform(GetArgumentsEx(iarray.Select(i => i.ToValue()).ToArray()), null);

            Assert.NotNull(result);
            Assert.IsAssignableFrom<IIndexableValue>(result);

            IIndexableValue resArray = result as IIndexableValue;
            Assert.Equal(2, resArray.Count());
            Assert.Equal(10 * 2, resArray[0.ToValue()].AsInt);
            Assert.Equal(23 * 2, resArray[1.ToValue()].AsInt);
            Assert.Equal(new int[] { 10 * 2, 23 * 2 }, resArray.Select(v => v.AsInt).ToArray());
        }

        [Fact]
        public void ArrayFilterLazyTest() {
            ArrayHelpers.ArrayLazyTest(new ArrayFilterFunction(), GetArguments(new IValue[] { 5.ToValue(), new FailValue() }));
        }

        [Fact]
        public void ArrayFilterExLazyTest() {
            ArrayHelpers.ArrayLazyTest(new ArrayFilterExFunction(), GetArgumentsEx(new IValue[] { 5.ToValue(), new FailValue() }));
        }

        [Fact]
        public void ArrayFilter_EvalCounTest() {
            ValueCounterWrapper[] array = new ValueCounterWrapper[] { new ValueCounterWrapper(10.ToValue()), new ValueCounterWrapper(20.ToValue()) };
            ArrayFilterFunction f = new ArrayFilterFunction();

            IValue val = f.Perform(GetArguments(array), null);
            Assert.Equal(new int[] { 0, 0 }, array.Select(v => v.Count).ToArray());

            ArrayFunctions.IsEmpty(val);
            Assert.Equal(new int[] { 1, 0 }, array.Select(v => v.Count).ToArray());

            ArrayFunctions.IsEmpty(val);
            Assert.Equal(new int[] { 2, 0 }, array.Select(v => v.Count).ToArray());
        }
    }
}
