﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Functions.Tests
{
    /// <summary>
    /// Summary description for IFunctionSignatureExtentionsTests
    /// </summary>
    
    public class IFunctionSignatureExtentionsTests
    {
        private IValue[] GetOperandsList() {
            return new int[] { 1, 3, 5 }.Select(i => i.ToValue()).ToArray();
        }

        [Fact]
        public void IFunctionSignatureExtension_GetFirstNext()
        {
            IEnumerator<IOperand> iter;
            IFunctionSignature func = new FunctionSignatureZeroArg(null);

            Assert.Equal(1, func.GetFirst(GetOperandsList(), out iter).Eval(null).AsInt);
            Assert.Equal(3, func.GetNext(iter).Eval(null).AsInt);
            Assert.Equal(5, func.GetNext(iter).Eval(null).AsInt);
            Assert.Throws<ArgumentException>(() => func.GetNext(iter));

            Assert.Equal(1, func.GetFirst(GetOperandsList(), out iter, null).AsInt);
            Assert.Equal(3, func.GetNext(iter, null).AsInt);
            Assert.Equal(5, func.GetNext(iter, null).AsInt);
            Assert.Throws<ArgumentException>(() => func.GetNext(iter, null));

            Assert.Equal(1, func.GetSingle(GetOperandsList(), null).AsInt);
        }
    }
}
