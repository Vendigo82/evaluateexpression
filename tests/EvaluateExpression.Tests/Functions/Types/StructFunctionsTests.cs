﻿using System;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Functions.Types.Tests
{
    
    public class StructFunctionsTests
    {
        [Fact]
        public void MakeStructFromContextFunction_Test() {
            MakeStructFromContextFunction f = new MakeStructFromContextFunction();
            Assert.Equal(0, f.ArgCount);

            ConstNameResolver nr = new ConstNameResolver() {
                { "a", 1.ToValue() },
                { "b", 2.ToValue() },
                { "c", 3.ToValue() }
            };

            IValue val = f.Perform(nr);
            Assert.IsAssignableFrom<IComplexValue>(val);
            IComplexValue cval = (IComplexValue)val;
            Assert.Equal(nr.Count(), cval.Count());
            foreach (var pair in nr) {
                Assert.Equal(pair.Value.AsInt, cval.GetSubValue(pair.Key).AsInt);
            }

            int cnt = cval.Count();
            nr.Add("d", 4.ToValue());
            Assert.NotEqual(nr.Count(), cval.Count());
            Assert.Equal(cnt, cval.Count());
        }
    }
}
