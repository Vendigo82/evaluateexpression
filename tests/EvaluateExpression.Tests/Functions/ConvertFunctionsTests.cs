﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using System.Globalization;

using EvaluateExpression.Values;
using EvaluateExpression.Values.Tests;

namespace EvaluateExpression.Functions.Tests
{
    /// <summary>
    /// Summary description for ConvertFunctionsTests
    /// </summary>
    
    public class ConvertFunctionsTests
    {
        [Fact]
        public void ToStringFunctionTest() {
            ToStringFunction f = new ToStringFunction(CultureInfo.InvariantCulture);

            Assert.Equal(1, f.ArgCount);
            IValue stringVal = "qwerty".ToValue();
            Assert.Same(stringVal, f.Perform(stringVal, null));

            ValueAssert.CheckString("12", f.Perform(new IntValue(12), null));
            ValueAssert.CheckString("12", f.Perform(new DoubleValue(12), null));
            ValueAssert.CheckString("1", f.Perform(BoolValue.True, null));
            ValueAssert.CheckString("0", f.Perform(BoolValue.False, null));
        }

        [Fact]
        public void ToIntFunctionTest() {
            ToIntFunction f = new ToIntFunction();

            Assert.Equal(1, f.ArgCount);
            IValue intVal = 10.ToValue();
            Assert.Same(intVal, f.Perform(intVal, null));

            ValueAssert.CheckInt(10, f.Perform(new StringValue("10"), null));
            ValueAssert.CheckInt(10, f.Perform(new DoubleValue(10.53), null));
            ValueAssert.CheckInt(10, f.Perform(new DoubleValue(10), null));
            ValueAssert.CheckInt(1, f.Perform(BoolValue.True, null));
            ValueAssert.CheckInt(0, f.Perform(BoolValue.False, null));
        }

        [Fact]
        public void ToDoubleFunctionTest() {
            ToDoubleFunction f = new ToDoubleFunction(CultureInfo.InvariantCulture);

            Assert.Equal(1, f.ArgCount);
            IValue doubleVal = 10.0.ToValue();
            Assert.Same(doubleVal, f.Perform(doubleVal, null));

            ValueAssert.CheckDouble(10, f.Perform(new IntValue(10), null));
            ValueAssert.CheckDouble(10.5, f.Perform(new StringValue("10.5"), null));
            ValueAssert.CheckDouble(1, f.Perform(BoolValue.True, null));
            ValueAssert.CheckDouble(0, f.Perform(BoolValue.False, null));
        }

        [Fact]
        public void ToBoolFunctionTest() {
            ToBoolFunction f = new ToBoolFunction();

            Assert.Equal(1, f.ArgCount);
            IValue bval = BoolValue.True;
            Assert.Same(bval, f.Perform(bval, null));

            ValueAssert.CheckBool(true, f.Perform(new StringValue("1"), null));
            ValueAssert.CheckBool(false, f.Perform(new StringValue("0"), null));
            ValueAssert.CheckBool(true, f.Perform(new IntValue(1), null));
            ValueAssert.CheckBool(true, f.Perform(new DoubleValue(1), null));

            ValueAssert.CheckBool(true, f.Perform(new StringValue("true"), null));
            ValueAssert.CheckBool(true, f.Perform(new StringValue("True"), null));
            ValueAssert.CheckBool(false, f.Perform(new StringValue("false"), null));
            ValueAssert.CheckBool(false, f.Perform(new StringValue("False"), null));
        }

        [Fact]
        public void ToDateTimeFunctionTest() {
            ToDateTimeFunction f = new ToDateTimeFunction(CultureInfo.InvariantCulture);

            Assert.Equal(1, f.ArgCount);
            IValue val = new DateTimeValue(DateTime.Now);
            Assert.Same(val, f.Perform(val, null));

            DateTime n = DateTime.Now;
            ValueAssert.CheckDateTime(n, f.Perform(new DateTimeValue(n), null));
            ValueAssert.CheckDateTime(new DateTime(2012, 05, 10), f.Perform(new StringValue("2012-05-10"), null));
            ValueAssert.CheckDateTime(new DateTime(2008, 12, 28, 1, 2, 3), f.Perform(new StringValue("2008-12-28T01:02:03"), null));
            ValueAssert.CheckDateTime(new DateTime(2018, 09, 10, 0, 22, 34), f.Perform(new StringValue("2018-09-10 00:22:34"), null));
        }

        [Fact]
        public void ToDateTime2FunctionTest() {
            ToDateTime2Function f = new ToDateTime2Function(CultureInfo.InvariantCulture);

            Assert.Equal(2, f.ArgCount);
            IValue val = new DateTimeValue(DateTime.Now);
            Assert.Same(val, f.Perform(val, "".ToValue(), null));

            DateTime n = DateTime.Now;
            ValueAssert.CheckDateTime(n, f.Perform(new DateTimeValue(n), "".ToValue(), null));
            ValueAssert.CheckDateTime(new DateTime(2012, 05, 10), f.Perform(new StringValue("2012-05-10"), new StringValue("yyyy-MM-dd"), null));
            ValueAssert.CheckDateTime(new DateTime(2012, 05, 10), f.Perform(new StringValue("2012-10-05"), new StringValue("yyyy-dd-MM"), null));
            ValueAssert.CheckDateTime(new DateTime(2012, 12, 05), f.Perform(new StringValue("05.12.2012"), new StringValue("dd.MM.yyyy"), null));

            DateTime dt = new DateTime(2018, 09, 10, 0, 22, 34, 120);
            IValue res = f.Perform(new StringValue("2018-09-10 00:22:34:12"), new StringValue("yyyy-MM-dd HH:mm:ss:FF"), null);
            ValueAssert.CheckDateTime(dt, res);

            ValueAssert.CheckDateTime(new DateTime(2018, 03, 13, 14, 28, 31, 0),
                f.Perform(new StringValue("13.03.18:14:28:31:0000"), new StringValue("dd.MM.yy:HH:mm:ss:FFFF"), null));
            ValueAssert.CheckDateTime(new DateTime(2018, 10, 22, 20, 44, 01, 437),
                f.Perform(new StringValue("2018-10-22T20:44:01.437"), new StringValue("yyyy-MM-ddTHH:mm:ss.FFF"), null));            
        }

        [Fact]
        public void DateToStringFunctionsTest() {
            ToString2Function f = new ToString2Function(CultureInfo.InvariantCulture);
            DateTime dt = new DateTime(2012, 10, 25, 10, 11, 12);
            Assert.Equal("20121025101112", f.Perform(dt.ToValue(), new StringValue("yyyyMMddhhmmss"), null).AsString);
        }
    }
}
