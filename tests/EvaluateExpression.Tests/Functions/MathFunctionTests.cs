﻿using System;
using System.Linq;
using Xunit;

namespace EvaluateExpression.Functions.Tests
{
    
    public class MathFunctionTests
    {
        [Fact]
        public void AbsTest() {
            ValueAssert.CheckInt(10, MathFunction.Abs(10.ToValue()));
            ValueAssert.CheckInt(10, MathFunction.Abs((-10).ToValue()));
            ValueAssert.CheckDouble(10.0, MathFunction.Abs((10.0).ToValue()));
            ValueAssert.CheckDouble(10.0, MathFunction.Abs((-10.0).ToValue()));
        }

        [Fact]
        public void Function_Abs_Test() {
            IFunctionSignature f = MathFunction.GetFunctions().Single(i => i.Key == "Abs").Value;
            Assert.Equal(1, f.ArgCount);
        }
    }
}
