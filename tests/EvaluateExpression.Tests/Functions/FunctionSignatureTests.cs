﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Functions.Tests {

    using Operands;
    using Values;

    /// <summary>
    /// Summary description for FunctionSignatureTests
    /// </summary>
    
    public class FunctionSignatureTests {
        private static IValue F1ArgMethod (IValue arg1) { 
            return new DoubleValue(-arg1.AsDouble);
        }

        private static IValue F2ArgMethod(IValue arg1, IValue arg2) {
            return new DoubleValue(arg1.AsDouble + arg2.AsDouble);
        }

        private static IValue F3ArgMethod(IValue arg1, IValue arg2, IValue arg3) {
            return new DoubleValue((arg1.AsDouble + arg2.AsDouble) * arg3.AsDouble);
        }

        public static IValue VarArgSum(IEnumerable<IValue> args) {
            return new DoubleValue(args.Select(v => v.AsDouble).Sum());
        }

        [Fact]
        public void FunctionSignature1ArgTest() {
            FunctionSignature1Arg f = new FunctionSignature1Arg(F1ArgMethod);
            LinkedList<IOperand> opers = new LinkedList<IOperand>();
            opers.AddLast(new StaticOperand(10));
            Assert.Equal(-10, f.Perform(opers, null).AsDouble);
        }

        [Fact]
        public void FunctionSignature2ArgTest() {
            FunctionSignature2Arg f = new FunctionSignature2Arg(F2ArgMethod);
            LinkedList<IOperand> opers = new LinkedList<IOperand>();
            opers.AddLast(new StaticOperand(10));
            opers.AddLast(new StaticOperand(5));
            Assert.Equal(15, f.Perform(opers, null).AsDouble);
        }

        [Fact]
        public void FunctionSignature3ArgTest() {
            FunctionSignature3Arg f = new FunctionSignature3Arg(F3ArgMethod);
            LinkedList<IOperand> opers = new LinkedList<IOperand>();
            opers.AddLast(new StaticOperand(10));
            opers.AddLast(new StaticOperand(5));
            opers.AddLast(new StaticOperand(2));
            Assert.Equal(30, f.Perform(opers, null).AsDouble);
        }

        [Fact]
        public void FunctionSignatureVArgTest() {
            FunctionSignatureVarArg f = new FunctionSignatureVarArg(VarArgSum);
            LinkedList<IOperand> opers = new LinkedList<IOperand>();
            opers.AddLast(new StaticOperand(10));
            opers.AddLast(new StaticOperand(5));
            opers.AddLast(new StaticOperand(2));
            Assert.Equal(17, f.Perform(opers, null).AsDouble);
        }

        [Fact]
        public void SimpleFunctionFactoryTest() {
            SimpleFunctionFactory factory = new();
            factory.Add("func", new FunctionSignature2Arg(F2ArgMethod));
            factory.Add("func", new FunctionSignature1Arg(F1ArgMethod));
            factory.Add("func", new FunctionSignature3Arg(F3ArgMethod));
            factory.Add("sum", new FunctionSignature1Arg(F1ArgMethod));
            factory.Add("sum", new FunctionSignatureVarArg(VarArgSum));

            List<IOperand> opers = new() {
                new StaticOperand(10)
            };
            Assert.Equal(-10, factory.GetFunction("func", opers).Eval(null).AsDouble);

            opers = new List<IOperand> {
                new StaticOperand(10),
                new StaticOperand(5)
            };
            Assert.Equal(15, factory.GetFunction("func", opers).Eval(null).AsDouble);

            opers = new List<IOperand> {
                new StaticOperand(10),
                new StaticOperand(5),
                new StaticOperand(2)
            };
            Assert.Equal(30, factory.GetFunction("func", opers).Eval(null).AsDouble);

            opers = new List<IOperand> {
                new StaticOperand(10),
                new StaticOperand(5),
                new StaticOperand(2),
                new StaticOperand(2)
            };
            Assert.Throws<ArgumentException>(() => factory.GetFunction("func", opers));

            Assert.Equal(19, factory.GetFunction("sum", opers).Eval(null).AsDouble);

            opers = new List<IOperand> {
                new StaticOperand(10)
            };
            Assert.Equal(-10, factory.GetFunction("sum", opers).Eval(null).AsDouble);
        }
    }
}
