﻿using System;
using System.Linq;
using Xunit;

using EvaluateExpression.Parser;
using EvaluateExpression.Exceptions;
using EvaluateExpression.Values;
using FluentAssertions;

namespace EvaluateExpression.Functions.Tests
{
    
    public class StringFunctionsTests
    {
        [Fact]
        public void StringFunction_ParserTest() {
            IParser parser = Parser.Parser.CreateFull();
            Assert.Equal(3, parser.Parse("\"qwe\".Length").Eval(null).AsInt);
            Assert.Equal("qwe", parser.Parse("SubString(\"qwerty\", 0, 3)").Eval(null).AsString);
            Assert.Equal(1, parser.Parse("PosInString(\"qwerty\", \"wer\")").Eval(null).AsInt);
        }

        [Fact]
        public void LevensteinDistanceTest() {
            Assert.Equal(3, StringFunctions.LevenshteinDistance("kitten", "sitting"));
            Assert.Equal(0, StringFunctions.LevenshteinDistance("abc", "abc"));
            Assert.Equal(1, StringFunctions.LevenshteinDistance("some tezxt", "some text"));
            Assert.Equal(3, StringFunctions.LevenshteinDistance("qwerty", "asdrty"));
            Assert.Equal(1, StringFunctions.LevenshteinDistance("party", "part"));
        }

        [Fact]
        public void RemoveNotLegalTest() {
            RemoveNotLegalCheckSame("abc");
            RemoveNotLegalCheckSame("abczzz");
            RemoveNotLegalCheckSame("ABCXYZ");
            Assert.Equal("abc", StringFunctions.RemoveNotLegal("ab c", StringFunctions.IsLatin));
            Assert.Equal("abc", StringFunctions.RemoveNotLegal("кккabc", StringFunctions.IsLatin));
            Assert.Equal("abc", StringFunctions.RemoveNotLegal("!a!b!c!", StringFunctions.IsLatin));
        }

        [Fact]
        public void RemoveNotLegalValTest() {
            StringFunctions.RemoveNonLatinTunction f = new StringFunctions.RemoveNonLatinTunction(StringFunctions.IsLatin);
            IValue val = "abc".ToValue();
            Assert.Same(val, f.Perform(val, null));

            val = "abc1".ToValue();
            Assert.NotSame(val, f.Perform(val, null));
            Assert.Equal("abc", f.Perform(val, null).AsString);
        }

        private void RemoveNotLegalCheckSame(string arg) {
            Assert.Same(arg, StringFunctions.RemoveNotLegal(arg, StringFunctions.IsLatin));
        }

        [Fact]
        public void IsLatinTest() {
            Assert.True(StringFunctions.IsLatin('A'));
            Assert.True(StringFunctions.IsLatin('a'));
            Assert.True(StringFunctions.IsLatin('h'));
            Assert.True(StringFunctions.IsLatin('z'));
            Assert.True(StringFunctions.IsLatin('Z'));
            Assert.False(StringFunctions.IsLatin(' '));
            Assert.False(StringFunctions.IsLatin('0'));
            Assert.False(StringFunctions.IsLatin('1'));
            Assert.False(StringFunctions.IsLatin('9'));

            Assert.True(StringFunctions.IsLatinOrDigit('A'));
            Assert.True(StringFunctions.IsLatinOrDigit('a'));
            Assert.True(StringFunctions.IsLatinOrDigit('h'));
            Assert.True(StringFunctions.IsLatinOrDigit('z'));
            Assert.True(StringFunctions.IsLatinOrDigit('Z'));
            Assert.False(StringFunctions.IsLatinOrDigit(' '));            
            Assert.True(StringFunctions.IsLatinOrDigit('0'));
            Assert.True(StringFunctions.IsLatinOrDigit('1'));
            Assert.True(StringFunctions.IsLatinOrDigit('9'));
        }

        [Fact]
        public void StringFunction_SubStringTest()
        {
            Assert.Equal("qwe", StringFunctions.SubString("qwerty".ToValue(), 0.ToValue(), 3.ToValue()).AsString);
            Assert.Equal("we", StringFunctions.SubString("qwerty".ToValue(), 1.ToValue(), 2.ToValue()).AsString);
            Assert.Equal("ty", StringFunctions.SubString("qwerty".ToValue(), 4.ToValue(), 50.ToValue()).AsString);
            Assert.Equal("", StringFunctions.SubString("qwerty".ToValue(), 40.ToValue(), 50.ToValue()).AsString);
            Assert.Equal("q", StringFunctions.SubString("qwerty".ToValue(), 0.ToValue(), 1.ToValue()).AsString);
            Assert.Equal("", StringFunctions.SubString("qwerty".ToValue(), 0.ToValue(), 0.ToValue()).AsString);
            Assert.Equal("y", StringFunctions.SubString("qwerty".ToValue(), 5.ToValue(), 1.ToValue()).AsString);
            Assert.Equal("y", StringFunctions.SubString("qwerty".ToValue(), 5.ToValue(), 2.ToValue()).AsString);
            Assert.Equal("", StringFunctions.SubString("qwerty".ToValue(), 6.ToValue(), 1.ToValue()).AsString);
            Assert.Equal("", StringFunctions.SubString("qwerty".ToValue(), 6.ToValue(), 2.ToValue()).AsString);
            Assert.Throws<EvalIndexOutOfRangeException>(() => StringFunctions.SubString("qwerty".ToValue(), (-10).ToValue(), 2.ToValue()));
        }

        [Fact]
        public void StringFunction_PosInString() {
            Assert.Equal(0, StringFunctions.PosInString("qwerty".ToValue(), "qw".ToValue()).AsInt);
            Assert.Equal(0, StringFunctions.PosInString("qwerty".ToValue(), "qwerty".ToValue()).AsInt);
            Assert.Equal(2, StringFunctions.PosInString("qwerty".ToValue(), "er".ToValue()).AsInt);
            Assert.Equal(2, StringFunctions.PosInString("qwerty".ToValue(), "erty".ToValue()).AsInt);
            Assert.Equal(5, StringFunctions.PosInString("qwerty".ToValue(), "y".ToValue()).AsInt);
            Assert.Equal(-1, StringFunctions.PosInString("qwerty".ToValue(), "z".ToValue()).AsInt);
            Assert.Equal(0, StringFunctions.PosInString("qwerty".ToValue(), "".ToValue()).AsInt);
            Assert.Equal(0, StringFunctions.PosInString("".ToValue(), "".ToValue()).AsInt);
            Assert.Equal(-1, StringFunctions.PosInString("".ToValue(), "zz".ToValue()).AsInt);
        }

        [Theory]
        [InlineData("abc", " ", new[] { "abc" })]
        [InlineData("abc xyz nm", " ", new[] { "abc", "xyz", "nm" })]
        [InlineData("abc,xyz,nm", ",", new[] { "abc", "xyz", "nm" })]
        [InlineData("abc,xyz nm", ", ", new[] { "abc", "xyz", "nm" })]
        public void Split_Test(string str, string separators, string[] result)
        {
            var value = StringFunctions.Split(str.ToValue(), separators.ToValue());
            Assert.NotNull(value);
            Assert.IsAssignableFrom<IArrayValue>(value);
            IArrayValue array = (IArrayValue)value;
            Assert.Equal(result, array.Select(i => i.AsString));
        }

        [Theory]
        [InlineData(new string[] { "abc", "xyz", "nm" }, " ", "abc xyz nm")]
        [InlineData(new string[] { "abc", "xyz", "nm" }, ",", "abc,xyz,nm")]
        [InlineData(new string[] { "abc" }, ",", "abc")]
        public void ConcatStringsWithSeparator_Test(string[] array, string sep, string result)
        {
            var value = StringFunctions.ConcatStringsWithSeparator(
                new ArrayValue(array.Select(i => i.ToValue()).ToArray()), 
                sep.ToValue());
            Assert.Equal(result, value.AsString);
        }

        [Fact]
        public void ConcatStringsWithSeparator_NotArray_Test()
        {
            Assert.Throws<EvalCastToArrayException>(() 
                => StringFunctions.ConcatStringsWithSeparator("abc".ToValue(), " ".ToValue()));
        }

        [Theory]
        [InlineData("0", 3, "000")]
        [InlineData("0", 4, "0000")]
        [InlineData("abc", 2, "abcabc")]
        [InlineData("0", 1, "0")]
        [InlineData("0", 0, "")]
        [InlineData("0", -1, "")]
        public void Repeat(string value, int count, string expected)
        {
            // setup

            // action
            var result = StringFunctions.Repeat(value.ToValue(), count.ToValue()).AsString;

            // asserts
            result.Should().Be(expected);
        }
    }
}
