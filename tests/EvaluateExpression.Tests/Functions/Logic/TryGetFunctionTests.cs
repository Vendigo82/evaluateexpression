﻿using System;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;
using EvaluateExpression.Values;
using Xunit;

namespace EvaluateExpression.Functions.Logic.Tests
{
    
    public class TryGetFunctionTests
    {
        readonly TryGetFunction f = new TryGetFunction();
        readonly TryGet2Function f2 = new TryGet2Function();

        [Fact]
        public void TryGetFunctionTest()
        {
            ConstNameResolver nr = new ConstNameResolver("val", 10.ToValue());
            nr.Add("complex", new StructValue());
            
            Assert.Equal(10, f.Perform(new VariableOperand("val"), nr).AsInt);
            Assert.True(f.Perform(new VariableOperand("nothing"), nr).IsNull);

            Assert.False(f.Perform(new VariableOperand("complex"), nr).IsNull);
            Assert.True(f.Perform(new GetSubValueOperator(new VariableOperand("complex"), new VariableOperand("val")), nr).IsNull);
        }

        [Fact]
        public void TryGet2FunctionTest()
        {
            ConstNameResolver nr = new ConstNameResolver("val", 10.ToValue());
            nr.Add("complex", new StructValue());
            
            Assert.Equal(10, f2.Perform(new VariableOperand("val"), 1.ToValue(), nr).AsInt);
            Assert.Equal(1, f2.Perform(new VariableOperand("nothing"), 1.ToValue(), nr).AsInt);

            Assert.IsAssignableFrom<StructValue> (f2.Perform(new VariableOperand("complex"), 1.ToValue(), nr));
            Assert.Equal(1, f2.Perform(new GetSubValueOperator(new VariableOperand("complex"), new VariableOperand("val")), 1.ToValue(), nr).AsInt);
        }
    }
}
