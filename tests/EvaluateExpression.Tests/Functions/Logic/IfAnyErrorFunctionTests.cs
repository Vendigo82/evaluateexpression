﻿using System;
using Xunit;
using Moq;
using FluentAssertions;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;

namespace EvaluateExpression.Functions.Logic.Tests
{
    
    public class IfAnyErrorFunctionTests
    {
        readonly MockRepository mockRepository = new (MockBehavior.Default);
        readonly Mock<IIfAnyErrorListener> listenerMock = new();
        readonly Mock<IOperand> mainOperandMock;
        readonly Mock<IOperand> errorOperandMock;
        readonly INameResolver contextMock = Mock.Of<INameResolver>();
        readonly IfAnyErrorFunction target;

        public IfAnyErrorFunctionTests()
        {
            listenerMock = mockRepository.Create<IIfAnyErrorListener>();
            mainOperandMock = mockRepository.Create<IOperand>();
            errorOperandMock = mockRepository.Create<IOperand>();

            target = new IfAnyErrorFunction(listenerMock.Object);
        }

        [Fact]
        public void ArgsCount_Test()
        {
            // asserts
            target.ArgCount.Should().Be(2);
        }

        private IValue Perform() => target.Perform(mainOperandMock.Object, errorOperandMock.Object, contextMock);

        [Theory, AutoMockData]
        public void SuccessTest(IValue mainResult)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Returns(mainResult);

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(mainResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void FailTest(IValue errorResult, Exception e)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            listenerMock.Verify(f => f.OnError(e, contextMock, mainOperandMock.Object, null), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void FailAndListenerThrowsExceptionTest(IValue errorResult, Exception e)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);
            listenerMock.Setup(f => f.OnError(It.IsAny<Exception>(), It.IsAny<INameResolver>(), It.IsAny<IOperand>(), null)).Throws(new Exception());

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            listenerMock.Verify(f => f.OnError(e, contextMock, mainOperandMock.Object, null), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void WithoutListenerTest(IValue errorResult, Exception e)
        {
            // setup
            var target = new IfAnyErrorFunction(null);

            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);

            // action
            var result = target.Perform(mainOperandMock.Object, errorOperandMock.Object, contextMock);

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }
    }
}
