﻿using EvaluateExpression.Values;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;
using FluentAssertions;
using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Functions.Logic.Tests
{

    /// <summary>
    /// Summary description for LogicFunctionsTests
    /// </summary>

    public class LogicFunctionsTests
    {
        [Fact]
        public void IIF_ResultValueShoudBeSameAsOperand_Test()
        {
            // action
            IValue t = new IntValue(10);
            IValue f = new IntValue(5);
            IIFFunction func = new();

            // action && asserts
            Assert.Same(t, func.Perform(BoolValue.True, t, f, null));
            Assert.Same(f, func.Perform(BoolValue.False, t, f, null));
        }

        [Theory]
        [InlineData(true, false)]
        [InlineData(false, true)]
        public void IIF_LazyArgumentsCalculation_Test(bool condition, bool expectedException)
        {
            // setup
            IValue t = new IntValue(10);
            IOperand f = new FailOperand();
            IIFFunction func = new();

            // action
            Action action = () => func.Perform(condition.ToValue(), t, f, null);

            // asserts
            if (expectedException)
                action.Should().Throw<Exception>();
            else
                action.Should().NotThrow();
        }

        [Fact]
        public void IIFNull_ShouldReturnFirstArgument()
        {
            // setup
            IValue val = new IntValue(5);
            IValue def = new IntValue(10);
            var func = new IIFNullFunction();

            // action
            var result = func.Perform(val, def, null);

            // asserts
            result.Should().BeSameAs(val);
        }

        [Fact]
        public void IIFNull_ShouldReturnSecondArgument()
        {
            // setup
            IValue val = NullValue.Null;
            IValue def = new IntValue(10);
            var func = new IIFNullFunction();

            // action
            var result = func.Perform(val, def, null);

            // asserts
            result.Should().BeSameAs(def);
        }

        [Fact]
        public void IIFNull_LazySecondArgumentsCalculation()
        {
            // setup
            IValue val = new IntValue(5);
            var def = new FailOperand();
            var func = new IIFNullFunction();

            // action
            Action action = () => func.Perform(val, def, null);

            // asserts
            action.Should().NotThrow();
        }

        [Fact]
        public void IsExistsFunctionTest() {
            ConstNameResolver nr = new ConstNameResolver("val", 10.ToValue());
            nr.Add("complex", new StructValue());
            IsExistsFunction f = new IsExistsFunction();
            Assert.True(f.Perform(new VariableOperand("val"), nr).AsBool);
            Assert.False(f.Perform(new VariableOperand("nothing"), nr).AsBool);

            Assert.True(f.Perform(new VariableOperand("complex"), nr).AsBool);
            Assert.False(f.Perform(new GetSubValueOperator(new VariableOperand("complex"), new VariableOperand("val")), nr).AsBool);
        }        
    }
}
