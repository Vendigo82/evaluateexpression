﻿using System;
using EvaluateExpression.Operands;
using EvaluateExpression.Operations;
using EvaluateExpression.Operations.Strategy;
using Xunit;

namespace EvaluateExpression.Functions.Logic.Tests
{
    
    public class WithFunctionTests
    {
        private IOperand[] GetArguments(IOperand first) {
            return new IOperand[] {
                first,
                new VariableOperand("X"),
                new BinaryOperation(new Multiply(), new VariableOperand("X"), 2.ToValue())
            };
        }

        [Fact]
        public void WithFunctionTest() {
            WithFunction f = new WithFunction();
            Assert.Equal(20, f.Perform(GetArguments(10.ToValue()), null).AsInt);
        }
    }
}
