﻿using EvaluateExpression.Functions;
using EvaluateExpression.Functions.Logic;
using FluentAssertions;
using Moq;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.Functions.Logic.Tests
{
    public class IfAnyErrorFunctionWithParamTests
    {
        readonly MockRepository mockRepository = new(MockBehavior.Default);
        readonly Mock<IIfAnyErrorListener> listenerMock = new();
        readonly Mock<IOperand> mainOperandMock;
        readonly Mock<IOperand> errorOperandMock;
        readonly Mock<IOperand> paramOperandMock;
        readonly INameResolver contextMock = Mock.Of<INameResolver>();
        readonly IfAnyErrorFunctionWithParam target;

        public IfAnyErrorFunctionWithParamTests()
        {
            listenerMock = mockRepository.Create<IIfAnyErrorListener>();
            mainOperandMock = mockRepository.Create<IOperand>();
            errorOperandMock = mockRepository.Create<IOperand>();
            paramOperandMock = mockRepository.Create<IOperand>();

            paramOperandMock.Setup(f => f.Eval(It.IsAny<INameResolver>())).Returns("abc".ToValue());

            target = new IfAnyErrorFunctionWithParam(listenerMock.Object);

        }

        [Fact]
        public void ArgsCount_Test()
        {
            // asserts
            target.ArgCount.Should().Be(3);
        }

        private IValue Perform() => target.Perform(mainOperandMock.Object, errorOperandMock.Object, paramOperandMock.Object, contextMock);

        [Theory, AutoMockData]
        public void SuccessTest(IValue mainResult)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Returns(mainResult);

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(mainResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void FailTest(IValue errorResult, string param, Exception e)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);
            paramOperandMock.Setup(f => f.Eval(contextMock)).Returns(param.ToValue());

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            paramOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            listenerMock.Verify(f => f.OnError(e, contextMock, mainOperandMock.Object, param), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void FailAndListenerThrowsExceptionTest(IValue errorResult, string param, Exception e)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);
            paramOperandMock.Setup(f => f.Eval(contextMock)).Returns(param.ToValue());
            listenerMock.Setup(f => f.OnError(It.IsAny<Exception>(), It.IsAny<INameResolver>(), It.IsAny<IOperand>(), null)).Throws(new Exception());

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            paramOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            listenerMock.Verify(f => f.OnError(e, contextMock, mainOperandMock.Object, param), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void WithoutListenerTest(IValue errorResult, Exception e)
        {
            // setup
            var target = new IfAnyErrorFunction(null);

            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);

            // action
            var result = target.Perform(mainOperandMock.Object, errorOperandMock.Object, contextMock);

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoMockData]
        public void ErrorOnCalculateParameter(IValue errorResult, Exception e, Exception pe)
        {
            // setup
            mainOperandMock.Setup(f => f.Eval(contextMock)).Throws(e);
            errorOperandMock.Setup(f => f.Eval(contextMock)).Returns(errorResult);
            paramOperandMock.Setup(f => f.Eval(contextMock)).Throws(pe);

            // action
            var result = Perform();

            // asserts
            result.Should().BeSameAs(errorResult);

            mainOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            errorOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            paramOperandMock.Verify(f => f.Eval(contextMock), Times.Once);
            listenerMock.Verify(f => f.OnError(e, contextMock, mainOperandMock.Object, "#error"), Times.Once);
            listenerMock.Verify(f => f.OnError(pe, contextMock, paramOperandMock.Object, null), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }
    }
}
