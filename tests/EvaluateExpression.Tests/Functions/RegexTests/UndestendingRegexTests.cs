﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xunit;

namespace EvaluateExpression.Functions.RegexTests
{
    public class UndestendingRegexTests
    {

        public static IEnumerable<object[]> RegexTestCases { 
            get {
                // two matches
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new [] { "12105.38", "1282.69" }
                };

                // first summ without ':'
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки) 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new [] { "1282.69" }
                };

                // first summ without 'р'
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 уб. Исполнительский сбор: 1282.69 руб.",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new [] { "1282.69" }
                };

                // without sum
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new [] { "1282.69" }
                };

                // single sum
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 30550 руб.",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new [] { "30550" }
                };

                // single sum
                yield return new object[] {
                    "Штраф ГИБДД: 500 руб.",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new [] { "500" }
                };

                yield return new object[] {
                    "Иные взыскания имущественного характера в пользу физических и юридических лиц",
                    "(: (?<value>\\d*\\.?\\d{2}?) руб.)",
                    "value",
                    new string [] {  }
                };                
            }
        }

        [Theory, MemberData(nameof(RegexTestCases))]
        public void RegexTest(string source, string pattern, string groupName, string[] expected)
        {
            // setup
            //var regex = new Regex("(?:: (?<value>\\d*\\.?\\d{2}?) руб.)");
            var regex = new Regex(pattern);

            // action
            var result = regex.Matches(source);

            // asserts
            var values = result.SelectMany(i => i.Groups.Values).Where(i => i.Name == groupName && i.Success).Select(i => i.Value);
            values.Should().Equal(expected);
        }

        [Fact]
        public void RegexTest2()
        {
            // setup
            //var regex = new Regex("(?:: (?<value>\\d*\\.?\\d{2}?) руб.)");
            var regex = new Regex("([0-9]+)");

            // action
            var result = regex.Matches("123 456 7890");

            // asserts
            result.Should().HaveCount(3).And.OnlyContain(i => i.Groups.Count == 2);
        }

        [Theory]
        [InlineData("(?:: (\\d*\\.?\\d{2}?) руб.)")]
        [InlineData(": (\\d*\\.?\\d{2}?) руб.")]
        public void RegexTest3(string pattern)
        {
            // setup
            //var regex = new Regex("(?:: (?<value>\\d*\\.?\\d{2}?) руб.)");
            var regex = new Regex(pattern);

            // action
            var result = regex.Matches("Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.");

            // asserts
            result.Should().HaveCount(2).And.OnlyContain(i => i.Groups.Count == 2);
            var values = result.Select(i => i.Groups[1]).Select(i => i.Value);
            values.Should().BeEquivalentTo(new[] { "12105.38", "1282.69" });
        }
    }
}
