﻿using EvaluateExpression.Asserts;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.RegexTests
{
    public class RegexMatchTests
    {
        public static IEnumerable<object[]> TestCases {
            get {
                // two matches
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    true,
                    new [] { "12105.38" }
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (\\d*\\.?\\d{2}?) руб.)",
                    true,
                    new [] { ": 12105.38 руб.", "12105.38" }
                };

                yield return new object[] {
                    "Иные взыскания имущественного характера в пользу физических и юридических лиц",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    false,
                    new string [] {  }
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(?:: (\\d*\\.?\\d{2}?) руб.)",
                    true,
                    new [] { "12105.38" }
                };

                yield return new object[] {
                    "123 456 7890",
                    "([0-9]+)",
                    true,
                    new [] {"123" }
                };
            }
        }

        [Theory, MemberData(nameof(TestCases))]
        public void Test(string input, string pattern, bool expectedResult, string[] expectedGroups)
        {
            // action
            var result = RegexFunctions.RegexMatch(input.ToValue(), pattern.ToValue());

            // asserts
            result.Property("Success").Should().NotBeNull().And.Be(expectedResult);
            result.Property("Groups").Should().BeArrayValue();
            ((IIndexableValue)result.Property("Groups")).Select(i => i.AsString).Should().Equal(expectedGroups);
        }
    }
}
