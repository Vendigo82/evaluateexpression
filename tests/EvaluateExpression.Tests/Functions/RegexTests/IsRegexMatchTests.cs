﻿using EvaluateExpression.Asserts;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.RegexTests
{
    public class IsRegexMatchTests
    {
        public static IEnumerable<object[]> TestCases {
            get {
                // two matches
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    true
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (\\d*\\.?\\d{2}?) руб.)",
                    true
                };

                yield return new object[] {
                    "Иные взыскания имущественного характера в пользу физических и юридических лиц",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    false
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(?:: (\\d*\\.?\\d{2}?) руб.)",
                    true
                };

                yield return new object[] {
                    "123 456 7890",
                    "([0-9]+)",
                    true
                };
            }
        }

        [Theory, MemberData(nameof(TestCases))]
        public void Test(string input, string pattern, bool expectedResult)
        {
            // action
            var result = RegexFunctions.IsRegexMatch(input.ToValue(), pattern.ToValue());

            // asserts
            result.Should().Be(expectedResult);
        }
    }
}
