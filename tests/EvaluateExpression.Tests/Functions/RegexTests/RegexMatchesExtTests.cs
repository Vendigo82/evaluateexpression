﻿using EvaluateExpression;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Functions.RegexTests
{
    public class RegexMatchesExtTests
    {
        public static IEnumerable<object[]> TestCases {
            get {
                // two matches
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "12105.38", "1282.69" }
                };

                // first summ without ':'
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки) 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "1282.69" }
                };

                // first summ without 'р'
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 уб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "1282.69" }
                };

                // without sum
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "1282.69" }
                };

                // single sum
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 30550 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "30550" }
                };

                // single sum
                yield return new object[] {
                    "Штраф ГИБДД: 500 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "500" }
                };

                yield return new object[] {
                    "Иные взыскания имущественного характера в пользу физических и юридических лиц",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new string [] {  }
                };

                yield return new object[] {
                    "123 456 7890",
                    "([0-9]+)",
                    new [] {"123", "456", "7890" }
                };
            }
        }

        [Theory, MemberData(nameof(TestCases))]
        public void Test(string input, string pattern, string[] expected)
        {
            // setup

            // action
            var result = RegexFunctions.RegexMatchesExt(input.ToValue(), pattern.ToValue());

            // asserts
            if (expected.Any())
                result.Should()
                    .BeAssignableTo<IIndexableValue>().Which.Should()
                    .OnlyContain(i => i is IIndexableValue).And.Subject
                    .Cast<IIndexableValue>().Should()
                        .OnlyContain(i => i.Count() == 1).And.Subject
                        .Select(i => i[0.ToValue()].AsString).Should()
                        .Equal(expected);
            else
                result.Should().BeAssignableTo<IIndexableValue>().Which.Should().BeEmpty();
        }

        public static IEnumerable<object[]> GroupsTestCases {
            get {
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "12105.38" }
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (\\d*\\.?\\d{2}?) руб.)",
                    new [] { ": 12105.38 руб.", "12105.38" }
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(?:: (\\d*\\.?\\d{2}?) руб.)",
                    new [] { "12105.38" }
                };

            }
        }

        [Theory, MemberData(nameof(GroupsTestCases))]
        public void GroupsTest(string input, string pattern, string[] firstExpected)
        {
            // setup

            // action
            var result = RegexFunctions.RegexMatchesExt(input.ToValue(), pattern.ToValue());

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which.Should()
                .OnlyContain(i => i is IIndexableValue).And.Subject
                .Cast<IIndexableValue>().First().Select(i => i.AsString).Should().Equal(firstExpected);
        }
    }
}
