﻿using EvaluateExpression.Functions;
using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.Tests.Functions
{
    public class MainFunctionsTests
    {
        [Fact]
        public void GenGuidTest()
        {
            // setup

            // action
            var result = MainFunctions.GenGuid();

            // asserts
            result.Should().BeOfType<StringValue>().Which.AsString.Should().MatchRegex(@"(?im)^[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}$");
        }
    }
}
