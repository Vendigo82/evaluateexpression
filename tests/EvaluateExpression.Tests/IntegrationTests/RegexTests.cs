﻿using EvaluateExpression.Values;
using EvaluateExpression.Asserts;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class RegexTests : BaseTests
    {
        public static IEnumerable<object[]> TestCases {
            get {
                // two matches
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "12105.38", "1282.69" }
                };

                // first summ without ':'
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки) 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "1282.69" }
                };

                // first summ without 'р'
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 уб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "1282.69" }
                };

                // without sum
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "1282.69" }
                };

                // single sum
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 30550 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "30550" }
                };

                // single sum
                yield return new object[] {
                    "Штраф ГИБДД: 500 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new [] { "500" }
                };

                yield return new object[] {
                    "Иные взыскания имущественного характера в пользу физических и юридических лиц",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    new string [] {  }
                };

                yield return new object[] {
                    "123 456 7890",
                    "([0-9]+)",
                    new [] {"123", "456", "7890" }
                };
            }
        }

        [Theory, MemberData(nameof(TestCases))]
        public void RegexMatchesTest(string input, string pattern, string[] expected)
        {
            // setup
            string expression = $"RegexMatches(input, \"{pattern}\")";
            var operand = parser.Parse(expression);
            var context = new ConstNameResolver() { { "input", input.ToValue() } };

            // action
            var result = operand.Eval(context);

            // asserts
            result.Should().BeAssignableTo<IIndexableValue>();
            ((IIndexableValue)result).Select(i => i.AsString).Should().Equal(expected);
        }

        [Theory, MemberData(nameof(TestCases))]
        public void RegexMatchesExtTest(string input, string pattern, string[] expected)
        {
            // setup
            string expression = $"RegexMatchesExt(input, \"{pattern}\")";
            var operand = parser.Parse(expression);
            var context = new ConstNameResolver() { { "input", input.ToValue() } };

            // action
            var result = operand.Eval(context);

            // asserts
            result.Should().BeArrayValue();
            if (expected.Any()) {                
                ((IIndexableValue)result).Select(i => i is IIndexableValue).Should().OnlyContain(i => i == true);
                ((IIndexableValue)result).Cast<IIndexableValue>().Select(i => i[0.ToValue()].AsString).Should().Equal(expected);
            }
            else {
                ((IIndexableValue)result).Any().Should().BeFalse();
            }
        }

        public static IEnumerable<object[]> RegexMatchTestCases {
            get {
                // two matches
                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    true,
                    new [] { "12105.38" }
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(: (\\d*\\.?\\d{2}?) руб.)",
                    true,
                    new [] { ": 12105.38 руб.", "12105.38" }
                };

                yield return new object[] {
                    "Иные взыскания имущественного характера в пользу физических и юридических лиц",
                    ": (\\d*\\.?\\d{2}?) руб.",
                    false,
                    new string [] {  }
                };

                yield return new object[] {
                    "Задолженность по кредитным платежам (кроме ипотеки): 12105.38 руб. Исполнительский сбор: 1282.69 руб.",
                    "(?:: (\\d*\\.?\\d{2}?) руб.)",
                    true,
                    new [] { "12105.38" }
                };

                yield return new object[] {
                    "123 456 7890",
                    "([0-9]+)",
                    true,
                    new [] {"123" }
                };
            }
        }

        [Theory, MemberData(nameof(RegexMatchTestCases))]
        public void RegexMatchTest(string input, string pattern, bool expectedResult, string[] expectedGroups)
        {
            // setup
            string expression = $"RegexMatch(input, \"{pattern}\")";
            var operand = parser.Parse(expression);
            var context = new ConstNameResolver() { { "input", input.ToValue() } };

            // action
            var result = operand.Eval(context);

            // asserts
            result.Property("Success").Should().NotBeNull().And.Be(expectedResult);
            result.Property("Groups").Should().BeArrayValue();
            ((IIndexableValue)result.Property("Groups")).Select(i => i.AsString).Should().Equal(expectedGroups);
        }

        [Theory, MemberData(nameof(RegexMatchTestCases))]
        public void IsRegexMatchTest(string input, string pattern, bool expectedResult, string[] expectedGroups)
        {
            // setup
            string expression = $"IsRegexMatch(input, \"{pattern}\")";
            var operand = parser.Parse(expression);
            var context = new ConstNameResolver() { { "input", input.ToValue() } };

            // action
            var result = operand.Eval(context);

            // asserts
            result.Should().Be(expectedResult);
        }
    }
}
