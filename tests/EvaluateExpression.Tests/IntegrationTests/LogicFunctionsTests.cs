﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class LogicFunctionsTests : BaseTests
    {
        [Theory]
        [InlineData("RaiseError(\"msg\")", "msg")]
        [InlineData("RaiseError(123)", "123")]
        [InlineData("RaiseError(5 + 3)", "8")]
        public void RaiseErrorTest(string expression, string expectedMessage)
        {
            // setup

            // action
            Action action = () => parser.Parse(expression).Eval(null);

            // asserts
            action.Should()
                .ThrowExactly<Functions.Logic.RaiseErrorException>()
                .WithMessage($"Raise error: {expectedMessage}").And
                .UserMessage.Should().Be(expectedMessage);
        }

        [Theory]
        [InlineData("true", "\"abc\"", "\"xyz\"", "abc")]
        [InlineData("false", "\"abc\"", "\"xyz\"", "xyz")]
        [InlineData("true", "\"abc\"", "err", "abc")] // lazy calculation test
        public void IIFTest(string condition, string trueExpr, string falseExpr, string expectedValue)
        {
            // setup
            var expr = $"IIF({condition}, {trueExpr}, {falseExpr})";

            // action
            var result = parser.Parse(expr).Eval(null).AsString;

            // asserts
            result.Should().Be(expectedValue);
        }

        [Theory]
        [InlineData("\"abc\"", "\"xyz\"", "abc")]
        [InlineData("null",    "\"xyz\"", "xyz")]
        [InlineData("\"abc\"", "err", "abc")]// lazy calculation test
        public void IIFNullTest(string value, string defaultValue, string expectedValue)
        {
            // setup
            var expr = $"IIFNull({value}, {defaultValue})";

            // action
            var result = parser.Parse(expr).Eval(null).AsString;

            // asserts
            result.Should().Be(expectedValue);
        }
    }
}
