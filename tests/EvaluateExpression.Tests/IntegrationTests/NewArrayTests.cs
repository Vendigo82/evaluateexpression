﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class NewArrayTests : BaseTests
    {
        [Theory, MemberData(nameof(ParseNewArrayTestCases))]
        public void ParseNewArrayTest(string expression, IEnumerable<object> expected)
        {
            // setup

            // action
            var result = parser.Parse(expression).Eval(null);

            // asserts
            var value = result.Should().BeAssignableTo<IIndexableValue>().Which;
            value.Should().HaveSameCount(expected).And.Equal(expected, (i, j) => {
                if (j is string str)
                    return i.AsString == str;
                else
                    return i.AsInt == (int)j;
            });
        }

        public static IEnumerable<object[]> ParseNewArrayTestCases()
        {
            yield return new object[] { "new [1, 3, 5]", new object[] { 1, 3, 5 } };
            yield return new object[] { "new [ 1 , 3 , 5 ]", new object[] { 1, 3, 5 } };
            yield return new object[] { "new [1 + 3]", new object[] { 4 } };
            yield return new object[] { "new [\"abc\", \"xyz\"]", new object[] { "abc", "xyz" } };
            yield return new object[] { "new [ ]", Array.Empty<object>() };
        }

        [Theory]
        [InlineData("new [1, ]")]
        [InlineData("new [,]")]
        [InlineData("new [1, ")]
        [InlineData("new [")]
        public void ParseNewArray_InvalidExpression_Test(string expression)
        {
            // setup

            // action
            Action action = () => parser.Parse(expression).Eval(null);

            // asserts
            action.Should().ThrowExactly<Exceptions.ParserException>();
        }        
    }
}
