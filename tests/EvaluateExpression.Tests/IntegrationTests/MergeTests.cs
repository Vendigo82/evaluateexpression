﻿using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class MergeTests : BaseTests
    {
        [Fact]
        public void SimpleMergeObjectsTest()
        {
            // setup
            var t1 = new StructValue() 
            {
                { "val1", 10.ToValue() }
            };

            var t2 = new StructValue()
            {
                { "val2", 20.ToValue() }
            };

            var context = new ConstNameResolver
            {
                { "t1", t1 },
                { "t2", t2 }
            };

            // action
            var result = parser.Parse("MergeObjects(t1, t2)").Eval(context);

            // asserts
            var obj = result.Should().BeAssignableTo<IComplexValue>().Which;
            obj.Should().ContainKey("val1").WhoseValue.AsInt.Should().Be(10);
            obj.Should().ContainKey("val2").WhoseValue.AsInt.Should().Be(20);
            obj.Select(i => i.Key).Should().BeEquivalentTo(new[] { "val1", "val2" });
        }

        [Fact]
        public void MergeObjects_FieldsWithSameNameShouldBeOverriden()
        {
            // setup
            var t1 = new StructValue()
            {
                { "val1", 10.ToValue() }
            };

            var t2 = new StructValue()
            {
                { "val1", 20.ToValue() }
            };

            var context = new ConstNameResolver
            {
                { "t1", t1 },
                { "t2", t2 }
            };

            // action
            var result = parser.Parse("MergeObjects(t1, t2)").Eval(context);

            // asserts
            var obj = result.Should().BeAssignableTo<IComplexValue>().Which;
            obj.Should().ContainKey("val1").WhoseValue.AsInt.Should().Be(20);
            obj.Select(i => i.Key).Should().BeEquivalentTo(new[] { "val1" });
        }

        [Theory]
        [InlineData("t1, t2")]
        [InlineData("t2, t1")]
        public void MergeObjects_ArgumentIsNotObject_ShouldBeException(string arguments)
        {
            // setup
            var t1 = 10.ToValue();

            var t2 = new StructValue()
            {
                { "val1", 20.ToValue() }
            };

            var context = new ConstNameResolver
            {
                { "t1", t1 },
                { "t2", t2 }
            };

            // action
            Action action = () => parser.Parse($"MergeObjects({arguments})").Eval(context);

            // asserts
            action.Should().ThrowExactly<Exceptions.EvalCastToComplexException>();
        }
    }
}
