﻿using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class StringTests : BaseTests
    {
        [Theory]
        [InlineData("LevenshteinDist(\"party\", \"part\")", 1)]
        [InlineData("PosInString(\"1abc23\", \"abc\")", 1)]
        [InlineData("PosInString(\"1abc23\", \"xyz\")", -1)]
        [InlineData("PosInString(\"1abc23abc\", \"abc\")", 1)]
        [InlineData("PosInStringLast(\"1abc23abc\", \"abc\")", 6)]
        [InlineData("CharCode(\"abc\", 1)", 98)]
        [InlineData("CharCode(\"abc\", 2)", 99)]
        public void StringFunctionWithIntegerResult(string expression, int expectedResult)
        {
            // setup

            // action
            var result = parser.Parse(expression).Eval(null).AsInt;

            // asserts
            result.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("IsNullOrEmpty(\"party\")", false)]
        [InlineData("IsNullOrEmpty(\" \")", false)]
        [InlineData("IsNullOrEmpty(\"\")", true)]
        [InlineData("IsNullOrEmpty(null)", true)]
        [InlineData("IsNullOrWhiteSpace(\"party\")", false)]
        [InlineData("IsNullOrWhiteSpace(\" \")", true)]
        [InlineData("IsNullOrWhiteSpace(\"      \")", true)]    // with tabs
        [InlineData("IsNullOrWhiteSpace(\"\")", true)]
        [InlineData("IsNullOrWhiteSpace(null)", true)]
        public void StringFunctionWithBooleanResult(string expression, bool expectedResult)
        {
            // setup

            // action
            var result = parser.Parse(expression).Eval(null).AsBool;

            // asserts
            result.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("ToUpper(\"Abc\")", "ABC")]
        [InlineData("ToLower(\"Abc\")", "abc")]
        [InlineData("RemoveNonLatin(\"1 a bc1\")", "abc")]
        [InlineData("RemoveNonLatinOrDigit(\"1 a bc1\")", "1abc1")]
        [InlineData("SubString(\"1abc23\", 1, 3)", "abc")]
        [InlineData("Substring(\"1abc23\", 1, 3)", "abc")]
        [InlineData("ReplaceSubstring(\"abc xyz\", \"abc\", \"123\")", "123 xyz")]
        [InlineData("ReplaceSubstring(\"abc xyz\", \"abcd\", \"123\")", "abc xyz")]
        [InlineData("StringFromANSIICodes(new [ 57, 113, 117, 97, 108, 105, 53, 50, 116, 121, 51 ])", "9quali52ty3")]
        public void StringFunctionWithStringResult(string expression, string expectedResult)
        {
            // setup

            // action
            var result = parser.Parse(expression).Eval(null).AsString;

            // asserts
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void SplitTest()
        {
            // setup
            var expression = "Split(\"abc xyz\", \" \")";

            // action
            var result = parser.Parse(expression).Eval(null);

            // asserts
            result.Should().BeAssignableTo<IArrayValue>().Which.Select(i => i.AsString).Should().Equal(new [] { "abc", "xyz" });
        }

        [Theory]
        [InlineData("0", 3, "000")]
        [InlineData("0", 4, "0000")]
        [InlineData("abc", 2, "abcabc")]
        [InlineData("0", 1, "0")]
        [InlineData("0", 0, "")]
        [InlineData("0", -1, "")]
        public void Repeat(string value, int count, string expected)
        {
            // setup
            var expr = $"RepeatString(\"{value}\", {count})";

            // action
            var result = parser.Parse(expr).Eval(null).AsString;

            // asserts
            result.Should().Be(expected);
        }

        [Fact]
        public void StringFunctions_ConcatStrings_Test()
        {
            var ns = new ConstNameResolver() {
                { "array", new ArrayValue(new [] { "abc", "xyz", "nm" }.Select(i => i.ToValue()).ToArray()) }
            };
            Assert.Equal("abc xyz nm", parser.Parse("ConcatStrings(array, \" \")").Eval(ns).AsString);
        }

        [Theory]
        [InlineData("Trim(\"abc\")", "abc")]
        [InlineData("Trim(\"   abc   \")", "abc")]
        [InlineData("Trim(\"\t\tabc\t\t\")", "abc")]
        [InlineData("TrimStart(\" abc \")", "abc ")]
        [InlineData("TrimEnd(\" abc \")", " abc")]
        public void TrimTest(string expression, string expectedResult)
        {
            // setup
            var expr = $"{expression}";

            // action
            var result = parser.Parse(expr).Eval(null).AsString;

            // asserts
            result.Should().Be(expectedResult);
        }
    }
}
