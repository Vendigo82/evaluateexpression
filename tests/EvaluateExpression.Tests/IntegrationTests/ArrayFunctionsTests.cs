﻿using AutoFixture.Xunit2;
using EvaluateExpression.Asserts;
using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class ArrayFunctionsTests : BaseTests
    {
        [Theory]
        [InlineAutoData("Sort(array)", true)]
        [InlineAutoData("SortDesc(array)", false)]
        public void SortPrimitive_Test(string expr, bool expectedAscending, int[] values)
        {
            // setup
            var array = new ArrayValue(values.Select(i => i.ToValue()).ToArray());
            var nr = new ConstNameResolver("array", array);

            // action
            var result = parser.Parse(expr).Eval(nr);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsInt).Should().Equal(expectedAscending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }

        [Theory]
        [InlineAutoData("Sort(array, \"fld\")", true)]
        [InlineAutoData("SortDesc(array, \"fld\")", false)]
        [InlineAutoData("Sort(array, X, X.fld)", true)]
        [InlineAutoData("SortDesc(array, X, X.fld)", false)]
        public void SortComplex_Ascending_Test(string expr, bool expectedAscending, int[] values)
        {
            // setup
            var array = new Values.ArrayValue(values
                .Select(i => new Values.StructValue(new[] { KeyValuePair.Create("fld", i.ToValue()) }))
                .ToArray());
            var nr = new ConstNameResolver("array", array);

            // action
            var result = parser.Parse(expr).Eval(nr);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Cast<IComplexValue>().Select(i => i.PropertyRequired("fld").AsInt)
                .Should().Equal(expectedAscending ? values.OrderBy(i => i) : values.OrderByDescending(i => i));
        }

        [Theory, AutoData]
        public void FlatTest(IEnumerable<int> array1, IEnumerable<int> array2)
        {
            // setup
            var arrayValue = new ArrayValue(new IValue[] {
                new ArrayValue(array1.Select(i => i.ToValue()).ToArray()),
                new ArrayValue(array2.Select(i => i.ToValue()).ToArray())
            });

            var nr = new ConstNameResolver("array", arrayValue);

            // action
            var result = parser.Parse("ArrayFlat(array)").Eval(nr);

            // asserts
            result.Should()
                .BeAssignableTo<IIndexableValue>().Which
                .Select(i => i.AsInt).Should().Equal(array1.Concat(array2));
        }

        [Theory, MemberData(nameof(RangeTestCases))]
        public void RangeTest(int from, int count, IEnumerable<int> expected)
        {
            // setup
            var expression = $"Range({from}, {count})";

            // action
            var result = parser.Parse(expression).Eval(null);

            // asserts
            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(expected);
        }

        public static IEnumerable<object[]> RangeTestCases()
        {
            yield return new object[] { 1, 3, new int[] { 1, 2, 3 } };
            yield return new object[] { 5, 2, new int[] { 5, 6 } };
            yield return new object[] { 5, 1, new int[] { 5 } };
            yield return new object[] { 5, 0, new int[] { } };
        }

        [Theory, MemberData(nameof(RangeStepTestCases))]
        public void RangeStepTest(int from, int count, int step, IEnumerable<int> expected)
        {
            // setup
            var expression = $"Range({from}, {count}, {step})";

            // action
            var result = parser.Parse(expression).Eval(null);

            // asserts
            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(expected);
        }

        public static IEnumerable<object[]> RangeStepTestCases()
        {
            yield return new object[] { 1, 3, 2, new int[] { 1, 3, 5 } };
            yield return new object[] { 5, 2, 3, new int[] { 5, 8 } };
            yield return new object[] { 5, 1, 1, new int[] { 5 } };
            yield return new object[] { 5, 0, 1, new int[] { } };
            yield return new object[] { 2, 2, 0, new int[] { 2, 2 } };
        }

        [Theory, AutoData]
        public void Concat2Test(IEnumerable<int> array1, IEnumerable<int> array2)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
                { "array2", new ArrayValue(array2.Select(i => i.ToValue()).ToArray()) },
            };

            var result = parser.Parse("Concat(array1, array2)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(array1.Concat(array2).ToArray());
        }

        [Theory, AutoData]
        public void Concat3Test(IEnumerable<int> array1, IEnumerable<int> array2, IEnumerable<int> array3)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
                { "array2", new ArrayValue(array2.Select(i => i.ToValue()).ToArray()) },
                { "array3", new ArrayValue(array3.Select(i => i.ToValue()).ToArray()) },
            };

            var result = parser.Parse("Concat(array1, array2, array3)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(array1.Concat(array2).Concat(array3).ToArray());
        }

        [Theory, AutoData]
        public void AddToTailTest(IEnumerable<int> array1, int value)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
                { "value", value.ToValue() },
            };

            var result = parser.Parse("AddToTail(array1, value)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(array1.Concat(new[] { value }).ToArray());
        }

        [Theory, AutoData]
        public void AddToHeadTest(IEnumerable<int> array1, int value)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
                { "value", value.ToValue() },
            };

            var result = parser.Parse("AddToHead(array1, value)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(new[] { value }.Concat(array1).ToArray());
        }

        [Theory, AutoData]
        public void FirstOrNull_WhenArrayNotEmpty_Test(IEnumerable<int> array1)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },                
            };

            var result = parser.Parse($"FirstOrNull(array1)").Eval(context);

            result.Should().Be(array1.First());
        }

        [Fact]
        public void FirstOrNull_WhenArrayEmpty_Test()
        {
            var context = new ConstNameResolver()
            {
                { "array1", ArrayValue.Empty },
            };

            var result = parser.Parse($"FirstOrNull(array1)").Eval(context);

            result.Should().BeNullValue();
        }

        [Theory, AutoData]
        public void FirstOrNull_WhenNotArray_Test(int someValue)
        {
            var context = new ConstNameResolver()
            {
                { "array1", someValue.ToValue() },
            };

            var result = parser.Parse($"FirstOrNull(array1)").Eval(context);

            result.Should().Be(someValue);
        }

        [Fact]
        public void FirstOrNull_WhenNull_Test()
        {
            var context = new ConstNameResolver()
            {
                { "array1", NullValue.Null },
            };

            var result = parser.Parse($"FirstOrNull(array1)").Eval(context);

            result.Should().BeNullValue();
        }

        [Theory, AutoData]
        public void FirstOrValue_WhenArrayNotEmpty_Test(IEnumerable<int> array1)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
            };

            var result = parser.Parse($"FirstOrValue(array1, -1)").Eval(context);

            result.Should().Be(array1.First());
        }

        [Theory, AutoData]
        public void FirstOrValue_WhenArrayEmpty_Test(int value)
        {
            var context = new ConstNameResolver()
            {
                { "array1", ArrayValue.Empty },
                { "value", value.ToValue() },
            };

            var result = parser.Parse($"FirstOrValue(array1, value)").Eval(context);

            result.Should().Be(value);
        }

        [Theory, AutoData]
        public void FirstOrValue_WhenNotArray_Test(int someValue, int value)
        {
            var context = new ConstNameResolver()
            {
                { "array1", someValue.ToValue() },
                { "value", value.ToValue() },
            };

            var result = parser.Parse($"FirstOrValue(array1, value)").Eval(context);

            result.Should().Be(someValue);
        }

        [Theory, AutoData]
        public void FirstOrValue_WhenNullValue_Test(int value)
        {
            var context = new ConstNameResolver()
            {
                { "array1", NullValue.Null },
                { "value", value.ToValue() },
            };

            var result = parser.Parse($"FirstOrValue(array1, value)").Eval(context);

            result.Should().Be(value);
        }

        [Theory, AutoData]
        public void LastOrNull_WhenArrayNotEmpty_Test(IEnumerable<int> array1)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
            };

            var result = parser.Parse($"LastOrNull(array1)").Eval(context);

            result.Should().Be(array1.Last());
        }

        [Fact]
        public void LastOrNull_WhenArrayEmpty_Test()
        {
            var context = new ConstNameResolver()
            {
                { "array1", ArrayValue.Empty },
            };

            var result = parser.Parse($"LastOrNull(array1)").Eval(context);

            result.Should().BeNullValue();
        }

        [Theory, AutoData]
        public void LastOrValue_WhenArrayNotEmpty_Test(IEnumerable<int> array1)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
            };

            var result = parser.Parse($"LastOrValue(array1, -1)").Eval(context);

            result.Should().Be(array1.Last());
        }

        [Theory, AutoData]
        public void LastOrValue_WhenArrayEmpty_Test(int value)
        {
            var context = new ConstNameResolver()
            {
                { "array1", ArrayValue.Empty },
                { "value", value.ToValue() },
            };

            var result = parser.Parse($"LastOrValue(array1, value)").Eval(context);

            result.Should().Be(value);
        }


        [Theory, AutoData]
        public void ReverseTest(IEnumerable<int> array1)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) }
            };

            var result = parser.Parse("Reverse(array1)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
                .Equal(array1.Reverse().ToArray());
        }

        [Theory, AutoData]
        public void ZipTest(IEnumerable<int> array1, IEnumerable<int> array2)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
                { "array2", new ArrayValue(array2.Select(i => i.ToValue()).ToArray()) }
            };

            var result = parser.Parse("Zip(array1, array2)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Should()
                .OnlyContain(i => i.Property("First") != null && i.Property("Second") != null).And
                .Equal(array1.Zip(array2), (v, z) => v.Property("First").AsInt == z.First && v.Property("Second").AsInt == z.Second);
        }

        [Theory, AutoData]
        public void Zip_WhenFirstArrayHaveMoreElements_Test(IEnumerable<int> array1, IEnumerable<int> array2)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Concat(Enumerable.Range(1,5)).Select(i => i.ToValue()).ToArray()) },
                { "array2", new ArrayValue(array2.Select(i => i.ToValue()).ToArray()) }
            };

            var result = parser.Parse("Zip(array1, array2)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Should()
                .OnlyContain(i => i.Property("First") != null && i.Property("Second") != null).And
                .Equal(array1.Zip(array2), (v, z) => v.Property("First").AsInt == z.First && v.Property("Second").AsInt == z.Second);
        }

        [Theory, AutoData]
        public void Zip_WhenSecondArrayHaveMoreElements_Test(IEnumerable<int> array1, IEnumerable<int> array2)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) },
                { "array2", new ArrayValue(array2.Concat(Enumerable.Range(1,5)).Select(i => i.ToValue()).ToArray()) }
            };

            var result = parser.Parse("Zip(array1, array2)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Should()
                .OnlyContain(i => i.Property("First") != null && i.Property("Second") != null).And
                .Equal(array1.Zip(array2), (v, z) => v.Property("First").AsInt == z.First && v.Property("Second").AsInt == z.Second);
        }

        [Theory, AutoData]
        public void ZipIndexTest(IEnumerable<int> array1)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) }
            };

            var result = parser.Parse("ZipIndex(array1)").Eval(context);

            result.Should()
                .BeAssignableTo<IEnumerable<IValue>>().Which.Should()
                .OnlyContain(i => i.Property("Item") != null && i.Property("Index") != null).And
                .Equal(array1.Zip(Enumerable.Range(0, array1.Count())), (v, z) => v.Property("Item").AsInt == z.First && v.Property("Index").AsInt == z.Second);
        }

        [Theory, MemberData(nameof(SliceTestCases))]
        public void SliceTest(IEnumerable<int> array1, int from, int to, IEnumerable<int> expected)
        {
            var context = new ConstNameResolver()
            {
                { "array1", new ArrayValue(array1.Select(i => i.ToValue()).ToArray()) }
            };

            var result = parser.Parse($"Slice(array1, {from}, {to})").Eval(context);

            result.Should()
               .BeAssignableTo<IEnumerable<IValue>>().Which.Select(i => i.AsInt).Should()
               .Equal(expected);
        }

        public static IEnumerable<object[]> SliceTestCases()
        {
            var source = new[] { 1, 2, 3, 4, 5 };
            yield return new object[] { source, 0, 4, source };
            yield return new object[] { source, 0, 3, new []{ 1, 2, 3, 4 } };
            yield return new object[] { source, 1, 3, new[] { 2, 3, 4 } };
            yield return new object[] { source, 1, 1, new[] { 2 } };
            yield return new object[] { source, 0, -1, source };
            yield return new object[] { source, 0, -2, new[] { 1, 2, 3, 4 } };
            yield return new object[] { source, -5, -2, new[] { 1, 2, 3, 4 } };
            yield return new object[] { source, -4, -2, new[] { 2, 3, 4 } };
            yield return new object[] { source, -3, -2, new[] { 3, 4 } };
            yield return new object[] { source, -2, -2, new[] { 4 } };

            yield return new object[] { source, 0, 100, source };
            yield return new object[] { source, -100, -1, source };
            yield return new object[] { source, 5, 1, Array.Empty<int>() };
        }
    }
}
