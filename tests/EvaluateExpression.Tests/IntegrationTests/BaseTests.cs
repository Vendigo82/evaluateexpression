﻿using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.IntegrationTests
{
    public class BaseTests
    {
        protected readonly IParser parser = Parser.Parser.CreateFull();

        protected static IValue GetDictValue(int i, double d) => new StructValue {
                { "field", i.ToValue() },
                { "val", d.ToValue() }
        };
    }
}
