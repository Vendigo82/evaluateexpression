﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.IntegrationTests
{
    public class NewStructureTests : BaseTests
    {
        [Theory, MemberData(nameof(ParseNewOperationTestCases))]
        public void ParseNewOperationTest(string expression, IEnumerable<KeyValuePair<string, object>> expectedValues)
        {
            // setup

            // action
            var result = parser.Parse(expression).Eval(null);

            // asserts
            var value = result.Should().BeAssignableTo<IComplexValue>().Which;
            value.Select(i => i.Key).Should().BeEquivalentTo(expectedValues.Select(i => i.Key));//"a", "b");
            foreach (var pair in expectedValues)
            {
                if (pair.Value is int intValue)
                    value.PropertyRequired(pair.Key).AsInt.Should().Be(intValue);
                else if (pair.Value is string stringValue)
                    value.PropertyRequired(pair.Key).AsString.Should().Be(stringValue);
            }
        }

        public static IEnumerable<object[]> ParseNewOperationTestCases()
        {
            yield return new object[] { "new { a: 10, b: 20 }",
                new [] {
                    new KeyValuePair<string, object>("a", 10),
                    new KeyValuePair<string, object>("b", 20)
                }
            };

            yield return new object[] { "new { a : 10, b : 20 }",
                new [] {
                    new KeyValuePair<string, object>("a", 10),
                    new KeyValuePair<string, object>("b", 20)
                }
            };

            yield return new object[] { "new{a:10,b:20}",
                new [] {
                    new KeyValuePair<string, object>("a", 10),
                    new KeyValuePair<string, object>("b", 20)
                }
            };

            yield return new object[] { "new { }",
                Array.Empty<KeyValuePair<string, object>>()
            };

            yield return new object[] { "new { field: \"abc\" }",
                new [] {
                    new KeyValuePair<string, object>("field", "abc")                    
                }
            };

            yield return new object[] { "new { field: 1 + 2 }",
                new [] {
                    new KeyValuePair<string, object>("field", 3)
                }
            };
        }

        [Theory, MemberData(nameof(ParseNestedNewOperationTestCases))]
        public void ParseNestedNewOperationTest(string expression, IEnumerable<string> expectedKeys, string nestedKey, IEnumerable<string> expectedNestedkeys)
        {
            // setup

            // action
            var result = parser.Parse(expression).Eval(null);

            // asserts
            var value = result.Should().BeAssignableTo<IComplexValue>().Which;
            value.Select(i => i.Key).Should().BeEquivalentTo(expectedKeys);
            value.PropertyRequired(nestedKey).Should()
                .BeAssignableTo<IComplexValue>().Which
                .Select(i => i.Key).Should().BeEquivalentTo(expectedNestedkeys);
        }

        public static IEnumerable<object[]> ParseNestedNewOperationTestCases()
        {
            yield return new object[] { "new { nested: new { a: 10 } }", new[] { "nested" }, "nested", new[] { "a" } };
            yield return new object[] { "new { a: 10, nested: new { a: 10 }, b: 10 }", new[] { "a", "b", "nested" }, "nested", new[] { "a" } };
        }

        [Theory]
        [InlineData("new { a: 10, b: 20 ")]
        [InlineData("new { a: 10, b: }")]
        [InlineData("new { a: 10, b 20 }")]
        [InlineData("new { a: 10 b: 20 }")]
        [InlineData("new {")]
        public void InvalidExpressionTest_ShouldBeException_Test(string expression)
        {
            // setup

            // action
            Action action = () => parser.Parse(expression).Eval(null);

            // asserts
            action.Should().ThrowExactly<Exceptions.ParserException>();
        }
    }
}
