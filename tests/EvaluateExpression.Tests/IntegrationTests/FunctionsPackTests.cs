﻿using System;
using System.Collections.Generic;
using System.Linq;
using EvaluateExpression.Values;
using EvaluateExpression.Asserts;
using FluentAssertions;
using Xunit;
using Moq;

namespace EvaluateExpression.IntegrationTests
{
    
    public class FunctionsPackTests
    {
        readonly IParser parser = Parser.Parser.CreateFull();
        readonly INameResolver nr = new ConstNameResolver() {
                { "a", 1.ToValue() },
                { "b", 2.ToValue() },
                { "c", 3.ToValue() }
            };

        [Fact]
        public void Functions_CallTest() {
            Assert.IsAssignableFrom<IComplexValue>(parser.Parse("MakeStructFromContext()").Eval(nr));
        }

        [Fact]
        public void Functions_DateTimeFunctionsTest() {
            DateTime d1 = new DateTime(2012, 10, 5);
            DateTime d2 = new DateTime(2013, 10, 5);
            DateTime d3 = new DateTime(2012, 10, 15);

            ConstNameResolver nr = new ConstNameResolver {
                { "d1", d1.ToValue() },
                { "d2", d2.ToValue() },
                { "d3", d3.ToValue() }
            };

            Assert.True(Math.Abs((DateTime.Now - parser.Parse("Now()").Eval(null).AsDateTime).TotalSeconds) < 1);
            Assert.True(Math.Abs((DateTime.UtcNow - parser.Parse("UtcNow()").Eval(null).AsDateTime).TotalSeconds) < 1);
            ValueAssert.CheckDouble(10, parser.Parse("TimeDiff(d1, d3)").Eval(nr));
            ValueAssert.CheckDouble(365, parser.Parse("TimeDiff(d1, d2)").Eval(nr));

            ValueAssert.CheckDateTime(d1.AddHours(3), parser.Parse("TimeAddHours(d1, 3)").Eval(nr));
            ValueAssert.CheckDateTime(d1.AddDays(50), parser.Parse("TimeAddDays(d1, 50)").Eval(nr));
            ValueAssert.CheckDateTime(d1.AddDays(-50), parser.Parse("TimeAddDays(d1, -50)").Eval(nr));
            ValueAssert.CheckDateTime(d1.AddMonths(5), parser.Parse("TimeAddMonths(d1, 5)").Eval(nr));
            ValueAssert.CheckDateTime(d1.AddYears(2), parser.Parse("TimeAddYears(d1, 2)").Eval(nr));
        }

        [Fact]
        public void Functions_Math_Test() {
            ValueAssert.CheckInt(10, parser.Parse("Abs(10)").Eval(nr));
            ValueAssert.CheckInt(10, parser.Parse("Abs(-10)").Eval(nr));
            ValueAssert.CheckDouble(10.0, parser.Parse("Abs(10.0)").Eval(nr));
            ValueAssert.CheckDouble(10.0, parser.Parse("Abs(-10.0)").Eval(nr));
        }

        [Theory]        
        [InlineData(1, "IfAnyError(val1.a, 100)")]
        [InlineData("abc", "IfAnyError(val2, 100)")]
        [InlineData(100, "IfAnyError(val1.a + val2, 100)")]
        [InlineData(10, "IfAnyError(val1.b.bb, 100)")]
        [InlineData(100, "IfAnyError(val1.b.c, 100)")]
        [InlineData(100, "IfAnyError(val1.a.c, 100)")]
        [InlineData(100, "IfAnyError(val2.a, 100)")]
        [InlineData(100, "IfAnyError(val3, 100)")]
        [InlineData(100, "IfAnyError(val3, 100, 123)")]
        [InlineData(100, "IfAnyError(val3, 100, \"123\")")]
        public void IfAnyError_Test(object expected, string expression)
        {
            // setup
            var s = new StructValue {
                { "a", 1.ToValue() },
                { "b", new StructValue {
                    { "bb", 10.ToValue() }
                } }
            };
            var nr = new ConstNameResolver {
                { "val1", s },
                { "val2", "abc".ToValue() }
            };

            // action
            var value = parser.Parse(expression).Eval(nr);

            // asserts
            if (expected is string str)
                value.Should().Be(str);
            else
                value.Should().Be((int)expected);
        }

        [Theory]

        [InlineData(100, "123", "IfAnyError(val3, 100, 123)")]
        [InlineData(100, "123", "IfAnyError(val3, 100, \"123\")")]
        [InlineData(100, "abc", "IfAnyError(val3, 100, \"abc\")")]
        [InlineData(100, null, "IfAnyError(val3, 100)")]
        [InlineData(100, "null", "IfAnyError(val3, 100, null)")]
        public void IfAnyErrorListenerTest_Test(object expected, string expectedParam, string expression) 
        {
            // setup
            var listenerMock = new Mock<Functions.Logic.IIfAnyErrorListener>();
            var parser = Parser.Parser.CreateFull(new FunctionSettings { IfAnyErrorListener = listenerMock.Object });

            var nr = new ConstNameResolver {
                { "val2", "abc".ToValue() }
            };

            // action
            var value = parser.Parse(expression).Eval(nr);

            // asserts
            if (expected is string str)
                value.Should().Be(str);
            else
                value.Should().Be((int)expected);

            listenerMock.Verify(f => f.OnError(It.IsNotNull<Exception>(), nr, It.IsNotNull<IOperand>(), expectedParam), Times.Once);
        }

        [Fact]
        public void Factory_ArrayFunctions_Test()
        {
            ArrayValue arraySimple = new ArrayValue(new int[] { 1, 3, 5 }.Select(i => i.ToValue()).ToArray());
            ArrayValue arraySimple2 = new ArrayValue(new int[] { 3, 5, 8 }.Select(i => i.ToValue()).ToArray());
            ArrayValue arrayComplex = new ArrayValue(new IValue[] { GetDictValue(1, 1.5), GetDictValue(3, 3.5), GetDictValue(5, 5) });

            ConstNameResolver nr = new ConstNameResolver {
                { "as", arraySimple },
                { "as2", arraySimple2 },
                { "ac", arrayComplex }
            };

            Assert.Equal(9, parser.Parse("Sum(as)").Eval(nr).AsInt);
            Assert.Equal(9, parser.Parse("Sum(ac, X, X.field)").Eval(nr).AsInt);
            Assert.Equal(10, parser.Parse("Sum(ac, X, X.val)").Eval(nr).AsDouble);
            Assert.Equal(19, parser.Parse("Sum(ac, X, X.field + X.val)").Eval(nr).AsDouble);

            Assert.Equal(1, parser.Parse("ArrayMin(as)").Eval(nr).AsInt);
            Assert.Equal(5, parser.Parse("ArrayMax(as)").Eval(nr).AsInt);
            Assert.Equal(1, parser.Parse("ArrayMin(ac, X, X.field)").Eval(nr).AsInt);
            Assert.Equal(5, parser.Parse("ArrayMax(ac, X, X.field)").Eval(nr).AsInt);

            Assert.Equal(5, parser.Parse("FirstOrNull(ArrayFilter(as, X, X >= 5))").Eval(nr).AsInt);
            Assert.Equal(2, parser.Parse("ArrayFilter(as, X, X >= 3).Count").Eval(nr).AsInt);
            Assert.Equal(0, parser.Parse("ArrayFilter(as, X, X >= 10).Count").Eval(nr).AsInt);

            Assert.Equal(new int[] { 1, 3, 5 },
                (parser.Parse("ArrayTransform(ac, X, X.field)").Eval(nr) as IEnumerable<IValue>).Select(i => i.AsInt).ToArray());
            Assert.Equal(new int[] { 3, 5 },
                (parser.Parse("ArrayFilter(ac, X, X.field > 2, X.field)").Eval(nr) as IEnumerable<IValue>).Select(i => i.AsInt).ToArray());

            Assert.True(parser.Parse("Contains(as, 1)").Eval(nr).AsBool);
            Assert.False(parser.Parse("Contains(as, 2)").Eval(nr).AsBool);
            Assert.True(parser.Parse("Contains(ac, X, X.field = 3)").Eval(nr).AsBool);
            Assert.False(parser.Parse("Contains(ac, X, X.field = 4)").Eval(nr).AsBool);
            Assert.Equal(new int[] { 3, 5 },
                (parser.Parse("Intersect(as, as2)").Eval(nr) as IEnumerable<IValue>).Select(i => i.AsInt).ToArray());
            Assert.False(parser.Parse("IsEmpty(ac)").Eval(nr).AsBool);
            Assert.True(parser.Parse("IsNotEmpty(ac)").Eval(nr).AsBool);

            Assert.Equal(new int[] { 3, 5 },
                (parser.Parse("Skip(as, 1)").Eval(nr) as IEnumerable<IValue>).Select(i => i.AsInt).ToArray());
        }

        [Fact]
        public void GenGuidTest()
        {
            // setup && action
            var value = parser.Parse("GenGuid()").Eval(Mock.Of<INameResolver>()).AsString;

            // asserts
            value.Should().MatchRegex(@"(?im)^[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}$");
        }

        private IValue GetDictValue(int i, double d) => new StructValue {
                { "field", i.ToValue() },
                { "val", d.ToValue() }
            };        
    }
}
