﻿using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvaluateExpression.Utils.Tests
{
    public class ValueComparerTests
    {
        [Theory, MemberData(nameof(TestCases))]
        public void Compare_Test(IValue x, IValue y, int expectedSign)
        {
            // setup
            var comparer = new ValueComparer();

            // action
            var result = comparer.Compare(x, y);

            // asserts
            Math.Sign(result).Should().Be(expectedSign);
        }

        public static IEnumerable<object[]> TestCases {
            get {
                yield return new object[] { NullValue.Null, NullValue.Null, 0 };
                yield return new object[] { NullValue.Null, 10.ToValue(), -1 };
                yield return new object[] { 10.ToValue(), NullValue.Null, 1 };

                yield return new object[] { 1.ToValue(), 2.ToValue(), -1 };
                yield return new object[] { 2.ToValue(), 2.ToValue(), 0 };
                yield return new object[] { 3.ToValue(), 2.ToValue(), 1 };

                yield return new object[] { 1.5.ToValue(), 2.5.ToValue(), -1 };
                yield return new object[] { 2.5.ToValue(), 2.5.ToValue(), 0 };
                yield return new object[] { 3.5.ToValue(), 2.5.ToValue(), 1 };

                yield return new object[] { BoolValue.False, BoolValue.True, -1 };
                yield return new object[] { BoolValue.True, BoolValue.True, 0 };
                yield return new object[] { BoolValue.False, BoolValue.False, 0 };
                yield return new object[] { BoolValue.True, BoolValue.False, 1 };

                yield return new object[] { "abc".ToValue(), "xyz".ToValue(), -1 };
                yield return new object[] { "xyz".ToValue(), "xyz".ToValue(), 0 };
                yield return new object[] { "xyz".ToValue(), "abc".ToValue(), 1 };

                yield return new object[] { 1.ToValue(), 2.5.ToValue(), -1 };
                yield return new object[] { 2.5.ToValue(), 1.ToValue(), 1 };
            }
        }
    }
}
