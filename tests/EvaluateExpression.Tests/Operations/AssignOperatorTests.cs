﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Operations.Parser.Tests {
    using EvaluateExpression.Exceptions;
    using EvaluateExpression.Parser;
    using Operands;

    /// <summary>
    /// Summary description for AssignOperatorTests
    /// </summary>
    
    public class AssignOperatorTests {

        [Fact]
        public void AssignOperatorTest()
        {
            IParser parser = new Parser(operations: OperationsPack.CreateDefFactory(true));
            IOperand operand = parser.Parse("X:=10");

            VarNameResolver nr = new VarNameResolver("X", Values.NullValue.Null);
            operand.Eval(nr);
            Assert.Equal(10, nr.Resolve("X").AsInt);

            ConstNameResolver cnr3 = new ConstNameResolver("X", Values.NullValue.Null);
            Assert.Throws<EvalInvalidOperationException>(() => operand.Eval(cnr3));
        }

    }
}
