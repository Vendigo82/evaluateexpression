﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Operations.Factory.Tests {
    /// <summary>
    /// Summary description for DefOpersParserTests
    /// </summary>
    
    public class DefOpersFactoryTests {
        
        class TestOperInfo : IOperationInfo {
            public TestOperInfo(string oper) {
                Oper = oper;
            }

            public string Oper { get; private set; }

            public int priority => 0;

            public IOperand Compile(IOperandStack operands) {
                throw new NotImplementedException();
            }
        }

        [Fact]
        public void InitializeInOrder1Test() {
            DefOpersFactory parser = new DefOpersFactory();
            parser.Add("+", new TestOperInfo("+"));
            parser.Add("++", new TestOperInfo("++"));
            parser.Add("+++", new TestOperInfo("+++"));
            CheckPPP(parser);
        }

        [Fact]
        public void InitializeInOrder2Test() {
            DefOpersFactory parser = new DefOpersFactory();
            parser.Add("+++", new TestOperInfo("+++"));
            parser.Add("++", new TestOperInfo("++"));
            parser.Add("+", new TestOperInfo("+"));
            CheckPPP(parser);
        }

        [Fact]
        public void InitializeInOrder1CharTest() {
            DefOpersFactory parser = new DefOpersFactory();
            parser.Add('+', new TestOperInfo("+"));
            parser.Add("++", new TestOperInfo("++"));
            parser.Add("+++", new TestOperInfo("+++"));
            CheckPPP(parser);
        }

        [Fact]
        public void InitializeInOrder2CharTest() {
            DefOpersFactory parser = new DefOpersFactory();
            parser.Add("+++", new TestOperInfo("+++"));
            parser.Add("++", new TestOperInfo("++"));
            parser.Add('+', new TestOperInfo("+"));
            CheckPPP(parser);
        }

        [Fact]
        public void ParsingTest() {
            DefOpersFactory parser = new DefOpersFactory();
            parser.Add("+++", new TestOperInfo("+++"));
            parser.Add("++", new TestOperInfo("++"));
            parser.Add('+', new TestOperInfo("+"));
            
            string expr = "1+2++4+++5";
            int index = 1;
            Assert.Equal("+", (parser.Parse(expr, ref index) as TestOperInfo).Oper);

            index += 1;
            Assert.Equal("++", (parser.Parse(expr, ref index) as TestOperInfo).Oper);

            index += 1;
            Assert.Equal("+++", (parser.Parse(expr, ref index) as TestOperInfo).Oper);
        }

        private void CheckPPP(DefOpersFactory parser) {
            int index = 0;
            Assert.Equal("+", (parser.Parse("+", ref index) as TestOperInfo).Oper);
            Assert.Equal(1, index);

            index = 0;
            Assert.Equal("++", (parser.Parse("++", ref index) as TestOperInfo).Oper);
            Assert.Equal(2, index);

            index = 0;
            Assert.Equal("+++", (parser.Parse("+++", ref index) as TestOperInfo).Oper);
            Assert.Equal(3, index);
        }
    }
}
