﻿using System;
using Xunit;

namespace EvaluateExpression.Operations.Strategy.Tests
{
    
    public class BooleanAndTests
    {
        [Fact]
        public void BooleanAndTest() {
            BooleanAnd oper = new BooleanAnd();
            Assert.True(oper.Perform(true.ToValue(), true.ToValue(), null).AsBool);
            Assert.False(oper.Perform(false.ToValue(), true.ToValue(), null).AsBool);
            Assert.False(oper.Perform(true.ToValue(), false.ToValue(), null).AsBool);
            Assert.False(oper.Perform(false.ToValue(), false.ToValue(), null).AsBool);
        }

        [Fact]
        public void BooleanAndLazyCalcTest() {
            BooleanAnd oper = new BooleanAnd();
            Assert.False(oper.Perform(false.ToValue(), new FailOperand(), null).AsBool);
        }
    }
}
