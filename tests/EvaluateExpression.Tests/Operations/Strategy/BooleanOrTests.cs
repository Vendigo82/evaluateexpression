﻿using System;
using Xunit;

namespace EvaluateExpression.Operations.Strategy.Tests
{
    
    public class BooleanOrTests
    {
        [Fact]
        public void BooleanOrTest() {
            BooleanOr oper = new BooleanOr();
            Assert.True(oper.Perform(true.ToValue(), true.ToValue(), null).AsBool);
            Assert.True(oper.Perform(false.ToValue(), true.ToValue(), null).AsBool);
            Assert.True(oper.Perform(true.ToValue(), false.ToValue(), null).AsBool);
            Assert.False(oper.Perform(false.ToValue(), false.ToValue(), null).AsBool);
        }

        [Fact]
        public void BooleanOrLazyCalcTest() {
            BooleanOr oper = new BooleanOr();
            Assert.True(oper.Perform(true.ToValue(), new FailOperand(), null).AsBool);
        }
    }
}
