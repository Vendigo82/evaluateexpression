﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Operations.Strategy.Tests {
    using Values;

    /// <summary>
    /// Summary description for OperationStrategyTests
    /// </summary>
    
    public class OperationStrategyTests {        

        [Fact]
        public void ArithmeticOperationTests() {
            Plus plus = new Plus();
            Check(1, plus.Perform(null, new IntValue(1)));
            Check(3, plus.Perform(new IntValue(2), new IntValue(1)));
            Check(3.5, plus.Perform(new IntValue(2), new DoubleValue(1.5)));
            Check(3.5, plus.Perform(new DoubleValue(2.5), new IntValue(1)));
            Check(4.0, plus.Perform(new DoubleValue(2.5), new DoubleValue(1.5)));

            Minus minus = new Minus();
            Check(-1, minus.Perform(null, new IntValue(1)));
            Check(1, minus.Perform(new IntValue(2), new IntValue(1)));
            Check(0.5, minus.Perform(new IntValue(2), new DoubleValue(1.5)));
            Check(1.5, minus.Perform(new DoubleValue(2.5), new IntValue(1)));
            Check(1.0, minus.Perform(new DoubleValue(2.5), new DoubleValue(1.5)));

            Multiply mp = new Multiply();
            Check(6, mp.Perform(new IntValue(2), new IntValue(3)));
            Check(3.0, mp.Perform(new IntValue(2), new DoubleValue(1.5)));
            Check(2.5, mp.Perform(new DoubleValue(2.5), new IntValue(1)));
            Check(3.75, mp.Perform(new DoubleValue(2.5), new DoubleValue(1.5)));

            Divide div = new Divide();
            Check(0.5, div.Perform(new IntValue(2), new IntValue(4)));
            Check(2.0, div.Perform(new IntValue(2), new DoubleValue(1)));
            Check(2.5, div.Perform(new DoubleValue(2.5), new IntValue(1)));
            Check(4.0, div.Perform(new DoubleValue(6), new DoubleValue(1.5)));
        }

        [Fact]
        public void StringOperationsTest() {
            Plus p = new Plus();
            Assert.Equal("qwerty", p.Perform(new StringValue("qw"), new StringValue("erty")).AsString);
        }

        [Fact]
        public void EqualOperationTest() {
            Equal eq = new Equal();
            Assert.True(eq.Perform(NullValue.Null, NullValue.Null).AsBool);
            Assert.False(eq.Perform(NullValue.Null, new IntValue(0)).AsBool);
            Assert.False(eq.Perform(new IntValue(0), NullValue.Null).AsBool);

            Assert.True(eq.Perform(new IntValue(1), new IntValue(1)).AsBool);
            Assert.True(eq.Perform(new IntValue(1), new DoubleValue(1)).AsBool);
            Assert.True(eq.Perform(new DoubleValue(1), new IntValue(1)).AsBool);
            Assert.True(eq.Perform(new DoubleValue(1), new DoubleValue(1)).AsBool);

            Assert.False(eq.Perform(new IntValue(1), new IntValue(2)).AsBool);
            Assert.False(eq.Perform(new IntValue(1), new DoubleValue(2)).AsBool);
            Assert.False(eq.Perform(new DoubleValue(1), new IntValue(2)).AsBool);
            Assert.False(eq.Perform(new DoubleValue(1), new DoubleValue(2)).AsBool);

            Assert.True(eq.Perform(BoolValue.True, new IntValue(1)).AsBool);
            Assert.True(eq.Perform(BoolValue.True, BoolValue.True).AsBool);
            Assert.True(eq.Perform(BoolValue.False, BoolValue.False).AsBool);
            Assert.False(eq.Perform(BoolValue.False, BoolValue.True).AsBool);

            Assert.True(eq.Perform(new StringValue("qwerty"), new StringValue("qwerty")).AsBool);
            Assert.False(eq.Perform(new StringValue("qwerty"), new StringValue("qwerty1")).AsBool);

            Assert.False(eq.Perform(new StringValue("qwerty"), new IntValue(2)).AsBool);
            Assert.False(eq.Perform(new IntValue(2), new StringValue("qwerty1")).AsBool);
            Assert.False(eq.Perform(new DoubleValue(1), new StringValue("qwerty1")).AsBool);
        }

        [Fact]
        public void NotEqualOperationTest() {
            NotEqual neq = new NotEqual();
            Assert.False(neq.Perform(NullValue.Null, NullValue.Null).AsBool);
            Assert.True(neq.Perform(NullValue.Null, new IntValue(0)).AsBool);
            Assert.True(neq.Perform(new IntValue(0), NullValue.Null).AsBool);

            Assert.False(neq.Perform(new IntValue(1), new IntValue(1)).AsBool);
            Assert.False(neq.Perform(new IntValue(1), new DoubleValue(1)).AsBool);
            Assert.False(neq.Perform(new DoubleValue(1), new IntValue(1)).AsBool);
            Assert.False(neq.Perform(new DoubleValue(1), new DoubleValue(1)).AsBool);

            Assert.True(neq.Perform(new IntValue(1), new IntValue(2)).AsBool);
            Assert.True(neq.Perform(new IntValue(1), new DoubleValue(2)).AsBool);
            Assert.True(neq.Perform(new DoubleValue(1), new IntValue(2)).AsBool);
            Assert.True(neq.Perform(new DoubleValue(1), new DoubleValue(2)).AsBool);

            Assert.False(neq.Perform(BoolValue.True, new IntValue(1)).AsBool);
            Assert.False(neq.Perform(BoolValue.True, BoolValue.True).AsBool);
            Assert.False(neq.Perform(BoolValue.False, BoolValue.False).AsBool);
            Assert.True(neq.Perform(BoolValue.False, BoolValue.True).AsBool);

            Assert.False(neq.Perform(new StringValue("qwerty"), new StringValue("qwerty")).AsBool);
            Assert.True(neq.Perform(new StringValue("qwerty"), new StringValue("qwerty1")).AsBool);

            Assert.True(neq.Perform(new StringValue("qwerty"), new IntValue(2)).AsBool);
            Assert.True(neq.Perform(new IntValue(2), new StringValue("qwerty1")).AsBool);
            Assert.True(neq.Perform(new DoubleValue(1), new StringValue("qwerty1")).AsBool);
        }

        private void Check(int expected, IValue result) {
            Assert.Equal(expected, result.AsInt);
            Assert.Equal(ValueType.Integer, result.Type);
        }

        private void Check(double expected, IValue result) {
            Assert.Equal(expected, result.AsDouble, 5);
            Assert.Equal(ValueType.Decimal, result.Type);
        }
    }
}
