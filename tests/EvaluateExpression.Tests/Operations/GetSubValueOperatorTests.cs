﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Operations.Tests
{
    using Values;
    using Operands;
    using EvaluateExpression.Parser;
    using EvaluateExpression.Operations.Parser;
    using EvaluateExpression.Operations.Factory;
    using EvaluateExpression.Functions;
    using EvaluateExpression.Exceptions;

    /// <summary>
    /// Summary description for GetSubValueOperatorTests
    /// </summary>
    
    public class GetSubValueOperatorTests
    {

        [Fact]
        public void GetSubValueOperatorTest()
        {
            StructValue val = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 10.ToValue()),
                new KeyValuePair<string, IValue>("B", true.ToValue())
            });

            Assert.Equal(10, new GetSubValueOperator(val, new VariableOperand("A")).Eval(null).AsInt);
            Assert.True(new GetSubValueOperator(val, new VariableOperand("B")).Eval(null).AsBool);
            Assert.Throws<EvalComplexSubValueNotFound>(() => new GetSubValueOperator(val, new VariableOperand("C")).Eval(null));
            Assert.Throws<EvalCastToComplexException>(() => new GetSubValueOperator(new StaticOperand(10), new VariableOperand("A")).Eval(null));
        }

        [Fact]
        public void GetSubValueOperatorExpressionTest() {
            StructValue valInner = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 1.ToValue()),
                new KeyValuePair<string, IValue>("B", 2.ToValue())
            });


            StructValue val = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 10.ToValue()),
                new KeyValuePair<string, IValue>("B", true.ToValue()),
                new KeyValuePair<string, IValue>("C", 20.ToValue()),
                new KeyValuePair<string, IValue>("inner", valInner)
            });
            INameResolver context = new ConstNameResolver("val", val);


            DefOpersFactory opers = OperationsPack.CreateDefFactory();
            opers.Add('.', new GetSubValueOperatorInfo(OperationsPack.SUB_VALUE));

            SimpleFunctionFactory funcFactory = new SimpleFunctionFactory();
            funcFactory.Add("func", new FunctionSignatureZeroArg(() => val));

            IParser parser = new Parser(operations:opers, functionFactory: funcFactory);
            Assert.Equal(30, parser.Parse("val.A + val.C").Eval(context).AsInt);
            Assert.Equal(15, parser.Parse("(val).A + 5").Eval(context).AsInt);
            Assert.Equal(13, parser.Parse("val.inner.A + val.inner.B + val.A").Eval(context).AsInt);
            Assert.Equal(2, parser.Parse("func().inner.B").Eval(context).AsInt);
        }

    }
}
