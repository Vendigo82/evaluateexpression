﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;

namespace EvaluateExpression.Operations.Tests
{
    using Functions;
    using Values;
    using EvaluateExpression.Parser;
    using EvaluateExpression.Operations.Factory;
    using EvaluateExpression.Exceptions;

    /// <summary>
    /// Summary description for IndexOperationTests
    /// </summary>
    
    public class IndexOperationTests
    {
        
        [Fact]
        public void IndexOperationTest() {
            ArrayValue val = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                30.ToValue()
            });

            Assert.Equal(10, new IndexOperation(val, 0.ToValue()).Eval(null).AsInt);
            Assert.Equal(20, new IndexOperation(val, 1.ToValue()).Eval(null).AsInt);
            Assert.Throws<EvalIndexOutOfRangeException>(() => new IndexOperation(val, 3.ToValue()).Eval(null));
            Assert.Throws<EvalCastToArrayException>(() => new IndexOperation(10.ToValue(), 0.ToValue()).Eval(null));
        }

        [Fact]
        public void IndexOperationParserTest()
        {
            ArrayValue valA = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                30.ToValue()
            });

            INameResolver context = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", valA),
                new KeyValuePair<string, IValue>("B", 10.ToValue())
            });

            SimpleFunctionFactory funcFactory = new SimpleFunctionFactory();
            funcFactory.Add("func", new FunctionSignatureZeroArg(() => valA));

            IParser parser = new Parser(functionFactory: funcFactory, enableIndexOperation: true);
            Assert.Equal(10, parser.Parse("A[0]").Eval(context).AsInt);
            Assert.Equal(60, parser.Parse("A[0] + A[1] + A[2]").Eval(context).AsInt);
            Assert.Equal(20, parser.Parse("A[5-4]").Eval(context).AsInt);
            Assert.Throws<EvalIndexOutOfRangeException>(() => parser.Parse("A[4]").Eval(context).AsInt);
            Assert.Equal(10, parser.Parse("func()[0]").Eval(context).AsInt);
            Assert.Throws<EvalCastToArrayException>(() => parser.Parse("B[0]").Eval(context).AsInt);
        }

        [Fact]
        public void IndexOperationWithComplexTypeTest() {
            StructValue arrInner = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 15.ToValue()),
            });


            ArrayValue valA = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                arrInner
            });

            StructValue valB = new StructValue(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", 1.ToValue()),
                new KeyValuePair<string, IValue>("B", valA),
            });

            INameResolver context = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", valA),
                new KeyValuePair<string, IValue>("B", valB)
            });

            DefOpersFactory opers = OperationsPack.CreateDefFactory();
            opers.Add('.', new GetSubValueOperatorInfo(OperationsPack.SUB_VALUE));
            IParser parser = new Parser(operations: opers, enableIndexOperation: true);

            Assert.Equal(15, parser.Parse("A[2].A").Eval(context).AsInt);
            Assert.Equal(15, parser.Parse("B.B[2].A").Eval(context).AsInt);
            Assert.Equal(10, parser.Parse("B.B[0]").Eval(context).AsInt);
            Assert.Equal(20, parser.Parse("A[0] + B.B[0]").Eval(context).AsInt);
        }

        [Fact]
        public void IndexOperationFailWithoutOperation()
        {
            ArrayValue valA = new ArrayValue(new IValue[] {
                10.ToValue(),
                20.ToValue(),
                30.ToValue()
            });

            INameResolver context = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("A", valA),
            });

            IParser parser = new Parser();
            Assert.Throws<ParserException>(() => parser.Parse("A[0]").Eval(context).AsInt);

            parser = new Parser(enableIndexOperation: true);
            Assert.Throws<ParserException>(() => parser.Parse("A[0").Eval(context).AsInt);
        }
    }
}
