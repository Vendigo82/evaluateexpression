﻿using FluentAssertions;
using System;
using Xunit;

namespace EvaluateExpression.Tests
{
    
    public class StringExtentionsTests
    {
        [Fact]
        public void StringExtentions_IsChars() {
            string str = "123456";
            Assert.True(str.IsChars('1', '2', 0));
            Assert.False(str.IsChars('1', '1', 0));
            Assert.False(str.IsChars('2', '2', 0));
            Assert.True(str.IsChars('2', '3', 1));
            Assert.False(str.IsChars('6', '6', 5));
        }

        [Fact]
        public void StringExtentions_IsNewLine() {
            string str = "123" + Environment.NewLine + "456";
            int shift;
            Assert.True(str.IsNewLine(3, out shift));
            Assert.Equal('4', str[3 + shift]);

            if (shift == 2)
            {
                Assert.True(str.IsNewLine(4, out shift), $"Simbol {str[4]} is not new line");
                Assert.Equal('4', str[4 + shift]);
            }

            Assert.False(str.IsNewLine(0, out shift));
            Assert.False(str.IsNewLine(str.Length - 1, out shift));
        }

        [Fact]
        public void StringExtentions_IndexToLineAndPosition()
        {
            string str = "123" + Environment.NewLine + "456" + Environment.NewLine + "789";

            int index;
            int position;
            index = str.IndexOf('1');
            Assert.Equal(0, str.IndexToLineAndPosition(index, out position));
            Assert.Equal(0, position);

            index = str.IndexOf('6');
            Assert.Equal(1, str.IndexToLineAndPosition(index, out position));
            Assert.Equal(2, position);

            index = str.IndexOf('8');
            Assert.Equal(2, str.IndexToLineAndPosition(index, out position));
            Assert.Equal(1, position);

            Assert.Throws<ArgumentOutOfRangeException>(() => str.IndexToLineAndPosition(str.Length, out position));
        }

        [Fact]
        public void StringExtentions_IsLineCommentaryBegins() {
            string str = "1//123/4/";
            int shift;
            Assert.False(str.IsLineCommentaryBegins(0, out shift));

            Assert.True(str.IsLineCommentaryBegins(1, out shift));
            Assert.Equal(2, shift);

            Assert.Equal('/', str[6]);
            Assert.False(str.IsLineCommentaryBegins(6, out shift));

            Assert.Equal('/', str[8]);
            Assert.False(str.IsLineCommentaryBegins(8, out shift));
        }

        [Fact]
        public void StringExtentions_SeekNextLine() {
            string str = "123" + Environment.NewLine + "456";
            int indexOfNewLine = str.IndexOf('4');

            for (int i = 0; i < indexOfNewLine; ++i)
                Assert.Equal(indexOfNewLine, str.SeekNextLine(i));

            for (int i = indexOfNewLine; i < str.Length; ++i)
                Assert.Equal(str.Length, str.SeekNextLine(i));
        }

        [Theory]
        [InlineData("abc", 'a', 0, true)]
        [InlineData("abc", 'a', 1, false)]
        [InlineData("abc", 'b', 0, false)]
        [InlineData("abc", 'b', 1, true)]
        [InlineData("abc", 'b', 2, false)]
        [InlineData("abc", 'c', 2, true)]
        [InlineData("abc", 'c', 3, false)]  // out of bounds
        public void IsCharTests(string str, char ch, int index, bool expectedResult)
        {
            // setup

            // action
            var result = str.IsChar(ch, index);

            // asserts
            result.Should().Be(expectedResult);
        }
    }
}
