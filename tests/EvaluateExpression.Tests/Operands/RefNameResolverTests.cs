﻿using System;
using Xunit;

using EvaluateExpression.Asserts;
using Moq;
using FluentAssertions;
using AutoFixture.Xunit2;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;

namespace EvaluateExpression.Operands.Tests
{
    
    public class RefNameResolverTests
    {
        readonly Mock<INameResolver> internalMock = new();

        [Theory]
        [InlineAutoMockData(null)]
        [InlineAutoMockData]
        public void DeclareTest(IValue value, string name, string declaredName)
        {
            // setup
            var nr = new RefNameResolver(internalMock.Object, name);

            // action
            nr.DeclareVariable(declaredName, value);

            // asserts
            internalMock.Verify(f => f.DeclareVariable(declaredName, value), Times.Once);
        }

        [Theory, AutoData]
        public void DeclareWithSameNameTest(string name)
        {
            // setup
            RefNameResolver nr = new (internalMock.Object, name);

            // action
            Action action = () => nr.DeclareVariable(name);

            // asserts
            action.Should().ThrowExactly<Exceptions.DeclareVariableException>();
        }

        [Theory]
        [InlineData(10, "a")]
        [InlineData(20, "b")]
        [InlineData(30, "x")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var nrOrigin = new ConstNameResolver() {
                { "a", 10.ToValue() },
                { "b", 20.ToValue() }
            };

            var nr = new RefNameResolver(nrOrigin, "x", 30.ToValue());

            // action
            var result = nr.Property(name);

            // asserts
            if (expected != null)
                result.Should().NotBeNull().And.Be(expected.Value);
            else
                result.Should().BeNull();
        }

        [Theory, AutoMockData]
        public void BaseContextShouldBeNullTest(INameResolver baseContext, string name)
        {
            // setup
            var target = new RefNameResolver(baseContext, name);

            // action && asserts
            target.BaseContext.Should().BeSameAs(baseContext);
        }
    }
}
