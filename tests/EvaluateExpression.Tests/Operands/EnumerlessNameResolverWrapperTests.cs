﻿using System;
using System.Linq;
using Xunit;
using EvaluateExpression.Asserts;
using FluentAssertions;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;

namespace EvaluateExpression.Operands.Tests
{
    
    public class EnumerlessNameResolverWrapperTests
    {
        private readonly INameResolver origin;
        private readonly INameResolver wrapped;

        public EnumerlessNameResolverWrapperTests() {
            origin = new ConstNameResolver() {
                { "a", 1.ToValue() },
                { "b", 2.ToValue() },
                { "c", 3.ToValue() }
            };
            wrapped = new EnumerlessNameResolverWrapper(origin);
        }

        [Fact]
        public void EnumerlessNameResolverWrapper_TestResolve() {
            int cnt = 0;
            foreach (var pair in origin) {
                Assert.Equal(pair.Value.AsInt, wrapped.Resolve(pair.Key).AsInt);
                cnt += 1;
            }
            Assert.Equal(3, cnt);
        }

        [Fact]
        public void EnumerlessNameResolverWrapper_TestEnumerate() {
            Assert.Empty(wrapped);
        }

        [Theory]
        [InlineData(10, "a")]
        [InlineData(20, "b")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var nrOrigin = new ConstNameResolver() {
                { "a", 10.ToValue() },
                { "b", 20.ToValue() }
            };

            var nr = new EnumerlessNameResolverWrapper(nrOrigin);

            // action
            var result = nr.Property(name);

            // asserts
            if (expected != null)
                result.Should().NotBeNull().And.Be(expected.Value);
            else
                result.Should().BeNull();
        }

        [Theory, AutoMockData]
        public void BaseContextShouldBeNullTest(INameResolver baseContext)
        {
            // setup
            var target = new EnumerlessNameResolverWrapper(baseContext);

            // action && asserts
            target.BaseContext.Should().BeSameAs(baseContext);
        }
    }
}
