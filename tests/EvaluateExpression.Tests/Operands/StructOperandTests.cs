﻿using EvaluateExpression.Operands;
using EvaluateExpression.Values;
using FluentAssertions;
using Moq;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.Tests.Operands
{
    public class StructOperandTests
    {
        [Theory, AutoMockData]
        public void EvaluateTest(IEnumerable<(string name, Mock<IOperand> operand, IValue result)> properties, INameResolver context)
        {
            // setup
            foreach (var p in properties)
                p.operand.Setup(f => f.Eval(context)).Returns(p.result);

            var target = new StructOperand(properties.Select(i => KeyValuePair.Create(i.name, i.operand.Object)));

            // action
            var result = target.Eval(context);

            // asserts
            result.Should()
                .BeOfType<StructValue>().Which.ToArray().Should()
                .BeEquivalentTo(properties.Select(i => KeyValuePair.Create(i.name, i.result)));
        }
    }
}
