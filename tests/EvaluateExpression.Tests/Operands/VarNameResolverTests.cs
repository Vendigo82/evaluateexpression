﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using FluentAssertions;
using EvaluateExpression.Asserts;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;

namespace EvaluateExpression.Operands.Tests {

    /// <summary>
    /// Summary description for VarNameResolverTests
    /// </summary>
    
    public class VarNameResolverTests {

        [Theory, AutoMockData]
        public void AssignWithoutAutoDeclateTest(string name, IValue value) 
        {
            // setup
            VarNameResolver nr = new ();

            // action
            var result = nr.Assign(name, value);

            // asserts
            result.Should().BeFalse();
            nr.Property(name).Should().BeNull();
        }

        [Theory, AutoMockData]
        public void AssignAlreadyExistedVariableTest(string name, IValue oldValue, IValue newValue)
        {
            // setup
            VarNameResolver nr = new(name, oldValue);

            // action
            var result = nr.Assign(name, newValue);

            // asserts
            result.Should().BeTrue();
            nr.Property(name).Should().BeSameAs(newValue);
        }

        [Theory, AutoMockData]
        public void EnumerateTest(IEnumerable<KeyValuePair<string, IValue>> values) 
        {
            // setup
            VarNameResolver nr = new(values);

            // action
            var names = nr.ToList();

            // asserts
            names.Should().BeEquivalentTo(values);
        }

        [Theory]
        [InlineAutoMockData(null)]
        [InlineAutoMockData]
        public void DeclareNewVariableTest(IValue value, string name) 
        {
            // setup
            VarNameResolver nr = new();

            // action
            var result = nr.DeclareVariable(name, value);

            // asserts
            result.Should().BeTrue();
            var property = nr.Property(name);
            if (value != null)
                property.Should().BeSameAs(value);
            else
                property.Should().NotBeNull().And.Subject.Type.Should().Be(ValueType.Null);
        }

        [Theory]
        [InlineAutoMockData(null)]
        [InlineAutoMockData]
        public void DeclareAlreadyExistedTest(IValue newValue, IValue oldValue, string name)
        {
            // setup
            VarNameResolver nr = new(name, oldValue);

            // action
            var result = nr.DeclareVariable(name, newValue);

            // asserts
            result.Should().BeFalse();
            var property = nr.Property(name);
            if (newValue != null)
                property.Should().BeSameAs(newValue);
            else
                property.Should().BeSameAs(oldValue);
        }

        [Theory]
        [InlineData(10, "a")]
        [InlineData(20, "b")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var nr = new VarNameResolver() {
                { "a", 10.ToValue() },
                { "b", 20.ToValue() }
            };

            // action
            var result = nr.Property(name);

            // asserts
            if (expected != null)
                result.Should().NotBeNull().And.Be(expected.Value);
            else
                result.Should().BeNull();
        }

        [Fact]
        public void BaseContextShouldBeNullTest()
        {
            // setup
            var target = new VarNameResolver();

            // action && asserts
            target.BaseContext.Should().BeNull();
        }
    }
}
