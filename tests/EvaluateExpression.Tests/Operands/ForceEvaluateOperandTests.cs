﻿using EvaluateExpression.Values;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvaluateExpression.Operands.Tests
{
    public class ForceEvaluateOperandTests
    {
        [Fact]
        public void EnumerableArrayTest()
        {
            // setup
            var array = new ArrayPatternValue(new EnumerableValueStrategy(new[] { 1, 2, 3 }.Select(i => i.ToValue())));
            var operand = new ForceEvaluateOperand(array);

            // action
            var result = operand.Eval(null);

            // asserts
            result.Should()
                .NotBeSameAs(array).And
                .BeOfType<ArrayValue>().Which
                .Array.Should().BeOfType<ArrayValueStrategy>();
        }

        [Fact]
        public void ArrayTest()
        {
            // setup
            var array = new ArrayPatternValue(new ArrayValueStrategy(new[] { 1, 2, 3 }.Select(i => i.ToValue()).ToArray()));
            var operand = new ForceEvaluateOperand(array);

            // action
            var result = operand.Eval(null);

            // asserts
            result.Should().BeSameAs(array);
        }
    }
}
