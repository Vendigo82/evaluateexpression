﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using EvaluateExpression.Exceptions;
using FluentAssertions;
using EvaluateExpression.Asserts;

namespace EvaluateExpression.Operands.Tests {
    /// <summary>
    /// Summary description for ConstNameResolverTests
    /// </summary>
    
    public class ConstNameResolverTests {
        [Fact]
        public void ConstNameResolverEnumerateTest()
        {
            var orig = new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("X1", new Values.IntValue(10)),
                new KeyValuePair<string, IValue>("A1", new Values.IntValue(20))
            };

            ConstNameResolver nr = new ConstNameResolver(orig);
            Assert.Equal(orig.OrderBy(p => p.Key).ToArray(), nr.OrderBy(p => p.Key).ToArray());
        }

        [Fact]
        public void ConstNameResolver_DeclareTest()
        {
            ConstNameResolver nr = new ConstNameResolver();
            Assert.Throws<EvalInvalidOperationException>(() => nr.DeclareVariable("test"));
        }

        [Theory]
        [InlineData(10, "a")]
        [InlineData(20, "b")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var nr = new ConstNameResolver() {
                { "a", 10.ToValue() },
                { "b", 20.ToValue() }
            };

            // action
            var result = nr.Property(name);

            // asserts
            if (expected != null)
                result.Should().NotBeNull().And.Be(expected.Value);
            else
                result.Should().BeNull();
        }

        [Fact]
        public void BaseContextShouldBeNullTest()
        {
            // setup
            var target = new ConstNameResolver();

            // action && asserts
            target.BaseContext.Should().BeNull();
        }
    }
}
