﻿using EvaluateExpression.Operands;
using EvaluateExpression.Values;
using FluentAssertions;
using Moq;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EvaluateExpression.Tests.Operands
{
    public class ArrayOperandTests
    {
        [Theory, AutoMockData]
        public void EvaluateTest(IEnumerable<(Mock<IOperand> operand, IValue result)> properties, INameResolver context)
        {
            // setup
            foreach (var p in properties)
                p.operand.Setup(f => f.Eval(context)).Returns(p.result);

            var target = new ArrayOperand(properties.Select(i => i.operand.Object));

            // action
            var result = target.Eval(context);

            // asserts
            IEnumerable<IValue> list = result.Should().BeOfType<ArrayValue>().Which;
            list.Should().Equal(properties.Select(i => i.result));
        }
    }
}
