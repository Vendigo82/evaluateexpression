﻿using System;
using System.Text;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using EvaluateExpression.Asserts;
using FluentAssertions;
using Objectivity.AutoFixture.XUnit2.AutoMoq.Attributes;

namespace EvaluateExpression.Operands.Tests {
    /// <summary>
    /// Summary description for CombineNameResolverTests
    /// </summary>
    
    public class CombineNameResolverTests {

        [Fact]
        public void CombineNameResolverTest() {
            INameResolver r1 = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("P1", 10.ToValue()), new KeyValuePair<string, IValue>("P", 1.ToValue())
            });
            INameResolver r2 = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("P2", 20.ToValue()), new KeyValuePair<string, IValue>("P", 2.ToValue())
            });
            INameResolver r3 = new ConstNameResolver(new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("P3", 30.ToValue()), new KeyValuePair<string, IValue>("P", 3.ToValue())
            });

            Check(new CombineNameResolver(r1, r2, r3), 1, true, true, true);
            Check(new CombineNameResolver(r2, r1, r3), 2, true, true, true);
            Check(new CombineNameResolver(r3, r2, r1), 3, true, true, true);
            Check(new CombineNameResolver(r1, r2), 1, true, true, false);
            Check(new CombineNameResolver(r2, r1), 2, true, true, false);
            Check(new CombineNameResolver(new INameResolver[] { r1, r2, r3}), 1, true, true, true);
            Check(new CombineNameResolver(new INameResolver[] { r2, r1, r3 }), 2, true, true, true);
            Check(new CombineNameResolver(new INameResolver[] { r3, r1, r2 }), 3, true, true, true);
            Check(CombineNameResolver.Combine(r1, r2), 1, true, true, false);
            Check(CombineNameResolver.Combine(r2, r1), 2, true, true, false);
            Assert.Same(r1, CombineNameResolver.Combine(r1, null));
            Assert.Same(r1, CombineNameResolver.Combine(null, r1));
            Assert.Null(CombineNameResolver.Combine(null, null));
        }

        private void Check(INameResolver r, int pval, bool r1, bool r2, bool r3) {
            Assert.Equal(pval, r.Resolve("P").AsInt);
            if (r1)
                Assert.Equal(10, r.Resolve("P1").AsInt);
            if (r2)
                Assert.Equal(20, r.Resolve("P2").AsInt);
            if (r3)
                Assert.Equal(30, r.Resolve("P3").AsInt);
        }

        [Fact]
        public void CombineNameResolverIteratortest() {
            var orig1 = new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("X1", new Values.IntValue(10)),
                new KeyValuePair<string, IValue>("A1", new Values.IntValue(20))
            };
            var orig2 = new KeyValuePair<string, IValue>[] {
                new KeyValuePair<string, IValue>("X2", new Values.IntValue(2)),
                new KeyValuePair<string, IValue>("A2", new Values.IntValue(3))
            };

            INameResolver nr = CombineNameResolver.Combine(new ConstNameResolver(orig1), new ConstNameResolver(orig2));
            Assert.Equal(new string[] { "A1", "A2", "X1", "X2" }, nr.Select(p => p.Key).OrderBy(p => p).ToArray());
        }

        [Theory]
        [InlineData(10, "a")]
        [InlineData(20, "b")]
        [InlineData(30, "c")]
        [InlineData(null, "unknown")]
        public void Property_Test(int? expected, string name)
        {
            // setup
            var nr1 = new ConstNameResolver() {
                { "a", 10.ToValue() },
                { "b", 20.ToValue() }
            };

            var nr2 = new ConstNameResolver() {
                { "c", 30.ToValue() },                
            };

            var nr = new CombineNameResolver(new[] { nr1, nr2 });

            // action
            var result = nr.Property(name);

            // asserts
            if (expected != null)
                result.Should().NotBeNull().And.Be(expected.Value);
            else
                result.Should().BeNull();
        }

        [Theory, AutoMockData]
        public void BaseContextShouldBeNullTest(INameResolver baseContext1, INameResolver baseContext2)
        {
            // setup
            var target = new CombineNameResolver(baseContext1, baseContext2);

            // action && asserts
            target.BaseContext.Should().BeSameAs(baseContext1);
        }
    }
}
