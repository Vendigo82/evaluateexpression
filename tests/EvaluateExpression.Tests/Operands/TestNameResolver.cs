﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operands
{
    public class TestNameResolver : INameResolver
    {
        public enum Operation {
            Assign, Declare, Resolve
        }

        public string Name { get; private set; }
        public IValue Value { get; set; }
        public Operation Oper { get; private set; }
        public bool DeclareResult { get; set; } = true;

        public string Description => "";

        public IContext BaseContext => throw new NotImplementedException();

        public bool Assign(string name, IValue value) {
            Name = name;
            Value = value;
            return true;
        }

        public bool DeclareVariable(string name, IValue value = null) {
            Name = name;
            Value = value;
            return DeclareResult;
        }

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            throw new NotImplementedException();
        }

        public IValue Resolve(string name) {
            Name = name;
            return Value;
        }

        public IValue Property(string name) => Resolve(name);

        IEnumerator IEnumerable.GetEnumerator() {
            throw new NotImplementedException();
        }
    }
}
