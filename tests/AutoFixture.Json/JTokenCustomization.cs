﻿using AutoFixture;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFixture
{
    public class JTokenCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<JToken>(c => c.FromFactory(() => new JValue(fixture.Create<string>())));

            //fixture.Customize<JValue>(c => c.FromFactory(() => new JValue(fixture.Create<string>())));
            //fixture.Customize<JValue>(c => c.)

            fixture.Customize<JObject>(c => c.FromFactory(() => {
                var jo = new JObject();
                foreach (var p in fixture.CreateMany<KeyValuePair<string, JToken>>())
                    jo.Add(p.Key, p.Value);
                return jo;
            }));

            fixture.Customize<JArray>(c => c.FromFactory(() => {
                var ja = new JArray();
                foreach (var item in fixture.CreateMany<JToken>())
                    ja.Add(item);
                return ja;
            }));
        }
    }
}
