﻿using AutoFixture.Xunit2;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoFixture.Xunit2
{  
    public class JTokenAutoDataAttribute : AutoDataAttribute
    {
        public JTokenAutoDataAttribute() : base (CreateFixture)
        {
        }


        public static Fixture CreateFixture()
        {
            var fixture = new Fixture();
            fixture.Customize(new JTokenCustomization());            

            return fixture;
        }
    }

    public class JTokenInlineAutoDataAttribute : InlineAutoDataAttribute
    {
        public JTokenInlineAutoDataAttribute(params object[] parameters) 
            : base(new JTokenAutoDataAttribute(), parameters) { }
    }

    // Not working, bug in MemberAutoDataAttribute
    // see: https://github.com/AutoFixture/AutoFixture/pull/1164
    //public class JTokenMemberAutoDataAttribute : MemberAutoDataAttribute
    //{
    //    public JTokenMemberAutoDataAttribute(string memberName, params object[] parameters)
    //        : base(new JTokenAutoDataAttribute(), memberName, parameters) { }
    //}
}
