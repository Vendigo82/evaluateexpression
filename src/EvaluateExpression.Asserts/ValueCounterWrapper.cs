﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression
{
    public class ValueCounterWrapper : IValue {
        private readonly IValue _value;

        public ValueCounterWrapper(IValue value) {
            _value = value;
        }

        public int Count { get; set; } = 0;

        public bool IsNull => _value.IsNull;
        public bool IsNumeric => _value.IsNumeric;
        public bool IsString => _value.IsString;
        public bool IsDateTime => _value.IsDateTime;
        public ValueType Type => _value.Type;
        public bool AsBool => _value.AsBool;
        public int AsInt { get {
                Count += 1;
                return _value.AsInt;
            }
        }
        public double AsDouble => _value.AsDouble;
        public string AsString => _value.AsString;
        public DateTime AsDateTime => _value.AsDateTime;
        public object Value => _value.Value;

        public string Description => _value.Description;

        public IValue Eval(INameResolver context) {
            return _value.Eval(context);
        }

        public IValue Property(string name) => null;

        public string ToString(string format, IFormatProvider provider) {
            return ToString();
        }
    }
}
