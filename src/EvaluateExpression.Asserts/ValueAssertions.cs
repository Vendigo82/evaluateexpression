﻿using FluentAssertions;
using FluentAssertions.Execution;
using FluentAssertions.Primitives;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EvaluateExpression.Values;
using EvaluateExpression.Json;

namespace EvaluateExpression.Asserts
{
    public static class ValueAssertionsExtensions
    {
        public static ValueAssertions Should(this IValue instance)
        {
            return new ValueAssertions(instance);
        }
    }

    public class ValueAssertions : ReferenceTypeAssertions<IValue, ValueAssertions>
    {
        public ValueAssertions(IValue subject) : base(subject)
        {
        }

        protected override string Identifier => "IValue";

        public AndConstraint<ValueAssertions> BeComplexValue()
        {
            Execute.Assertion
                .ForCondition(Subject.Type == ValueType.Unknown)
                .FailWith("Expected value type is {0}, but is {1}", ValueType.Unknown, Subject.Type)
                .Then
                .ForCondition(Subject is IComplexValue)
                .FailWith("Value expected to be inherited IComplexValue interface, but not");

            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> BeArrayValue()
        {
            Execute.Assertion
                .ForCondition(Subject.Type == ValueType.Unknown)
                .FailWith("Expected value type is {0}, but is {1}", ValueType.Unknown, Subject.Type)
                .Then
                .ForCondition(Subject is IArrayValue)
                .FailWith("Value expected to be inherited from IArrayValue interface, but not");

            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(int expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsInt == expected)
                .FailWith("Expected 'AsInt' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(double expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsDouble == expected)
                .FailWith("Expected 'AsDouble' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(long expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsInt == expected)
                .FailWith("Expected 'AsInt' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(string expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsString == expected)
                .FailWith("Expected 'AsString' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(bool expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsBool == expected)
                .FailWith("Expected 'AsBool' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(Guid expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsString == expected.ToString())
                .FailWith("Expected 'AsString' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(DateTime expected)
        {
            Execute.Assertion
                .ForCondition(Subject.AsDateTime == expected)
                .FailWith("Exected 'AsDateTime' value is {0}, but is {1}; Value type is {2}", expected, Subject.Value, Subject.Type);
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> Be(JToken jtoken)
        {
            Execute.Assertion
                .Given(() => Subject as IJTokenContainer)
                .ForCondition(c => c != null)
                .FailWith("Value is not implements IJTokenContainer")
                .Then
                .ForCondition(c => JToken.DeepEquals(c.JToken, jtoken))
                .FailWith("Json value is not DeepEquals to expected value.\r\nCurrentValue:\r\n{0}\r\nExpected:\r\n{1}", (c) => c.JToken, (c) => jtoken);

            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> BeNullValue()
        {
            Execute.Assertion
                .ForCondition(Subject.IsNull == true)
                .FailWith("Exected 'IsNull' is true, not; Value type is {0}", Subject.Type)
                .Then
                .ForCondition(Subject.GetType() == typeof(NullValue))
                .FailWith("Expected value of type 'NullValue', but found is {0}", Subject.GetType());
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndConstraint<ValueAssertions> HaveProperties(IEnumerable<string> properties)
        {
            Execute.Assertion
                .Given(() => Subject as IComplexValue)
                .ForCondition(v => v != null)
                .FailWith("Value is not implements IComplexValue and does not contains any property")
                .Then
                .ForCondition((v) => properties.OrderBy(i => i).SequenceEqual(v.Select(i => i.Key).OrderBy(i => i)))
                .FailWith("Value list is not contains the same list of properties.\r\nActual:\r\n{0}\r\nExpected:\r\n{1}", 
                    (v) => "[ " + string.Join(", ", v.Select(i => i.Key)) + " ]",
                    (v) => "[ " + string.Join(", ", properties + " ]"));
            return new AndConstraint<ValueAssertions>(this);
        }

        public AndWhichConstraint<ValueAssertions, IValue> HaveProperty(string propertyName)
        {
            Execute.Assertion
                .Given(() => Subject.Property(propertyName))
                .ForCondition(p => p != null)
                .FailWith("Value expected to has property with name '{0}' but not.\r\nProperties list:\r\n{1}", propertyName);

            return new AndWhichConstraint<ValueAssertions, IValue>(this, Subject.Property(propertyName));
        }
    }    
}
