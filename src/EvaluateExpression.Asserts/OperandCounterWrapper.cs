﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression
{
    public class OperandCounterWrapper : IOperand
    {
        private readonly IOperand _operand;

        public OperandCounterWrapper(IOperand operand) {
            _operand = operand;
        }

        public int Count { get; private set; } = 0;

        public IValue Eval(INameResolver context) {
            Count += 1;
            return _operand.Eval(context);
        }
    }
}
