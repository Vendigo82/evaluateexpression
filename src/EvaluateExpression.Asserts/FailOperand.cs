﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression
{
    public class FailOperand : IOperand
    {
        public IValue Eval(INameResolver context) {
            throw new NotImplementedException();
        }
    }

    public class FailValue : IValue
    {
        public bool IsNull => throw new NotImplementedException();

        public bool IsNumeric => throw new NotImplementedException();

        public bool IsString => throw new NotImplementedException();

        public bool IsDateTime => throw new NotImplementedException();

        public ValueType Type => throw new NotImplementedException();

        public bool AsBool => throw new NotImplementedException();

        public int AsInt => throw new NotImplementedException();

        public double AsDouble => throw new NotImplementedException();

        public string AsString => throw new NotImplementedException();

        public DateTime AsDateTime => throw new NotImplementedException();

        public object Value => throw new NotImplementedException();

        public string Description => nameof(FailValue);

        public IValue Eval(INameResolver context) {
            throw new NotImplementedException();
        }

        public string ToString(string format, IFormatProvider provider) {
            return ToString();
        }

        public IValue Property(string name) => null;
    }
}
