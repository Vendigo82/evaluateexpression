﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression
{
    public class ActionOperand : IOperand
    {
        private readonly Func<INameResolver, IValue> _func;

        public ActionOperand(Func<INameResolver, IValue> func) {
            _func = func ?? throw new ArgumentNullException(nameof(func));
        }

        public IValue Eval(INameResolver context) {
            return _func(context);
        }
    }
}
