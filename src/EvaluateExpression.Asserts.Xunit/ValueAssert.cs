﻿using System;
using Xunit;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression
{
    public class ValueAssertException : Exception
    {
        public ValueAssertException(string message) : base(message)
        {
        }
    }

    public static class ValueAssert
    {
        public static void CheckAsAccessors(IValue val, bool AsBool, bool AsInt, bool AsDouble, bool AsString, bool AsDateTime) {
            try {
                var v = val.AsBool;
                if (!AsBool)
                    throw new ValueAssertException("Test fail");
            } catch (EvalInvalidValueTypeException) {
                if (AsBool)
                    throw;
            }

            try {
                var v = val.AsInt;
                if (!AsInt)
                    throw new ValueAssertException("Test fail");
            } catch (EvalInvalidValueTypeException) {
                if (AsInt)
                    throw;
            }

            try {
                var v = val.AsDouble;
                if (!AsDouble)
                    throw new ValueAssertException("Test fail");
            } catch (EvalInvalidValueTypeException) {
                if (AsDouble)
                    throw;
            }

            try {
                var v = val.AsString;
                if (!AsString)
                    throw new ValueAssertException("Test fail");
            } catch (EvalInvalidValueTypeException) {
                if (AsString)
                    throw;
            }

            try {
                var v = val.AsDateTime;
                if (!AsDateTime)
                    throw new ValueAssertException("Test fail");
            } catch (EvalInvalidValueTypeException) {
                if (AsDateTime)
                    throw;
            }
        }

        public static void CheckIsProps(IValue val, bool IsNull, bool IsNumeric, bool IsString, bool IsDateTime, ValueType t) {
            Assert.Equal(IsNull, val.IsNull);
            Assert.Equal(IsNumeric, val.IsNumeric);
            Assert.Equal(IsString, val.IsString);
            Assert.Equal(IsDateTime, val.IsDateTime);
            Assert.Equal(t, val.Type);
        }

        public static void CheckInt(int expected, IValue val) {
            Assert.True(val.IsNumeric);
            Assert.Equal(ValueType.Integer, val.Type);
            Assert.Equal(expected, val.AsInt);
        }

        public static void CheckDouble(double expected, IValue val) {
            Assert.True(val.IsNumeric);
            Assert.Equal(ValueType.Decimal, val.Type);
            Assert.Equal(expected, val.AsDouble);
        }

        public static void CheckBool(bool expected, IValue val) {
            Assert.True(val.IsNumeric);
            Assert.Equal(ValueType.Boolean, val.Type);
            Assert.Equal(expected, val.AsBool);
        }

        public static void CheckString(string expected, IValue val) {
            Assert.True(val.IsString);
            Assert.Equal(ValueType.String, val.Type);
            Assert.Equal(expected, val.AsString);
        }

        public static void CheckDateTime(DateTime expected, IValue val) {
            Assert.True(val.IsDateTime);
            Assert.Equal(ValueType.DateTime, val.Type);
            Assert.Equal(expected, val.AsDateTime);
        }
    }
}
