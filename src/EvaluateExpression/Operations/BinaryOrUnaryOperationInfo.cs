﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Operations {
    public class BinaryOrUnaryOperationInfo : OperationInfoBase {
        public interface ICreator {
            IOperand create(IOperand a, IOperand b);
            IOperand create(IOperand b);
        }

        private ICreator m_creator;

        public BinaryOrUnaryOperationInfo(int priority, ICreator creator) : base(priority) {
            m_creator = creator;
        }

        public override IOperand Compile(IOperandStack operands) {
            IOperand b = operands.extract();
            if (operands.empty)
                return m_creator.create(b);

            IOperand a = operands.extract();
            return m_creator.create(a, b);
        }
    }
}
