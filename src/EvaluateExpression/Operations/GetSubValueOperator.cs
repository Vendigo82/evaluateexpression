﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Operations
{
    using Operands;

    /// <summary>
    /// Get subvalue from IComplexOperand
    /// </summary>
    public class GetSubValueOperator : IOperand
    {
        private readonly IOperand _left;
        private readonly INamedOperand _right;

        public GetSubValueOperator(IOperand left, INamedOperand right) {
            _left = left;
            _right = right;
        }

        public IValue Eval(INameResolver context) {
            IValue leftValue = _left.Eval(context);
            if (leftValue is IComplexValue) {
                IValue result = ((IComplexValue)leftValue).GetSubValue(_right.Name);
                if (result == null)
                    throw new EvalComplexSubValueNotFound(leftValue, _right.Name);
                return result;
            } else
                throw new EvalCastToComplexException(leftValue);
        }
    }

    public class GetSubValueOperatorInfo : IOperationInfo
    {
        public GetSubValueOperatorInfo(int prior = OperationsPack.SUB_VALUE) {
            priority = prior;
        }

        public int priority { get; }

        public IOperand Compile(IOperandStack operands) {
            IOperand b = operands.extract();
            IOperand a = operands.extract();
            if (b is INamedOperand)
                return new GetSubValueOperator(a, (INamedOperand)b);
            else
                throw new MissedNameException(String.Concat("Expected some name at this position, but accepted ", b.ToString()), 0);
        }
    }
}
