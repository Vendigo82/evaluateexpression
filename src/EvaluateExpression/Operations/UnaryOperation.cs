﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations {
    public class UnaryOperation : IOperand {
        private readonly IUnaryOperationStrategy m_strategy;
        private readonly IOperand m_a;

        public UnaryOperation(IUnaryOperationStrategy strategy, IOperand a) {
            m_strategy = strategy;
            m_a = a;
        }

        public IValue Eval(INameResolver context) {
            return m_strategy.Perform(m_a.Eval(context));
        }

        public class Creator<T> : UnaryOperationInfo.ICreator where T : IUnaryOperationStrategy, new() {
            private readonly T m_strategy = new T();

            public IOperand Create(IOperand a) {
                return new UnaryOperation(m_strategy, a);
            }
        }
    }
}
