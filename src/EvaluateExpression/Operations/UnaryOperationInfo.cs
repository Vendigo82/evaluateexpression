﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Operations {
    /// <summary>
    /// Unary operation class
    /// </summary>
    public class UnaryOperationInfo : OperationInfoBase {
        /// <summary>
        /// Unary operation create method
        /// </summary>
        public interface ICreator {
            IOperand Create(IOperand a);
        }

        private readonly ICreator m_creator;

        public UnaryOperationInfo(int priority, ICreator creator) : base(priority) {
            m_creator = creator;
        }

        public override IOperand Compile(IOperandStack operands) {
            IOperand a = operands.extract();
            return m_creator.Create(a);
        }
    }
}
