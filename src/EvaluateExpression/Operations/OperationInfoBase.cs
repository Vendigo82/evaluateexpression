﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operands;

namespace EvaluateExpression.Operations {
    public abstract class OperationInfoBase : IOperationInfo {
        private readonly int m_priority;

        public OperationInfoBase(int priority) {
            m_priority = priority;
        }

        public int priority {
            get {
                return m_priority;
            }
        }

        public abstract IOperand Compile(IOperandStack operands);
    }
}
