﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using Values;

    public class Plus : IBinaryOperationStrategy {
        public IValue Perform(IValue a, IValue b) {
            if (a != null) {
                if (a.IsNumeric && b.IsNumeric) {
                    if (a.IsFloatPoint() || b.IsFloatPoint())
                        return new DoubleValue(a.AsDouble + b.AsDouble);
                    else
                        return new IntValue(a.AsInt + b.AsInt);
                }
                else if (a.IsString && b.IsString)
                    return new StringValue(a.AsString + b.AsString);
                else
                    throw new ArgumentException(String.Concat("Cannot perform plus operation on types: ",
                        a.Type.ToString(), " and ", b.Type.ToString()));
            }
            else
                return b;
        }
    }
}
