﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using EvaluateExpression.Exceptions;
    using Values;

    /// <summary>
    /// operation >=
    /// </summary>
    public class GreaterOrEqual : IBinaryOperationStrategy {
        public IValue Perform(IValue a, IValue b) {
            if (a.IsInteger() && b.IsInteger())
                return a.AsInt >= b.AsInt ? BoolValue.True : BoolValue.False;
            else if (a.IsNumeric && b.IsNumeric)
                return a.AsDouble >= b.AsDouble ? BoolValue.True : BoolValue.False;
            else if (a.IsDateTime && b.IsDateTime)
                return a.AsDateTime >= b.AsDateTime ? BoolValue.True : BoolValue.False;
            else
                throw new EvalInvalidOperationException(String.Concat("Cannot perform operation <GreaterOrEqual> on ", a.ToString(), " and ", b.ToString()));
        }
    }
}
