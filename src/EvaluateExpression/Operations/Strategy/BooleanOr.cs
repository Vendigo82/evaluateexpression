﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using Values;

    /// <summary>
    /// operation 'or'
    /// </summary>
    public class BooleanOr : IBinaryOperationExStrategy {
        public IValue Perform(IOperand a, IOperand b, INameResolver context) {
            if (a.Eval(context).AsBool)
                return BoolValue.True;
            else
                return b.Eval(context).AsBool.ToValue();
        }
    }
}
