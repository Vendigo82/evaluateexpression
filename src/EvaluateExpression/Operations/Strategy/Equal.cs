﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Operations.Strategy
{
    public class Equal : IBinaryOperationStrategy {
        public IValue Perform(IValue a, IValue b) {
            return IsEqual(a, b) ? Values.BoolValue.True : Values.BoolValue.False;
        }

        internal static bool IsEqual(IValue a, IValue b) {
            if (a.IsNull && b.IsNull)
                return true;
            else if (a.IsNull || b.IsNull)
                return false;
            else {
                if (a.IsNumeric && b.IsNumeric) {
                    if (a.IsFloatPoint() || b.IsFloatPoint())
                        return a.AsDouble == b.AsDouble ? true : false;
                    else
                        return a.AsInt == b.AsInt ? true : false;
                }
                else if (a.IsString && b.IsString)
                    return a.AsString == b.AsString ? true : false;
                else 
                    return a.Equals(b);  
            }
        }
    }

    public class NotEqual : IBinaryOperationStrategy {
        public IValue Perform(IValue a, IValue b) {
            return Equal.IsEqual(a, b) ? Values.BoolValue.False : Values.BoolValue.True;
        }
    }
}
