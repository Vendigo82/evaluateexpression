﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using Values;

    /// <summary>
    /// operation 'not'
    /// </summary>
    public class BooleanNot : IUnaryOperationStrategy {
        public IValue Perform(IValue a) {
            return a.AsBool ? BoolValue.False : BoolValue.True;
        }
    }
}
