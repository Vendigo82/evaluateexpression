﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using Values;

    public class BooleanAnd : IBinaryOperationExStrategy {
        /// <summary>
        /// operation 'and'
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public IValue Perform(IOperand a, IOperand b, INameResolver context) {
            if (a.Eval(context).AsBool)
                return b.Eval(context).AsBool.ToValue();
            else
                return BoolValue.False;
        }
    }
}
