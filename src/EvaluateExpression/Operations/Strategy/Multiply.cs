﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using Values;

    public class Multiply : IBinaryOperationStrategy {
        public IValue Perform(IValue a, IValue b) {
            if (a.IsFloatPoint() || b.IsFloatPoint())
                return new DoubleValue(a.AsDouble * b.AsDouble);
            else
                return new IntValue(a.AsInt * b.AsInt);
        }
    }
}
