﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations.Strategy {
    using Values;

    public class Divide : IBinaryOperationStrategy {
        public IValue Perform(IValue a, IValue b) {
            return new DoubleValue(a.AsDouble / b.AsDouble);
        }
    }
}
