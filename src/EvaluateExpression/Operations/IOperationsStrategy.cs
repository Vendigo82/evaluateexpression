﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations {
    public interface IBinaryOperationStrategy {
        IValue Perform(IValue a, IValue b);
    }

    public interface IBinaryOperationExStrategy
    {
        IValue Perform(IOperand a, IOperand b, INameResolver context);
    }

    public interface IUnaryOperationStrategy {
        IValue Perform(IValue a);
    }
}
