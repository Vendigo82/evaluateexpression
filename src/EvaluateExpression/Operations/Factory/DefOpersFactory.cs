﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Operations.Factory
{
    public class DefOpersFactory : IOperationFactory {
        readonly private IDictionary<char, LinkedList<KeyValuePair<string, IOperationInfo>>> _operations = new Dictionary<char, LinkedList<KeyValuePair<string, IOperationInfo>>>();

        public void Add(string operation, IOperationInfo info) 
        {
            LinkedList<KeyValuePair<string, IOperationInfo>> opers;
            if (_operations.TryGetValue(operation[0], out opers)) {
                LinkedListNode<KeyValuePair<string, IOperationInfo>> node = opers.First;
                while (node != null) {
                    if (node.Value.Key == operation)
                        throw new ArgumentException("Dublicate operation " + operation);

                    if (node.Value.Key == null && operation.Length == 1)
                        throw new ArgumentException("Dublicate operation " + operation);

                    if (node.Value.Key == null || node.Value.Key.Length <= operation.Length) {
                        opers.AddBefore(node, new KeyValuePair<string, IOperationInfo>(operation.Length == 1 ? null : operation, info));
                        return;
                    }
                    node = node.Next;
                }
                opers.AddLast(new KeyValuePair<string, IOperationInfo>(operation.Length == 1 ? null : operation, info));
            }
            else {
                opers = new LinkedList<KeyValuePair<string, IOperationInfo>>();
                opers.AddLast(new KeyValuePair<string, IOperationInfo>(operation.Length == 1 ? null : operation, info));
                _operations.Add(operation[0], opers);
            }
        }

        public void Add(char operation, IOperationInfo info) {
            LinkedList<KeyValuePair<string, IOperationInfo>> opers;
            if (_operations.TryGetValue(operation, out opers)) {
                if (opers.Last.Value.Key == null)
                    throw new ArgumentException("Dublicate operation " + operation);

                opers.AddLast(new KeyValuePair<string, IOperationInfo>(null, info));
            }
            else {
                opers = new LinkedList<KeyValuePair<string, IOperationInfo>>();
                opers.AddLast(new KeyValuePair<string, IOperationInfo>(null, info));
                _operations.Add(operation, opers);
            }
        }

        public IOperationInfo Parse(string str, ref int index) {
            LinkedList<KeyValuePair<string, IOperationInfo>> opers;
            if (_operations.TryGetValue(str[index], out opers)) {
                foreach (KeyValuePair<string, IOperationInfo> pair in opers) {
                    if (pair.Key == null) {
                        index += 1;
                        return pair.Value;
                    }

                    if (IsEqual(str, index, pair.Key)) {
                        index = index + pair.Key.Length;
                        return pair.Value;
                    }
                }
            }

            return null;
        }

        private bool IsEqual(string str, int index, string operation) {
            for (int i = 0; i < operation.Length; ++i) {
                if (index + i < str.Length) {
                    if (str[index + i] != operation[i])
                        return false;
                }
                else
                    return false;
            }
            return true;
        }
    }
}
