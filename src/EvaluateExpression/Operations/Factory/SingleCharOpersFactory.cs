﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Operations.Factory
{
    public class SingleCharOpersFactory : IOperationFactory {
        readonly private IDictionary<char, IOperationInfo> _operations;

        public SingleCharOpersFactory(IDictionary<char, IOperationInfo> operations) {
            _operations = operations;
        }

        public IOperationInfo Parse(string str, ref int index) {
            IOperationInfo result;
            if (_operations.TryGetValue(str[index], out result)) {
                index = index + 1;
                return result;
            }
            else
                return null;
        }
    }
}
