﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations
{
    public class BinaryOperationEx : IOperand
    {
        private readonly IBinaryOperationExStrategy m_strategy;
        private readonly IOperand m_a;
        private readonly IOperand m_b;

        public BinaryOperationEx(IBinaryOperationExStrategy strategy, IOperand a, IOperand b) {
            m_strategy = strategy;
            m_a = a;
            m_b = b;
        }

        public IValue Eval(INameResolver context) {
            return m_strategy.Perform(m_a, m_b, context);
        }

        public class CreatorBinary<T> : BinaryOperationInfo.ICreator where T : IBinaryOperationExStrategy, new()
        {
            private readonly T m_strategy = new T();

            public IOperand create(IOperand a, IOperand b) {
                return new BinaryOperationEx(m_strategy, a, b);
            }
        }        
    }
}
