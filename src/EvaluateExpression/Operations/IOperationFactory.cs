﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Operations
{
    public interface IOperationFactory {
        /// <summary>
        /// Detect operation in <paramref name="str"/> and move string index <paramref name="index"/>.
        /// If operation not found, then returns null
        /// </summary>
        /// <param name="str">expression string</param>
        /// <param name="index">current index</param>
        /// <returns>Operation info or NULL, if not found</returns>
        IOperationInfo Parse(string str, ref int index);
    }
}
