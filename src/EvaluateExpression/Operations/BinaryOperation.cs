﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations {
    using Values;

    public class BinaryOperation : IOperand {
        private readonly IBinaryOperationStrategy m_strategy;
        private readonly IOperand m_a;
        private readonly IOperand m_b;

        public BinaryOperation(IBinaryOperationStrategy strategy, IOperand a, IOperand b) {
            m_strategy = strategy;
            m_a = a;
            m_b = b;
        }

        public IValue Eval(INameResolver context) {
            if (m_a != null)
                return m_strategy.Perform(m_a.Eval(context), m_b.Eval(context));
            else
                return m_strategy.Perform(null, m_b.Eval(context));
        }

        public class CreatorBinary<T> : BinaryOperationInfo.ICreator where T : IBinaryOperationStrategy, new() {
            private readonly T m_strategy = new T();

            public IOperand create(IOperand a, IOperand b) {
                return new BinaryOperation(m_strategy, a, b);
            }
        }

        public class CreatorBinaryOrUnary<T> : BinaryOrUnaryOperationInfo.ICreator 
            where T : IBinaryOperationStrategy, new() {
            private readonly T m_strategy = new T();

            public IOperand create(IOperand a, IOperand b) {
                return new BinaryOperation(m_strategy, a, b);
            }

            public IOperand create(IOperand b) {
                return new BinaryOperation(m_strategy, null, b);
            }
        }
    }
}
