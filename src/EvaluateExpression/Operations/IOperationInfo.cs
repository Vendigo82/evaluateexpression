﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operands;

namespace EvaluateExpression.Operations {
    /// <summary>
    /// information about operation
    /// </summary>
    public interface IOperationInfo {
        /// <summary>
        /// operation priority
        /// </summary>
        int priority { get; }

        /// <summary>
        /// extract operands from stack and create new operand with current operation
        /// </summary>
        /// <param name="operands"></param>
        /// <returns></returns>
        IOperand Compile(IOperandStack operands);
    }
}
