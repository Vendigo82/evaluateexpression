﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operations {
    using Operands;

    public class AssignOperator : IOperand {
        private readonly VariableOperand _a;
        private readonly IOperand _b;

        public AssignOperator(VariableOperand a, IOperand b) {
            _a = a;
            _b = b;
        }

        public IValue Eval(INameResolver context) {
            IValue value = _b.Eval(context);
            if ( !context.Assign(_a.Name, value))
                throw new KeyNotFoundException(String.Concat("Variable <", _a.Name, "> not found for assigment"));
            return value;
        }
    }

    public class AssignOperatorInfo : IOperationInfo {
        public AssignOperatorInfo(int prior = OperationsPack.ASSIGN) {
            priority = prior;
        }

        public int priority { get; }

        public IOperand Compile(IOperandStack operands) {
            IOperand b = operands.extract();
            IOperand a = operands.extract();
            if (a is VariableOperand)
                return new AssignOperator((VariableOperand)a, b);
            else
                throw new InvalidCastException("Assign operation's left operand must be VariableOperand");
        }
    }
}
