﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Operations
{
    public class IndexOperation : IOperand
    {
        private readonly IOperand _left;
        private readonly IOperand _index;

        public IndexOperation(IOperand left, IOperand index) {
            _left = left;
            _index = index;
        }

        public IValue Eval(INameResolver context) {
            IValue val = _left.Eval(context);
            if (val is IIndexableValue) {
                IValue index = _index.Eval(context);
                try {
                    return ((IIndexableValue)val)[index];
                } catch (IndexOutOfRangeException e) {
                    throw new EvalIndexOutOfRangeException(e.Message, e);
                } catch (ArgumentOutOfRangeException e) {
                    throw new EvalIndexOutOfRangeException(e.Message, e);
                }
            } else
                throw new EvalCastToArrayException(val);
        }
    }

    public class IndexOperationInfo : IOperationInfo
    {
        public IndexOperationInfo(int prior) {
            priority = prior;
        }

        public int priority { get; }

        public IOperand Compile(IOperandStack operands) {
            IOperand b = operands.extract();
            IOperand a = operands.extract();
            return new IndexOperation(a, b);
        }
    }
}
