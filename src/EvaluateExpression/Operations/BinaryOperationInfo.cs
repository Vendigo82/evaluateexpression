﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Operations {
    /// <summary>
    /// Binary operation class info
    /// </summary>
    public class BinaryOperationInfo : OperationInfoBase {
        /// <summary>
        /// Binary operation create method
        /// </summary>
        public interface ICreator {
            IOperand create(IOperand a, IOperand b);
        }

        private readonly ICreator m_creator;

        public BinaryOperationInfo(int priority, ICreator creator) : base(priority) {
            m_creator = creator;
        }

        public override IOperand Compile(IOperandStack operands) {
            IOperand b = operands.extract();
            IOperand a = operands.extract();
            return m_creator.create(a, b);
        }
    }
}
