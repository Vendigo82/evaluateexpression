﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression {
    /// <summary>
    /// simple class for NameResolver
    /// only for constants values
    /// Cannot be assign. If try to assign existed value, then throw exception
    /// </summary>
    public class ConstNameResolver : INameResolver {
        protected readonly Dictionary<string, IValue> m_values = new Dictionary<string, IValue>();

        public string Description => ToString();

        #region Constructors and add variables
        public ConstNameResolver() {
        }

        public ConstNameResolver(IEnumerable<KeyValuePair<string, IValue>> values) {
            Add(values);
        }

        public ConstNameResolver(string name, IValue value) {
            Add(name, value);
        }

        /// <summary>
        /// add new const value
        /// </summary>
        public void Add(string name, IValue value) {
            m_values[name] = value;
        }

        public void Add(IEnumerable<KeyValuePair<string, IValue>> values) {
            foreach (var pair in values)
                Add(pair.Key, pair.Value);
        }
        #endregion

        #region INameResolver
        public IValue Resolve(string name) {
            IValue value;
            if (m_values.TryGetValue(name, out value))
                return value;
            else
                return null;
        }

        public IValue Property(string name) => Resolve(name);

        public bool Assign(string name, IValue value) {
            if (m_values.ContainsKey(name))
                throw new EvalInvalidOperationException(String.Concat("Can't assign value to const variable [", name, "]"));
            return false;
        }

        public bool DeclareVariable(string name, IValue value = null) => throw new EvalInvalidOperationException($"Can't declare new variable in static context '{Description}'");

        public IContext BaseContext => null;
        #endregion 

        #region Enumerator region
        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return m_values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
        #endregion
    }
}
