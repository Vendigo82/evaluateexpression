﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression
{
    public interface IPropertyNavigation
    {
        /// <summary>
        /// Returns property value by name or null, if property with this name not found
        /// </summary>
        /// <param name="name">Property name</param>
        /// <returns>Value or null</returns>
        IValue Property(string name);

        /// <summary>
        /// Get value description
        /// </summary>
        string Description { get; }
    }

    public static class PropertyNavigationExtensions
    {
        /// <summary>
        /// Get property value or throws exception if not found
        /// </summary>
        /// <param name="container"></param>
        /// <param name="name">property name</param>
        /// <returns>Value</returns>
        /// <exception cref="EvalValueNotFoundException">Throws if property with this name not found</exception>
        public static IValue PropertyRequired(this IPropertyNavigation container, string name)
            => container.Property(name) ?? throw new EvalValueNotFoundException(container, name);

        /// <summary>
        /// Get property value or throws exception if not found
        /// </summary>
        /// <param name="container"></param>
        /// <param name="name">property name</param>
        /// <returns>Value</returns>
        /// <exception cref="EvalComplexSubValueNotFound">Throws if property with this name not found</exception>
        public static IValue PropertyRequired(this IValue value, string name)
            => value.Property(name) ?? throw new EvalComplexSubValueNotFound(value, name);
    }
}
