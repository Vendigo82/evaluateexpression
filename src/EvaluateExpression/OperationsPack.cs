﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operations;
using EvaluateExpression.Operations.Strategy;
using EvaluateExpression.Operations.Factory;

namespace EvaluateExpression {
    public static class OperationsPack {

        //operations priority
        public const int SUB_VALUE = 50;
        public const int INDEX = 50;

        public const int MULTIPLY = 15;
        public const int PLUS_MINUS = 10;

        public const int GREATER_LESS = 8;
        public const int EQUAL = 7;

        public const int AND = 5;
        public const int OR = 4;
        public const int NOT = 3;

        public const int ASSIGN = 1;

        public static IDictionary<char, IOperationInfo> Create() {
            Dictionary<char, IOperationInfo> opers = new Dictionary<char, IOperationInfo>();
            opers.Add('+', new BinaryOrUnaryOperationInfo(PLUS_MINUS, new BinaryOperation.CreatorBinaryOrUnary<Plus>()));
            opers.Add('-', new BinaryOrUnaryOperationInfo(PLUS_MINUS, new BinaryOperation.CreatorBinaryOrUnary<Minus>()));
            opers.Add('*', new BinaryOperationInfo(MULTIPLY, new BinaryOperation.CreatorBinary<Multiply>()));
            opers.Add('/', new BinaryOperationInfo(MULTIPLY, new BinaryOperation.CreatorBinary<Divide>()));

            opers.Add('&', new BinaryOperationInfo(AND, new BinaryOperationEx.CreatorBinary<BooleanAnd>()));
            opers.Add('|', new BinaryOperationInfo(OR, new BinaryOperationEx.CreatorBinary<BooleanOr>()));
            opers.Add('!', new UnaryOperationInfo(NOT, new UnaryOperation.Creator<BooleanNot>()));
            opers.Add('>', new BinaryOperationInfo(GREATER_LESS, new BinaryOperation.CreatorBinary<Greater>()));
            opers.Add('<', new BinaryOperationInfo(GREATER_LESS, new BinaryOperation.CreatorBinary<Less>()));

            opers.Add('=', new BinaryOperationInfo(EQUAL, new BinaryOperation.CreatorBinary<Equal>()));

            return opers;
        }

        public static DefOpersFactory CreateDefFactory() {
            DefOpersFactory parser = new DefOpersFactory();
            parser.Add('+', new BinaryOrUnaryOperationInfo(PLUS_MINUS, new BinaryOperation.CreatorBinaryOrUnary<Plus>()));
            parser.Add('-', new BinaryOrUnaryOperationInfo(PLUS_MINUS, new BinaryOperation.CreatorBinaryOrUnary<Minus>()));
            parser.Add('*', new BinaryOperationInfo(MULTIPLY, new BinaryOperation.CreatorBinary<Multiply>()));
            parser.Add('/', new BinaryOperationInfo(MULTIPLY, new BinaryOperation.CreatorBinary<Divide>()));

            parser.Add("&&", new BinaryOperationInfo(AND, new BinaryOperationEx.CreatorBinary<BooleanAnd>()));
            parser.Add("||", new BinaryOperationInfo(OR, new BinaryOperationEx.CreatorBinary<BooleanOr>()));
            parser.Add('!', new UnaryOperationInfo(NOT, new UnaryOperation.Creator<BooleanNot>()));
            parser.Add('>', new BinaryOperationInfo(GREATER_LESS, new BinaryOperation.CreatorBinary<Greater>()));
            parser.Add('<', new BinaryOperationInfo(GREATER_LESS, new BinaryOperation.CreatorBinary<Less>()));
            parser.Add(">=", new BinaryOperationInfo(GREATER_LESS, new BinaryOperation.CreatorBinary<GreaterOrEqual>()));
            parser.Add("<=", new BinaryOperationInfo(GREATER_LESS, new BinaryOperation.CreatorBinary<LessOrEqual>()));

            parser.Add("=", new BinaryOperationInfo(EQUAL, new BinaryOperation.CreatorBinary<Equal>()));
            parser.Add("!=", new BinaryOperationInfo(EQUAL, new BinaryOperation.CreatorBinary<NotEqual>()));
            return parser;
        }

        public static DefOpersFactory CreateDefFactory(bool assign) {
            DefOpersFactory parser = CreateDefFactory();

            if (assign)
                parser.Add(":=", new AssignOperatorInfo(ASSIGN));

            return parser;
        }

        public static DefOpersFactory CreateDefFactory(IEnumerable<KeyValuePair<string, IOperationInfo>> extraOpers) {
            DefOpersFactory parser = CreateDefFactory();

            foreach (var pair in extraOpers)
                parser.Add(pair.Key, pair.Value);

            return parser;
        }
    }
}
