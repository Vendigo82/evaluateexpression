﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions
{
    public static class IFunctionSignatureExtentions
    {
        public const int VariableArgsCount = -1;

        static public bool IsVariableArgsCount(this IFunctionSignature f) {
            return f.ArgCount == VariableArgsCount;
        }

        static private void ThrowMissArgException(this IFunctionSignature func) {
            throw new ArgumentException(String.Concat("Not enough arguments at function ", func.ToString()));
        }

        /// <summary>
        /// Extract first Operand form operands list
        /// </summary>
        public static IOperand GetFirst(this IFunctionSignature func, IEnumerable<IOperand> operands, out IEnumerator<IOperand> iter) {
            iter = operands.GetEnumerator();
            if (!iter.MoveNext())
                func.ThrowMissArgException();
            return iter.Current;
        }

        /// <summary>
        /// Extract next operand from operands list
        /// </summary>
        public static IOperand GetNext(this IFunctionSignature func, IEnumerator<IOperand> iter) {
            if (!iter.MoveNext())
                func.ThrowMissArgException();
            return iter.Current;
        }

        public static IValue GetFirst(this IFunctionSignature func, IEnumerable<IOperand> operands, out IEnumerator<IOperand> iter, INameResolver context) {
            IOperand arg = GetFirst(func, operands, out iter);
            return arg.Eval(context);
        }

        public static IOperand GetSingle(this IFunctionSignature func, IEnumerable<IOperand> operands) {
            IEnumerator<IOperand> iter;
            return func.GetFirst(operands, out iter);
        }

        public static IValue GetSingle(this IFunctionSignature func, IEnumerable<IOperand> operands, INameResolver context) {
            return func.GetSingle(operands).Eval(context);
        }

        public static IValue GetNext(this IFunctionSignature func, IEnumerator<IOperand> iter, INameResolver context) {
            IOperand arg = GetNext(func, iter);
            return arg.Eval(context);
        }

        public static void GetArgs(this IFunctionSignature func, IEnumerable<IOperand> operands, INameResolver context, out IValue arg1, out IValue arg2) {
            IEnumerator<IOperand> iter;
            arg1 = func.GetFirst(operands, out iter, context);
            arg2 = func.GetNext(iter, context);
        }

        /// <summary>
        /// Call perform without arguments
        /// </summary>
        public static IValue Perform(this IFunctionSignature func, INameResolver context) {
            return func.Perform(Enumerable.Empty<IValue>(), context);
        }

        /// <summary>
        /// Call perform with one argument
        /// </summary>
        public static IValue Perform(this IFunctionSignature func, IOperand oper, INameResolver context) {
            return func.Perform(Enumerable.Repeat(oper, 1), context);
        }

        /// <summary>
        /// Call perform with two argument
        /// </summary>
        public static IValue Perform(this IFunctionSignature func, IOperand oper, IOperand oper2, INameResolver context) {
            return func.Perform(new IOperand[] { oper, oper2 }, context);
        }

        /// <summary>
        /// Call perform with three argument
        /// </summary>
        public static IValue Perform(this IFunctionSignature func, IOperand oper, IOperand oper2, IOperand oper3, INameResolver context) {
            return func.Perform(new IOperand[] { oper, oper2, oper3 }, context);
        }
    }

}
