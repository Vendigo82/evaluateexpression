﻿using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EvaluateExpression.Functions
{
    public static class RegexFunctions
    {
        /// <summary>
        /// Returns list of string functions
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions()
        {
            yield return new KeyValuePair<string, IFunctionSignature>("RegexMatchesExt", new FunctionSignature2Arg(RegexMatchesExt));
            yield return new KeyValuePair<string, IFunctionSignature>("RegexMatches", new FunctionSignature2Arg(RegexMatches));
            yield return new KeyValuePair<string, IFunctionSignature>("RegexMatch", new FunctionSignature2Arg(RegexMatch));
            yield return new KeyValuePair<string, IFunctionSignature>("IsRegexMatch", new FunctionSignature2Arg(IsRegexMatch));
        }

        /// <summary>
        /// Perform regex matches and returns arrays or groups' values (array of array)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static IValue RegexMatchesExt(IValue input, IValue pattern)
        {
            var regex = new Regex(pattern.AsString);
            var result = regex.Matches(input.AsString);

            var groupsList = new List<IValue>();
            foreach (var capture in result.Cast<Match>().Where(i => i.Success)) {

                var itemValue = new List<IValue>();
                for  (int index = 1; index < capture.Groups.Count; ++index) {
                    var group = capture.Groups[index];
                    itemValue.Add(group.Success ? group.Value.ToValue() : NullValue.Null);
                }

                groupsList.Add(new ArrayValue(itemValue));
            }

            return new ArrayPatternValue(new ListValueStrategy(groupsList));
        }

        /// <summary>
        /// Perform regex matches and returns array of first group's values (array of values)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static IValue RegexMatches(IValue input, IValue pattern)
        {
            var regex = new Regex(pattern.AsString);
            var result = regex.Matches(input.AsString);

            var valuesList = new List<IValue>();
            foreach (var capture in result.Cast<Match>().Where(i => i.Success)) {
                if (capture.Groups.Count > 1)
                    valuesList.Add(capture.Groups[1].Value.ToValue());
            }

            return new ArrayPatternValue(new ListValueStrategy(valuesList));
        }

        /// <summary>
        /// Perform regex match and returns structure:
        /// {
        ///    "Success": true|false,
        ///    "Groups": [ group values ]
        /// }
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static IValue RegexMatch(IValue input, IValue pattern)
        {
            var regex = new Regex(pattern.AsString);
            var result = regex.Match(input.AsString);

            var itemValue = new List<IValue>();
            for (int index = 1; index < result.Groups.Count; ++index) {
                var group = result.Groups[index];
                itemValue.Add(group.Success ? group.Value.ToValue() : NullValue.Null);
            }

            var resultValue = new StructValue {
                { "Success", BoolValue.Get(result.Success) },
                { "Groups", new ArrayValue(itemValue) }
            };

            return resultValue;
        }

        /// <summary>
        /// Perform regex match and returns match result: true or false
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static IValue IsRegexMatch(IValue input, IValue pattern)
        {
            var regex = new Regex(pattern.AsString);
            var result = regex.Match(input.AsString);
            return BoolValue.Get(result.Success);
        }
    }
}
