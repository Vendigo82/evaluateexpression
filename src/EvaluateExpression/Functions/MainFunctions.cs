﻿using System;
using System.Collections.Generic;

namespace EvaluateExpression.Functions
{
    public static class MainFunctions
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions()
        {
            yield return new KeyValuePair<string, IFunctionSignature>("GenGuid", new FunctionSignatureZeroArg(GenGuid));
            
        }

        public static IValue GenGuid() => Guid.NewGuid().ToString().ToValue();
    }
}
