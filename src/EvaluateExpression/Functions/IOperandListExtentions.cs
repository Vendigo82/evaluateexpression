﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Functions
{
    public static class IOperandListExtentions
    {
        public static RefNameResolver GetTmpArgs(
            this IFunctionSignature func, IEnumerable<IOperand> operands, INameResolver context,
            out IOperand first, out IOperand operation, out IEnumerator<IOperand> iter) {

            first = func.GetFirst(operands, out iter, context);
            INamedOperand refName = func.GetNext(iter) as INamedOperand;
            if (refName == null)
                throw new EvalInvalidValueTypeException(String.Concat(func.ToString(), "function's second argument must be variable name"));

            operation = func.GetNext(iter);

            return new RefNameResolver(context, refName.Name);
        }

        public static RefNameResolver GetTmpArgs(
            this IFunctionSignature func, IEnumerable<IOperand> operands, INameResolver context,
            out IOperand first, out IOperand operation) {

            IEnumerator<IOperand> iter;
            return func.GetTmpArgs(operands, context, out first, out operation, out iter);
        }

        public static RefNameResolver GetArrayArgs(this IFunctionSignature func, IEnumerable<IOperand> operands, INameResolver context,
            out IEnumerable<IValue> array, out IOperand operation, out IEnumerator<IOperand> iter) {

            //array = func.GetFirst(operands, out iter, context) as IIndexableValue;
            //if (array == null)
            //    throw new EvalInvalidValueTypeException(String.Concat(func.ToString(), " function's first argument must be array"));

            //INamedOperand refName = func.GetNext(iter) as INamedOperand;
            //if (refName == null)
            //    throw new EvalInvalidValueTypeException(String.Concat(func.ToString(), "function's second argument must be variable name"));

            //operation = func.GetNext(iter);

            //return new RefNameResolver(context, refName.Name);
            IOperand first;
            RefNameResolver result = GetTmpArgs(func, operands, context, out first, out operation, out iter);
            array = first as IIndexableValue;
            if (array == null)
                throw new EvalInvalidValueTypeException(String.Concat(func.ToString(), " function's first argument must be array"));
            return result;
        }

        public static RefNameResolver GetArrayArgs(this IFunctionSignature func, IEnumerable<IOperand> operands, INameResolver context,
            out IEnumerable<IValue> array, out IOperand operation) {

            IEnumerator<IOperand> iter;
            return func.GetArrayArgs(operands, context, out array, out operation, out iter);
        }
    }
}
