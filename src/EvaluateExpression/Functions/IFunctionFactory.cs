﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Functions
{
    public interface IFunctionFactory
    {
        IOperand GetFunction(string name, IList<IOperand> operands);
    }
}
