﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Functions {

    public class SimpleFunctionFactory : IFunctionFactory {
        readonly private Dictionary<string, LinkedList<IFunctionSignature>> _functions = new Dictionary<string, LinkedList<IFunctionSignature>>();

        public SimpleFunctionFactory() {

        }

        public SimpleFunctionFactory(IEnumerable<KeyValuePair<string, IFunctionSignature>> functions) {
            Add(functions);
        }

        public void Add(string name, IFunctionSignature func) {
            LinkedList<IFunctionSignature> list;
            if (_functions.TryGetValue(name, out list)) {
                LinkedListNode<IFunctionSignature> node = list.First;
                while (node != null) {
                    if (node.Value.ArgCount == func.ArgCount)
                        throw new ArgumentException(String.Concat("Dublicate function ", name, " with arg count ", func.ArgCount.ToString()));

                    if (func.ArgCount < node.Value.ArgCount && !func.IsVariableArgsCount()) {
                        list.AddBefore(node, func);
                        return;
                    }

                    node = node.Next;
                }

                list.AddLast(func);

            } else {
                list = new LinkedList<IFunctionSignature>();
                list.AddLast(func);
                _functions.Add(name, list);
            }
        }

        public void Add(IEnumerable<KeyValuePair<string, IFunctionSignature>> functions) {
            foreach (var pair in functions)
                Add(pair.Key, pair.Value);
        }

        public IOperand GetFunction(string name, IList<IOperand> operands) {
            LinkedList<IFunctionSignature> list;
            if (_functions.TryGetValue(name, out list)) {
                LinkedListNode<IFunctionSignature> node = list.First;
                while (node != null) {
                    if (node.Value.ArgCount == operands.Count || node.Value.IsVariableArgsCount())
                        return new FunctionSignOperand(node.Value, operands);
                    node = node.Next;
                }
            }

            throw new ArgumentException(String.Concat("Function ", name, "#", operands.Count.ToString(), " not found"));
        }

    }
}
