﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EvaluateExpression.Functions {
   
    public interface IFunctionSignature
    {        
        int ArgCount { get; }

        IValue Perform(IEnumerable<IOperand> operands, INameResolver context);
    }
    
    public class FunctionSignatureZeroArg : IFunctionSignature
    {
        readonly private Func<IValue> _method;

        public int ArgCount => 0;

        public FunctionSignatureZeroArg(Func<IValue> method) {
            _method = method;
        }

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            return _method();
        }
    }

    public class FunctionSignature1Arg : IFunctionSignature
    {
        readonly private Func<IValue, IValue> _method;

        public FunctionSignature1Arg(Func<IValue, IValue> method) {
            _method = method;
        }

        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            return _method(operands.First().Eval(context));
        }
    }

    public class FunctionSignature2Arg : IFunctionSignature {
        readonly private Func<IValue, IValue, IValue> _method;

        public FunctionSignature2Arg(Func<IValue, IValue, IValue> method) {
            _method = method;
        }

        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerator<IOperand> iter;
            IValue arg1 = this.GetFirst(operands, out iter, context);
            IValue arg2 = this.GetNext(iter, context);
            return _method(arg1, arg2);
        }
    }

    public class FunctionSignature3Arg : IFunctionSignature {
        readonly private Func<IValue, IValue, IValue, IValue> _method;

        public FunctionSignature3Arg(Func<IValue, IValue, IValue, IValue> method) {
            _method = method;
        }

        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerator<IOperand> iter;
            IValue arg1 = this.GetFirst(operands, out iter, context);
            IValue arg2 = this.GetNext(iter, context);
            IValue arg3 = this.GetNext(iter, context);
            return _method(arg1, arg2, arg3);
        }
    }

    public class FunctionSignatureVarArg : IFunctionSignature {
        readonly private Func<IEnumerable<IValue>, IValue> _method;

        public FunctionSignatureVarArg(Func<IEnumerable<IValue>, IValue> method) {
            _method = method;
        }

        public int ArgCount => IFunctionSignatureExtentions.VariableArgsCount;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            return _method(operands.Select(o => o.Eval(context)));
        }
    }
}
