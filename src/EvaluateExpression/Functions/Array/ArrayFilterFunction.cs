﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions.Array
{
    using Operands;
    using Values;

    /// <summary>
    /// Filter array function
    /// </summary>
    public class ArrayFilterFunction : IFunctionSignature
    {
        /// <inheritdoc/>
        public int ArgCount => 3;

        /// <inheritdoc/>
        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerable<IValue> array;
            IOperand operation;
            RefNameResolver localContext = this.GetArrayArgs(operands, context, out array, out operation);

            var list = array.Where(v => {
                localContext.SetReference(v);
                return operation.Eval(localContext).AsBool;
            });

            return new ArrayPatternValue(new EnumerableValueStrategy(list));
        }

        /// <inheritdoc/>
        public override string ToString() {
            return nameof(ArrayFilterFunction);
        }
    }

    /// <summary>
    /// Фильтр и трансформация в одном флаконе
    /// </summary>
    public class ArrayFilterExFunction : IFunctionSignature
    {
        /// <inheritdoc/>
        public int ArgCount => 4;

        /// <inheritdoc/>
        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerable<IValue> array;
            IOperand operation;
            IEnumerator<IOperand> iter;
            RefNameResolver localContext = this.GetArrayArgs(operands, context, out array, out operation, out iter);
            IOperand transOperation = this.GetNext(iter);

            var list = array.Where(v => {
                localContext.SetReference(v);
                return operation.Eval(localContext).AsBool;
            }).Select(v => {
                localContext.SetReference(v);
                return transOperation.Eval(localContext);
            });

            return new ArrayPatternValue(new EnumerableValueStrategy(list));
        }

        /// <inheritdoc/>
        public override string ToString() {
            return nameof(ArrayFilterFunction);
        }
    }
}
