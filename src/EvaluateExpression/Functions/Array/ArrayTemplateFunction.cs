﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operands;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Functions.Array
{
    public class ArrayTemplateFunction<T> : IFunctionSignature where T : IArrayFunctionStrategy, new()
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IIndexableValue array = operands.First().Eval(context) as IIndexableValue;
            if (array == null)
                throw new EvalInvalidValueTypeException("ArraySumFunction function's argument must be array");

            T collector = new T();
            foreach (IValue val in array)
                collector.Eval(val);

            return collector.Result;
        }
    }

    public class ArrayTemplateFunctionEx<T> : IFunctionSignature where T : IArrayFunctionStrategy, new()
    {
        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            //IEnumerator<IOperand> iter;
            //IIndexableValue array = this.GetFirst(operands, out iter, context) as IIndexableValue;
            //if (array == null)
            //    throw new EvalInvalidValueTypeException("ArraySumExFunction function's first argument must be array");

            //INamedOperand refName = this.GetNext(iter) as INamedOperand;
            //if (refName == null)
            //    throw new EvalInvalidValueTypeException("ArraySumExFunction function's second argument must be variable name");

            //IOperand operation = this.GetNext(iter);

            //RefNameResolver localContext = new RefNameResolver(context, refName.Name);
            IEnumerable<IValue> array;
            IOperand operation;
            RefNameResolver localContext = this.GetArrayArgs(operands, context, out array, out operation);

            var arrayOrItems = array.Select(item => {
                localContext.SetReference(item);
                return operation.Eval(localContext);
            });

            T collector = new T();
            foreach (IValue val in arrayOrItems)
                collector.Eval(val);

            return collector.Result;
        }
    }
}
