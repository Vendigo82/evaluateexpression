﻿using System.Collections.Generic;
using System.Linq;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Functions.Array
{
    using Values;

    /// <summary>
    /// Array functions pack
    /// </summary>
    public static class ArrayFunctions
    {
        /// <summary>
        /// Get all array functions
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions() {
            return GetArrayFunction<ArraySumStrategy>("Sum").Concat(
                GetArrayFunction<ArrayMinStrategy>("ArrayMin")).Concat(
                GetArrayFunction<ArrayMaxStrategy>("ArrayMax")).Concat(
                GetOthers());
        }

        /// <summary>
        /// create array function by strategy
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        private static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetArrayFunction<T>(string name) where T : IArrayFunctionStrategy, new() {
            yield return new KeyValuePair<string, IFunctionSignature>(name, new ArrayTemplateFunction<T>());
            yield return new KeyValuePair<string, IFunctionSignature>(name, new ArrayTemplateFunctionEx<T>());
        }

        /// <summary>
        /// Get list of array functions
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetOthers() {
            yield return new KeyValuePair<string, IFunctionSignature>("ArrayFilter", new ArrayFilterFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("ArrayFilter", new ArrayFilterExFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("ArrayTransform", new ArrayTransformFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("FirstOrNull", new FunctionSignature1Arg(FirstOrNull));
            yield return new KeyValuePair<string, IFunctionSignature>("FirstOrValue", new FunctionSignature2Arg(FirstOrValue));
            yield return new KeyValuePair<string, IFunctionSignature>("LastOrNull", new FunctionSignature1Arg(LastOrNull));
            yield return new KeyValuePair<string, IFunctionSignature>("LastOrValue", new FunctionSignature2Arg(LastOrValue));
            yield return new KeyValuePair<string, IFunctionSignature>("Contains", new FunctionSignature2Arg(Contains));
            yield return new KeyValuePair<string, IFunctionSignature>("Intersect", new FunctionSignature2Arg(Intersect));
            yield return new KeyValuePair<string, IFunctionSignature>("Contains", new ArrayContaintsFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("IsEmpty", new FunctionSignature1Arg(IsEmpty));
            yield return new KeyValuePair<string, IFunctionSignature>("IsNotEmpty", new FunctionSignature1Arg(IsNotEmpty));
            yield return new KeyValuePair<string, IFunctionSignature>("ToArray", new FunctionSignature1Arg(ToArray));
            yield return new KeyValuePair<string, IFunctionSignature>("Skip", new FunctionSignature2Arg(Skip));
            yield return new KeyValuePair<string, IFunctionSignature>("ArrayFlat", new FunctionSignature1Arg(Flat));
            yield return new KeyValuePair<string, IFunctionSignature>("Slice", new FunctionSignature3Arg(Slice));

            yield return new KeyValuePair<string, IFunctionSignature>("Sort", new ArraySortPrimitiveFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("Sort", new ArraySortByFieldNameFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("Sort", new ArraySortWithExpressionFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("SortDesc", new ArraySortPrimitiveFunction() { Ascending = false });
            yield return new KeyValuePair<string, IFunctionSignature>("SortDesc", new ArraySortByFieldNameFunction() { Ascending = false });
            yield return new KeyValuePair<string, IFunctionSignature>("SortDesc", new ArraySortWithExpressionFunction() { Ascending = false });

            yield return new KeyValuePair<string, IFunctionSignature>("Range", new FunctionSignature2Arg(Range2));
            yield return new KeyValuePair<string, IFunctionSignature>("Range", new FunctionSignature3Arg(Range3));

            yield return new KeyValuePair<string, IFunctionSignature>("Concat", new FunctionSignature2Arg(Concat2));
            yield return new KeyValuePair<string, IFunctionSignature>("Concat", new FunctionSignature3Arg(Concat3));
            yield return new KeyValuePair<string, IFunctionSignature>("Reverse", new FunctionSignature1Arg(Reverse));
            yield return new KeyValuePair<string, IFunctionSignature>("Zip", new FunctionSignature2Arg(Zip));
            yield return new KeyValuePair<string, IFunctionSignature>("ZipIndex", new FunctionSignature1Arg(ZipIndex));

            yield return new KeyValuePair<string, IFunctionSignature>("AddToHead", new FunctionSignature2Arg(AddToHead));
            yield return new KeyValuePair<string, IFunctionSignature>("AddToTail", new FunctionSignature2Arg(AddToTail));            
        }

        /// <summary>
        /// First element or null
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static IValue FirstOrNull(IValue arg) => arg.IsArray(out var list) ? list.FirstOrDefault() ?? NullValue.Null : arg;

        /// <summary>
        /// First element or value
        /// </summary>
        /// <param name="array"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static IValue FirstOrValue(IValue array, IValue defaultValue)
        {            
            if (array.IsArray(out var list))
                return list.FirstOrDefault() ?? defaultValue;
            else
                return array.IsNull ? defaultValue : array;
        }

        /// <summary>
        /// First element or null
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static IValue LastOrNull(IValue arg) => arg.IsArray(out var list) ? list.LastOrDefault() ?? NullValue.Null : arg;

        /// <summary>
        /// First element or value
        /// </summary>
        /// <param name="array"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static IValue LastOrValue(IValue array, IValue defaultValue)
        {
            if (array.IsArray(out var list))
                return list.LastOrDefault() ?? defaultValue;
            else
                return array.IsNull ? defaultValue : array;
        }

        /// <summary>
        /// Is array contains element
        /// </summary>
        /// <param name="array"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IValue Contains(IValue array, IValue value) 
            => array.IsArray(out var list) ? list.Contains(value).ToValue() : array.Equals(value).ToValue();

        /// <summary>
        /// Intersect two arrays
        /// </summary>
        /// <param name="array1"></param>
        /// <param name="array2"></param>
        /// <returns></returns>
        public static IValue Intersect(IValue array1, IValue array2)
        {
            IEnumerable<IValue> list1 = array1 as IEnumerable<IValue>;
            IEnumerable<IValue> list2 = array2 as IEnumerable<IValue>;
            return new ArrayPatternValue(new EnumerableValueStrategy(list1.Intersect(list2)));
        }

        /// <summary>
        /// Is array empty
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static IValue IsEmpty(IValue array) => (array.AsArray().FirstOrDefault() == null).ToValue();        

        /// <summary>
        /// Is array not empty
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static IValue IsNotEmpty(IValue array) => (array.AsArray().FirstOrDefault() != null).ToValue();        

        /// <summary>
        /// transforms to array
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static IValue ToArray(IValue array) {
            IEnumerable<IValue> list = array as IEnumerable<IValue>;
            ArrayPatternValue patter = list as ArrayPatternValue;
            if (patter != null && patter.Array.IsSolid)
                return array;
            return new ArrayValue(list.ToArray());
        }

        /// <summary>
        /// Skip <paramref name="count"/> elements in <paramref name="array"/> sequence
        /// </summary>
        /// <param name="array"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IValue Skip(IValue array, IValue count)
        {
            int cnt = count.AsInt;
            if (cnt == 0)
                return array.ThrowIfNotArray();

            return ArrayPatternValue.Create(array.AsArray().Skip(cnt));
        }

        /// <summary>
        /// Array of arrays transform to single flat array by concatenating all nested arrays.
        /// If nested item is not array, then include item to new array
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="EvalCastToArrayException">if <paramref name="value"/> is not array</exception>
        public static IValue Flat(IValue value)
        {
            var flat = value.AsArray().SelectMany(item => item is IIndexableValue itemArray ? itemArray : Enumerable.Repeat(item, 1));
            return ArrayPatternValue.Create(flat);
        }

        /// <summary>
        /// Create sequence of integer values
        /// </summary>
        /// <param name="from">sequence starts from</param>
        /// <param name="count">count elements in sequence</param>
        /// <returns></returns>
        public static IValue Range2(IValue from, IValue count) 
            => ArrayPatternValue.Create(Enumerable.Range(from.AsInt, count.AsInt).Select(i => i.ToValue()));

        /// <summary>
        /// Create sequence from integer values
        /// </summary>
        /// <param name="from">sequence starts from</param>
        /// <param name="count">count of elements</param>
        /// <param name="step">step bewtween elements</param>
        /// <returns></returns>
        public static IValue Range3(IValue from, IValue count, IValue step)
        {
            var stepInt = step.AsInt;
            var fromInt = from.AsInt;
            return ArrayPatternValue.Create(Enumerable.Range(0, count.AsInt).Select(i => (i * stepInt + fromInt).ToValue()));
        }

        public static IValue Concat2(IValue array1, IValue array2)
             => ArrayPatternValue.Create(array1.AsArray().Concat(array2.AsArray()));

        public static IValue Concat3(IValue array1, IValue array2, IValue array3)
             => ArrayPatternValue.Create(array1.AsArray().Concat(array2.AsArray()).Concat(array3.AsArray()));

        public static IValue AddToHead(IValue array1, IValue value)
            => ArrayPatternValue.Create(Enumerable.Repeat(value, 1).Concat(array1.AsArray()));

        public static IValue AddToTail(IValue array1, IValue value)
            => ArrayPatternValue.Create(array1.AsArray().Concat(Enumerable.Repeat(value, 1)));

        public static IValue Reverse(IValue array1) => array1.IsArray(out var list) ? ArrayPatternValue.Create(list.Reverse()) : array1;

        public static IValue Zip(IValue array1, IValue array2) 
            => ArrayPatternValue.Create(Enumerable.Zip(array1.AsArray(), array2.AsArray(), 
                (f, s) => new StructValue() {
                    { "First", f },
                    { "Second", s }
                }));

        public static IValue ZipIndex(IValue array1)
            => ArrayPatternValue.Create(Enumerable.Zip(array1.AsArray(), Enumerable.Range(0, int.MaxValue),
                (f, s) => new StructValue() {
                    { "Item", f },
                    { "Index", s.ToValue() }
                }));

        public static IValue Slice(IValue array, IValue from, IValue to) => SliceRoutine(array.AsArray(), from.AsInt, to.AsInt);

        private static IValue SliceRoutine(IEnumerable<IValue> array, int from, int to)
        {
            int skip = GetSmartIndex(array, from);
            int take = GetSmartIndex(array, to) - skip + 1;
            return ArrayPatternValue.Create(array.Skip(skip).Take(System.Math.Max(0, take)));
        }

        private static int GetSmartIndex(IEnumerable<IValue> array, int index) => index >= 0 ? index : array.Count() + index;
    }
}
