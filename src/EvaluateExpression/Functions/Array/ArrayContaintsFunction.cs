﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Functions.Array
{
    /// <summary>
    /// Is array contains element function
    /// </summary>
    public class ArrayContaintsFunction : IFunctionSignature
    {
        /// <inheritdoc/>
        public int ArgCount => 3;

        /// <inheritdoc/>
        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerable<IValue> array;
            IOperand operation;
            RefNameResolver localContext = this.GetArrayArgs(operands, context, out array, out operation);

            IValue value = array.FirstOrDefault(v => {
                localContext.SetReference(v);
                return operation.Eval(localContext).AsBool;
            });

            return (value != null).ToValue();
        }

        /// <inheritdoc/>
        public override string ToString() {
            return nameof(ArrayContaintsFunction);
        }
    }
}
