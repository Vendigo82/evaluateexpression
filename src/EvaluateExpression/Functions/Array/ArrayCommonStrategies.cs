﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression;
using EvaluateExpression.Functions;
using EvaluateExpression.Operands;
using EvaluateExpression.Values;

namespace EvaluateExpression.Functions.Array
{
    public class ArraySumStrategy : IArrayFunctionStrategy
    {
        int si = 0;
        double sd = 0;

        public IValue Result => sd == 0 ? si.ToValue() : (si + sd).ToValue();

        public void Eval(IValue value) {
            if (value.IsInteger())
                si = si + value.AsInt;
            else
                sd = sd + value.AsDouble;
        }
    }

    public class ArrayMinStrategy : IArrayFunctionStrategy
    {
        public IValue _result = null;
        public IValue Result => _result ?? NullValue.Null;

        public void Eval(IValue value) {
            if (_result == null)
                _result = value;
            else if (value.AsDouble < _result.AsDouble)
                _result = value;
        }
    }

    public class ArrayMaxStrategy : IArrayFunctionStrategy
    {
        public IValue _result = null;
        public IValue Result => _result ?? NullValue.Null;

        public void Eval(IValue value) {
            if (_result == null)
                _result = value;
            else if (value.AsDouble > _result.AsDouble)
                _result = value;
        }
    }
}
