﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Operands;
using EvaluateExpression.Utils;
using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EvaluateExpression.Functions.Array
{
    public class ArraySortFunctionBase
    {
        public bool Ascending { get; set; } = true;

        private IComparer<IValue> CreateComparer() => new ValueComparer() { Ascending = Ascending };

        public IValue Order(IEnumerable<IValue> array, Func<IValue, IValue> keySelector)
        {
            var list = array.OrderBy(keySelector, CreateComparer());
            return new ArrayPatternValue(new EnumerableValueStrategy(list));
        }
    }

    /// <summary>
    /// Array sorting function with arguments (array, X, X.field)
    /// </summary>
    public class ArraySortWithExpressionFunction : ArraySortFunctionBase, IFunctionSignature
    {
        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            RefNameResolver localContext = this.GetArrayArgs(operands, context, out IEnumerable<IValue> array, out IOperand operation);
            return Order(array, v => {
                localContext.SetReference(v);
                return operation.Eval(localContext);
            });
        }
    }

    /// <summary>
    /// Array sorting function with specify object's field name (array, "field")
    /// </summary>
    public class ArraySortByFieldNameFunction : ArraySortFunctionBase, IFunctionSignature
    {
        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            var array = this.GetFirst(operands, out IEnumerator<IOperand> iter, context) as IIndexableValue; 
            if (array == null)
                throw new EvalInvalidValueTypeException($"'{this}' function's first argument must be array");

            var fieldName = this.GetNext(iter);
            return Order(array, v => v.PropertyRequired(fieldName.Eval(context).AsString));
        }
    }

    /// <summary>
    /// Array sorting function or primitive types, arguments (array)
    /// </summary>
    public class ArraySortPrimitiveFunction : ArraySortFunctionBase, IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            var array = this.GetFirst(operands, out IEnumerator<IOperand> iter, context) as IIndexableValue;
            if (array == null)
                throw new EvalInvalidValueTypeException($"'{this}' function's first argument must be array");

            return Order(array, v => v);
        }
    }
}
