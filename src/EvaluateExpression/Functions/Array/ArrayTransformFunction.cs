﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions.Array
{
    using Operands;
    using Values;

    public class ArrayTransformFunction : IFunctionSignature
    {
        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerable<IValue> array;
            IOperand operation;
            RefNameResolver localContext = this.GetArrayArgs(operands, context, out array, out operation);

            var list = array.Select(v => {
                localContext.SetReference(v);
                return operation.Eval(localContext);
            });

            return new ArrayPatternValue(new EnumerableValueStrategy(list));
        }

        public override string ToString() {
            return nameof(ArrayFilterFunction);
        }
    }
}
