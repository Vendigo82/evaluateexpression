﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression;

namespace EvaluateExpression.Functions.Array
{
    public interface IArrayFunctionStrategy
    {
        void Eval(IValue value);

        IValue Result { get; }
    }
}
