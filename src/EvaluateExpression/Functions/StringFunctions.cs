﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EvaluateExpression.Exceptions;
using EvaluateExpression.Values;

namespace EvaluateExpression.Functions
{
    /// <summary>
    /// Contains string functions
    /// </summary>
    public static class StringFunctions {

        /// <summary>
        /// Returns list of string functions
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions()
        {
            yield return new KeyValuePair<string, IFunctionSignature>("LevenshteinDist", new FunctionSignature2Arg(LevenshteinDistance));
            yield return new KeyValuePair<string, IFunctionSignature>("ToUpper", new FunctionSignature1Arg(ToUpper));
            yield return new KeyValuePair<string, IFunctionSignature>("ToLower", new FunctionSignature1Arg(ToLower));
            yield return new KeyValuePair<string, IFunctionSignature>("RemoveNonLatin", new RemoveNonLatinTunction(IsLatin));
            yield return new KeyValuePair<string, IFunctionSignature>("RemoveNonLatinOrDigit", new RemoveNonLatinTunction(IsLatinOrDigit));
            yield return new KeyValuePair<string, IFunctionSignature>("SubString", new FunctionSignature3Arg(SubString));
            yield return new KeyValuePair<string, IFunctionSignature>("Substring", new FunctionSignature3Arg(SubString));
            yield return new KeyValuePair<string, IFunctionSignature>("PosInString", new FunctionSignature2Arg(PosInString));
            yield return new KeyValuePair<string, IFunctionSignature>("PosInStringLast", new FunctionSignature2Arg(PosInStringLast));
            yield return new KeyValuePair<string, IFunctionSignature>("Split", new FunctionSignature2Arg(Split));
            yield return new KeyValuePair<string, IFunctionSignature>("ConcatStrings", new FunctionSignature2Arg(ConcatStringsWithSeparator));
            yield return new KeyValuePair<string, IFunctionSignature>("RepeatString", new FunctionSignature2Arg(Repeat));
            yield return new KeyValuePair<string, IFunctionSignature>("ReplaceSubstring", new FunctionSignature3Arg(ReplaceSubstring));
            yield return new KeyValuePair<string, IFunctionSignature>("CharCode", new FunctionSignature2Arg(CharCode));
            yield return new KeyValuePair<string, IFunctionSignature>("StringFromANSIICodes", new FunctionSignature1Arg(StringFromANSIICodes));
            yield return new KeyValuePair<string, IFunctionSignature>("IsNullOrEmpty", new FunctionSignature1Arg(IsNullOrEmpty));
            yield return new KeyValuePair<string, IFunctionSignature>("IsNullOrWhiteSpace", new FunctionSignature1Arg(IsNullOrWhiteSpace));
            yield return new KeyValuePair<string, IFunctionSignature>("Trim", new FunctionSignature1Arg(Trim));
            yield return new KeyValuePair<string, IFunctionSignature>("TrimStart", new FunctionSignature1Arg(TrimStart));
            yield return new KeyValuePair<string, IFunctionSignature>("TrimEnd", new FunctionSignature1Arg(TrimEnd));
        }

        /// <summary>
        /// Returns uppercase string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static IValue ToUpper(IValue s) => s.AsString.ToUpper().ToValue();

        /// <summary>
        /// Returns lowercase string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static IValue ToLower(IValue s) => s.AsString.ToLower().ToValue();

        #region LevenshteinDistance
        /// <summary>
        /// calculate levenshtein distance between two strings
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static IValue LevenshteinDistance(IValue s1, IValue s2) {
            return LevenshteinDistance(s1.AsString, s2.AsString).ToValue();
        }

        /// <summary>
        /// calculate levenshtein distance between two strings
        /// </summary>
        public static int LevenshteinDistance(string s1, string s2) {
            uint s1len, s2len, x, y, lastdiag, olddiag;
            s1len = (uint)s1.Length;
            s2len = (uint)s2.Length;
            uint[] column = new uint[s1len + 1];

            for (y = 1; y <= s1len; ++y)
                column[y] = y;

            for (x = 1; x <= s2len; ++x) {
                column[0] = x;

                for (y = 1, lastdiag = x - 1; y <= s1len; ++y) {
                    olddiag = column[y];
                    column[y] = MIN3(column[y] + 1, column[y - 1] + 1, (uint)(lastdiag + (s1[(int)(y - 1)] == s2[(int)(x - 1)] ? 0 : 1)));
                    lastdiag = olddiag;
                }
            }

            return (int)(column[s1len]);
        }

        /// <summary>
        /// Returns minimal value 
        /// </summary>
        private static uint MIN3(uint a, uint b, uint c) {
            return ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)));
        }
        #endregion

        #region RemoveNonLatin
        /// <summary>
        /// Remove all non latim simbols from string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="IsLegal"></param>
        /// <returns></returns>
        public static string RemoveNotLegal(string str, Func<char, bool> IsLegal ) {
            StringBuilder sb = null;
            for (int i = 0; i < str.Length; ++i) {
                if (!IsLegal(str[i])) {
                    if (sb == null) {
                        sb = new StringBuilder(str.Length);
                        if (i > 0)
                            sb.Append(str, 0, i);
                    }
                } else {
                    if (sb != null)
                        sb.Append(str[i]);
                }
            }
            if (sb == null)
                return str;
            else
                return sb.ToString();
        }

        public static bool IsLatin(char ch) {
            return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
        }

        public static bool IsLatinOrDigit(char ch) {
            return IsLatin(ch) || Char.IsDigit(ch);
        }

        public class RemoveNonLatinTunction : IFunctionSignature
        {
            private readonly Func<char, bool> _isLegalFunc;

            public RemoveNonLatinTunction(Func<char, bool> isLegalFunc) {
                _isLegalFunc = isLegalFunc;
            }

            public int ArgCount => 1;

            public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
                IValue val = this.GetSingle(operands, context);
                string arg = val.AsString;
                string res = RemoveNotLegal(arg, _isLegalFunc);
                if (ReferenceEquals(arg, res))
                    return val;
                else
                    return res.ToValue();
            }
        }
        #endregion

        /// <summary>
        /// Extract substring from string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static IValue SubString(IValue str, IValue startIndex, IValue length) {
            string s = str.AsString;
            int index = startIndex.AsInt;
            int l = length.AsInt;

            if (s.Length == 0 || l <= 0 || index >= s.Length )
                return "".ToValue();

            if (index < 0)
                throw new EvalIndexOutOfRangeException("SubString starting index must be >= 0", null);

            if (index < 0) index = 0;
            if (index >= s.Length) index = s.Length - 1;
            if (l < 0) l = 0;
            if (index + l > s.Length) l = s.Length - index;


            return s.Substring(index, l).ToValue();
        }

        /// <summary>
        /// Search subsring in string. Returns position of substring
        /// </summary>
        public static IValue PosInString(IValue str, IValue substr) => str.AsString.IndexOf(substr.AsString).ToValue();

        /// <summary>
        /// Reverse search substring in string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="substr"></param>
        /// <returns></returns>
        public static IValue PosInStringLast(IValue str, IValue substr) => str.AsString.LastIndexOf(substr.AsString).ToValue();

        /// <summary>
        /// Split given string using separators from second argument
        /// </summary>
        /// <param name="str">given string to split</param>
        /// <param name="separator">separators</param>
        /// <returns>Array of splitted string</returns>
        public static IValue Split(IValue str, IValue separator)
            => new ArrayValue(str.AsString.Split(separator.AsString.ToCharArray()).Select(i => i.ToValue()).ToArray());

        /// <summary>
        /// Concatenate array of strings with separator
        /// </summary>
        /// <param name="array"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static IValue ConcatStringsWithSeparator(IValue array, IValue separator)
        {
            string strSeparator = separator.AsString;
            if (array is IIndexableValue arrayValue) {
                string resultString = arrayValue
                    .Select(i => i.AsString)
                    .Aggregate((s, i) => string.IsNullOrEmpty(s) ? i : string.Concat(s, strSeparator, i));
                return resultString.ToValue();
            } else
                throw new EvalCastToArrayException(array);            
        }

        public static IValue Repeat(IValue value, IValue count) => string.Concat(Enumerable.Repeat(value.AsString, Math.Max(count.AsInt, 0))).ToValue();

        /// <summary>
        /// Returns a new string in which all occurrences of a specified string in the current
        ///     instance are replaced with another specified string.
        /// </summary>
        /// <param name="sourceString">origin string</param>
        /// <param name="substring">substring to replace</param>
        /// <param name="newstring">new substring</param>
        /// <returns></returns>
        public static IValue ReplaceSubstring(IValue sourceString, IValue substring, IValue newstring) 
            => sourceString.AsString.Replace(substring.AsString, newstring.AsString).ToValue();

        /// <summary>
        /// Return ANSII code of char from string in position
        /// </summary>
        /// <param name="sourceString"></param>
        /// <param name="index">char's position</param>
        /// <returns></returns>
        public static IValue CharCode(IValue sourceString, IValue index) => ((int)(sourceString.AsString[index.AsInt])).ToValue();

        /// <summary>
        /// Make string from ansii char codes
        /// </summary>
        /// <param name="codes"></param>
        /// <returns></returns>
        /// <exception cref="EvalCastToArrayException"></exception>
        public static IValue StringFromANSIICodes(IValue codes)
        {
            if (!(codes is IIndexableValue arrayValue))
                throw new EvalCastToArrayException(codes);

            var result = Encoding.ASCII.GetString(arrayValue.Select(i => (byte)i.AsInt).ToArray());
            return result.ToValue();
        }

        /// <summary>
        /// Returns true if string null or empty
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static IValue IsNullOrEmpty(IValue stringValue) =>
            stringValue.IsNull ? BoolValue.True : string.IsNullOrEmpty(stringValue.AsString).ToValue();

        /// <summary>
        /// Returns true if string null or empty or only contains spaces
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static IValue IsNullOrWhiteSpace(IValue stringValue) => 
            stringValue.IsNull ? BoolValue.True : string.IsNullOrWhiteSpace(stringValue.AsString).ToValue();

        public static IValue Trim(IValue stringValue) => stringValue.AsString.Trim().ToValue();

        public static IValue TrimStart(IValue stringValue) => stringValue.AsString.TrimStart().ToValue();

        public static IValue TrimEnd(IValue stringValue) => stringValue.AsString.TrimEnd().ToValue();
    }
}
