﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions
{
    public class RandomFunction : IFunctionSignature
    {
        private readonly Random _rnd = new Random();

        public int ArgCount => 0;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            lock (_rnd) {
                return _rnd.NextDouble().ToValue();
            }
        }
    }
}
