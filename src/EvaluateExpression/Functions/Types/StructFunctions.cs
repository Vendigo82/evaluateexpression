﻿using EvaluateExpression.Values;
using System.Collections.Generic;

namespace EvaluateExpression.Functions.Types
{
    public static class StructFunctions
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions() 
        {
            yield return new KeyValuePair<string, IFunctionSignature>("MakeStructFromContext", new MakeStructFromContextFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("MergeObjects", new FunctionSignature2Arg(SimpleMerge));
        }

        public static IValue SimpleMerge(IValue val1, IValue val2)
        {
            if (!(val1 is IComplexValue obj1))
                throw new Exceptions.EvalCastToComplexException(val1);
            if (!(val2 is IComplexValue obj2))
                throw new Exceptions.EvalCastToComplexException(val2);

            var result = new StructValue();
            foreach (var p in obj1)
                result.Add(p.Key, p.Value);

            foreach (var p in obj2)
                result.Add(p.Key, p.Value);

            return result;
        }
    }

    /// <summary>
    /// Make new Struct value by union all values from context
    /// </summary>
    public class MakeStructFromContextFunction : IFunctionSignature
    {
        public int ArgCount => 0;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) => new StructValue(context);        
    }
}
