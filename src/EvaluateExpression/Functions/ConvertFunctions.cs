﻿using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions
{
    public static class ConvertFunctions
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions(IFormatProvider format) {
            yield return new KeyValuePair<string, IFunctionSignature>("ToString", new ToStringFunction(format));
            yield return new KeyValuePair<string, IFunctionSignature>("ToString", new ToString2Function(format));
            yield return new KeyValuePair<string, IFunctionSignature>("ToInt", new ToIntFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("ToDouble", new ToDoubleFunction(format));
            yield return new KeyValuePair<string, IFunctionSignature>("ToBool", new ToBoolFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("ToDateTime", new ToDateTimeFunction(format));
            yield return new KeyValuePair<string, IFunctionSignature>("ToDateTime", new ToDateTime2Function(format));
        }
    }

    public class ToStringFunction : IFunctionSignature
    {
        private IFormatProvider _format;

        public ToStringFunction(IFormatProvider info) {
            _format = info;
        }

        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IValue value = this.GetSingle(operands, context);
            switch (value.Type) {
                case ValueType.String:
                    return value;
                case ValueType.Integer:
                    return value.AsInt.ToString().ToValue();
                case ValueType.Decimal:
                    return value.AsDouble.ToString(_format).ToValue();
                case ValueType.Boolean:
                    return (value.AsBool ? "1" : "0").ToValue();
                default:
                    return value.ToString().ToValue();
            }
        }
    }

    public class ToString2Function : IFunctionSignature
    {
        private IFormatProvider _format;

        public ToString2Function(IFormatProvider info) {
            _format = info;
        }

        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerator<IOperand> iter;
            IValue value = this.GetFirst(operands, out iter, context);
            IValue format = this.GetNext(iter, context);
            switch (value.Type) {
                case ValueType.DateTime:
                    return value.AsDateTime.ToString(format.AsString, _format).ToValue();
                default:
                    return value.ToString().ToValue();
            }
        }
    }

    public class ToIntFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IValue value = this.GetSingle(operands, context);
            switch (value.Type) {
                case ValueType.Integer:
                    return value;
                case ValueType.Decimal:
                    return ((int)value.AsDouble).ToValue();
                case ValueType.String:
                    return int.Parse(value.AsString).ToValue();
                default:
                    return value.AsInt.ToValue();
            }
        }
    }

    public class ToDoubleFunction : IFunctionSignature
    {
        private IFormatProvider _format;

        public ToDoubleFunction(IFormatProvider info) {
            _format = info;
        }

        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IValue value = this.GetSingle(operands, context);
            switch (value.Type) {
                case ValueType.Decimal:
                    return value;
                case ValueType.String:
                    return Double.Parse(value.AsString, _format).ToValue();
                default:
                    return value.AsDouble.ToValue();
            }
        }
    }

    public class ToBoolFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IValue value = this.GetSingle(operands, context);
            switch (value.Type) {
                case ValueType.Boolean:
                    return value;
                case ValueType.String:
                    var str = value.AsString;
                    try {
                        return (int.Parse(str) != 0).ToValue();
                    } catch (FormatException) {
                        if (str.Length <= 5) {
                            str = str.ToLower();
                            if (str == "true")
                                return BoolValue.True;
                            else if (str == "false")
                                return BoolValue.False;
                        }
                        throw;
                    }
                default:
                    return value.AsBool.ToValue();
            }
        }
    }

    public class ToDateTimeFunction : IFunctionSignature
    {
        private IFormatProvider _format;

        public ToDateTimeFunction(IFormatProvider format) {
            _format = format;
        }

        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IValue value = this.GetSingle(operands, context);
            switch (value.Type) {
                case ValueType.DateTime:
                    return value;
                case ValueType.String:
                    return DateTime.Parse(value.AsString, _format).ToValue();
                default:
                    return value.AsDateTime.ToValue();
            }
        }
    }

    public class ToDateTime2Function : IFunctionSignature
    {
        private IFormatProvider _format;

        public ToDateTime2Function(IFormatProvider format) {
            _format = format;
        }

        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerator<IOperand> iter;
            IValue value = this.GetFirst(operands, out iter, context);
            IValue pattern = this.GetNext(iter, context);
            switch (value.Type) {
                case ValueType.DateTime:
                    return value;
                case ValueType.String:
                    return DateTime.ParseExact(value.AsString, pattern.AsString, _format).ToValue();
                default:
                    return value.AsDateTime.ToValue();
            }
        }
    }
}
