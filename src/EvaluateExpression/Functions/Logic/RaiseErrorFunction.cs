﻿using System.Collections.Generic;

namespace EvaluateExpression.Functions.Logic
{
    public class RaiseErrorFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            var messageOperand = this.GetSingle(operands);
            var messageValue = messageOperand.Eval(context);

            throw new RaiseErrorException(messageValue.ToString());
        }
    }

    public class RaiseErrorException : Exceptions.EvaluateException
    {
        public RaiseErrorException(string message) : base($"Raise error: {message}")
        {
            UserMessage = message;
        }

        public string UserMessage { get; }
    }
}
