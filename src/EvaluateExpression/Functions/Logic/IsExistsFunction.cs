﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions.Logic
{
    using EvaluateExpression.Exceptions;
    using Values;

    public class IsExistsFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IOperand operand = this.GetSingle(operands);            
            return IsExists(operand, context, out IValue val).ToValue();
        }

        public static bool IsExists(IOperand operand, INameResolver context, out IValue value) {
            try {
                value = operand.Eval(context);
                if (value == null)
                    return false;
                else
                    return true;
            } catch (EvalValueNotFoundException) {
                value = null;
                return false;
            }
        }
    }
}
