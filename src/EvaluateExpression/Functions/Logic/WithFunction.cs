﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;

namespace EvaluateExpression.Functions.Logic
{
    public class WithFunction : IFunctionSignature
    {
        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IOperand first;
            IOperand operation;
            RefNameResolver localContext = this.GetTmpArgs(operands, context, out first, out operation);

            localContext.SetReference(first.Eval(context));
            return operation.Eval(localContext);
        }
    }
}
