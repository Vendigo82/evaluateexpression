﻿using System.Collections.Generic;

namespace EvaluateExpression.Functions.Logic
{
    public static class LogicFunctions
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions(IIfAnyErrorListener ifAnyErrorListener)
        {
            yield return new KeyValuePair<string, IFunctionSignature>("IIF", new IIFFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("IIFNull", new IIFNullFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("IsExists", new IsExistsFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("TryGet", new TryGetFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("TryGet", new TryGet2Function());
            yield return new KeyValuePair<string, IFunctionSignature>("With", new WithFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("IfAnyError", new IfAnyErrorFunction(ifAnyErrorListener));
            yield return new KeyValuePair<string, IFunctionSignature>("IfAnyError", new IfAnyErrorFunctionWithParam(ifAnyErrorListener));
            yield return new KeyValuePair<string, IFunctionSignature>("RaiseError", new RaiseErrorFunction());
        }
    }

    public class IIFFunction : IFunctionSignature
    {
        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            IValue arg1 = this.GetFirst(operands, out IEnumerator<IOperand> iter, context);
            IOperand arg2 = this.GetNext(iter);
            IOperand arg3 = this.GetNext(iter);
            if (arg1.AsBool)
                return arg2.Eval(context);
            else
                return arg3.Eval(context);
        }
    }

    public class IIFNullFunction : IFunctionSignature
    {
        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            IValue arg1 = this.GetFirst(operands, out IEnumerator<IOperand> iter, context);
            IOperand arg2 = this.GetNext(iter);
            if (!arg1.IsNull)
                return arg1;
            else
                return arg2.Eval(context);
        }
    }
}
