﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions.Logic
{
    using Values;

    public class TryGetFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            return IsExistsFunction.IsExists(this.GetSingle(operands), context, out IValue val) ? val : NullValue.Null;
        }
    }

    public class TryGet2Function : IFunctionSignature
    {
        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {
            IEnumerator<IOperand> iter;
            IOperand arg1 = this.GetFirst(operands, out iter);
            IOperand arg2 = this.GetNext(iter);
            return IsExistsFunction.IsExists(arg1, context, out IValue val) ? val : arg2.Eval(context);
        }
    }
}
