﻿using System;
using System.Collections.Generic;

namespace EvaluateExpression.Functions.Logic
{
    /// <summary>
    /// Listen errors which function TryAnyErrors receive
    /// </summary>
    public interface IIfAnyErrorListener
    {
        void OnError(Exception e, INameResolver context, IOperand argument, string parameter);
    }

    /// <summary>
    /// if any error occurs on evaluating first argument, then argument 2 will return.
    /// </summary>
    public class IfAnyErrorFunction : IFunctionSignature
    {
        private readonly IIfAnyErrorListener _listener;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listener">Errors listener. Can be null</param>
        public IfAnyErrorFunction(IIfAnyErrorListener listener)
        {
            _listener = listener;
        }

        public int ArgCount => 2;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            IOperand arg1 = this.GetFirst(operands, out IEnumerator<IOperand> iter);
            IOperand arg2 = this.GetNext(iter);

            try {
                return arg1.Eval(context) ?? throw new NullReferenceException("Argument 1 returns null");                    
            } catch (Exception e) {                
                try {
                    _listener?.OnError(e, context, arg1, null);
                } catch { }

                return arg2.Eval(context);
            }
        }
    }

    public class IfAnyErrorFunctionWithParam : IFunctionSignature
    {
        private readonly IIfAnyErrorListener _listener;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listener">Errors listener. Can be null</param>
        public IfAnyErrorFunctionWithParam(IIfAnyErrorListener listener)
        {
            _listener = listener;
        }

        public int ArgCount => 3;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            IOperand arg1 = this.GetFirst(operands, out IEnumerator<IOperand> iter);
            IOperand arg2 = this.GetNext(iter);
            IOperand arg3 = this.GetNext(iter);

            try
            {
                return arg1.Eval(context) ?? throw new NullReferenceException("Argument 1 returns null");
            }
            catch (Exception e)
            {
                if (_listener != null)
                {
                    string param;
                    try
                    {
                        param = arg3.Eval(context).ToString();                        
                    }
                    catch (Exception pe)
                    {
                        param = "#error";
                        _listener?.OnError(pe, context, arg3, null);
                    }

                    try
                    {
                        _listener.OnError(e, context, arg1, param);
                    }
                    catch { }
                }

                return arg2.Eval(context);
            }
        }
    }
}
