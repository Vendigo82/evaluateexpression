﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Functions
{
    public class FunctionSignOperand : IOperand {
        private readonly IFunctionSignature _function;
        private readonly IList<IOperand> _operands;

        public FunctionSignOperand(IFunctionSignature function, IList<IOperand> operands) {
            _function = function;
            _operands = operands;
            CheckArgumentCount();
        }

        public IValue Eval(INameResolver context) {
            return _function.Perform(_operands, context);
        }

        private void CheckArgumentCount() {
            if (_function.IsVariableArgsCount() == false && _function.ArgCount != _operands.Count)
                throw new ArgumentException(String.Concat("Function ", _function.ToString(), " argument count exception: need ", 
                    _function.ArgCount.ToString(), "; received ", _operands.Count));
        }
    }
}
