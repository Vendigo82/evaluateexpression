﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Values;

namespace EvaluateExpression.Functions
{
    public static class DateTimeFunctions
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions() {
            yield return new KeyValuePair<string, IFunctionSignature>("Now", new FunctionSignatureZeroArg(Now));
            yield return new KeyValuePair<string, IFunctionSignature>("UtcNow", new FunctionSignatureZeroArg(UtcNow));
            yield return new KeyValuePair<string, IFunctionSignature>("TimeDiff", new FunctionSignature2Arg(TimeDiffDays));
            //yield return new KeyValuePair<string, IFunctionSignature>("TimeDiffYears", new FunctionSignature2Arg(TimeDiffYears));

            yield return new KeyValuePair<string, IFunctionSignature>("TimeAddHours", new FunctionSignature2Arg(TimeAddHours));
            yield return new KeyValuePair<string, IFunctionSignature>("TimeAddDays", new FunctionSignature2Arg(TimeAddDays));
            yield return new KeyValuePair<string, IFunctionSignature>("TimeAddMonths", new FunctionSignature2Arg(TimeAddMonths));
            yield return new KeyValuePair<string, IFunctionSignature>("TimeAddYears", new FunctionSignature2Arg(TimeAddYears));
        }

        public static IValue Now() =>DateTime.Now.ToValue();
        public static IValue UtcNow() => DateTime.UtcNow.ToValue();

        public static IValue TimeDiffDays(IValue d1, IValue d2) => (d2.AsDateTime - d1.AsDateTime).TotalDays.ToValue();
        //public static IValue TimeDiffYears(IValue d1, IValue d2) => ((d2.AsDateTime - d1.AsDateTime).TotalDays / 365.25).ToValue();

        public static IValue TimeAddHours(IValue d1, IValue hours) => (d1.AsDateTime.AddHours(hours.AsDouble)).ToValue();
        public static IValue TimeAddDays(IValue d1, IValue days) => (d1.AsDateTime.AddDays(days.AsDouble)).ToValue();
        public static IValue TimeAddMonths(IValue d1, IValue months) => (d1.AsDateTime.AddMonths(months.AsInt)).ToValue();        
        public static IValue TimeAddYears(IValue d1, IValue years) => (d1.AsDateTime.AddYears(years.AsInt)).ToValue();        
    }
}
