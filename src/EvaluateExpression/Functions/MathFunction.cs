﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Functions
{
    public static class MathFunction
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions() {
            yield return new KeyValuePair<string, IFunctionSignature>("Round", new FunctionSignature1Arg(Round));
            yield return new KeyValuePair<string, IFunctionSignature>("Round", new FunctionSignature2Arg(Round));
            yield return new KeyValuePair<string, IFunctionSignature>("Floor", new FunctionSignature1Arg(Floor));
            yield return new KeyValuePair<string, IFunctionSignature>("Ceil", new FunctionSignature1Arg(Ceil));
            yield return new KeyValuePair<string, IFunctionSignature>("Max", new FunctionSignature2Arg(Max));
            yield return new KeyValuePair<string, IFunctionSignature>("Min", new FunctionSignature2Arg(Min));
            yield return new KeyValuePair<string, IFunctionSignature>("exp", new FunctionSignature1Arg(Exp));
            yield return new KeyValuePair<string, IFunctionSignature>("Abs", new FunctionSignature1Arg(Abs));
        }

        public static IValue Round(IValue value) {
            return ((int)Math.Round(value.AsDouble)).ToValue();
        }

        public static IValue Round(IValue value, IValue digits) {
            return Math.Round(value.AsDouble, digits.AsInt).ToValue();
        }

        public static IValue Floor(IValue value) {
            return ((int)Math.Floor(value.AsDouble)).ToValue();
        }

        public static IValue Ceil(IValue value) {
            return ((int)Math.Ceiling(value.AsDouble)).ToValue();
        }

        public static IValue Max(IValue a, IValue b) {
            if (a.IsInteger() && b.IsInteger())
                return Math.Max(a.AsInt, b.AsInt).ToValue();
            else
                return Math.Max(a.AsDouble, b.AsDouble).ToValue();
        }

        public static IValue Min(IValue a, IValue b) {
            if (a.IsInteger() && b.IsInteger())
                return Math.Min(a.AsInt, b.AsInt).ToValue();
            else
                return Math.Min(a.AsDouble, b.AsDouble).ToValue();
        }

        public static IValue Exp(IValue val) {
            return (Math.Exp(val.AsDouble)).ToValue();
        }

        public static IValue Abs(IValue val) 
            => val.IsInteger() ? Math.Abs(val.AsInt).ToValue() : Math.Abs(val.AsDouble).ToValue();
    }
}
