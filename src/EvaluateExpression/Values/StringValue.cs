﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvaluateExpression.Values
{
    public class StringValue : IValue, IComplexValue {
        private string _value;

        public const string LENGTH = "Length";

        public StringValue(string value) {
            _value = value;
        }

        public bool IsNull => _value == null;
        public bool IsNumeric => false;
        public bool IsString => true;
        public bool IsDateTime => false;

        public ValueType Type => ValueType.String;

        public bool AsBool => throw new EvalInvalidValueTypeException(Type, ValueType.Boolean);
        public int AsInt => throw new EvalInvalidValueTypeException(Type, ValueType.Integer);
        public double AsDouble => throw new EvalInvalidValueTypeException(Type, ValueType.Decimal);
        public string AsString => _value ?? throw new EvalInvalidValueTypeException(ValueType.Null, ValueType.String);
        public DateTime AsDateTime => throw new EvalInvalidValueTypeException(Type, ValueType.DateTime);

        public object Value => _value;

        public string Description => ToString();

        public IValue Eval(INameResolver context) {
            return this;
        }

        public override bool Equals(object obj) {
            try {
                if (obj is IValue)
                    return AsString == ((IValue)obj).AsString;
                else
                    return false;
            } catch (EvalInvalidValueTypeException) {
                return false;
            }
        }

        public override int GetHashCode() {
            return _value.GetHashCode();
        }

        public override string ToString() {
            return _value;
        }

        public string ToString(string format, IFormatProvider provider) {
            return _value;
        }

        public IValue GetSubValue(string name) {
            if (name == LENGTH)
                return _value.Length.ToValue();
            else
                return null;
        }

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return Enumerable.Empty<KeyValuePair<string, IValue>>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public IValue Property(string name) => GetSubValue(name);
    }
}
