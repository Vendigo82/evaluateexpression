﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Values.Base;
using System;

namespace EvaluateExpression.Values
{
    public class IntValue : PrimitiveValueBase, IValue {
        private int _value;

        public IntValue(int val) {
            _value = val;
        }

        public IntValue(long val) {
            _value = (int)val;
        }

        public bool IsNull => false;
        public bool IsNumeric => true;
        public bool IsString => false;
        public bool IsDateTime => false;

        public ValueType Type => ValueType.Integer;

        public bool AsBool => _value != 0;
        public int AsInt => _value;
        public double AsDouble => _value;
        public string AsString => throw new EvalInvalidValueTypeException(Type, ValueType.String);
        public DateTime AsDateTime => throw new EvalInvalidValueTypeException(Type, ValueType.DateTime);

        public object Value => _value;

        public IValue Eval(INameResolver context) {
            return this;
        }

        public override bool Equals(object obj) {
            try {
                if (obj is IValue && ((IValue)obj).IsNumeric)
                    return _value.Equals(((IValue)obj).AsInt);
                else
                    return false;
            } catch (EvalInvalidValueTypeException) {
                return false;
            }
        }

        public override int GetHashCode() {
            return _value.GetHashCode();
        }

        public override string ToString() {
            return _value.ToString();
        }

        public string ToString(string format, IFormatProvider provider) {
            return _value.ToString(format, provider);
        }
    }
}
