﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    public class KeyValuePairValue : SpecialValueBase, IComplexValue
    {
        private readonly KeyValuePair<IValue, IValue> _pair;

        public KeyValuePairValue(KeyValuePair<IValue, IValue> pair) {
            _pair = pair;
        }

        public KeyValuePairValue(IValue key, IValue value) {
            _pair = new KeyValuePair<IValue, IValue>(key, value);
        }

        public const string KEY = "Key";
        public const string VALUE = "Value";

        public override object Value => _pair;

        public IValue GetSubValue(string name) {
            switch (name) {
                case KEY: return _pair.Key;
                case VALUE: return _pair.Value;
                default: return null;
            }
        }

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            yield return new KeyValuePair<string, IValue>(KEY, _pair.Key);
            yield return new KeyValuePair<string, IValue>(VALUE, _pair.Value);
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public string Description => string.Concat("{key=", _pair.Key.Description, ",value=", _pair.Value.Description, "}");

        public override string ToString() {
            return String.Concat("{key=", _pair.Key.ToString(), ",value=", _pair.Value.ToString(), "}");
        }

        public override string ToString(string format, IFormatProvider provider) {
            return String.Concat("{key=", _pair.Key.ToString(format, provider), ",value=", _pair.Value.ToString(format, provider), "}");
        }

        public IValue Eval(INameResolver context) => this;

        public IValue Property(string name) => GetSubValue(name);
    }
}
