﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Values.Base
{
    public class PrimitiveValueBase : IPropertyNavigation
    {
        public string Description => ToString();

        public IValue Property(string name) => null;
    }
}
