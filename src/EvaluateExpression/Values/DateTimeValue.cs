﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    public class DateTimeValue : IComplexValue
    {
        private readonly DateTime _dt;

        public DateTimeValue(DateTime dt) {
            _dt = dt;
        }

        public string Description => ToString();
        public bool IsNull => false;
        public bool IsNumeric => false;
        public bool IsString => false;
        public bool IsDateTime => true;

        public ValueType Type => ValueType.DateTime;

        public bool AsBool => throw new EvalInvalidValueTypeException(Type, ValueType.Boolean);
        public int AsInt => throw new EvalInvalidValueTypeException(Type, ValueType.Integer);
        public double AsDouble => throw new EvalInvalidValueTypeException(Type, ValueType.Decimal);
        public string AsString => throw new EvalInvalidValueTypeException(Type, ValueType.String);
        public DateTime AsDateTime => _dt;

        public object Value => _dt;

        public const string YEAR = "Year";
        public const string MONTH = "Month";
        public const string DAY = "Day";
        public const string HOUR = "Hour";
        public const string MINUTE = "Min";
        public const string SECOND = "Sec";
        public const string DATE = "Date";

        public IValue GetSubValue(string name) {
            switch (name) {
                case YEAR: return _dt.Year.ToValue();
                case MONTH: return _dt.Month.ToValue();
                case DAY: return _dt.Day.ToValue();
                case HOUR: return _dt.Hour.ToValue();
                case MINUTE: return _dt.Minute.ToValue();
                case SECOND: return _dt.Second.ToValue();
                case DATE: return _dt.Date.ToValue();
            }
            return null;
        }


        public IValue Property(string name) => GetSubValue(name);


        public IValue Eval(INameResolver context) => this;

        public override string ToString() {
            return _dt.ToString();
        }

        public string ToString(string format, IFormatProvider provider) {
            return _dt.ToString(format, provider);
        }

        public override int GetHashCode() {
            return _dt.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (obj is IValue && ((IValue)obj).IsDateTime)
                return _dt.Equals(((IValue)obj).AsDateTime);
            else
                return base.Equals(obj);
        }

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return Enumerable.Empty<KeyValuePair<string, IValue>>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }
}
