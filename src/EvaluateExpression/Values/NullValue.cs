﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Values.Base;
using System;

namespace EvaluateExpression.Values
{
    public class NullValue : PrimitiveValueBase, IValue {
        ValueType _type;

        private NullValue() {
            _type = ValueType.Null;
        }

        public bool IsNull => true;
        public bool IsNumeric => _type.IsNumeric();
        public bool IsString => _type == ValueType.String;
        public bool IsDateTime => false;

        public bool AsBool => throw new EvalInvalidValueTypeException(Type, ValueType.Boolean);
        public int AsInt => throw new EvalInvalidValueTypeException(Type, ValueType.Integer);
        public double AsDouble => throw new EvalInvalidValueTypeException(Type, ValueType.Decimal);
        public string AsString => throw new EvalInvalidValueTypeException(Type, ValueType.String);
        public DateTime AsDateTime => throw new EvalInvalidValueTypeException(Type, ValueType.DateTime);

        public object Value => null;

        public ValueType Type => _type;

        public static NullValue Null = new NullValue();

        public IValue Eval(INameResolver context) {
            return this;
        }

        public override bool Equals(object obj) {
            if (obj is NullValue)
                return true;
            else
                return false;
        }

        public override int GetHashCode() {
            return 0;
        }

        public override string ToString() {
            return "null";
        }

        public string ToString(string format, IFormatProvider provider) {            
            return ToString();
        }
    }
}
