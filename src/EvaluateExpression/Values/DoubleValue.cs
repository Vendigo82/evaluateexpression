﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Values.Base;
using System;

namespace EvaluateExpression.Values
{
    public class DoubleValue : PrimitiveValueBase, IValue {
        private double _value;

        public DoubleValue(double value) {
            _value = value;
        }

        public bool IsNull => false;
        public bool IsNumeric => true;
        public bool IsString => false;
        public bool IsDateTime => false;

        public ValueType Type => ValueType.Decimal;

        public bool AsBool => _value != 0;
        public int AsInt {
            get {
                if (_value == Math.Round(_value))
                    return (int)Math.Round(_value);
                else
                    throw new EvalInvalidValueTypeException(Type, ValueType.Integer, ToString());
            }
        }
        public double AsDouble => _value;
        public string AsString => throw new EvalInvalidValueTypeException(Type, ValueType.String);
        public DateTime AsDateTime => throw new EvalInvalidValueTypeException(Type, ValueType.DateTime);

        public object Value => _value;

        public IValue Eval(INameResolver context) {
            return this;
        }

        public override bool Equals(object obj) {
            if (obj is IValue && ((IValue)obj).IsNumeric)
                return _value.Equals(((IValue)obj).AsDouble);
            else
                return false;
        }

        public override int GetHashCode() {
            return _value.GetHashCode();
        }

        public override string ToString() {
            return _value.ToString();
        }

        public string ToString(string format, IFormatProvider provider) {
            return _value.ToString(format, provider);
        }
    }
}
