﻿using EvaluateExpression.Values.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    [Obsolete]
    public class LazyValue : PrimitiveValueBase, IValue
    {
        private readonly Func<IValue> _calcValueFunc;
        private IValue _result = null;

        public LazyValue(Func<IValue> calcValueFunc) {
            _calcValueFunc = calcValueFunc;
        }

        private IValue Result {
            get {
                if (_result == null)
                    _result = _calcValueFunc();
                return _result;
            }
        }

        public bool IsNull => Result.IsNull;
        public bool IsNumeric => Result.IsNumeric;
        public bool IsString => Result.IsString;
        public bool IsDateTime => Result.IsDateTime;

        public ValueType Type => Result.Type;

        public bool AsBool => Result.AsBool;
        public int AsInt => Result.AsInt;
        public double AsDouble => Result.AsDouble;
        public string AsString => Result.AsString;
        public DateTime AsDateTime => Result.AsDateTime;
        public object Value => Result.Value;

        public IValue Eval(INameResolver context) {
            return Result;
        }

        public override string ToString() {
            return Result.ToString();
        }

        public string ToString(string format, IFormatProvider provider) {
            return Result.ToString(format, provider);
        }
    }
}
