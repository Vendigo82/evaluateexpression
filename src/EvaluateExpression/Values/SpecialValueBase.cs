﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    public abstract class SpecialValueBase// : IValue
    {
        public bool IsNull => false;
        public bool IsNumeric => false;
        public bool IsString => false;
        public bool IsDateTime => false;
        public ValueType Type => ValueType.Unknown;

        public bool AsBool => throw new EvalInvalidValueTypeException(GetType().Name, ValueType.Boolean);
        public int AsInt => throw new EvalInvalidValueTypeException(GetType().Name, ValueType.Integer);
        public double AsDouble => throw new EvalInvalidValueTypeException(GetType().Name, ValueType.Decimal);
        public string AsString => throw new EvalInvalidValueTypeException(GetType().Name, ValueType.String);
        public DateTime AsDateTime => throw new EvalInvalidValueTypeException(GetType().Name, ValueType.DateTime);

        public abstract object Value { get; }

        //public IValue Eval(INameResolver context) {            
        //    return this;
        //}

        public virtual string ToString(string format, IFormatProvider provider) {
            return ToString();
        }
    }
}
