﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Values
{
    public class DictValue : SpecialValueBase, IValue, IDictionaryValue, IComplexValue
    {
        private readonly Dictionary<IValue, IValue> _dict = new Dictionary<IValue, IValue>();

        public void Add(IValue index, IValue value) {
            try {
                _dict.Add(index, value);
            } catch (ArgumentException e) {
                throw new EvalDictDublicateKetException(String.Concat("Key value ", index.ToString(), " already exists"), e);
            }
        }

        public override object Value => _dict;

        public IValue this[IValue index] {
            get {
                if (_dict.TryGetValue(index, out IValue result))
                    return result;
                else
                    return NullValue.Null;
            }
        }

        public IValue GetSubValue(string name) => name == this.CountPropertyName() ? _dict.Count.ToValue() : null;

        public IEnumerator<IValue> GetEnumerator() {
            return _dict.Select(p => new KeyValuePairValue(p)).GetEnumerator(); //Enumerable.Empty<IValue>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        IEnumerator<KeyValuePair<string, IValue>> IEnumerable<KeyValuePair<string, IValue>>.GetEnumerator() {
            return Enumerable.Empty<KeyValuePair<string, IValue>>().GetEnumerator();
        }

        public string Description => string.Concat(
                "{\n",
                string.Join(",\n", _dict.Select(p => string.Concat(p.Key.Description, ": ", p.Value.Description))),
                "\n}");

        public override string ToString() {
            return String.Concat(
                "{\n",
                String.Join(",\n", _dict.Select(p => String.Concat(p.Key.ToString(), ": ", p.Value.ToString()))),
                "\n}");
        }

        public override string ToString(string format, IFormatProvider provider) {
            return String.Concat(
                "{",
                String.Join(",", _dict.Select(p => String.Concat(p.Key.ToString(format, provider), ": ", p.Value.ToString(format, provider)))),
                "}");
        }

        public IValue Eval(INameResolver context) => this;

        public IValue Property(string name) => GetSubValue(name);
    }
}
