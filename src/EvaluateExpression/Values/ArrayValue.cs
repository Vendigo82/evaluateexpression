﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    public class ArrayValue : ArrayPatternValue
    {
        public ArrayValue(IValue[] array) : base(new ArrayValueStrategy(array)) {
        }

        public ArrayValue(List<IValue> array) : base(new ListValueStrategy(array)) {
        }

        public static readonly ArrayValue Empty = new ArrayValue(new IValue[] { });
    }
}
