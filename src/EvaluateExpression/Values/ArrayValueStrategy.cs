﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    public class ArrayValueStrategy : IArrayValueStrategy
    {
        private readonly IValue[] _array;

        public ArrayValueStrategy(IValue[] array) {
            _array = array;
        }

        public IValue this[int index] => _array[index];
        public int Count => _array.Length;
        public object Value => _array;
        public bool IsSolid => true;

        public IEnumerator<IValue> GetEnumerator() => _array.Select(i => i).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class ListValueStrategy : IArrayValueStrategy
    {
        private readonly List<IValue> _list = new List<IValue>();

        public ListValueStrategy() {
        }

        public ListValueStrategy(List<IValue> list) {
            _list.AddRange(list);
        }

        public void Add(IValue value) => _list.Add(value);

        public IValue this[int index] => _list[index];
        public int Count => _list.Count;
        public object Value => _list;
        public bool IsSolid => true;

        public IEnumerator<IValue> GetEnumerator() => _list.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class EnumerableValueStrategy : IArrayValueStrategy
    {
        private IEnumerable<IValue> _values;

        public EnumerableValueStrategy(IEnumerable<IValue> values) {
            _values = values;
        }

        public IValue this[int index] => _values.Skip(index).First();
        public object Value => _values;
        public int Count => _values.Count();
        public bool IsSolid => false;

        public IEnumerator<IValue> GetEnumerator() => _values.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
