﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Values
{
    public class StructValue : SpecialValueBase, IComplexValue
    {
        private Dictionary<string, IValue> _dict;

        public StructValue() {
            _dict = new Dictionary<string, IValue>();
        }

        public StructValue(IEnumerable<KeyValuePair<string, IValue>> values) {
            _dict = new Dictionary<string, IValue>();
            foreach (var val in values)
                _dict.Add(val.Key, val.Value);
        }

        public StructValue(Dictionary<string, IValue> values) {
            _dict = values;
        }

        public override object Value => _dict;

        public IValue GetSubValue(string name) {
            if (_dict.TryGetValue(name, out IValue val))
                return val;
            else
                return null;
        }

        public void Add(string name, IValue val) {
            _dict[name] = val;
        }

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return _dict.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return _dict.GetEnumerator();
        }

        public string Description => string.Concat(
                "{\n",
                string.Join(",\n", _dict.Select(p => string.Concat(p.Key, ": ", p.Value.Description))),
                "\n}");
        public override string ToString() {
            return String.Concat(
                "{\n", 
                String.Join(",\n", _dict.Select(p => String.Concat(p.Key, ": ", p.Value.ToString()))), 
                "\n}");
        }

        public override string ToString(string format, IFormatProvider provider) {
            return String.Concat(
                "{",
                String.Join(",", _dict.Select(p => String.Concat(p.Key, ": ", p.Value.ToString(format, provider)))),
                "}");
        }

        public IValue Eval(INameResolver context) => this;

        public IValue Property(string name) => GetSubValue(name);
    }
}
