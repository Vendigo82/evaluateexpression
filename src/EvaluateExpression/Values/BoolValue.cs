﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Values.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Values
{
    public class BoolValue : PrimitiveValueBase, IValue {
        private bool _value;

        private BoolValue(bool val) {
            _value = val;
        }

        public bool IsNull => false;
        public bool IsNumeric => true;
        public bool IsString => false;
        public bool IsDateTime => false;

        public ValueType Type => ValueType.Boolean;

        public bool AsBool => _value;
        public int AsInt => _value ? 1 : 0;
        public double AsDouble => AsInt;
        public string AsString => throw new EvalInvalidValueTypeException(Type, ValueType.Boolean);
        public DateTime AsDateTime => throw new EvalInvalidValueTypeException(Type, ValueType.DateTime);

        public object Value => _value;

        public static BoolValue True = new BoolValue(true);
        public static BoolValue False = new BoolValue(false);

        public static BoolValue Get(bool val) {
            return val ? True : False;
        }

        public IValue Eval(INameResolver context) {
            return this;
        }

        public override bool Equals(object obj) {
            if (obj is IValue && ((IValue)obj).IsInteger())
                return _value.Equals(((IValue)obj).AsBool);
            else
                return false;
        }

        public override int GetHashCode() {
            return _value.GetHashCode();
        }

        public override string ToString() {
            return _value.ToString();
        }

        public string ToString(string format, IFormatProvider provider) {
            return _value.ToString(provider);
        }
    }
}
