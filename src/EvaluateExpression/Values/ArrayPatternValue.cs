﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace EvaluateExpression.Values
{
    public class ArrayPatternValue : SpecialValueBase, IValue, IArrayValue, IComplexValue
    {
        private readonly IArrayValueStrategy _array;

        public static ArrayPatternValue Create(IEnumerable<IValue> list) => new ArrayPatternValue(new EnumerableValueStrategy(list));

        public ArrayPatternValue(IArrayValueStrategy array) {
            _array = array;
        }

        public override object Value => _array.Value;

        public IArrayValueStrategy Array => _array;

        public IValue this[IValue index] => _array[index.AsInt];

        public IValue GetSubValue(string name) => name == this.CountPropertyName() ? _array.Count.ToValue() : null;

        public IEnumerator<IValue> GetEnumerator() {
            return _array.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        IEnumerator<KeyValuePair<string, IValue>> IEnumerable<KeyValuePair<string, IValue>>.GetEnumerator() {
            return Enumerable.Empty<KeyValuePair<string, IValue>>().GetEnumerator();
        }

        public string Description => _array.IsSolid ? string.Concat("[ ", string.Join(", ", _array.Select(i => i.Description)), " ]") : "[Enumerable]";

        public IValue Eval(INameResolver context)
        {
            return this;
        }

        public override string ToString() {
            return String.Concat(
                "[\n",
                String.Join(",\n", _array.Select(p => p.ToString())),
                "\n]");
        }

        public override string ToString(string format, IFormatProvider provider) {
            return String.Concat(
                "[",
                String.Join(",", _array.Select(p => p.ToString(format, provider))),
                "]");
        }

        public IValue Property(string name) => name == this.CountPropertyName() ? _array.Count.ToValue() : null;
    }
}
