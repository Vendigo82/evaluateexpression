﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression {
    /// <summary>
    /// Операнд для выражения
    /// </summary>
    public interface IOperand {
        IValue Eval(INameResolver context);
    }

    /// <summary>
    /// операнд с именем
    /// </summary>
    public interface INamedOperand : IOperand
    {
        string Name { get; }
    }
}
