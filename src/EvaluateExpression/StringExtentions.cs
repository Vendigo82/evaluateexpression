﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression
{
    public static class StringExtentions
    {
        public static bool IsChar(this string str, char ch, int index) => index < str.Length && str[index] == ch;

        public static bool IsChars(this string str, char ch1, char ch2, int index) {
            return (str[index] == ch1 && index + 1 < str.Length && str[index + 1] == ch2);
        }

        public static bool IsNewLine(this string str, int index, out int shift) {
            if (str.IsChars('\r', '\n', index)) {
                shift = 2;
                return true;
            } else if (str[index] == '\n' || CharUnicodeInfo.GetUnicodeCategory(str, index) == UnicodeCategory.LineSeparator) {
                shift = 1;
                return true;
            } else {
                shift = 0;
                return false;
            }
        }

        /// <summary>
        /// Convert string index in line number and position in line
        /// </summary>
        /// <param name="str">string</param>
        /// <param name="index">string index</param>
        /// <param name="position">returns position in line</param>
        /// <returns>returns line number</returns>
        /// <exception cref="ArgumentOutOfRangeException">throws if index out of range</exception>
        public static int IndexToLineAndPosition(this string str, int index, out int position) {
            if (index < 0 || index >= str.Length)
                throw new ArgumentOutOfRangeException($"Index {index.ToString()} is out of range (0 - {(str.Length - 1).ToString()})");
            int line = 0;
            position = 0;

            int i = 0;
            while (i < index) {
                if (str.IsNewLine(i, out int shift)) {
                    i += shift;
                    line += 1;
                    position = 0;
                } else {
                    position += 1;
                    ++i;
                }
            }
            return line;
        }

        /// <summary>
        /// Returns true if <paramref name="str"/> at position <paramref name="index"/> contains commentary '//' 
        /// and move <paramref name="index"/> to first symbol after double slash
        /// </summary>
        /// <param name="str">string</param>
        /// <param name="index">index</param>
        /// <returns>true if commentary begins</returns>
        public static bool IsLineCommentaryBegins(this string str, int index, out int shift) {
            if (str.IsChars('/', '/', index)) {
                shift = 2;
                return true;
            } else {
                shift = 0;
                return false;
            }
        }

        /// <summary>
        /// Returns index of first character in new line after position <paramref name="index"/>.
        /// If string <paramref name="str"/> has not new line after this position, then returns length of string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="index">current position</param>
        /// <returns>position of first character in new line</returns>
        public static int SeekNextLine(this string str, int index) {
            while (index < str.Length) {
                if (str.IsNewLine(index, out int shift))
                    return index + shift;
                else
                    index += 1;
            }
            return index;
        }
    }
}
