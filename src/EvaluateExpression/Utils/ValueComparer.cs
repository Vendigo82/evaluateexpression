﻿using EvaluateExpression.Exceptions;
using System.Collections.Generic;

namespace EvaluateExpression.Utils
{
    /// <summary>
    /// Compare tow values by ascending or descending order
    /// </summary>
    public class ValueComparer : IComparer<IValue>
    {
        public bool Ascending { get; set; } = true;

        public int Compare(IValue x, IValue y)
        {
            if (x.Type == ValueType.Null && y.Type == ValueType.Null)
                return 0;

            if (x.Type == ValueType.Null)
                return -1;

            if (y.Type == ValueType.Null)
                return 1;

            try {
                return CompareValues(x, y) * (Ascending ? 1 : -1);
            } catch (EvalInvalidValueTypeException e) {
                throw new EvalCompareValuesException(x, y, e);
            }
        }

        private int CompareValues(IValue x, IValue y)
        {
            switch (x.Type) {
                case ValueType.Boolean: return x.AsBool.CompareTo(y.AsBool);
                case ValueType.Integer when y.Type == ValueType.Integer: return x.AsInt.CompareTo(y.AsInt);
                case ValueType.Integer: return x.AsDouble.CompareTo(y.AsDouble);
                case ValueType.Decimal: return x.AsDouble.CompareTo(y.AsDouble);
                case ValueType.DateTime: return x.AsDateTime.CompareTo(y.AsDateTime);
                case ValueType.String: return x.AsString.CompareTo(y.AsString);
                default: throw new EvalCompareValuesException(x, y);
            }
        }
    }
}
