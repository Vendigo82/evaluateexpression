﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operands {
    public class VariableOperand : INamedOperand
    {
        private readonly string m_name;

        public VariableOperand(string name) {
            m_name = name;
        }

        public string Name => m_name;

        public IValue Eval(INameResolver context) {
            IValue val = context.Resolve(m_name);
            //if (val == null)
            //    throw new KeyNotFoundException(String.Concat("Value ", m_name, " not found"));
            return val;
        }
    }
}
