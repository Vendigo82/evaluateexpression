﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operands {
    public interface IOperandStack {
        /// <summary>
        /// put single value in stack
        /// </summary>
        /// <param name="operand">operand for put</param>
        void put(IOperand operand);

        /// <summary>
        /// extract top element in stack
        /// </summary>
        /// <exception cref="InvalidOperationException">if stack is empty</exception>
        IOperand extract();

        /// <summary>
        /// count of elements
        /// </summary>
        int count { get; }

        /// <summary>
        /// true if can't extract last element
        /// </summary>
        bool empty { get; }
    }
}
