﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EvaluateExpression.Operands
{
    /// <summary>
    /// Combine some INameResolver. 
    /// Called name resolvers in initial order
    /// </summary>
    public class CombineNameResolver : INameResolver 
    {
        private readonly List<INameResolver> _resolvers;

        public string Description => ToString();

        public CombineNameResolver(INameResolver first, INameResolver second) 
        {
            _resolvers = new List<INameResolver>(2) {
                first,
                second
            };
        }

        public CombineNameResolver(INameResolver first, INameResolver second, INameResolver third) 
        {
            _resolvers = new List<INameResolver>(3) {
                first,
                second,
                third
            };
        }

        public CombineNameResolver(IEnumerable<INameResolver> list) 
        {
            _resolvers = new List<INameResolver>();
            _resolvers.AddRange(list);
        }

        public IValue Resolve(string name) {
            IValue result;
            foreach (INameResolver nr in _resolvers) {
                result = nr.Resolve(name);
                if (result != null)
                    return result;
            }

            return null;
        }

        public IValue Property(string name) => Resolve(name);

        public bool Assign(string name, IValue value) {
            foreach (INameResolver nr in _resolvers) {
                if (nr.Assign(name, value))
                    return true;
            }

            return false;
        }

        public bool DeclareVariable(string name, IValue value = null) => _resolvers[0].DeclareVariable(name, value);        

        /// <summary>
        /// If <paramref name="first"/> and <paramref name="second"/> is not null, then returns combined name resolver.
        /// otherwise returns one of <paramref name="first"/> or <paramref name="second"/>, which is not null
        /// if <paramref name="first"/> and <paramref name="second"/> all is null, then returns null
        /// </summary>
        /// <param name="first">First name resolver</param>
        /// <param name="second">Second name resolver</param>
        /// <returns>Combined or null, if both arguments is null</returns>
        public static INameResolver Combine(INameResolver first, INameResolver second) {
            if (first != null && second != null)
                return new CombineNameResolver(first, second);
            if (first != null)
                return first;
            if (second != null)
                return second;
            return null;
        }

        public IContext BaseContext => _resolvers.FirstOrDefault();

        #region Enumerator
        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return _resolvers.SelectMany(nr => nr).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
        #endregion

    }
}
