﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EvaluateExpression.Operands
{
    public class StructOperand : IOperand
    {
        private readonly IEnumerable<KeyValuePair<string, IOperand>> _properties;

        public StructOperand(IEnumerable<KeyValuePair<string, IOperand>> properties)
        {
            _properties = properties ?? throw new ArgumentNullException(nameof(properties));
        }

        public IValue Eval(INameResolver context)
        {
            var values = _properties.Select(i => {
                try
                {
                    return new KeyValuePair<string, IValue>(i.Key, i.Value.Eval(context));
                } 
                catch (Exception e)
                {
                    throw new EvalStructMemberException(i.Key, e);
                }
                }).ToArray();

            var result = new StructValue(values);
            return result;
        }        
    }
}
