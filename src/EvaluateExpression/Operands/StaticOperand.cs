﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operands {
    using Values;

    //obsolete?
    public class StaticOperand : IOperand {
        private readonly IValue m_value;

        public StaticOperand(IValue value) {
            m_value = value;
        }

        public StaticOperand(int value) {
            m_value = new IntValue(value);
        }

        public StaticOperand(double value) {
            m_value = new DoubleValue(value);
        }

        public IValue Eval(INameResolver context) {
            return m_value;
        }
    }
}
