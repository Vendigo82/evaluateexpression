﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operands
{
    /// <summary>
    /// Hide all members in INameResolver from Enumerate
    /// </summary>
    public class EnumerlessNameResolverWrapper : INameResolver
    {
        private readonly INameResolver _nr;

        public string Description => ToString();

        public EnumerlessNameResolverWrapper(INameResolver nr) {
            _nr = nr;
        }



        public bool Assign(string name, IValue value) => _nr.Assign(name, value);
        public bool DeclareVariable(string name, IValue value = null) => _nr.DeclareVariable(name, value);
        public IValue Resolve(string name) => _nr.Resolve(name);

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() =>Enumerable.Empty<KeyValuePair<string, IValue>>().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IValue Property(string name) => _nr.Property(name);

        public IContext BaseContext => _nr;
    }
}
