﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Values;

namespace EvaluateExpression.Operands {

    /// <summary>
    /// Name resolver with assign and declare values possibility
    /// </summary>
    public class VarNameResolver : INameResolver 
    {
        protected readonly Dictionary<string, IValue> _values = new Dictionary<string, IValue>();

        public string Description => "Context: " + string.Join(", ", _values.Keys);

        public VarNameResolver() 
        {            
        }

        public VarNameResolver(IEnumerable<KeyValuePair<string, IValue>> values) 
        {
            Add(values);
        }

        public VarNameResolver(string name, IValue value)
        {
            Add(name, value);
        }

        /// <summary>
        /// add new const value
        /// </summary>
        public void Add(string name, IValue value)
        {
            _values[name] = value;
        }

        public void Add(IEnumerable<KeyValuePair<string, IValue>> values)
        {
            foreach (var pair in values)
                Add(pair.Key, pair.Value);
        }

        public IValue Resolve(string name) 
        {
            if (_values.TryGetValue(name, out IValue value))
                return value;
            else
                return null;
        }

        public IValue Property(string name) => Resolve(name);

        public bool Assign(string name, IValue value) 
        {
            if (_values.ContainsKey(name)) {
                _values[name] = value;
                return true;
            }
            else
                return false;
        }

        public bool DeclareVariable(string name, IValue value = null) 
        {
            if (_values.ContainsKey(name)) {
                if (value != null)
                    _values[name] = value;
                return false;
            }

            _values[name] = value ?? NullValue.Null;
            return true;
        }

        public IContext BaseContext => null;

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() => _values.GetEnumerator();        

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();                
    }
}
