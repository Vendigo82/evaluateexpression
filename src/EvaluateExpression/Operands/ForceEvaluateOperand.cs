﻿using EvaluateExpression.Values;
using System;

namespace EvaluateExpression.Operands
{
    /// <summary>
    /// Force evaluate for lazy values, such a Array on IEnumerable pattern
    /// </summary>
    public class ForceEvaluateOperand : IOperand
    {
        private readonly IOperand _origin;

        public ForceEvaluateOperand(IOperand origin)
        {
            _origin = origin ?? throw new ArgumentNullException(nameof(origin));
        }

        public IValue Eval(INameResolver context)
        {
            var result = _origin.Eval(context);
            if (result is ArrayPatternValue array && !array.Array.IsSolid)
                return Functions.Array.ArrayFunctions.ToArray(result);
            else
                return result;
        }
    }
}
