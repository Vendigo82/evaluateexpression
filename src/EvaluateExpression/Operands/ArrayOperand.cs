﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Values;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EvaluateExpression.Operands
{
    public class ArrayOperand : IOperand
    {
        private readonly IEnumerable<IOperand> _values;

        public ArrayOperand(IEnumerable<IOperand> values)
        {
            _values = values ?? throw new ArgumentNullException(nameof(values));
        }

        public IValue Eval(INameResolver context)
        {
            var values = _values.Select(i => {
                try
                {
                    return i.Eval(context);
                }
                catch (Exception e)
                {
                    throw new EvalArrayMemberException(e);
                }
            }).ToArray();

            var result = new ArrayValue(values);
            return result;
        }
    }
}
