﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Operands
{
    public class RefNameResolver : INameResolver
    {
        private readonly INameResolver _nr;
        private readonly string _name;
        private IValue _value;


        public RefNameResolver(INameResolver nr, string name) {
            _nr = nr;
            _name = name;
            _value = null;
        }

        public RefNameResolver(INameResolver nr, string name, IValue refValue) {
            _nr = nr;
            _name = name;
            _value = refValue;
        }

        public void SetReference(IValue value) {
            _value = value;
        }

        public IValue RefValue { get => _value; set => _value = value; }

        public string Description => $"Reference context '{_name}'";

        public bool Assign(string name, IValue value) {
            return _nr.Assign(name, value);
        }

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return _nr.GetEnumerator();
        }

        public IValue Resolve(string name) {
            if (name == _name)
                return _value;
            else
                return _nr.Resolve(name);
        }

        public IValue Property(string name) => Resolve(name);

        public bool DeclareVariable(string name, IValue value = null) 
        {
            if (name == _name)
                throw new DeclareVariableException(name, this);

            return _nr.DeclareVariable(name, value);
        }

        public IContext BaseContext => _nr;

        IEnumerator IEnumerable.GetEnumerator() {
            return ((IEnumerable)_nr).GetEnumerator();
        }
    }
}
