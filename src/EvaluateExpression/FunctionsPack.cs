﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Functions;
using EvaluateExpression.Functions.Array;
using EvaluateExpression.Functions.Logic;
using EvaluateExpression.Functions.Types;

namespace EvaluateExpression
{
    public class FunctionSettings
    {
        public IIfAnyErrorListener IfAnyErrorListener { get; set; }
        
        public IFormatProvider Format { get; set; }
    }

    public static class FunctionsPack
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions(FunctionSettings settings) 
        {
            var format = settings?.Format ?? CultureInfo.InvariantCulture;
            
            var f = new KeyValuePair<string, IFunctionSignature>[] {
                new KeyValuePair<string, IFunctionSignature>("Random", new RandomFunction()),
            };

            return
                RegexFunctions.GetFunctions().Concat(
                LogicFunctions.GetFunctions(settings?.IfAnyErrorListener)).Concat(
                StringFunctions.GetFunctions()).Concat(
                MathFunction.GetFunctions()).Concat(
                ArrayFunctions.GetFunctions()).Concat(
                ConvertFunctions.GetFunctions(format)).Concat(
                DateTimeFunctions.GetFunctions()).Concat(
                StructFunctions.GetFunctions()).Concat(
                MainFunctions.GetFunctions()).Concat(
                f);
        }
    }
}
