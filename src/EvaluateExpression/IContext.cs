﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression
{
    public interface IContext
    {
        /// <summary>
        /// Base context. Can be null
        /// </summary>
        IContext BaseContext { get; }
    }
}
