﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Exceptions
{
    /// <summary>
    /// Error on evaluate value for new structure operation
    /// </summary>
    public class EvalStructMemberException : EvaluateException
    {
        public EvalStructMemberException(string propertyName, Exception innerException) 
            : base($"Error on evaluate value for property '{propertyName}': {innerException.Message}", innerException)
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; }
    }
}
