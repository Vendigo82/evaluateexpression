﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Exceptions
{
    /// <summary>
    /// Base class for evaluate expression errors
    /// </summary>
    public class EvaluateException : EvaluateExpressionException
    {
        public EvaluateException() {
        }

        public EvaluateException(string message) : base(message) {
        }

        public EvaluateException(string message, Exception innerException) : base(message, innerException) {
        }
    }

    /// <summary>
    /// Get value from IValue as wrong type (As... methods)
    /// </summary>
    public class EvalInvalidValueTypeException : EvaluateException
    {
        public EvalInvalidValueTypeException(string message) : base(message) { }

        public EvalInvalidValueTypeException(string typeName, string targetTypeName)
            : this(String.Concat(typeName, " can't be use as ", targetTypeName, " value")) { }

        public EvalInvalidValueTypeException(string typeName, string targetTypeName, string originValue)
            : this(String.Concat(typeName, " value ", originValue, " can't be use as ", targetTypeName, " value")) { }

        public EvalInvalidValueTypeException(string typeName, ValueType type)
            : this(typeName, type.ToString()) { }

        public EvalInvalidValueTypeException(ValueType typeOriging, ValueType typeTarger)
            : this(typeOriging.ToString(), typeTarger.ToString()) { }

        public EvalInvalidValueTypeException(ValueType typeOriging, ValueType typeTarger, string value)
            : this(typeOriging.ToString(), typeTarger.ToString(), value) { }
    }

    public class EvalCastToComplexException : EvalInvalidValueTypeException
    {
        public EvalCastToComplexException(IValue val) : base(string.Concat("Cannot use value [", val.Description, "] as complex value")) {
            Value = val;
        }

        public IValue Value { get; }
    }

    public class EvalComplexSubValueNotFound : EvalValueNotFoundException
    {
        public EvalComplexSubValueNotFound(IValue val, string subname) : base(val, subname) {
            Value = val;
        }

        public new IValue Value { get; }
    }

    public class EvalCastToArrayException : EvalInvalidValueTypeException
    {
        public EvalCastToArrayException(IValue val) : base(string.Concat("Cannot use value [", val.Description, "] as array")) {
        }
    }

    public class EvalIndexOutOfRangeException : EvaluateException
    {
        public EvalIndexOutOfRangeException(string message, Exception innerException) : base(message, innerException) {
        }
    }

    public class EvalInvalidOperationException : EvaluateException
    {
        public EvalInvalidOperationException(string message) : base(message) {
        }
    }

    public class EvalDictDublicateKetException : EvaluateException
    {
        public EvalDictDublicateKetException(string message, Exception innerException) : base(message, innerException) {
        }
    }

    public class EvalDictKeyNotFoundException : EvaluateException
    {
        public EvalDictKeyNotFoundException(string message, Exception innerException) : base(message, innerException) {
        }
    }
}
