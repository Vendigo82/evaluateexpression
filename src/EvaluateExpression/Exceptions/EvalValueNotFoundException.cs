﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Exceptions
{
    /// <summary>
    /// Throws when values does not found
    /// </summary>
    public class EvalValueNotFoundException : EvaluateException
    {
        public IPropertyNavigation Value { get; }
        public string Name { get; }

        public EvalValueNotFoundException(IPropertyNavigation container, string name) 
            : base($"Complex subvalue [{name}] not found in [{container.Description}]")
        {
            Value = container;
            Name = name;
        }
    }
}
