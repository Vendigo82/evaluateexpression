﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Exceptions
{
    /// <summary>
    /// Invalid two values compare
    /// </summary>
    public class EvalCompareValuesException : EvaluateException
    {
        public EvalCompareValuesException(IValue x, IValue y) 
            : this(x, y, null)
        {

        }

        public EvalCompareValuesException(IValue x, IValue y, Exception innerException) 
            : base($"Invalid compare values operation: try to compare objects of types: type of X - '{x.Type}'; type of Y - '{y.Type}'", innerException)
        {
        }
    }
}
