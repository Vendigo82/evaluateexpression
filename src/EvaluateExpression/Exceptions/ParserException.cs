﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Exceptions
{
    public class ParserException : EvaluateExpressionException
    {
        public ParserException(string message, int index) : this(message, index, null, null) {
        }

        public ParserException(string message, int index, string expression) : this(message, index, expression, null) {
        }

        public ParserException(string message, int index, string expression, Exception inner)
            : base(GetDescription(message, index, expression), inner) {
            Index = index;
            Expression = expression;
        }

        public int Index { get; }
        public String Expression { get;  }

        private static String GetDescription(string message, int index, string expression) {
            if (expression == null)
                return String.Concat("At index ", index.ToString(), ": ", message);
            else {
                int line = expression.IndexToLineAndPosition(Math.Min(index, expression.Length - 1), out int position);
                return String.Concat("Line ", (line + 1).ToString(), ", Pos: ", (position + 1).ToString(), ": ", message);
            }
        }
    }
}
