﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Exceptions
{
    /// <summary>
    /// Base class for all EvaluateExpression exceptions
    /// </summary>
    public class EvaluateExpressionException : Exception
    {
        public EvaluateExpressionException() {
        }

        public EvaluateExpressionException(string message) : base(message) {
        }

        public EvaluateExpressionException(string message, Exception innerException) : base(message, innerException) {
        }
    }
}
