﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Exceptions
{
    public class DeclareVariableException : EvalInvalidOperationException
    {
        public DeclareVariableException(string variableName, IPropertyNavigation context)
            : base($"Can't declare valiable '{variableName}' in context '{context.Description}'")
        {
            VariableName = variableName ?? throw new ArgumentNullException(nameof(variableName));
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public string VariableName { get; }

        public IPropertyNavigation Context { get; }
    }
}
