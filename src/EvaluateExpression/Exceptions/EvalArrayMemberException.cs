﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Exceptions
{
    /// <summary>
    /// Error on evaluate value for new structure operation
    /// </summary>
    public class EvalArrayMemberException : EvaluateException
    {
        public EvalArrayMemberException(Exception innerException)
            : base($"Error on evaluate array's value: {innerException.Message}", innerException)
        {
        }
    }
}
