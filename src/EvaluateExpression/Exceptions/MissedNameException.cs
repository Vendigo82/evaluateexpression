﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Exceptions
{
    public class MissedNameException : ParserException
    {
        public MissedNameException(string message, int index) : base(message, index) {
        }

        public MissedNameException(string message, int index, Exception inner) : base(message, index, null, inner) {
        }
    }
}
