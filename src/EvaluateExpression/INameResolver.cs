﻿using EvaluateExpression.Exceptions;
using System.Collections.Generic;

namespace EvaluateExpression {
    /// <summary>
    /// Map of variables
    /// </summary>
    public interface INameResolver : IEnumerable<KeyValuePair<string, IValue>>, IPropertyNavigation, IContext
    {
        /// <summary>
        /// Search and return variable by name. If variable not found, then returns null
        /// </summary>
        /// <param name="name">variable name</param>
        /// <returns>IValue or null</returns>
        IValue Resolve(string name);

        /// <summary>
        /// Assign value by name. If value not found, then returns false
        /// If value founded and assigned, then returns true
        /// If value cannot be assigned, then throw exception
        /// </summary>
        /// <param name="name">Variable name</param>
        /// <param name="value">Value</param>
        /// <returns>true if value was assigned</returns>
        bool Assign(string name, IValue value);

        /// <summary>
        /// Declare new variable with value. If value is null, then variable has Null value
        /// </summary>
        /// <param name="name">variable name</param>
        /// <param name="value"></param>
        /// <returns>true, if success; false if variable with this name already existed</returns>
        /// <exception cref="DeclareVariableException">This operation not support</exception>
        bool DeclareVariable(string name, IValue value = null);
    }
}
