﻿using System;
using System.Collections.Generic;
using System.Text;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression
{
    public enum ValueType { Unknown, Null, Boolean, Integer, Decimal, String, DateTime }

    public interface IValue : IOperand, IPropertyNavigation
    {
        bool IsNull { get; }
        bool IsNumeric { get; }
        bool IsString { get; }
        bool IsDateTime { get; }

        ValueType Type { get; }

        bool AsBool { get; }
        int AsInt { get; }
        double AsDouble { get; }
        String AsString { get; }
        DateTime AsDateTime { get; }
        object Value { get; }

        string ToString(string format, IFormatProvider provider);
    }

    /// <summary>
    /// Structural value type, contains others sub values
    /// </summary>
    public interface IComplexValue : IValue, IEnumerable<KeyValuePair<string, IValue>>
    {
        /// <summary>
        /// Get subvalue by name
        /// </summary>
        /// <param name="name">subvalue name</param>
        /// <returns>IValue or null, if not found</returns>
        IValue GetSubValue(string name);
    }

    /// <summary>
    /// Indexable value
    /// </summary>
    public interface IIndexableValue : IValue, IEnumerable<IValue>
    {
        /// <summary>
        /// Get element by Index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        /// <exception cref="EvalIndexOutOfRangeException">throw exception if index out of range</exception>
        IValue this[IValue index] { get; }
    }

    public interface IArrayValue : IIndexableValue
    {
        IArrayValueStrategy Array { get; }
    }

    public interface IDictionaryValue : IIndexableValue
    {
    }

    public interface IArrayValueStrategy : IEnumerable<IValue>
    {
        IValue this[int index] { get; }

        int Count { get; }

        object Value { get; }

        /// <summary>
        /// true - if is array, false - if is Enumarable
        /// </summary>
        bool IsSolid { get; }
    }
}
