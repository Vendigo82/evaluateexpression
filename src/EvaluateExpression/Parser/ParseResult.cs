﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Parser
{
    /// <summary>
    /// Now using only for test purpose, but should replace interface in future
    /// </summary>
    public class ParseResult : IParseResult
    {
        public ParseResult(IOperand singleOperand, ParseFinishReason finishReason)
        {
            Count = 1;
            SingleOperand = singleOperand ?? throw new ArgumentNullException(nameof(singleOperand));
            FinishReason = finishReason;
        }

        public ParseResult(ParseFinishReason finishReason)
        {
            Count = 0;
            SingleOperand = null;
            FinishReason = finishReason;
        }

        public int Count { get; }

        public IOperand SingleOperand { get; }

        public ParseFinishReason FinishReason { get; }

        public int KeyWord { get; }

        public IEnumerator<IOperand> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
