﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operands;
using EvaluateExpression.Operations;
using EvaluateExpression.Operations.Factory;
using EvaluateExpression.Functions;
using EvaluateExpression.Parser.Parsers;
using System.Collections;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Parser {

    public class Parser : IParser {
        private readonly IOperationFactory m_operations;
        private readonly IFunctionFactory m_functions;
        private readonly IValueParser _valueParser;
        private readonly INameResolver _consts;
        private readonly ILexemeParser _lexemeParser;
        private readonly ISkipSeparators _skipSeparators;
        private readonly IKeyWords _keyWords;

        public static Parser CreateFull(FunctionSettings settings = null) {
            return new Parser(
                valueParser: new ValueParserList(new IValueParser[] { new ValueParser(), new NullTrueFalseParser() }, true),
                lexemeParser: null,
                operations: OperationsPack.CreateDefFactory(new KeyValuePair<string, IOperationInfo>[] {
                    new KeyValuePair<string, IOperationInfo>(".", new GetSubValueOperatorInfo()),
                }),
                functionFactory: new SimpleFunctionFactory(FunctionsPack.GetFunctions(settings)),
                consts: null,
                keyWords: null,
                enableIndexOperation: true);
        }

        public Parser(
            IValueParser valueParser = null, 
            ILexemeParser lexemeParser = null, 
            IOperationFactory operations = null, 
            IFunctionFactory functionFactory = null, 
            INameResolver consts = null,
            IKeyWords keyWords = null,
            bool enableIndexOperation = false)  {

            _valueParser = valueParser ?? new ValueParser();
            _lexemeParser = lexemeParser ?? new LexemeParser();
            m_operations = operations ?? OperationsPack.CreateDefFactory();
            m_functions = functionFactory;
            _consts = consts;
            _skipSeparators = new SkipSeparators();
            _keyWords = keyWords;
            if (enableIndexOperation)
                IndexOperation = new IndexOperationInfo(OperationsPack.INDEX);
        }

        public Parser(IOperationFactory operations, IFunctionFactory functionFactory) : this(null, null, operations, functionFactory, null) {
        }

        public Parser(IDictionary<char, IOperationInfo> operations) : this(null, null, new SingleCharOpersFactory(operations), null, null) {
        }

        public IOperationInfo IndexOperation { get; set; }

        public IParseResult Parse(string expression, ref int index) {
            return ParserRecurse.Parse(this, expression, ref index);
        }

        private class ParserRecurse : IParseResult {
            private ParserCombineStack m_stack = new ParserCombineStack();
            private readonly Parser m_parser;
            private readonly string _expression;
            private int _index;
            private readonly LinkedList<IOperand> m_operands = new LinkedList<IOperand>();

            static public ParserRecurse Parse(Parser parent, string expression, ref int index) {
                ParserRecurse parser = new ParserRecurse(parent, expression, index);
                parser.FinishReason = parser.Parse();
                parser.FinishCompile();
                index = parser._index;
                return parser;
            }

            public ParserRecurse(Parser parent, string expression, int index) {
                m_parser = parent;
                _expression = expression;
                _index = index;
            }

            public int Count => m_operands.Count;

            public ParseFinishReason FinishReason { get; private set; } = ParseFinishReason.Eof;
            /// <summary>
            /// Founded KeyWord id
            /// </summary>
            public int KeyWord { get; private set; } = 0;

            public IOperand SingleOperand {
                get {
                    if (m_operands.Count != 1)
                        throw new ArgumentException("Expression contains more then one partition");
                    return m_operands.First.Value;
                }
            }

            public LinkedList<IOperand> Operands => m_operands;

            /// <summary>
            /// perform parse part of string
            /// </summary>
            /// <returns></returns>
            public ParseFinishReason Parse() {
                string lexema = null;
                string tmp = null;                

                //enumerate symbols from current index to end of string
                while (_index < _expression.Length) {

                    //skip empty symbols
                    if (m_parser._skipSeparators.Skip(_expression, ref _index, out bool newLine))
                        continue;

                    //skip comentary
                    if (SkipComment(_expression, ref _index))
                        continue;

                    //get current symbol
                    char ch = _expression[_index];
                    if (ch == ';') {
                        if (lexema != null) {
                            m_stack.addOperand(CreateVariable(lexema));
                            lexema = null;
                        }
                        _index += 1;
                        return ParseFinishReason.EndLine;
                    }

                    IOperationInfo oper = null;
                    IValue value = null;

                    //is digit and first symbol in morphem
                    if ((value = m_parser._valueParser.Parse(_expression, ref _index)) != null) {
                        if (lexema != null)
                            throw new ParserException("invalid sequence of lexema and value", _index, _expression);
                        //then make static operand
                        m_stack.addOperand(value);
                    } else if ((oper = m_parser.m_operations.Parse(_expression, ref _index)) != null) {
                        //operation symbol branch

                        //if exist morhpem
                        if (lexema != null) {
                            //then create symbolic operand
                            m_stack.addOperand(CreateVariable(lexema));
                            lexema = null;
                        }

                        //get operation info
                        m_stack.addOperation(oper);
                    } else if (ch == '(') {
                        //parse brackets expression
                        _index += 1;

                        //if exist previous symbols, then this is function
                        List<IOperand> arguments = new List<IOperand>();
                        ParserRecurse inner = Parse(m_parser, _expression, ref _index);                        
                        while (inner.FinishReason == ParseFinishReason.CommaSeparator)
                        {
                            arguments.Add(inner.SingleOperand);
                            inner = Parse(m_parser, _expression, ref _index);
                        };

                        if (inner.FinishReason != ParseFinishReason.ClosedBracket)
                            throw new ParserException("Invalid expression: closing bracket has not found", _index, _expression);

                        if (inner.Count != 0)
                            arguments.Add(inner.SingleOperand);

                        if (lexema != null) {
                            IOperand func = m_parser.m_functions.GetFunction(lexema, arguments);
                            lexema = null;
                            m_stack.addOperand(func);
                        } else {
                            m_stack.addOperand(inner.SingleOperand);
                        }
                    } else if (ch == '[' && m_parser.IndexOperation != null) {
                        _index += 1;

                        if (lexema == "new")
                        {
                            lexema = null;
                            ParseNewArrayBlock();
                        }
                        else
                        {
                            if (lexema != null)
                            {
                                m_stack.addOperand(CreateVariable(lexema));
                                lexema = null;
                            }
                            m_stack.addOperation(m_parser.IndexOperation);
                            ParserRecurse inner = Parse(m_parser, _expression, ref _index);
                            if (inner.FinishReason != ParseFinishReason.ClosedSquareBracket)
                                throw new ParserException("Invalid expression: closing square bracket has not found", _index, _expression);
                            m_stack.addOperand(inner.SingleOperand);
                        }
                    } 
                    else if (ch == '{')
                    {
                        if (lexema != "new")
                            throw new ParserException("Invalid open curly bracket", _index, _expression);
                        lexema = null;

                        ParseNewStructureBlock();                        
                    }
                    else if (ch == ')' || ch == ',' || ch == '}' || (ch == ']' && m_parser.IndexOperation != null)) {
                        //if exist morhpem
                        if (lexema != null) {
                            //then create symbolic operand
                            m_stack.addOperand(CreateVariable(lexema));
                            lexema = null;
                        }
                        //finish parse brackets expression
                        _index += 1;
                        //if is closed brackets, then stop parse that expression 
                        if (ch == ')')
                            return ParseFinishReason.ClosedBracket;
                        else if (ch == ']')
                            return ParseFinishReason.ClosedSquareBracket;
                        else if (ch == '}')
                            return ParseFinishReason.CurlyBracket;
                        else
                        {
                            //FinishCompile();
                            return ParseFinishReason.CommaSeparator;
                        }
                        //othewise continue parse next part of expression
                    } else if ((tmp = m_parser._lexemeParser.Parse(_expression, ref _index)) != null) {
                        if (m_parser._keyWords != null && m_parser._keyWords.IsKeyWord(tmp, out int kwId)) {
                            if (lexema != null) {
                                m_stack.addOperand(CreateVariable(lexema));
                                lexema = null;
                            }
                            KeyWord = kwId;
                            return ParseFinishReason.KeyWord;
                        }
                        else
                            lexema = tmp;
                    } else {
                        throw new ParserException("Unknown syntax", _index, _expression);
                    }
                }

                if (lexema != null)
                    m_stack.addOperand(CreateVariable(lexema));

                return ParseFinishReason.Eof;
            }

            private void FinishCompile() {
                m_stack.finish();
                if (m_stack.getOperands().count > 1)
                    throw new ArgumentException("Expression error");
                if (!m_stack.getOperands().empty) {
                    IOperand operand = m_stack.getOperands().extract();
                    m_operands.AddLast(operand);
                }
            }

            private IOperand CreateVariable(string name) {
                IValue constValue;
                if (m_parser._consts != null && (constValue = m_parser._consts.Resolve(name)) != null)
                    return new StaticOperand(constValue);
                else
                    return new VariableOperand(name);
            }

            public IEnumerator<IOperand> GetEnumerator() {
                return m_operands.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator() {
                return m_operands.GetEnumerator();
            }

            private void ParseNewStructureBlock()
            {
                var properties = new List<KeyValuePair<string, IOperand>>();
                _index++;
                EnsureNotEof();

                ParserRecurse inner = null;
                while (inner == null || inner.FinishReason == ParseFinishReason.CommaSeparator)
                {
                    m_parser._skipSeparators.SkipAll(_expression, ref _index);
                    EnsureNotEof();

                    if (_expression[_index] == '}')
                    {
                        _index++;
                        break;
                    }

                    var lexeme = m_parser._lexemeParser.Parse(_expression, ref _index) ?? throw new ParserException("Expected valid property name", _index, _expression);
                    EnsureNotEof();

                    m_parser._skipSeparators.SkipAll(_expression, ref _index);
                    EnsureNotEof();

                    if (!_expression.IsChar(':', _index))
                        throw new ParserException($"Expected symbol ':' after property name '{lexeme}' in new expression", _index, _expression);
                    _index++;

                    inner = Parse(m_parser, _expression, ref _index);
                    if (inner.FinishReason == ParseFinishReason.CommaSeparator || inner.FinishReason == ParseFinishReason.CurlyBracket)
                    {
                        // add property
                        if (inner.Count == 0)
                            throw new ParserException($"Expression for property '{lexeme}' not found", _index, _expression);

                        properties.Add(new KeyValuePair<string, IOperand>(lexeme, inner.SingleOperand));
                        lexeme = null;
                    }
                    else
                        throw new ParserException($"Unexpected symbol when parsing new expression for property '{lexeme}'", _index, _expression);
                }

                m_stack.addOperand(new StructOperand(properties));
            }

            private void ParseNewArrayBlock()
            {
                var values = new List<IOperand>();
                //_index++;
                EnsureNotEof();

                ParserRecurse inner = null;
                while (inner == null || inner.FinishReason == ParseFinishReason.CommaSeparator)
                {
                    inner = Parse(m_parser, _expression, ref _index);
                    if (inner.FinishReason == ParseFinishReason.CommaSeparator || inner.FinishReason == ParseFinishReason.ClosedSquareBracket)
                    {
                        // add value
                        if (inner.Count == 0)
                        {
                            if (inner.FinishReason != ParseFinishReason.ClosedSquareBracket || values.Count != 0)
                                throw new ParserException($"Expression for array value not found", _index, _expression);
                        }
                        else
                            values.Add(inner.SingleOperand);
                    }
                    else
                        throw new ParserException($"Unexpected symbol when parsing new array expression", _index, _expression);
                }

                m_stack.addOperand(new ArrayOperand(values));
            }

            /// <summary>
            /// detect commentaty at position <paramref name="index"/> and skip it
            /// </summary>
            /// <param name="expression">expression</param>
            /// <param name="index">current position</param>
            /// <returns>true if commentary was skipped</returns>
            private static bool SkipComment(string expression, ref int index) {
                if (expression.IsLineCommentaryBegins(index, out int shift)) {
                    index = expression.SeekNextLine(index + shift);
                    return true;
                } else
                    return false;
            }

            /// <summary>
            /// Throws exception if <see cref="_index" is out of range/>
            /// </summary>
            private void EnsureNotEof()
            {
                if (_index >= _expression.Length)
                    throw new ParserException("Unexpected end of expression", _index, _expression);
            }
        } //class ParserRecurse
    }
}
