﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvaluateExpression.Operands;

using EvaluateExpression.Operations;

namespace EvaluateExpression.Parser {
    /// <summary>
    /// Include two stacks - operands stack and operations stack
    /// </summary>
    class ParserCombineStack {
        private readonly BlockedOperandStack m_operands = new BlockedOperandStack();
        private readonly LinkedList<IOperationInfo> m_opers = new LinkedList<IOperationInfo>();

        public void addOperand(IOperand operand) {
            m_operands.put(operand);
        }

        public void addOperation(IOperationInfo operationInfo) {
            smartPutOperation(operationInfo);            
        }

        public void finish() {
            while (existOperations()) {
                compileLastOperation();
            }
        }

        public IOperandStack getOperands() {
            return m_operands;
        }

        private IOperationInfo extractOperation() {
            if (existOperations())
                m_operands.removeLastBlock();
            IOperationInfo result = m_opers.Last.Value;
            m_opers.RemoveLast();
            return result;
        }

        private void smartPutOperation(IOperationInfo operation) {
            if (!existOperations())
            addOperationWithBlock(operation);
        else {
                int lastPrior = getLastOperationPriority();
                if (lastPrior < operation.priority)
                    addOperationWithBlock(operation);
                else {
                    compileLastOperation();
                    smartPutOperation(operation);
                }
            }
        }

        private int getLastOperationPriority() {
            return m_opers.Last.Value.priority;
        }

        private bool existOperations() {
            return m_opers.Count != 0;
        }

        private void addOperationWithBlock(IOperationInfo operation) {
            m_opers.AddLast(operation);
            m_operands.addBlock();            
        }

        private void compileLastOperation() {
            IOperationInfo info = extractOperation();
            m_operands.put(info.Compile(m_operands));
        }
    }
    
}
