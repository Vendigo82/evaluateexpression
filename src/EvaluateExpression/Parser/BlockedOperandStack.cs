﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Operands;

namespace EvaluateExpression.Parser {
    /// <summary>
    /// operands stack with blocking possibility
    /// </summary>
    public class BlockedOperandStack : IOperandStack {
        private readonly LinkedList<IOperand> m_operands = new LinkedList<IOperand>();
        private int m_count = 0;
        private readonly Block m_block = new Block();

        public void put(IOperand operand) {
            m_operands.AddLast(operand);
            m_count += 1;
        }

        public void addBlock() {
            m_operands.AddLast(m_block);
        }

        public void removeLastBlock() {
            LinkedListNode<IOperand> node = m_operands.Last;
            while (node != null) {
                if (node.Value == m_block) {
                    m_operands.Remove(node);
                    return;
                }
                node = node.Previous;
            }
        }

        public IOperand extract() {
            if (m_operands.Count == 0 || m_operands.Last.Value == m_block)
                throw new InvalidOperationException("Operands stack is blocked");

            IOperand result = m_operands.Last.Value;
            m_operands.RemoveLast();
            m_count -= 1;
            return result;
        }

        public int count {
            get {
                return m_count;
            }
        }

        public bool empty {
            get {
                return m_operands.Count == 0 || m_operands.Last.Value == m_block;
            }
        }

        private class Block : IOperand {
            public IValue Eval(INameResolver context) {
                throw new NotImplementedException();
            }
        }
    }
}
