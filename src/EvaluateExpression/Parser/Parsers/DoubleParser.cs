﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers {
    using System.Globalization;
    using Values;

    public class DoubleParser : IValueParser
    {
        private CultureInfo _format;

        public DoubleParser() {
            CultureInfo info = CultureInfo.InvariantCulture;
            _format = info;            
        }

        public DoubleParser(CultureInfo format) {
            _format = format;
        }

        public IValue Parse(string str, ref int index) {
            bool existPoint = false;
            StringBuilder sb = new StringBuilder();
            while (index < str.Length) {
                char ch = str[index];
                if (isDigit(ch))
                    sb.Append(ch);
                else if (ch == _format.NumberFormat.NumberDecimalSeparator[0]) {
                    if (existPoint)
                        throw new InvalidOperationException("Parse error: too many points in digit");
                    sb.Append(ch);
                    existPoint = true;
                } else {
                    return CreateValue(sb, existPoint);
                }
                index += 1;
            }

            return CreateValue(sb, existPoint);
        }

        /// <summary>
        /// return true if ch is digit
        /// </summary>
        public bool isDigit(char ch) {
            return ch >= '0' && ch <= '9';
        }

        private IValue CreateValue(StringBuilder sb, bool existPoint) {
            if (sb.Length == 0)
                return null;

            if (existPoint)
                return new DoubleValue(Double.Parse(sb.ToString(), _format));
            else
                return new IntValue(Int32.Parse(sb.ToString()));
        }
    }
}
