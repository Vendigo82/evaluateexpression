﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers {
    public class LexemeParserWithDot : LexemeParser {
        public LexemeParserWithDot() : base(IsValidStartingCharFunc, IsValidCharOrDotFunc) {
        }

        public static bool IsValidCharOrDotFunc(char ch) {
            return LexemeParser.IsValidCharFunc(ch) || ch == '.';
        }
    }
}
