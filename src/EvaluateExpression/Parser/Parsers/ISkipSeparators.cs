﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers 
{
    public interface ISkipSeparators
    {
        bool Skip(string str, ref int index, out bool newLine);
    }

    public static class SkipSeparatorsExtensions
    {
        public static void SkipAll(this ISkipSeparators skip, string str, ref int index)
        {
            while (index < str.Length && skip.Skip(str, ref index, out var _)) {

            }
        }
    }
}
