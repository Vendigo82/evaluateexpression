﻿using System.Globalization;

namespace EvaluateExpression.Parser.Parsers 
{
    public class SkipSeparators : ISkipSeparators
    {
        public bool Skip(string str, ref int index, out bool newLine) 
        {
            //skip empty symbols
            newLine = false;
            if (char.IsWhiteSpace(str, index)) {
                if (str[index] == '\n' || CharUnicodeInfo.GetUnicodeCategory(str, index) == UnicodeCategory.LineSeparator)
                    newLine = true;
                index += 1;
                return true;
            }

            return false;
        }
    }
}
