﻿#nullable enable

namespace EvaluateExpression.Parser.Parsers
{
    public interface IValueParser
    {
        /// <summary>
        /// Returns value or null, if string position is not value
        /// </summary>
        /// <param name="str">string for parse</param>
        /// <param name="index">in: current position; out - next position to the end of value</param>
        /// <returns>Value or null</returns>
        IValue? Parse(string str, ref int index);
    }
}
