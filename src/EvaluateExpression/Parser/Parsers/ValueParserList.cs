﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Parser.Parsers
{
    public class ValueParserList : IValueParser {
        private readonly List<IValueParser> _list = new List<IValueParser>();
        private bool _skipSpaces;

        public ValueParserList(IEnumerable<IValueParser> parsers, bool skipSpaces) {
            _list.AddRange(parsers);
            _skipSpaces = skipSpaces;
        }

        public IValue Parse(string str, ref int index) {
            while (str[index] == ' ')
                if (++index >= str.Length)
                    return null;

            foreach (IValueParser p in _list) {
                IValue v = p.Parse(str, ref index);
                if (v != null)
                    return v;
            }
            return null;
        }
    }
}
