﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers
{
    /// <summary>
    /// Extrends lexeme parser. Can parse lexeme like @"xxxxx"
    /// </summary>
    public class LexemeExParser : ILexemeParser
    {
        private readonly ILexemeParser _origin;

        public LexemeExParser() : this(new LexemeParser()) {
        }

        public LexemeExParser(ILexemeParser parser) {
            _origin = parser;
        }

        public string Parse(string str, ref int index) {
            if (index + 1 < str.Length && str[index] == '@' && str[index + 1] == '"') {
                //skip starting symbols
                int start = index + 2;
                index = start;
                while (index < str.Length) {
                    if (str[index] == '"') {
                        if (index - start == 0)
                            throw new ParserException("Lexeme name can't be empty", index);
                        index += 1;
                        return str.Substring(start, index - start - 1);
                    } else
                        index += 1;                    
                }
                throw new ParserException("Missed close quotes for extended lexeme", index);
            } else
                return _origin.Parse(str, ref index);
        }
    }
}
