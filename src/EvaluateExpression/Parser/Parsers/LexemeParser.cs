﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers {
    public class LexemeParser : ILexemeParser {
        private readonly Func<char, bool> IsValidStartingChar;
        private readonly Func<char, bool> IsValidChar;

        public LexemeParser(Func<char, bool> IsValidStartingChar, Func<char, bool> IsValidChar) {
            this.IsValidChar = IsValidChar;
            this.IsValidStartingChar = IsValidStartingChar;
        }

        public LexemeParser() : this(IsValidStartingCharFunc, IsValidCharFunc) {
        }

        public string Parse(string str, ref int index) {
            int startIndex = index;
            if (IsValidStartingChar(str[index])) {
                do {
                    index += 1;
                } while (index < str.Length && IsValidChar(str[index]));
            }

            if (startIndex == index)
                return null;
            return str.Substring(startIndex, index - startIndex);
        }

        public static bool IsValidStartingCharFunc(char ch) {
            return Char.IsLetter(ch) || ch == '_';
        }

        public static bool IsValidCharFunc(char ch) {
            return Char.IsLetterOrDigit(ch) || ch == '_';
        }
    }
}
