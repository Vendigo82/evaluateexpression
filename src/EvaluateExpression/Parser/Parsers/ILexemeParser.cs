﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Parser.Parsers {
    /// <summary>
    /// Extract next lexema from input string
    /// </summary>
    public interface ILexemeParser {
        /// <summary>
        /// Extract lexema from input stirng <paramref name="str"/> starting from index <paramref name="index"/>
        /// and returns extracted lexema
        /// </summary>
        /// <param name="str">input string</param>
        /// <param name="index">starting index in input string. Resulting value is position after extracted lexema</param>
        /// <returns>Extracted lexema or null</returns>
        string Parse(string str, ref int index);
    }
}
