﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluateExpression.Parser.Parsers
{
    public class NullTrueFalseParser : IValueParser {

        public const string KEYWORD_NULL = "null";
        public const string KEYWORD_TRUE = "true";
        public const string KEYWORD_FALSE = "false";

        public IValue Parse(string str, ref int index) {
            switch (str[index]) {
                case 't':
                    if (IsWord(str, ref index, KEYWORD_TRUE))
                        return Values.BoolValue.True;
                    break;
                case 'f':
                    if (IsWord(str, ref index, KEYWORD_FALSE))
                        return Values.BoolValue.False;
                    break;
                case 'n':
                    if (IsWord(str, ref index, KEYWORD_NULL))
                        return Values.NullValue.Null;
                    break;
            }
            return null;
        }

        private bool IsWord(string str, ref int index, string word) {
            if (index + word.Length > str.Length)
                return false;

            for (int i = 0; i < word.Length; ++i) {
                if (str[index + i] != word[i])
                    return false;
            }

            if (index + word.Length == str.Length || !Char.IsLetterOrDigit(str[index + word.Length])) {
                index += word.Length;
                return true;
            }

            return false;
        }
    }
}
