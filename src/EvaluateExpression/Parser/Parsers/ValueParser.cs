﻿using EvaluateExpression.Exceptions;

#nullable enable

namespace EvaluateExpression.Parser.Parsers
{
    public class ValueParser : IValueParser
    {
        public const char STRING_COMMAS = '"';
        private readonly DoubleParser _doubleParser = new DoubleParser();

        public IValue? Parse(string str, ref int index)
        {
            if (_doubleParser.isDigit(str[index]))
            {
                return _doubleParser.Parse(str, ref index);
            }
            else if (str[index] == STRING_COMMAS)
            {
                return ParseString(str, ref index);
            }
            else
            {
                return null;
            }
        }

        private static IValue ParseString(string str, ref int index)
        {
            int start = ++index;
            while (index < str.Length)
            {
                if (str[index] == '\\' && TryGetNext(str, index) == STRING_COMMAS)
                {
                    index += 2;
                }
                else if (str[index] == STRING_COMMAS)
                {
                    index += 1;
                    return new Values.StringValue(str.Substring(start, index - start - 1).Replace("\\\"", "\""));
                }
                else
                    index += 1;
            }

            throw new ParserException($"Cannot extract string from <{str}> in position {index}", index);
        }

        private static char TryGetNext(string str, int index) => (index + 1) < str.Length ? str[index + 1] : (char)0;
    }
}
