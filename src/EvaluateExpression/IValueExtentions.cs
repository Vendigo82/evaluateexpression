﻿using System;
using System.Collections.Generic;
using System.Linq;
using EvaluateExpression.Values;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression
{

    /// <summary>
    /// <see cref="IValue"/> extensions methods
    /// </summary>
    public static class IValueExtentions
    {
        public const string COUNT_PROPERTY = "Count";

        /// <summary>
        /// Returns true if <paramref name="type"/> is numeric value (integer or floating point)
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        static public bool IsNumeric(this ValueType type) => !(type == ValueType.Unknown || type == ValueType.String);

        /// <summary>
        /// Returns true if <paramref name="val"/> is floating point value
        /// </summary>
        static public bool IsFloatPoint(this IValue val) => val.Type == ValueType.Decimal;

        /// <summary>
        /// Returns true if <paramref name="val"/> is integer value
        /// </summary>
        static public bool IsInteger(this IValue val) => val.Type == ValueType.Boolean || val.Type == ValueType.Integer;

        /// <summary>
        /// Converts <see cref="double"/> to <see cref="IValue"/>
        /// </summary>

        static public IValue ToValue(this double val) => new DoubleValue(val);

        /// <summary>
        /// Converts <see cref="int"/> to <see cref="IValue"/>
        /// </summary>

        static public IValue ToValue(this int val) => new IntValue(val);

        /// <summary>
        /// Converts <see cref="bool"/> to <see cref="IValue"/>
        /// </summary>

        static public IValue ToValue(this bool val) => BoolValue.Get(val);

        /// <summary>
        /// Converts <see cref="string"/> to <see cref="IValue"/>
        /// </summary>
        static public IValue ToValue(this string val) => val == null ? (IValue)NullValue.Null : new StringValue(val);

        /// <summary>
        /// Converts <see cref="DateTime"/> to <see cref="IValue"/>
        /// </summary>
        static public IValue ToValue(this DateTime val) => new Values.DateTimeValue(val);

        /// <summary>
        /// Converts <paramref name="val"/> to <see cref="IValue"/>. Throws exception if type of value does not support
        /// </summary>
        /// <param name="val">converted value</param>
        /// <returns></returns>
        /// <exception cref="EvalInvalidValueTypeException">throws if type of <paramref name="val"/> does not support</exception>
        static public IValue ToValue(this object val) {
            if (val == null)
                return NullValue.Null;

            if (val is IValue value)
                return value;

            Type t = val.GetType();

            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>)) {
                if ((bool)(t.GetProperty("HasValue").GetGetMethod().Invoke(val, null)) == false)
                    return NullValue.Null;
                t = Nullable.GetUnderlyingType(t);
            }

            //t = Nullable.GetUnderlyingType(t) ?? t;

            if (t.IsPrimitive && t.IsValueType) {
                if (t == typeof(byte)) return ((int)((byte)val)).ToValue();
                if (t == typeof(short)) return ((int)(short)val).ToValue();
                if (t == typeof(int)) return ((int)val).ToValue();
                if (t == typeof(ushort)) return ((int)(ushort)val).ToValue();
                if (t == typeof(uint)) return ((int)(uint)val).ToValue();
                if (t == typeof(long)) return ((int)(long)val).ToValue();
                if (t == typeof(ulong)) return ((int)(ulong)val).ToValue();
                if (t == typeof(bool)) return ((bool)val).ToValue();
                if (t == typeof(float)) return ((double)((float)val)).ToValue();
                if (t == typeof(double)) return ((double)val).ToValue();
            }

            if (val is decimal dec)
                return decimal.ToDouble(dec).ToValue();

            if (val is Guid guid)
                return guid.ToString().ToValue();

            if (val is string str)
                return str.ToValue();

            if (val is DateTime time)
                return time.ToValue();

            if (val is Dictionary<string, object> dict)
                return dict.ToValue();

            throw new EvalInvalidValueTypeException(string.Concat("Type [", t.Name, "] is not support"));
        }

        /// <summary>
        /// Returns name of the "Count" property
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        static public string CountPropertyName(this IIndexableValue _) => COUNT_PROPERTY;

        /// <summary>
        /// Convert dictionary to <see cref="StructValue"/>.
        /// Dictionary key become to struct fields and values become to struct values
        /// </summary>
        /// <param name="dict">Origin dictionary</param>
        /// <returns>Value</returns>
        public static IValue ToValue(this Dictionary<string, object> dict)
            => new StructValue(dict.Select(i => new KeyValuePair<string, IValue>(i.Key, i.Value.ToValue())));

        /// <summary>
        /// Cast value to <see cref="IIndexableValue"/> or throw exception
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="EvalCastToArrayException"></exception>
        public static IEnumerable<IValue> AsArray(this IValue value) => value as IEnumerable<IValue> ?? throw new EvalCastToArrayException(value);

        public static IValue ThrowIfNotArray(this IValue value) => value.IsArray(out var _) ? value : throw new EvalCastToArrayException(value);

        public static bool IsArray(this IValue value, out IEnumerable<IValue> list)
        {
            if (value is IEnumerable<IValue> tmp)
            { 
                list = tmp; 
                return true; 
            }
            else
            {
                list = null;
                return false;
            }
        }
    }
}
