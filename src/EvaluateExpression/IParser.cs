﻿using System;
using System.Collections.Generic;

namespace EvaluateExpression {

    /// <summary>
    /// статус завершения парсинга
    /// </summary>
    public enum ParseFinishReason {
        /// <summary>
        /// End of data
        /// </summary>
        Eof,    

        /// <summary>
        /// End logic line with
        /// </summary>
        EndLine,   

        /// <summary>
        /// Close bracket
        /// </summary>
        ClosedBracket,  

        /// <summary>
        /// Key word
        /// </summary>
        KeyWord,    

        /// <summary>
        /// Close square bracket
        /// </summary>
        ClosedSquareBracket,    

        /// <summary>
        /// Comma separator ','
        /// </summary>
        CommaSeparator,

        /// <summary>
        /// Finished because of curly bracket '}'
        /// </summary>
        CurlyBracket
    }

    public static class ParseFinishReasonExtensions {
        public static bool IsComplete(this ParseFinishReason r) {
            return r == ParseFinishReason.Eof || r == ParseFinishReason.EndLine;
        }
    }

    /// <summary>
    /// Extendend result or parsing expression
    /// </summary>
    public interface IParseResult : IEnumerable<IOperand> {
        /// <summary>
        /// count of operands
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Get single operand. If have not operand or operand more then 1, then throw exception
        /// </summary>
        IOperand SingleOperand { get; }

        /// <summary>
        /// Finish parsing reason
        /// </summary>
        ParseFinishReason FinishReason { get; }

        /// <summary>
        /// Founded KeyWord id
        /// </summary>
        int KeyWord { get; }
    }


    /// <summary>
    /// parser interface
    /// </summary>
    public interface IParser
    {
        IParseResult Parse(String expression, ref int index);
    }



    public static class IParserExtensions
    {
        /// <summary>
        /// Parse whole string as single expression
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static IOperand Parse(this IParser parser, string expression) 
        {
            int index = 0;
            IParseResult result = parser.Parse(expression, ref index);
            if (index < expression.Length || !result.FinishReason.IsComplete())
                throw new Exceptions.ParserException("Expression error", index, expression);

            return result.SingleOperand;
        }

        /// <summary>
        /// Parsing comma separated list, like 'a, b, c'.
        /// </summary>
        /// <param name="parser"></param>
        /// <param name="expression"></param>
        /// <param name="index"></param>
        /// <returns>Finishing reason and list of operands</returns>
        public static Tuple<IParseResult, IEnumerable<IOperand>> ParseCommaSeparatedList(this IParser parser, string expression, ref int index)
        {
            var operands = new List<IOperand>();
            IParseResult prevResult = null;
            while (operands.Count == 0 || prevResult.FinishReason == ParseFinishReason.CommaSeparator)
            {
                prevResult = parser.Parse(expression, ref index);
                if (prevResult.Count == 0)
                    throw new Exceptions.ParserException("Missing required element", index, expression);
                operands.Add(prevResult.SingleOperand);
            }

            return Tuple.Create(prevResult, (IEnumerable<IOperand>)operands);
        }
    }
}
