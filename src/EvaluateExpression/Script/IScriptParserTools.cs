﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script {
    public interface IScriptParserTools {
        IScriptBlockParser GetBlockParser(int KeyWord);
        IScriptBlockParser LinearParser { get; }

        IParser Parser { get; }
    }

    public static class IScriptParserToolsExtentions
    {
        /// <summary>
        /// Parse script while not reach expected key words
        /// </summary>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <param name="parsers"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static LinkedList<IScriptBlock> ParseBlock(
            this IScriptParserTools parsers,
            string source,
            ref int index,
            ref IParseResult result,
            Func<int, bool> CheckKeyword,
            bool checkIsLineClosedAfterKeyWord = false) {

            LinkedList<IScriptBlock> list = new LinkedList<IScriptBlock>();
            while (true) {
                IScriptBlock block = parsers.LinearParser.Parse(source, ref index, parsers, ref result);
                if (block != null)
                    list.AddLast(block);

                if (result.FinishReason == ParseFinishReason.KeyWord) {
                    if (CheckKeyword(result.KeyWord)) {
                        if (checkIsLineClosedAfterKeyWord) {
                            CheckNextIsEndOfLine(parsers, source, ref index, ref result);
                        }
                        return list;
                    } else
                        list.AddLast(parsers.GetBlockParser(result.KeyWord).Parse(source, ref index, parsers, ref result));
                } else
                    throw new ParserException("Invalid syntax", index);
            };
        }

        public static void CheckNextIsEndOfLine(this IScriptParserTools parsers, string source, ref int index, ref IParseResult result) {
            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason != ParseFinishReason.EndLine || result.Count > 0)
                throw new ParserException("Missed end of line", index);
        }
    }
}
