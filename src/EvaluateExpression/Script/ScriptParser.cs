﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script {
    using Blocks;

    public class ScriptParser : IScriptParser, IScriptParserTools {

        private IScriptBlockParser _ifParser = new IfBlockParser((int)KeyWords.Then, (int)KeyWords.Else, (int)KeyWords.End);
        private IScriptBlockParser _returnParser = new ReturnBlockParser();
        private IScriptBlockParser _declareParser = new DeclareBlockParser();
        private IScriptBlockParser _foreachParser = new ForeachBlockParser((int)KeyWords.In, (int)KeyWords.Do, (int)KeyWords.End);
        private IScriptBlockParser _continueParser = new EmptyEndLineParser(new ThrowBlock<ScriptContinueInstruction>());
        private IScriptBlockParser _breakParser = new EmptyEndLineParser(new ThrowBlock<ScriptBreakInstruction>());
        private IScriptBlockParser _whileParser = new WhileBlockParser((int)KeyWords.Do, (int)KeyWords.End);

        public IScriptBlockParser LinearParser { get; } = new LinearBlockParser();


        public IParser Parser { get; }

        private enum KeyWords{
            If = 1,
            Then = 2,
            Else = 3,
            End = 4,
            Return = 5,
            Declare = 6,
            Foreach = 7,
            In = 8,
            Do = 9,
            Break = 10,
            Continue = 11,
            While = 12,
        }

        public ScriptParser (IParser parser) {
            Parser = parser;
        }

        public ScriptParser() : this(new Parser.Parser(keyWords: GetKeyWords(), operations: OperationsPack.CreateDefFactory(true))) {
        }

        public IScript Parse(string script) {
            int index = 0;
            IParseResult result = null;
            LinearBlock blocks = new LinearBlock();
            while (true) {
                IScriptBlock b = LinearParser.Parse(script, ref index, this, ref result);
                if (b != null)
                    blocks.Blocks.AddLast(b);

                if (result.FinishReason == ParseFinishReason.KeyWord)
                    b = GetBlockParser(result.KeyWord).Parse(script, ref index, this, ref result);
                else if (result.FinishReason == ParseFinishReason.Eof)
                    return new Script(blocks);

                if (b != null)
                    blocks.Blocks.AddLast(b);
            }
        }

        public static IEnumerable<KeyValuePair<string, int>> GetWordsList() {
            return new KeyValuePair<string, int>[] {
                new KeyValuePair<string, int>("IF", (int)KeyWords.If),
                new KeyValuePair<string, int>("THEN", (int)KeyWords.Then),
                new KeyValuePair<string, int>("ELSE", (int)KeyWords.Else),
                new KeyValuePair<string, int>("END", (int)KeyWords.End),
                new KeyValuePair<string, int>("RETURN", (int)KeyWords.Return),
                new KeyValuePair<string, int>("DECLARE", (int)KeyWords.Declare),
                new KeyValuePair<string, int>("FOREACH", (int)KeyWords.Foreach),
                new KeyValuePair<string, int>("IN", (int)KeyWords.In),
                new KeyValuePair<string, int>("DO", (int)KeyWords.Do),
                new KeyValuePair<string, int>("BREAK", (int)KeyWords.Break),
                new KeyValuePair<string, int>("CONTINUE", (int)KeyWords.Continue),
                new KeyValuePair<string, int>("WHILE", (int)KeyWords.While)
            };
        }

        public static IKeyWords GetKeyWords() {
            return new KeyWordCollection(GetWordsList());
        }

        public IScriptBlockParser GetBlockParser(int keyWord) {
            KeyWords word = (KeyWords)keyWord;
            if ((int)word != keyWord)
                throw new Exception("Unknown keyword: " + keyWord.ToString());
            switch (word) {
                case KeyWords.If:
                    return _ifParser;
                case KeyWords.Return:
                    return _returnParser;
                case KeyWords.Declare:
                    return _declareParser;
                case KeyWords.Foreach:
                    return _foreachParser;
                case KeyWords.Continue:
                    return _continueParser;
                case KeyWords.Break:
                    return _breakParser;
                case KeyWords.While:
                    return _whileParser;
                default:
                    throw new Exception("Illegal syntax: " + keyWord.ToString());
            }
        }
    }
}
