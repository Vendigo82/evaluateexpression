﻿namespace EvaluateExpression.Script 
{
    public interface IScriptBlockParser 
    {
        IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result);
    }    
}
