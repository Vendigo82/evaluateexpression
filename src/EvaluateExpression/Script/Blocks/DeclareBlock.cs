﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Script.Blocks
{
    public class DeclareBlock : IScriptBlock
    {
        private readonly IEnumerable<string> _names;

        public DeclareBlock(IEnumerable<string> names)
        {
            _names = names;
        }

        public DeclareBlock(string name)
        {
            _names = new string[] { name };
        }

        public IEnumerable<string> Names => _names;

        public void Perform(INameResolver context) 
        {
            foreach (string name in _names) {
                if (!context.DeclareVariable(name))
                    throw new DublicateDeclareVariableException(name);
            }
        }
    }

    public class DublicateDeclareVariableException : EvalInvalidOperationException
    {
        public DublicateDeclareVariableException(string name) : base($"Variable with name [{name}] already exists")
        { 
            VariableName = name;
        }

        public string VariableName { get; }
    }
}
