﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks {
    public class ReturnBlockParser : IScriptBlockParser {
        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason == ParseFinishReason.EndLine) {
                if (result.Count == 0)
                    return new ReturnBlock(Values.NullValue.Null);
                else
                    return new ReturnBlock(result.SingleOperand);
            }

            throw new Exception(String.Concat("Invalid syntax at ", index.ToString()));
        }
    }
}
