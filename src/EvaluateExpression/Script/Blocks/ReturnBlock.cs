﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks {
    public class ReturnBlock : IScriptBlock {
        private readonly IOperand _operand;

        public ReturnBlock(IOperand operand) {
            _operand = operand;
        }

        public void Perform(INameResolver context) {
            throw new ScriptReturnInstruction(_operand.Eval(context));
        }
    }
}
