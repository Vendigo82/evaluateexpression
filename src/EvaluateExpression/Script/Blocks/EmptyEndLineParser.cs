﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    /// <summary>
    /// Just looking for end line symbol. If has any another operands, then throw exception
    /// </summary>
    public class EmptyEndLineParser : IScriptBlockParser
    {
        private IScriptBlock _block;

        public EmptyEndLineParser(IScriptBlock block) {
            _block = block;
        }

        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason == ParseFinishReason.EndLine && result.Count == 0)
                return _block;
            else
                throw new ParserException("Missed end of line", index);
        }
    }
}
