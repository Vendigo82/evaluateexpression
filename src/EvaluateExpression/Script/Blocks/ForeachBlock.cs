﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    public class ForeachBlock : IScriptBlock
    {
        public ForeachBlock(string localName, IOperand list, IScriptBlock block) 
        {
            LocalName = localName;
            List = list;
            Block = block;
        }

        public string LocalName { get; }
        public IScriptBlock Block { get; }
        public IOperand List { get; }

        public void Perform(INameResolver context) 
        {
            IValue value = List.Eval(context);
            if (!(value is IEnumerable<IValue> array))
                throw new EvalCastToArrayException(value);

            context.DeclareVariable(LocalName);
            foreach (IValue iter in array) {
                context.Assign(LocalName, iter);
                try {
                    Block.Perform(context);
                } catch (ScriptBreakInstruction) {
                    return;
                } catch (ScriptContinueInstruction) {
                    //make new iteration
                }
            }
        }
    }
}
