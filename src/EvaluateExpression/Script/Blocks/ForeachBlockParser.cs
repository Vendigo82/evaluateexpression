﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Script.Blocks
{
    public class ForeachBlockParser : IScriptBlockParser
    {
        private readonly int _kwIn;
        private readonly int _kwDo;
        private readonly int _kwEnd;

        public ForeachBlockParser(int kwIn, int kwDo, int kwEnd) {
            _kwIn = kwIn;
            _kwDo = kwDo;
            _kwEnd = kwEnd;
        }

        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason != ParseFinishReason.KeyWord || result.KeyWord != _kwIn)
                throw new ParserException("Foreach construction error: expected keyword IN", index);

            INamedOperand iterOperand = result.SingleOperand as INamedOperand;
            if (iterOperand is null)
                throw new ParserException("Foreach construction error: invalid iterator variable name", index);

            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason != ParseFinishReason.KeyWord || result.KeyWord != _kwDo)
                throw new ParserException("Foreach construction error: expected keyword DO", index);

            IOperand iterable = result.SingleOperand;
            if (iterable is null)
                throw new ParserException("Foreach construction error: expected single iterable value", index);

            LinkedList<IScriptBlock> inner = parsers.ParseBlock(source, ref index, ref result, (kw) => kw == _kwEnd);
            return new ForeachBlock(iterOperand.Name, iterable, new LinearBlock(inner));
        }
    }
}
