﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks {
    public class IfBlockParser : IScriptBlockParser {
        private readonly int _kwThen;
        private readonly int _kwElse;
        private readonly int _kwEnd;

        public IfBlockParser(int kwThen, int kwElse, int kwEnd) {
            _kwThen = kwThen;
            _kwElse = kwElse;
            _kwEnd = kwEnd;
        }

        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason != ParseFinishReason.KeyWord || result.KeyWord != _kwThen || result.Count != 1)
                throw new Exception(String.Concat("Invalid operator if syntax at ", index.ToString()));

            IOperand condition = result.SingleOperand;

            LinkedList<IScriptBlock> then = ParseBlock(source, ref index, parsers, ref result);
            if (result.KeyWord == _kwEnd)
                return new Blocks.IfBlock(condition, then, null);
            else {
                LinkedList<IScriptBlock> els = ParseBlock(source, ref index, parsers, ref result);
                if (result.KeyWord == _kwEnd)
                    return new Blocks.IfBlock(condition, then, els);
                else
                    throw new Exception(String.Concat("Invalid operator if syntax at ", index.ToString()));
            }
        }

        private LinkedList<IScriptBlock> ParseBlock(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            LinkedList<IScriptBlock> list = parsers.ParseBlock(source, ref index, ref result, (kw) => kw == _kwElse || kw == _kwEnd, false);
            //if (result.KeyWord == _kwEnd)
            //    parsers.CheckNextIsEndOfLine(source, ref index, ref result);
            return list;
            /*LinkedList<IScriptBlock> list = new LinkedList<IScriptBlock>();
            while (true) {
                IScriptBlock block = parsers.LinearParser.Parse(source, ref index, parsers, ref result);
                if (block != null)
                    list.AddLast(block);

                if (result.FinishReason == ParseFinishReason.KeyWord) {
                    if (result.KeyWord == _kwElse || result.KeyWord == _kwEnd)
                        return list;
                    else
                        list.AddLast(parsers.GetBlockParser(result.KeyWord).Parse(source, ref index, parsers, ref result));
                }
                else {
                    throw new Exception(String.Concat("Invalid operator if syntax at ", index.ToString()));
                }
            };*/
        }
    }
}
