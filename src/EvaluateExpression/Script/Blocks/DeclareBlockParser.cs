﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    public class DeclareBlockParser : IScriptBlockParser
    {
        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            var parseResult = parsers.Parser.ParseCommaSeparatedList(source, ref index); //parsers.Parser.Parse(source, ref index);
            result = parseResult.Item1;
            if (result.FinishReason == ParseFinishReason.EndLine) {
                try {
                    IEnumerable<string> list = parseResult.Item2.Cast<INamedOperand>().Select(o => o.Name).ToArray();
                    return new DeclareBlock(list);
                } catch (InvalidCastException e) {
                    throw new ParserException("Declare block should contains only valid variables names", index, source, e);
                }
            } else {
                throw new ParserException("Declare block should ends with semicolon", index);
            }
        }
    }
}
