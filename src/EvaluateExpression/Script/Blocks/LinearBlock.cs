﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks {
    public class LinearBlock : IScriptBlock {
        private readonly LinkedList<IScriptBlock> _list;

        public LinearBlock(LinkedList<IScriptBlock> list) {
            _list = list;
        }

        public LinearBlock() : this(new LinkedList<IScriptBlock>()) {
        }

        public LinkedList<IScriptBlock> Blocks => _list;

        public void Perform(INameResolver context) {
            _list.Perform(context);
        }
    }
}
