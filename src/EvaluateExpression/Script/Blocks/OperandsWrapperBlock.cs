﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks {
    public class OperandsWrapperBlock : IScriptBlock {
        private readonly LinkedList<IOperand> _list = new LinkedList<IOperand>();

        public LinkedList<IOperand> Lines => _list;

        public void Perform(INameResolver context) {
            foreach (var oper in _list)
                oper.Eval(context);
        }
    }
}
