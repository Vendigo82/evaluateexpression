﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks {
    public class IfBlock : IScriptBlock {
        private IOperand _condition;
        private IEnumerable<IScriptBlock> _then;
        private IEnumerable<IScriptBlock> _else;

        public IfBlock (IOperand condition, IEnumerable<IScriptBlock> then, IEnumerable<IScriptBlock> els) {
            _condition = condition;
            _then = then;
            _else = els;
        }

        public void Perform(INameResolver context) {
            IEnumerable<IScriptBlock> block = _condition.Eval(context).AsBool ? _then : _else;
            if (block != null)
                block.Perform(context);
        }
    }
}
