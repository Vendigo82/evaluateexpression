﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    public class WhileBlock : IScriptBlock
    {
        public WhileBlock(IOperand condition, IScriptBlock block) {
            Condition = condition;
            Block = block;
        }

        public IOperand Condition { get; }
        public IScriptBlock Block { get; }
        public ulong Limit { get; set; } = 10000;

        public void Perform(INameResolver context) {
            ulong cnt = 0;
            while (Condition.Eval(context).AsBool) {
                cnt += 1;
                try {
                    Block.Perform(context);
                } catch (ScriptBreakInstruction) {
                    return;
                } catch (ScriptContinueInstruction) {
                    //make new iteration
                }

                if (cnt >= Limit)
                    throw new EvalEndlessLoopException();
            }
        }
    }
}
