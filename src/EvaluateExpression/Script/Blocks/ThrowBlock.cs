﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    public class ThrowBlock<T> : IScriptBlock where T : ScriptPerformInstructionException, new()
    {
        public void Perform(INameResolver context) {
            throw new T();
        }
    }
}
