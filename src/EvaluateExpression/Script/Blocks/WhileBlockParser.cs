﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    public class WhileBlockParser : IScriptBlockParser
    {
        private readonly int _kwDo;
        private readonly int _kwEnd;

        public WhileBlockParser(int kwDo, int kwEnd) {
            _kwDo = kwDo;
            _kwEnd = kwEnd;
        }

        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {
            result = parsers.Parser.Parse(source, ref index);
            if (result.FinishReason != ParseFinishReason.KeyWord || result.KeyWord != _kwDo)
                throw new ParserException("Expected DO keyword", index);

            IOperand condition = result.SingleOperand;

            LinkedList<IScriptBlock> blocks = parsers.ParseBlock(source, ref index, ref result, (kw) => kw == _kwEnd);
            return new WhileBlock(condition, new LinearBlock(blocks));
        }
    }
}
