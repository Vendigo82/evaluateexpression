﻿using EvaluateExpression.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script.Blocks
{
    public class LinearBlockParser : IScriptBlockParser
    {
        public IScriptBlock Parse(string source, ref int index, IScriptParserTools parsers, ref IParseResult result) {            
            Blocks.OperandsWrapperBlock operandsBlock = null;

            while (true) {
                result = parsers.Parser.Parse(source, ref index);

                switch (result.FinishReason) {
                    case ParseFinishReason.EndLine:
                        if (result.Count != 0) {
                            if (operandsBlock == null)
                                operandsBlock = new Blocks.OperandsWrapperBlock();
                            operandsBlock.Lines.AddLast(result.SingleOperand);
                        }
                        break;

                    case ParseFinishReason.KeyWord:
                        if (result.Count > 0)
                            throw new Exception(String.Concat("Line containts key word in illegal position, position: ", index.ToString()));
                        return operandsBlock;

                    case ParseFinishReason.Eof:
                        if (result.Count > 0)
                            throw new ParserException("Unexpected end of file", index);
                        return operandsBlock;

                    default:
                        throw new Exception(String.Concat("Illegal syntax at ", index.ToString()));
                }
            }
        }
    }
}
