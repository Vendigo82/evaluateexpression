﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression.Script {
    public class KeyWordCollection : IKeyWords {
        private readonly Dictionary<string, int> _words = new Dictionary<string, int>();

        public KeyWordCollection(IEnumerable<KeyValuePair<string, int>> words) {
            foreach (var kw in words)
                _words.Add(kw.Key.ToUpper(), kw.Value);
        }

        public bool IsKeyWord(string lex, out int keywordId) {
            return _words.TryGetValue(lex.ToUpper(), out keywordId);
        }
    }
}
