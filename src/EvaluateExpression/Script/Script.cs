﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression.Values;

namespace EvaluateExpression.Script
{
    public class Script : IScript
    {
        private readonly IScriptBlock _block;

        public Script(IScriptBlock block) {
            _block = block;
        }

        public IValue Perform(INameResolver context) {
            try {
                _block.Perform(context);
            } catch (ScriptReturnInstruction e) {
                return e.Value;
            } catch (ScriptPerformInstructionException) {
            }

            return NullValue.Null;
        }
    }

    public class ScriptPerformInstructionException : Exception {}

    public class ScriptReturnInstruction : ScriptPerformInstructionException
    {
        public IValue Value { get; }
        public ScriptReturnInstruction(IValue value) { Value = value; }
    }

    public class ScriptBreakInstruction : ScriptPerformInstructionException { }
    public class ScriptContinueInstruction : ScriptPerformInstructionException { }
}
