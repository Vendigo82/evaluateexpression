﻿using System.Collections.Generic;

namespace EvaluateExpression.Script {
    public interface IScriptBlock 
    {
        void Perform(INameResolver context);
    }

    public static class IScriptBlockExtensions 
    {
        public static void Perform(this IEnumerable<IScriptBlock> list, INameResolver context) 
        {
            foreach (IScriptBlock block in list)
                block.Perform(context);
        }
    }
}
