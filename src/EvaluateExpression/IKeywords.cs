﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluateExpression {
    /// <summary>
    /// Check if lexeme is keyword then returns Keyword id
    /// </summary>
    public interface IKeyWords {
        bool IsKeyWord(string lex, out int keywordId);
    }
}
