﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;

namespace EvaluateExpression.Moq
{
    public static class MoqParserExtensions
    {
        delegate void SubmitCallback(string s, ref int index);

        /// <summary>
        /// Setup parse method with capture expressions in <paramref name="captureExpressions"/>.
        /// Set default mock IOperand as parse result
        /// </summary>
        /// <param name="fakeParser"></param>
        /// <param name="captureExpressions"></param>
        public static void SetupParse(this Mock<IParser> fakeParser, List<string> captureExpressions, Action<Mock<IOperand>> adjustOperand = null)
        {
            fakeParser.Setup(f => f.Parse(Capture.In(captureExpressions), ref It.Ref<int>.IsAny))
                .Returns(() => {
                    var result = new Mock<IParseResult>();
                    result.Setup(f => f.Count).Returns(1);
                    result.Setup(f => f.FinishReason).Returns(ParseFinishReason.Eof);
                    var mockOperand = new Mock<IOperand>();
                    adjustOperand?.Invoke(mockOperand);
                    result.Setup(f => f.SingleOperand).Returns(mockOperand.Object);
                    return result.Object;
                })
                .Callback(new SubmitCallback((string s, ref int index) => index = s.Length));
        }

        /// <summary>
        /// Setup parse method with capture expressions in <paramref name="captureExpressions"/>.
        /// Set <paramref name="operand"/> as parse result
        /// </summary>
        /// <param name="fakeParser"></param>
        /// <param name="captureExpressions"></param>
        public static void SetupParse(this Mock<IParser> fakeParser, List<string> captureExpressions, IOperand operand)
        {
            fakeParser.Setup(f => f.Parse(Capture.In(captureExpressions), ref It.Ref<int>.IsAny))
                .Returns(() => {
                    var result = new Mock<IParseResult>();
                    result.Setup(f => f.Count).Returns(1);
                    result.Setup(f => f.FinishReason).Returns(ParseFinishReason.Eof);
                    result.Setup(f => f.SingleOperand).Returns(operand);
                    return result.Object;
                })
                .Callback(new SubmitCallback((string s, ref int index) => index = s.Length));
        }
    }
}
