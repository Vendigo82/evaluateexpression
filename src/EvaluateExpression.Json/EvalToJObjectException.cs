﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using EvaluateExpression;
using EvaluateExpression.Values;
using EvaluateExpression.Exceptions;

namespace EvaluateExpression.Json
{
    public class EvalToJObjectException : EvalInvalidValueTypeException
    {
        public EvalToJObjectException(IValue value) : base(value.Type.ToString(), "JObject") { }
    }
}
