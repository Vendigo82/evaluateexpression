﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Functions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace EvaluateExpression.Json.Functions
{
    /// <summary>
    /// Parse string parameter with json value to complex object
    /// </summary>
    public class ParseJsonFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context) {            
            IValue value = this.GetSingle(operands, context);
            try {
                var json = JToken.Parse(value.AsString);
                return json.ToValue();
            } catch (Newtonsoft.Json.JsonException e) {
                throw new ParseJsonFunctionException(value, e);
            }
        }
    }

    /// <summary>
    /// Exception for ParseJsonFunction
    /// </summary>
    public class ParseJsonFunctionException : EvaluateException
    {
        public ParseJsonFunctionException(IValue value, Exception innerException) 
            : base($"Cannot parse value to json: {innerException.Message}", innerException) {
            OriginValue = value;
        }

        /// <summary>
        /// Value witch tried to parse to json
        /// </summary>
        public IValue OriginValue { get; set; }
    }
}
