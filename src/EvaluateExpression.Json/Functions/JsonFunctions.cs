﻿using EvaluateExpression.Functions;
using System.Collections.Generic;

namespace EvaluateExpression.Json.Functions
{
    public static class JsonFunctions
    {
        public static IEnumerable<KeyValuePair<string, IFunctionSignature>> GetFunctions()
        {
            yield return new KeyValuePair<string, IFunctionSignature>("ParseJson", new ParseJsonFunction());
            yield return new KeyValuePair<string, IFunctionSignature>("SerializeJson", new ToJsonStringFunction());
        }
    }
}
