﻿using EvaluateExpression.Exceptions;
using EvaluateExpression.Functions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EvaluateExpression.Json.Functions
{
    public class ToJsonStringFunction : IFunctionSignature
    {
        public int ArgCount => 1;

        public IValue Perform(IEnumerable<IOperand> operands, INameResolver context)
        {
            IValue value = this.GetSingle(operands, context);
            try
            {
                var json = JsonConvert.SerializeObject(value.ToJson());
                return json.ToValue();
            }
            catch (JsonException e)
            {
                throw new ParseJsonFunctionException(value, e);
            }
        }
    }

    /// <summary>
    /// Exception for ParseJsonFunction
    /// </summary>
    public class ToJsonJsonStringFunctionException : EvaluateException
    {
        public ToJsonJsonStringFunctionException(IValue value, Exception innerException)
            : base($"Cannot serialize value as json string: {innerException.Message}", innerException)
        {
            OriginValue = value;
        }

        /// <summary>
        /// Value witch tried to parse to json
        /// </summary>
        public IValue OriginValue { get; set; }
    }
}
