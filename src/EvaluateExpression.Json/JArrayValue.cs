﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using EvaluateExpression.Values;
using EvaluateExpression.Exceptions;
using System.Collections;

namespace EvaluateExpression.Json
{
    /// <summary>
    /// Adapt <see cref="JArray"/> to <see cref="IArrayValueStrategy"/>
    /// </summary>
    public class JArrayStrategy : IArrayValueStrategy
    {
        /// <summary>
        /// origin array
        /// </summary>
        private readonly JArray _json;

        /// <summary>
        /// Construct object from <see cref="JArray"/>
        /// </summary>
        /// <param name="json">json array</param>
        public JArrayStrategy(JArray json) {
            _json = json ?? throw new ArgumentNullException(nameof(json));
        }

        /// <summary>
        /// Access to array element by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        /// <exception cref="EvalIndexOutOfRangeException">if index out of range</exception>
        public IValue this[int index] {
            get {
                try {
                    return _json[index].ToValue();
                } catch (ArgumentOutOfRangeException e) {
                    throw new EvalIndexOutOfRangeException(e.Message, e);
                }
            }
        }

        /// <summary>
        /// Count of elements
        /// </summary>
        public int Count => _json.Count;

        /// <summary>
        /// Native value accessor
        /// </summary>
        public object Value => _json;

        /// <summary>
        /// Is array
        /// </summary>
        public bool IsSolid => true;

        public IEnumerator<IValue> GetEnumerator() {
            foreach (JToken token in _json) {
                yield return token.ToValue();
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }

    /// <summary>
    /// Adapt <see cref="JArray"/> to <see cref="IValue"/>
    /// </summary>
    public class JArrayValue : ArrayPatternValue, IJTokenContainer
    {
        public JArrayValue(JArray json) : base(new JArrayStrategy(json)) {
        }

        public JToken JToken => (JToken)Value;
    }
}
