﻿using Newtonsoft.Json.Linq;

namespace EvaluateExpression.Json
{
    /// <summary>
    /// Contains JToken
    /// </summary>
    public interface IJTokenContainer
    {
        /// <summary>
        /// Get JToken
        /// </summary>
        JToken JToken { get; }
    }
}
