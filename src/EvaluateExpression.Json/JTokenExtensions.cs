﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EvaluateExpression;
using EvaluateExpression.Values;
using Newtonsoft.Json.Linq;

namespace EvaluateExpression.Json {
    /// <summary>
    /// Json token extensions
    /// </summary>
    public static class JTokenExtensions 
    {
        /// <summary>
        /// Converts <see cref="JToken"/> to <see cref="IValue"/>
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static IValue ToValue(this JToken val) 
        {
            if (val == null)
                return NullValue.Null;

            switch (val.Type) {
                case JTokenType.Integer: return new IntValue((int)val);
                case JTokenType.Float: return new DoubleValue((double)val);
                case JTokenType.String:
                case JTokenType.Guid:
                case JTokenType.Uri: return new StringValue((string)val);
                case JTokenType.Boolean: return BoolValue.Get((bool)val);
                case JTokenType.Null: return NullValue.Null;
                case JTokenType.Array: return new JArrayValue(val as JArray);
                case JTokenType.Object: return new JObjectValue(val as JObject);
                case JTokenType.Date: return new DateTimeValue((DateTime)val);
                case JTokenType.Bytes: return new ArrayValue(((byte[])val).Select(i => i.ToValue()).ToArray());
                case JTokenType.Undefined: return new StringValue((string)val);
                case JTokenType.TimeSpan: return new DoubleValue(((TimeSpan)val).TotalDays);
                default:
                    throw new InvalidCastException($"Unsupported JSON type: {val.Type}");
            }
        }

        /// <summary>
        /// Converts <see cref="IValue"/> to <see cref="JToken"/>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static JToken ToJson(this IValue value)
        {
            if (value is IJTokenContainer container) {
                return container.JToken;
            } else if (value.Type == EvaluateExpression.ValueType.Unknown) {
                if (value is IIndexableValue indValue)
                    return ToJArray(indValue);
                else if (value is IComplexValue cmplxValue)
                    return ToJObject(cmplxValue);
                else
                    throw new EvalToJObjectException(value);
            } else
                return ToJValue(value);
        }

        /// <summary>
        /// Converts indexable object <see cref="IIndexableValue"/> to <see cref="JArray"/>
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        private static JToken ToJArray(IIndexableValue array)
        {
            JArray result = new JArray();
            foreach (IValue val in array)
                result.Add(val.ToJson());
            return result;
        }

        /// <summary>
        /// Converts complex value <see cref="IComplexValue"/> to <see cref="JObject"/>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static JToken ToJObject(IComplexValue value)
        {
            JObject result = new JObject();
            foreach (KeyValuePair<string, IValue> prop in value)
                result.Add(prop.Key, prop.Value.ToJson());
            return result;
        }

        /// <summary>
        /// Converts primitive <see cref="IValue"/> to <see cref="JValue"/>
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static JToken ToJValue(IValue val)
        {
            switch (val.Type) {
                case ValueType.Boolean:
                    return new JValue(val.AsBool);
                case ValueType.DateTime:
                    return new JValue(val.AsDateTime);
                case ValueType.Decimal:
                    return new JValue(val.AsDouble);
                case ValueType.Integer:
                    return new JValue(val.AsInt);
                case ValueType.Null:
                    return JValue.CreateNull();
                case ValueType.String:
                    return JValue.CreateString(val.AsString);
                default:
                    throw new EvalToJObjectException(val);
            }
        }
        /// <summary>
        /// Append <paramref name="token"/> to <paramref name="jo"/>. Create necessary internal elements for path <paramref name="path"/>
        /// </summary>
        /// <param name="jo"><see cref="JObject"/> value container</param>
        /// <param name="path">inserting path. last segment is property name</param>
        /// <param name="token">inserting json value</param>
        /// <exception cref="InvalidOperationException">If can't add value to path</exception>
        public static void AppendPath(this JObject jo, string path, JToken token)
        {
            var segments = string.IsNullOrEmpty(path) ? new string[] { } : path.Split('.');
            
            if (segments.Length == 0) {
                if (token is JObject additional) {
                    jo.Merge(additional);
                    return;
                } else
                    throw new InvalidOperationException($"Can't JObject merge with type {token.Type}");
            }

            for (int i = 0; i < segments.Length - 1; ++i) {
                if (!jo.TryGetValue(segments[i], out JToken next)) {
                    next = new JObject();
                    jo[segments[i]] = next;
                }

                if (next.Type == JTokenType.Null) {
                    next = new JObject();
                    jo[segments[i]] = next;
                }

                jo = (next as JObject)
                    ?? throw new InvalidOperationException($"Json property at {path} is not object and child value can't be inserted. Actual property type is {next.Type}");
            }

            if (token is JObject additional2 && jo[segments[segments.Length - 1]]?.Type == JTokenType.Object)
                (jo[segments[segments.Length - 1]] as JObject).Merge(additional2);
            else
                jo[segments[segments.Length - 1]] = token;
        }
    }
}
