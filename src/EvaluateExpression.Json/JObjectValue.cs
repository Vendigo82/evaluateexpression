﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json.Linq;

using EvaluateExpression.Values;

namespace EvaluateExpression.Json
{
    /// <summary>
    /// Adapt <see cref="JObject"/> to <see cref="IValue"/>
    /// </summary>
    public class JObjectValue : SpecialValueBase, IComplexValue, IJTokenContainer
    {
        /// <summary>
        /// Origin object
        /// </summary>
        private readonly JObject _json;

        /// <summary>
        /// Access to origin object
        /// </summary>
        public override object Value => _json;

        /// <summary>
        /// Access to JToken value
        /// </summary>
        public JToken JToken => _json;

        public string Description => ToString();

        /// <summary>
        /// Construct object
        /// </summary>
        /// <param name="json"></param>
        public JObjectValue(JObject json) {
            _json = json ?? throw new ArgumentNullException(nameof(json));
        }

        /// <summary>
        /// Access to subvalue.        
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IValue GetSubValue(string name) {
            JToken val = _json[name];
            if (val == null)
                return null;
            return val.ToValue();
        }

        public IValue Property(string name) => GetSubValue(name);

        public IValue Eval(INameResolver context) => this;

        public IEnumerator<KeyValuePair<string, IValue>> GetEnumerator() {
            return (_json.Properties() as IEnumerable<JProperty>)
                .Select(p => new KeyValuePair<string, IValue>(p.Name, p.Value.ToValue()))
                .GetEnumerator();
            //foreach (JProperty prop in _json.Properties())                
            //    yield return new KeyValuePair<string, IValue>(prop.Name, prop.Value.ToValue());
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public override string ToString() {
            return _json.ToString();
        }

    }
}
